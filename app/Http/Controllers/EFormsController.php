<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;

class EFormsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function form1()
    {
         return view('pages.form1');  
    }

    public function form1agency()
    {
        $form1 = \App\Form1::all();
        return view('pages.form1agency', compact('form1'));  
    }

    public function form1agency_add($id)
    {
        $form1                      = new \App\Agency_form1();
        $form1->status              = "Draft";
        $form1->fy                  = $id;
        $form1->agency_id           = auth()->user()->agency_id;

        $tmp_form1 = \App\Agency_form1::where([['agency_id', '=', auth()->user()->agency_id], ['fy', '=', $id]])->first();
        if ($tmp_form1 == null) {
            $form1->save();
            alert()->success('Form 1 Created!');
        } else {
            alert()->success('Form 1 already exist!');
        }

        return back();
    }

    public function editform1($id)
    {

        $id = decrypt($id);

        $form1 = \App\Agency_form1::find($id);
        
        $view_only = 0;
        if ($form1->status == 'Submitted') {
            $view_only = 1;
        } else {
            $frm1 = \App\Form1::where('fy', '=', $form1->fy)->first();
            if ($frm1->is_lock == 1) {
                $view_only = 1;
            }
        }

        $projects_in_form1 = \App\Agency_form1s_project::join('agency_form1s', 'agency_form1s.id', '=', 'agency_form1s_projects.agency_form1s_id')
        ->leftJoin('projects', 'projects.id','=','agency_form1s_projects.project_id')
        ->where('agency_id','=',auth()->user()->agency_id)
        ->where('fy', '=', $form1->fy)
        ->select('agency_form1s_projects.id','projects.title', 'agency_form1s_projects.*')
        ->get();
        // dd($projects_in_form1);

        $projects = \App\Projects::where('implementingagency','=',auth()->user()->agency_id)
        ->select('id','title')
        ->whereNotIn('id', \App\Agency_form1s_project::join('agency_form1s', 'agency_form1s.id','=','agency_form1s_projects.agency_form1s_id')->select('agency_form1s_projects.project_id')
            ->where('agency_id','=',auth()->user()->agency_id)
            ->where('fy', '=', $form1->fy)
            ->get()->toArray())
        ->get();

        // dd($projects);

        return view('pages.editform1', compact('form1','projects','projects_in_form1', 'view_only'));
    }

    public function form1agency_projects_add($id)
    {
        $form1                      = new \App\Agency_form1s_project();
        $form1->project_id          = request('project');
        $form1->status              = 'Draft';
        $form1->agency_form1s_id    = $id;
        $form1->modified_by         = auth()->user()->name;
        $form1->save();
        alert()->success('Project Added!');
        return back();
    }

    public function endorse($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1s_project::find($id);
        $form1submit->status = 'Endorsed';
        $form1submit->agency_remarks = request('agency_remarks');
        $form1submit->modified_by    = auth()->user()->name;
        $form1submit->save();
        alert()->success('Project Endorsed!');

        $projects = \App\Projects::find($form1submit->project_id);

        $logs                        = new \App\Logs();
        $logs->log                   = "Endorsed Project for Form 1: ".$projects->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back(); 
    }
    
    public function form1agency_submit($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1::find($id);
        $form1submit->status = 'Submitted';
        $form1submit->save();

        $logs                        = new \App\Logs();
        $logs->log                   = "Submitted Form 1";
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Form Submitted!');
        return back(); 
    }

    public function form1agency_submitfs($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1s_project::find($id);
        $form1submit->fs_1 = request('fs_1');
        $form1submit->fs_2 = request('fs_2');
        $form1submit->fs_3 = request('fs_3');
        $form1submit->fs_4 = request('fs_4');
        $form1submit->fs_5 = request('fs_5');
        $form1submit->fs_6 = request('fs_6');
        $form1submit->fs_7 = request('fs_7');
        $form1submit->fs_8 = request('fs_8');
        $form1submit->fs_9 = request('fs_9');
        $form1submit->fs_10 = request('fs_10');
        $form1submit->fs_11 = request('fs_11');
        $form1submit->fs_12 = request('fs_12');
        $form1submit->pt_1 = request('pt_1');
        $form1submit->pt_2 = request('pt_2');
        $form1submit->pt_3 = request('pt_3');
        $form1submit->pt_4 = request('pt_4');
        $form1submit->pt_5 = request('pt_5');
        $form1submit->pt_6 = request('pt_6');
        $form1submit->pt_7 = request('pt_7');
        $form1submit->pt_8 = request('pt_8');
        $form1submit->pt_9 = request('pt_9');
        $form1submit->pt_10 = request('pt_10');
        $form1submit->pt_11 = request('pt_11');
        $form1submit->pt_12 = request('pt_12');
        $form1submit->oi_1 = request('oi_1');
        $form1submit->oi_2 = request('oi_2');
        $form1submit->oi_3 = request('oi_3');
        $form1submit->oi_4 = request('oi_4');
        $form1submit->oi_5 = request('oi_5');
        $form1submit->oi_6 = request('oi_6');
        $form1submit->oi_7 = request('oi_7');
        $form1submit->oi_8 = request('oi_8');
        $form1submit->oi_9 = request('oi_9');
        $form1submit->oi_10 = request('oi_10');
        $form1submit->oi_11 = request('oi_11');
        $form1submit->oi_12 = request('oi_12');
        $form1submit->eg_1 = request('eg_1');
        $form1submit->eg_2 = request('eg_2');
        $form1submit->eg_3 = request('eg_3');
        $form1submit->eg_4 = request('eg_4');
        $form1submit->eg_5 = request('eg_5');
        $form1submit->eg_6 = request('eg_6');
        $form1submit->eg_7 = request('eg_7');
        $form1submit->eg_8 = request('eg_8');
        $form1submit->eg_9 = request('eg_9');
        $form1submit->eg_10 = request('eg_10');
        $form1submit->eg_11 = request('eg_11');
        $form1submit->eg_12 = request('eg_12');
        $form1submit->eg_13 = request('eg_13');
        $form1submit->eg_14 = request('eg_14');
        $form1submit->eg_15 = request('eg_15');
        $form1submit->eg_16 = request('eg_16');
        $form1submit->eg_17 = request('eg_17');
        $form1submit->eg_18 = request('eg_18');
        $form1submit->eg_19 = request('eg_19');
        $form1submit->eg_20 = request('eg_20');
        $form1submit->eg_21 = request('eg_21');
        $form1submit->eg_22 = request('eg_22');
        $form1submit->eg_23 = request('eg_23');
        $form1submit->eg_24 = request('eg_24');
        $form1submit->modified_by = auth()->user()->name;
        $form1submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form1agency_submitpt($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1s_project::find($id);
        $form1submit->pt_1 = request('pt_1');
        $form1submit->pt_2 = request('pt_2');
        $form1submit->pt_3 = request('pt_3');
        $form1submit->pt_4 = request('pt_4');
        $form1submit->pt_5 = request('pt_5');
        $form1submit->pt_6 = request('pt_6');
        $form1submit->pt_7 = request('pt_7');
        $form1submit->pt_8 = request('pt_8');
        $form1submit->pt_9 = request('pt_9');
        $form1submit->pt_10 = request('pt_10');
        $form1submit->pt_11 = request('pt_11');
        $form1submit->pt_12 = request('pt_12');
        $form1submit->modified_by = auth()->user()->name;
        $form1submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form1agency_submitoi($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1s_project::find($id);
        $form1submit->oi_1 = request('oi_1');
        $form1submit->oi_2 = request('oi_2');
        $form1submit->oi_3 = request('oi_3');
        $form1submit->oi_4 = request('oi_4');
        $form1submit->oi_5 = request('oi_5');
        $form1submit->oi_6 = request('oi_6');
        $form1submit->oi_7 = request('oi_7');
        $form1submit->oi_8 = request('oi_8');
        $form1submit->oi_9 = request('oi_9');
        $form1submit->oi_10 = request('oi_10');
        $form1submit->oi_11 = request('oi_11');
        $form1submit->oi_12 = request('oi_12');
        $form1submit->modified_by = auth()->user()->name;
        $form1submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form1agency_submiteg($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1s_project::find($id);
        $form1submit->eg_1 = request('eg_1');
        $form1submit->eg_2 = request('eg_2');
        $form1submit->eg_3 = request('eg_3');
        $form1submit->eg_4 = request('eg_4');
        $form1submit->eg_5 = request('eg_5');
        $form1submit->eg_6 = request('eg_6');
        $form1submit->eg_7 = request('eg_7');
        $form1submit->eg_8 = request('eg_8');
        $form1submit->eg_9 = request('eg_9');
        $form1submit->eg_10 = request('eg_10');
        $form1submit->eg_11 = request('eg_11');
        $form1submit->eg_12 = request('eg_12');
        $form1submit->eg_13 = request('eg_13');
        $form1submit->eg_14 = request('eg_14');
        $form1submit->eg_15 = request('eg_15');
        $form1submit->eg_16 = request('eg_16');
        $form1submit->eg_17 = request('eg_17');
        $form1submit->eg_18 = request('eg_18');
        $form1submit->eg_19 = request('eg_19');
        $form1submit->eg_20 = request('eg_20');
        $form1submit->eg_21 = request('eg_21');
        $form1submit->eg_22 = request('eg_22');
        $form1submit->eg_23 = request('eg_23');
        $form1submit->eg_24 = request('eg_24');
        $form1submit->modified_by = auth()->user()->name;
        $form1submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form2agency()
    {
        $form2 = \App\Form2::all();
        return view('pages.form2agency', compact('form2'));  
    }

    public function form2agency_add($id)
    {
        $form1                      = new \App\Agency_form2();
        $form1->status              = "Draft";
        $form1->period              = $id;
        $form1->agency_id           = auth()->user()->agency_id;

        $tmp_form2 = \App\Agency_form2::where([['agency_id', '=', auth()->user()->agency_id], ['period', '=', $id]])->first();
        if ($tmp_form2 == null) {
            $form1->save();
            alert()->success('Form 2 Created!');
        } else {
            alert()->success('Form 2 already exist!');
        }
        return back();
    }

    public function form2agency_submit($id)
    {
        // dd($id);
        $form2submit = \App\Agency_form2::find($id);
        $form2submit->status = 'Submitted';
        $form2submit->save();

        $logs                        = new \App\Logs();
        $logs->log                   = "Submitted Form 2";
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Form Submitted!');
        return back(); 
    }

    public function editform2($id)
    {
        $id = decrypt($id);


        $form2 = \App\Agency_form2::find($id);
        
        $view_only = 0;
        if ($form2->status == 'Submitted') {
            $view_only = 1;
        } else {
            $frm2 = \App\Form2::where('period', '=', $form2->period)->first();
            if ($frm2->is_lock == 1) {
                $view_only = 1;
            }
        }


        $projects_in_form2 = \App\Agency_form2s_project::join('agency_form2s', 'agency_form2s.id', '=', 'agency_form2s_projects.agency_form2s_id')
        ->leftJoin('projects', 'projects.id','=','agency_form2s_projects.project_id')
        ->where('agency_id','=',auth()->user()->agency_id)
        ->where('period', '=', $form2->period)
        ->select('agency_form2s_projects.id','projects.title', 'agency_form2s_projects.*')
        ->get();
        // dd($projects_in_form1);

        $projects = \App\Projects::where('implementingagency','=',auth()->user()->agency_id)
        ->select('id','title')
        ->whereNotIn('id', \App\Agency_form2s_project::join('agency_form2s', 'agency_form2s.id','=','agency_form2s_projects.agency_form2s_id')->select('agency_form2s_projects.project_id')
            ->where('agency_id','=',auth()->user()->agency_id)
            ->where('period', '=', $form2->period)
            ->get()->toArray())
        ->get();

        // dd($projects);

        return view('pages.editform2', compact('form2','projects','projects_in_form2', 'view_only'));
    }

    public function form2agency_projects_add($id)
    {
        $form2                      = new \App\Agency_form2s_project();
        $form2->project_id          = request('project');
        $form2->status              = 'Draft';
        $form2->agency_form2s_id    = $id;
        $form2->modified_by = auth()->user()->name;
        $form2->save();
        alert()->success('Project Added!');
        return back();
    }

    public function endorse2($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form2s_project::find($id);
        $form1submit->status = 'Endorsed';
        $form1submit->agency_remarks = request('agency_remarks');
        $form1submit->modified_by = auth()->user()->name;
        $form1submit->save();

        $projects = \App\Projects::find($form1submit->project_id);

        $logs                        = new \App\Logs();
        $logs->log                   = "Endorsed Project for Form 2: ".$projects->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Project Endorsed!');
        return back(); 
    }

    public function form2agency_submitfs($id)
    {
        // dd($id);
        $form2submit = \App\Agency_form2s_project::find($id);
        $form2submit->alloc_asof = request('alloc_asof');
        $form2submit->alloc_month = request('alloc_month');
        $form2submit->releases_asof = request('releases_asof');
        $form2submit->releases_month = request('releases_month');
        $form2submit->obligations_asof = request('obligations_asof');
        $form2submit->obligations_month = request('obligations_month');
        $form2submit->expenditures_asof = request('expenditures_asof');
        $form2submit->expenditures_month = request('expenditures_month');
        $form2submit->male = request('male');
        $form2submit->female = request('female');
        $form2submit->remarks = request('remarks');
        $form2submit->oi = request('oi');
        $form2submit->ttd = request('ttd');
        $form2submit->tftm = request('tftm');
        $form2submit->atd = request('atd');
        $form2submit->aftm = request('aftm');
        $form2submit->modified_by = auth()->user()->name;
        $form2submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }
    
    public function form2agency_submiteg($id)
    {
        // dd($id);
        $form2submit = \App\Agency_form2s_project::find($id);
        $form2submit->male = request('male');
        $form2submit->female = request('female');
        $form2submit->modified_by = auth()->user()->name;
        $form2submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form2agency_submitpt($id)
    {
        // dd($id);
        $form2submit = \App\Agency_form2s_project::find($id);
        $form2submit->oi = request('oi');
        $form2submit->ttd = request('ttd');
        $form2submit->tftm = request('tftm');
        $form2submit->atd = request('atd');
        $form2submit->aftm = request('aftm');
        $form2submit->modified_by = auth()->user()->name;
        $form2submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }
    
    public function form3agency()
    {
        $form3 = \App\Form3::all();
        return view('pages.form3agency', compact('form3'));  
    }    

    public function form3agency_add($id)
    {
        $form3                      = new \App\Agency_form3();
        $form3->status              = "Draft";
        $form3->period              = $id;
        $form3->agency_id           = auth()->user()->agency_id;

        $tmp_form3 = \App\Agency_form3::where([['agency_id', '=', auth()->user()->agency_id], ['period', '=', $id]])->first();
        if ($tmp_form3 == null) {
            $form3->save();
            alert()->success('Form 3 Created!');
        } else {
            alert()->success('Form 3 already exist!');
        }
        return back();
    }

    public function editform3($id)
    {

        $id = decrypt($id);


        $form3 = \App\Agency_form3::find($id);
        
        $view_only = 0;
        if ($form3->status == 'Submitted') {
            $view_only = 1;
        } else {
            $frm3 = \App\Form3::where('period', '=', $form3->period)->first();
            if ($frm3->is_lock == 1) {
                $view_only = 1;
            }
        }

        $impstatuses = \App\Impstatuses::all();

        $projects_in_form3 = \App\Agency_form3s_project::join('agency_form3s', 'agency_form3s.id', '=', 'agency_form3s_projects.agency_form3s_id')
        ->leftJoin('projects', 'projects.id','=','agency_form3s_projects.project_id')
        ->leftJoin('impstatuses', 'agency_form3s_projects.impstatus','=','impstatuses.id')
        ->where('agency_id','=',auth()->user()->agency_id)
        ->where('period', '=', $form3->period)
        ->select('agency_form3s_projects.id','projects.title', 'agency_form3s_projects.*','impstatuses.type')
        ->get();
        // dd($projects_in_form1);

        $projects = \App\Projects::where('implementingagency','=',auth()->user()->agency_id)
        ->select('id','title')
        ->whereNotIn('id', \App\Agency_form3s_project::join('agency_form3s', 'agency_form3s.id','=','agency_form3s_projects.agency_form3s_id')->select('agency_form3s_projects.project_id')
            ->where('agency_id','=',auth()->user()->agency_id)
            ->where('period', '=', $form3->period)
            ->get()->toArray())
        ->get();

        // dd($projects);

        return view('pages.editform3', compact('form3','projects','projects_in_form3','impstatuses', 'view_only'));
    }

    public function form3agency_projects_add($id)
    {
        $form3                      = new \App\Agency_form3s_project();
        $form3->project_id          = request('project');
        $form3->impstatus           = request('impstatus');
        $form3->status              = 'Draft';
        $form3->agency_form3s_id    = $id;
        $form3->modified_by = auth()->user()->name;
        $form3->save();
        alert()->success('Project Added!');
        return back();
    }

    public function form3agency_submitfs($id)
    {
        $form3submit = \App\Agency_form3s_project::find($id);

        $form3submit->findings           = request('findings');
        $form3submit->possible           = request('possible');
        $form3submit->recommendations           = request('recommendations');
        $form3submit->modified_by = auth()->user()->name;
        $form3submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }
    
    public function form3agency_submitpt($id)
    {
        $form3submit = \App\Agency_form3s_project::find($id);
        $form3submit->possible           = request('possible');
        $form3submit->modified_by = auth()->user()->name;
        $form3submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }
      
    public function form3agency_submiteg($id)
    {
        $form3submit = \App\Agency_form3s_project::find($id);
        $form3submit->recommendations           = request('recommendations');
        $form3submit->modified_by = auth()->user()->name;
        $form3submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function endorse3($id)
    {
        // dd($id);
        $form3submit = \App\Agency_form3s_project::find($id);
        $form3submit->status = 'Endorsed';
        $form3submit->agency_remarks = request('agency_remarks');
        $form3submit->modified_by = auth()->user()->name;
        $form3submit->save();

        $projects = \App\Projects::find($form3submit->project_id);

        $logs                        = new \App\Logs();
        $logs->log                   = "Endorsed Project for Form 3: ".$projects->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Project Endorsed!');
        return back(); 
    }

    public function form3agency_submit($id)
    {
        // dd($id);
        $form3submit = \App\Agency_form3::find($id);
        $form3submit->status = 'Submitted';
        $form3submit->save();

        $logs                        = new \App\Logs();
        $logs->log                   = "Submitted Form 3";
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Form Submitted!');
        return back(); 
    }

    public function form4agency()
    {
        $form4 = \App\Form4::all();
        return view('pages.form4agency', compact('form4'));  
    }

    public function form4agency_add($id)
    {
        $form4                      = new \App\Agency_form4();
        $form4->status              = "Draft";
        $form4->period              = $id;
        $form4->agency_id           = auth()->user()->agency_id;

        $tmp_form4 = \App\Agency_form4::where([['agency_id', '=', auth()->user()->agency_id], ['period', '=', $id]])->first();
        if ($tmp_form4 == null) {
            $form4->save();
            alert()->success('Form 4 Created!');
        } else {
            alert()->success('Form 4 already exist!');
        }
        return back();
    }

    public function editform4($id)
    {

        $id = decrypt($id);

        
        $form4 = \App\Agency_form4::find($id);
        
        $view_only = 0;
        if ($form4->status == 'Submitted') {
            $view_only = 1;
        } else {
            $frm4 = \App\Form4::where('period', '=', $form4->period)->first();
            if ($frm4->is_lock == 1) {
                $view_only = 1;
            }
        }

        $impstatuses = \App\Impstatuses::all();

        $projects_in_form4 = \App\Agency_form4s_project::join('agency_form4s', 'agency_form4s.id', '=', 'agency_form4s_projects.agency_form4s_id')
        ->leftJoin('projects', 'projects.id','=','agency_form4s_projects.project_id')
        ->leftJoin('impstatuses', 'agency_form4s_projects.impstatus','=','impstatuses.id')
        ->where('agency_id','=',auth()->user()->agency_id)
        ->where('period', '=', $form4->period)
        ->select('agency_form4s_projects.id','projects.title', 'agency_form4s_projects.*','impstatuses.type')
        ->get();
        // dd($projects_in_form1);

        $projects = \App\Projects::where('implementingagency','=',auth()->user()->agency_id)
        ->select('id','title')
        ->whereNotIn('id', \App\Agency_form4s_project::join('agency_form4s', 'agency_form4s.id','=','agency_form4s_projects.agency_form4s_id')->select('agency_form4s_projects.project_id')
            ->where('agency_id','=',auth()->user()->agency_id)
            ->where('period', '=', $form4->period)
            ->get()->toArray())
        ->get();

        // dd($projects);

        return view('pages.editform4', compact('form4','projects','projects_in_form4','impstatuses', 'view_only'));
    }

    public function form4agency_projects_add($id)
    {
        $form4                      = new \App\Agency_form4s_project();
        $form4->project_id          = request('project');
        $form4->status              = 'Draft';
        $form4->agency_form4s_id    = $id;
        $form4->modified_by = auth()->user()->name;
        $form4->save();
        alert()->success('Project Added!');
        return back();
    }

    public function form4agency_submitpo($id)
    {
        $form4submit = \App\Agency_form4s_project::find($id);
        $form4submit->objectives           = request('objectives');
        $form4submit->indicator           = request('indicator');
        $form4submit->results           = request('results');
        $form4submit->modified_by = auth()->user()->name;

        $form4submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form4agency_submitri($id)
    {
        $form4submit = \App\Agency_form4s_project::find($id);
        $form4submit->indicator           = request('indicator');
        $form4submit->modified_by = auth()->user()->name;
        $form4submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function form4agency_submitor($id)
    {
        $form4submit = \App\Agency_form4s_project::find($id);
        $form4submit->results           = request('results');
        $form4submit->modified_by = auth()->user()->name;
        $form4submit->save();
        alert()->success('Project Saved!');
        return back(); 
    }

    public function endorse4($id)
    {
        // dd($id);
        $form4submit = \App\Agency_form4s_project::find($id);
        $form4submit->status = 'Endorsed';
        $form4submit->agency_remarks = request('agency_remarks');
        $form4submit->modified_by = auth()->user()->name;
        $form4submit->save();

        $projects = \App\Projects::find($form4submit->project_id);

        $logs                        = new \App\Logs();
        $logs->log                   = "Endorsed Project for Form 4: ".$projects->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Project Endorsed!');
        return back(); 
    }

    public function form4agency_submit($id)
    {
        // dd($id);
        $form4submit = \App\Agency_form4::find($id);
        $form4submit->status = 'Submitted';
        $form4submit->save();

        $logs                        = new \App\Logs();
        $logs->log                   = "Submitted Form 4";
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success('Form Submitted!');
        return back(); 
    }

    public function deleteprojectform1($id)
    {
        // dd($id);

        $form1 = \App\Agency_form1s_project::find($id);
        $form1->delete();

        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Project in Form 1";
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Project successfully deleted.'));
    }

    public function deleteprojectform2($id)
    {
        // dd($id);
        
        $form1 = \App\Agency_form2s_project::find($id);
        $form1->delete();

        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Project in Form 2";
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Project successfully deleted.'));
    }

    public function deleteprojectform3($id)
    {
        // dd($id);

        $form1 = \App\Agency_form3s_project::find($id);
        $form1->delete();

        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Project in Form 3";
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Project successfully deleted.'));
    }

    public function deleteprojectform4($id)
    {
        // dd($id);
        
        $form1 = \App\Agency_form4s_project::find($id);
        $form1->delete();

        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Project in Form 4";
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Project successfully deleted.'));
    }

    public function deleteprojectform5($id)
    {
        // dd($id);
        
        $form1 = \App\Agency_form5::find($id);
        $form1->delete();

        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Row in Form 5";
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Row successfully deleted.'));
    }

    public function deleteprojectform6($id)
    {
        // dd($id);
        
        $form1 = \App\Agency_form6::find($id);
        $form1->delete();

        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Row in Form 6";
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Row successfully deleted.'));
    }
}
