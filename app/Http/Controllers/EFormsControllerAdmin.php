<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;

class EFormsControllerAdmin extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function form1()
    {
        // $spreadsheet = new Spreadsheet();
        // $sheet = $spreadsheet->getActiveSheet();
        // $sheet->setCellValue("A1", "Hello World !");

        // $writer = new Xlsx($spreadsheet);
        // $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment; filename="hello world.xlsx"');
        // $writer->save("php://output");

        $form1 = \App\Form1::all();
        return view('pages.form1', compact('form1'));  
    }

    public function unlock($id)
    {
        $form1 = \App\Form1::find($id);
        $form1->is_lock = 0;
        $form1->save();
        alert()->success('Form Unlocked!');
        return back(); 
    }

    public function lockform1($id)
    {
        $form1 = \App\Form1::find($id);
        $form1->is_lock = 1;
        $form1->save();
        alert()->success('Form Locked!');
        return back(); 
    }

    public function form1submission($id)
    {
        $year = $id;
        $form1 = \App\Agency_form1::join('agencies', 'agencies.id','=','agency_form1s.agency_id')->where('fy','=',$id)->select('agencies.UACS_AGY_DSC', 'agency_form1s.fy', 'agency_form1s.id', 'agency_form1s.status', 'agency_form1s.nro_status_review', 'agency_form1s.nro_remarks')->get();
        return view('pages.form1submission', compact('form1','year'));  
    } 

    public function form1submissionview($id)
    {
        $getagency = \App\Agency_form1::find($id);
        $agency = \App\Agencies::find($getagency->agency_id);
        $projects_in_form1 = \App\Agency_form1s_project::join('projects', 'projects.id','=','agency_form1s_projects.project_id')->where('agency_form1s_projects.agency_form1s_id','=',$id)->where(function ($query) {
        $query->where('agency_form1s_projects.status', '=', 'Reviewed')
          ->orWhere('agency_form1s_projects.status', '=', 'Endorsed');
        })->select('agency_form1s_projects.id','projects.title','agency_form1s_projects.status', 'agency_form1s_projects.*')->get();
        return view('pages.form1submissionview', compact('projects_in_form1','agency','getagency'));  
    } 

    public function review($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1s_project::find($id);
        $form1submit->status = 'Reviewed';
        $form1submit->nro_remarks = request('nro_remarks');
        $form1submit->modified_by = auth()->user()->name;
        $form1submit->save();
        alert()->success('Project Reviewed!');
        return back(); 
    }

    public function addform1()
    {
        // dd($id);
        $form1submit = new \App\Form1();
        $form1submit->fy = request('year');
        $form1submit->is_lock = 1;
        $form1submit->save();
        alert()->success('Form Submission Created!');
        return back(); 
    }

    public function formreview($id)
    {
        // dd($id);
        $form1submit = \App\Agency_form1::find($id);
        $form1submit->nro_status_review = 'NRO Reviewed';
        $form1submit->nro_remarks = request('nro_remarks');
        $form1submit->save();
        alert()->success('Form Reviewed!');
        return back(); 
    }

    public function form2()
    {
        $form2 = \App\Form2::all();
        return view('pages.form2', compact('form2'));  
    }

    public function addform2()
    {
        // dd($id);
        $form1submit = new \App\Form2();
        $form1submit->period = request('period');
        $form1submit->is_lock = 1;
        $form1submit->save();
        alert()->success('Form Submission Created!');
        return back(); 
    }

    public function unlock2($id)
    {
        $form2 = \App\Form2::find($id);
        $form2->is_lock = 0;
        $form2->save();
        alert()->success('Form Unlocked!');
        return back(); 
    }

    public function lockform2($id)
    {
        $form2 = \App\Form2::find($id);
        $form2->is_lock = 1;
        $form2->save();
        alert()->success('Form Locked!');
        return back(); 
    }

    public function form2submission($id)
    {
        $period = $id;
        $form2 = \App\Agency_form2::join('agencies', 'agencies.id','=','agency_form2s.agency_id')->where('period','=',$id)->select('agencies.UACS_AGY_DSC', 'agency_form2s.period', 'agency_form2s.id', 'agency_form2s.status', 'agency_form2s.nro_status_review','agency_form2s.nro_remarks')->get();
        return view('pages.form2submission', compact('form2','period'));  
    } 

    public function form2review($id)
    {
        // dd($id);
        $form2submit = \App\Agency_form2::find($id);
        $form2submit->nro_status_review = 'NRO Reviewed';
        $form2submit->nro_remarks = request('nro_remarks');
        $form2submit->save();
        alert()->success('Form Reviewed!');
        return back(); 
    }

    public function form2submissionview($id)
    {
        $getagency = \App\Agency_form2::find($id);
        $agency = \App\Agencies::find($getagency->agency_id);
        $projects_in_form2 = \App\Agency_form2s_project::join('projects', 'projects.id','=','agency_form2s_projects.project_id')->where('agency_form2s_projects.agency_form2s_id','=',$id)->where(function ($query) {
        $query->where('agency_form2s_projects.status', '=', 'Reviewed')
          ->orWhere('agency_form2s_projects.status', '=', 'Endorsed');
        })->select('agency_form2s_projects.id','projects.title','agency_form2s_projects.status', 'agency_form2s_projects.*')->get();
        return view('pages.form2submissionview', compact('projects_in_form2','agency','getagency'));  
    } 

    public function review2($id)
    {
        // dd($id);
        $form2submit = \App\Agency_form2s_project::find($id);
        $form2submit->status = 'Reviewed';
        $form2submit->nro_remarks = request('nro_remarks');
        $form2submit->modified_by = auth()->user()->name;
        $form2submit->save();
        alert()->success('Project Reviewed!');
        return back(); 
    }

    public function form3()
    {
        $form3 = \App\Form3::all();
        return view('pages.form3', compact('form3'));  
    }

    public function addform3()
    {
        // dd($id);
        $form3submit = new \App\Form3();
        $form3submit->period = request('period');
        $form3submit->is_lock = 1;
        $form3submit->save();
        alert()->success('Form Submission Created!');
        return back(); 
    }

    public function unlock3($id)
    {
        $form3 = \App\Form3::find($id);
        $form3->is_lock = 0;
        $form3->save();
        alert()->success('Form Unlocked!');
        return back(); 
    }

    public function lockform3($id)
    {
        $form3 = \App\Form3::find($id);
        $form3->is_lock = 1;
        $form3->save();
        alert()->success('Form Locked!');
        return back(); 
    }

    public function form3submission($id)
    {
        $period = $id;
        $form3 = \App\Agency_form3::join('agencies', 'agencies.id','=','agency_form3s.agency_id')->where('period','=',$id)->select('agencies.UACS_AGY_DSC', 'agency_form3s.period', 'agency_form3s.id', 'agency_form3s.status', 'agency_form3s.nro_status_review','agency_form3s.nro_remarks')->get();
        return view('pages.form3submission', compact('form3','period'));  
    } 

    public function form3submissionview($id)
    {
        $getagency = \App\Agency_form3::find($id);
        $agency = \App\Agencies::find($getagency->agency_id);
        $projects_in_form3 = \App\Agency_form3s_project::join('projects', 'projects.id','=','agency_form3s_projects.project_id')->where('agency_form3s_projects.agency_form3s_id','=',$id)->where(function ($query) {
        $query->where('agency_form3s_projects.status', '=', 'Reviewed')
          ->orWhere('agency_form3s_projects.status', '=', 'Endorsed');
        })->select('agency_form3s_projects.id','projects.title','agency_form3s_projects.status', 'agency_form3s_projects.*')->get();
        return view('pages.form3submissionview', compact('projects_in_form3','agency','getagency'));  
    } 

    public function review3($id)
    {
        // dd($id);
        $form3submit = \App\Agency_form3s_project::find($id);
        $form3submit->status = 'Reviewed';
        $form3submit->nro_remarks = request('nro_remarks');
        $form3submit->modified_by = auth()->user()->name;
        $form3submit->save();
        alert()->success('Project Reviewed!');
        return back(); 
    }

    public function form3review($id)
    {
        // dd($id);
        $form3submit = \App\Agency_form3::find($id);
        $form3submit->nro_status_review = 'NRO Reviewed';
        $form3submit->nro_remarks = request('nro_remarks');
        $form3submit->save();
        alert()->success('Form Reviewed!');
        return back(); 
    }

    public function form4()
    {
        $form4 = \App\Form4::all();
        return view('pages.form4', compact('form4'));  
    }

    public function addform4()
    {
        // dd($id);
        $form4submit = new \App\Form4();
        $form4submit->period = request('period');
        $form4submit->is_lock = 1;
        $form4submit->save();
        alert()->success('Form Submission Created!');
        return back(); 
    }

    public function unlock4($id)
    {
        $form4 = \App\Form4::find($id);
        $form4->is_lock = 0;
        $form4->save();
        alert()->success('Form Unlocked!');
        return back(); 
    }

    public function lockform4($id)
    {
        $form4 = \App\Form4::find($id);
        $form4->is_lock = 1;
        $form4->save();
        alert()->success('Form Locked!');
        return back(); 
    }

    public function form4submission($id)
    {
        $period = $id;
        $form4 = \App\Agency_form4::join('agencies', 'agencies.id','=','agency_form4s.agency_id')->where('period','=',$id)->select('agencies.UACS_AGY_DSC', 'agency_form4s.period', 'agency_form4s.id', 'agency_form4s.status', 'agency_form4s.nro_status_review','agency_form4s.nro_remarks')->get();
        return view('pages.form4submission', compact('form4','period'));  
    } 

    public function form4submissionview($id)
    {
        $getagency = \App\Agency_form4::find($id);
        $agency = \App\Agencies::find($getagency->agency_id);
        $projects_in_form4 = \App\Agency_form4s_project::join('projects', 'projects.id','=','agency_form4s_projects.project_id')->where('agency_form4s_projects.agency_form4s_id','=',$id)->where(function ($query) {
        $query->where('agency_form4s_projects.status', '=', 'Reviewed')
          ->orWhere('agency_form4s_projects.status', '=', 'Endorsed');
        })->select('agency_form4s_projects.id','projects.title','agency_form4s_projects.status', 'agency_form4s_projects.*')->get();
        return view('pages.form4submissionview', compact('projects_in_form4','agency','getagency'));  
    } 

    public function review4($id)
    {
        // dd($id);
        $form4submit = \App\Agency_form4s_project::find($id);
        $form4submit->status = 'Reviewed';
        $form4submit->nro_remarks = request('nro_remarks');
        $form4submit->modified_by = auth()->user()->name;
        $form4submit->save();
        alert()->success('Project Reviewed!');
        return back(); 
    }

    public function form4review($id)
    {
        // dd($id);
        $form4submit = \App\Agency_form4::find($id);
        $form4submit->nro_status_review = 'NRO Reviewed';
        $form4submit->nro_remarks = request('nro_remarks');
        $form4submit->save();
        alert()->success('Form Reviewed!');
        return back(); 
    }

        public function form5()
    {
        $form5 = \App\Form5::all();
        return view('pages.form5', compact('form5'));  

    }

        public function addform5()
    {
        // dd($id);
        $form5submit = new \App\Form5();
        $form5submit->period = request('period');
        $form5submit->is_lock = 1;
        $form5submit->save();
        alert()->success('Form Submission Created!');
        return back(); 
    }

    public function form5submission($id)
    {
        $period = $id;
        $agencies = \App\Agencies::orderBy('UACS_AGY_DSC', 'ASC')->get();
        $form5id = \App\Agency_form5::where('period','=',$period)->select('id')->first();
        if(empty($form5id)){
            $formid = "";
        }else{
            $formid = $form5id->id;
        }
        $form5 = \App\Agency_form5::join('agencies', 'agencies.id','=','agency_form5s.agency_id')->where('period','=',$id)->select('agencies.UACS_AGY_DSC', 'agency_form5s.period', 'agency_form5s.id', 'agency_form5s.status', 'agency_form5s.nro_status_review','agency_form5s.nro_remarks','agency_form5s.agency_id')->get();
        return view('pages.form5submission', compact('form5','period','agencies','formid'));  
    } 

    public function form5add()
    {
        // dd($id);
        $form5submit = new \App\Agency_form5();
        $form5submit->period = request('period');
        $form5submit->agency_id = request('agency_id');
        $form5submit->status = "Draft";
        $form5submit->save();

        $form5id = \App\Agency_form5::where('period','=',$form5submit->period)->where('agency_id','=',$form5submit->agency_id)->select('id')->first();
        if(empty($form5id)){
            $formid = "";
        }else{
            $formid = $form5id->id;
        }

        $getidofperiod = \App\Agency_form2::where('agency_id','=',$form5submit->agency_id)->where('period','=',$form5submit->period)->first();

        // dd($getidofperiod->id);

        if(empty($getidofperiod)){
            alert()->success('Agency has no Form 2 Submission!');
            return back(); 
        }else{
            $getprojectsform2 = \App\Agency_form2s_project::join('projects', 'projects.id','=','agency_form2s_projects.project_id')
            ->where('projects.implementingagency','=',$form5submit->agency_id)
            ->where(function ($query) {
                $query->where('agency_form2s_projects.status', '=', 'Reviewed')
                  ->orWhere('agency_form2s_projects.status', '=', 'Endorsed');
                })
            ->where('agency_form2s_id','=',$getidofperiod->id)
            ->select('projects.id','agency_form2s_projects.alloc_asof','agency_form2s_projects.releases_asof','agency_form2s_projects.obligations_asof','agency_form2s_projects.expenditures_asof','agency_form2s_projects.ttd','agency_form2s_projects.atd','agency_form2s_projects.male','agency_form2s_projects.female','agency_form2s_projects.agency_form2s_id','agency_form2s_projects.agency_remarks', 'agency_form2s_projects.remarks', 'agency_form2s_projects.oi')
            ->get();

            foreach($getprojectsform2 as $projects){
                $form5submit = new \App\Agency_form5s_project();
                $form5submit->project_id = $projects->id;
                $form5submit->agency_form5s_id = $formid;
                $form5submit->allocation = $projects->alloc_asof;
                $form5submit->releases = $projects->releases_asof;
                $form5submit->obligations = $projects->obligations_asof;
                $form5submit->expenditures = $projects->expenditures_asof;
                $form5submit->ttd = $projects->ttd;
                $form5submit->atd = $projects->atd;
                $form5submit->male = $projects->male;
                $form5submit->female = $projects->female;
                $form5submit->oi = $projects->oi;
                //$form5submit->agency_remarks = $projects->agency_remarks;
                $form5submit->agency_remarks = $projects->remarks;
                $form5submit->modified_by = auth()->user()->name;

                            if($form5submit->ttd == 0.00){
                $form5submit->negativeslippage = 0;
            }else{
                $checkslip = round(($form5submit->atd-$form5submit->ttd), 2);
                if($checkslip < 0){
                    $form5submit->negativeslippage = 1;
                }else{
                    $form5submit->negativeslippage = 0;
                }
                
            }

                $form5submit->save();
            }
            alert()->success('Form Submission Created!');
            return back(); 
            }
    }

    public function form5submissionview($id)
    {
        $getagency = \App\Agency_form5::find($id);
        $agency = \App\Agencies::find($getagency->agency_id);
        $projects_in_form5 = \App\Agency_form5s_project::join('projects', 'projects.id','=','agency_form5s_projects.project_id')->where('agency_form5s_projects.agency_form5s_id','=',$id)->select('agency_form5s_projects.id','projects.title','agency_form5s_projects.status', 'agency_form5s_projects.*')->get();
        $projects_in_form5_total_cost = \App\Agency_form5s_project::join('projects', 'projects.id','=','agency_form5s_projects.project_id')->where('agency_form5s_projects.agency_form5s_id','=',$id)->select('agency_form5s_projects.id','projects.title','agency_form5s_projects.status', 'agency_form5s_projects.*')->sum('allocation');
        return view('pages.form5submissionview', compact('projects_in_form5','agency','getagency','projects_in_form5_total_cost'));  
    } 

 public function form5generate($id)
    {
        $getprojectsform2 = \App\Agency_form2s_project::join('projects', 'projects.id','=','agency_form2s_projects.project_id')->where('projects.implementingagency','=',$id)->where('agency_form2s_projects.status','=','Endorsed')->select('projects.id','agency_form2s_projects.alloc_asof','agency_form2s_projects.releases_asof','agency_form2s_projects.obligations_asof','agency_form2s_projects.expenditures_asof','agency_form2s_projects.ttd','agency_form2s_projects.atd','agency_form2s_projects.male','agency_form2s_projects.female', 'agency_form2s_projects.remarks')->get();

        foreach($getprojectsform2 as $projects){
            $form5submit = new \App\Agency_form5s_project();
            $form5submit->project_id = $projects->id;
            $form5submit->agency_form5s_id = request('form5s_id');
            $form5submit->allocation = $projects->alloc_asof;
            $form5submit->releases = $projects->releases_asof;
            $form5submit->obligations = $projects->obligations_asof;
            $form5submit->expenditures = $projects->expenditures_asof;
            $form5submit->ttd = $projects->ttd;
            $form5submit->atd = $projects->atd;
            $form5submit->male = $projects->male;
            $form5submit->female = $projects->female;
            $form5submit->agency_remarks = $projects->remarks;
            $form5submit->modified_by = auth()->user()->name;



            $form5submit->save();
        }
        alert()->success('Form Submission Created!');
        return back(); 

    }


    public function form5addsubmit($id)
    {
        $form5submit = \App\Agency_form5s_project::find($id);
        $form5submit->nro_remarks = request('nro_remarks');
        $form5submit->status = "Reviewed";
        $form5submit->modified_by = auth()->user()->name;
        $form5submit->save();
        alert()->success('Project Reviewed!');
        return back(); 
    }
    
    public function form6()
    {
        $form6 = \App\Form6::all();
        return view('pages.form6', compact('form6'));  

    }

    public function addform6()
    {
        // dd($id);
        $form6submit = new \App\Form6();
        $form6submit->period = request('period');
        $form6submit->is_lock = 1;
        $form6submit->save();
        alert()->success('Form Submission Created!');
        return back(); 
    }

    public function form6submission($id)
    {
        $period = $id;
        $agencies = \App\Agencies::orderBy('UACS_AGY_DSC', 'ASC')->get();
        $form6id = \App\Agency_form6::where('period','=',$period)->select('id')->first();
        if(empty($form6id)){
            $formid = "";
        }else{
            $formid = $form6id->id;
        }
        $form6 = \App\Agency_form6::join('agencies', 'agencies.id','=','agency_form6s.agency_id')->where('period','=',$id)->select('agencies.UACS_AGY_DSC', 'agency_form6s.period', 'agency_form6s.id', 'agency_form6s.status', 'agency_form6s.nro_status_review','agency_form6s.nro_remarks','agency_form6s.agency_id')->get();
        return view('pages.form6submission', compact('form6','period','agencies','formid'));  
    } 

    public function form6add()
    {
        // dd($id);
        $form6submit = new \App\Agency_form6();
        $form6submit->period = request('period');
        $form6submit->agency_id = request('agency_id');
        $form6submit->status = "Draft";
        $form6submit->save();

        $agency = \App\Agencies::find($form6submit->agency_id);

        $form6id = \App\Agency_form6::where('period','=',$form6submit->period)->where('agency_id','=',$form6submit->agency_id)->select('id')->first();

        if(empty($form6id)){
            $formid = "";
        }else{
            $formid = $form6id->id;
        }

        $getidofperiod = \App\Agency_form2::where('agency_id','=',$form6submit->agency_id)->where('period','=',$form6submit->period)->first();
        // dd($getidofperiod->id);

        $getidofperiod2 = \App\Agency_form3::where('agency_id','=',$form6submit->agency_id)->where('period','=',$form6submit->period)->first();

        // dd($getidofperiod2);

        if(empty($getidofperiod)){
            alert()->success('Agency has no Form 2 Submission!');
        }else{
            $getprojectsform2 = \App\Agency_form2s_project::join('projects', 'projects.id','=','agency_form2s_projects.project_id')
            ->where('agency_form2s_id','=',$getidofperiod->id)
            ->where('projects.implementingagency','=',$form6submit->agency_id)
            ->where(function ($query) {
                $query->where('agency_form2s_projects.status', '=', 'Reviewed')
                  ->orWhere('agency_form2s_projects.status', '=', 'Endorsed');
                })
            ->select('projects.id','agency_form2s_projects.releases_asof','agency_form2s_projects.expenditures_asof','agency_form2s_projects.ttd','agency_form2s_projects.atd','agency_form2s_projects.agency_form2s_id','agency_form2s_projects.agency_remarks')->get();

        foreach($getprojectsform2 as $projects){
            $form6submit = new \App\Agency_form6s_project();
            $form6submit->project_id = $projects->id;
            $form6submit->agency_form6s_id = $formid;
            $form6submit->releases = $projects->releases_asof;
            $form6submit->expenditures = $projects->expenditures_asof;
            $form6submit->ttd = $projects->ttd;
            $form6submit->atd = $projects->atd;
            $form6submit->modified_by = auth()->user()->name;

            $recommendations = "";
            if(empty($getidofperiod2)){
                alert()->success('Agency has no Form 3 Submission!');
            }else{
                $getprojectsform3 = \App\Agency_form3s_project::join('projects', 'projects.id','=','agency_form3s_projects.project_id')
                ->where('agency_form3s_id','=',$getidofperiod2->id)
                ->where('agency_form3s_projects.project_id','=',$form6submit->project_id)
                ->select('projects.id','agency_form3s_projects.findings','agency_form3s_projects.recommendations')->first();

                if(empty($getprojectsform3->findings)){
                $issuetosave = "";
                }else{
                    $issuetosave = $getprojectsform3->findings; 
                }

                if(!empty($getprojectsform3->recommendations)){
                    $recommendations = $getprojectsform3->recommendations; 
                }


            }

            if(empty($issuetosave)){
                $issuetosave = "";
            }else{
                
            }

            $form6submit->issues = $issuetosave;
            $form6submit->source = "Submitted RPMES input forms of ".$agency->Abbreviation." as of ".$getidofperiod->period;
                //$form6submit->action = $projects->agency_remarks;
            $form6submit->action = $recommendations;

            if($form6submit->atd - $form6submit->ttd < 0){
                $form6submit->save();
            }else{
                
            }
            
        }

        alert()->success('Form Submission Created!');
        
        }
        return back(); 
    }

    public function form6submissionview($id)
    {
        $getagency = \App\Agency_form6::find($id);
        $agency = \App\Agencies::find($getagency->agency_id);
        $projects_in_form6 = \App\Agency_form6s_project::join('projects', 'projects.id','=','agency_form6s_projects.project_id')->where('agency_form6s_projects.agency_form6s_id','=',$id)->select('agency_form6s_projects.id','projects.title','agency_form6s_projects.status', 'agency_form6s_projects.*')->get();
        return view('pages.form6submissionview', compact('projects_in_form6','agency','getagency'));  
    } 

    public function form6addsubmit($id)
    {
        $form5submit = \App\Agency_form6s_project::find($id);
        $form5submit->nro_remarks = request('nro_remarks');
        $form5submit->issues = request('issues');
        $form5submit->source = request('source');
        $form5submit->action = request('action');
        $form5submit->status = "Reviewed";
        $form5submit->modified_by = auth()->user()->name;
        $form5submit->save();
        alert()->success('Project Reviewed!');
        return back(); 
    }

    public function form1revert($id)
    {
        $form5submit = \App\Agency_form1::find($id);
        $form5submit->status = "Draft";
        $form5submit->save();
        alert()->success('Submission Reverted!');
        return back(); 
    }


    public function form2revert($id)
    {
        $form5submit = \App\Agency_form2::find($id);
        $form5submit->status = "Draft";
        $form5submit->save();
        alert()->success('Submission Reverted!');
        return back(); 
    }

    public function form3revert($id)
    {
        $form5submit = \App\Agency_form3::find($id);
        $form5submit->status = "Draft";
        $form5submit->save();
        alert()->success('Submission Reverted!');
        return back(); 
    }

    public function form4revert($id)
    {
        $form5submit = \App\Agency_form4::find($id);
        $form5submit->status = "Draft";
        $form5submit->save();
        alert()->success('Submission Reverted!');
        return back(); 
    }
}
