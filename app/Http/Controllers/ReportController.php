<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  
    public function exportform3agency($id){
        $form3proj = \App\Agency_form3s_project::find($id);

        $form3 = \App\Agency_form3::where('id','=',$form3proj->agency_form3s_id)->first();

        $proj = \App\Projects::leftJoin('sectors','sectors.id','=','projects.sector')->find($form3proj->project_id);
        $projlocs = \App\locations::where('proj_id', '=', $form3proj->project_id)->get();

        $agency = \App\Agencies::find($proj->implementingagency);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("C1", "RPMES FORM 3");
        $sheet->setCellValue("A2", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
        $sheet->setCellValue("A3", "PROJECT EXCEPTION REPORT");
        $sheet->setCellValue("A4", "As of ".$form3->period);
        $sheet->setCellValue("A6", "Name of Project: ".$proj->title);
        $sheet->setCellValue("A7", "Sector/Subsector: ".$proj->sector);

        if($proj->imp_status == 1){
            $sheet->setCellValue("A8", "Implementation Status: ( / ) Ahead of Schedule    (   ) On Schedule     (   ) Behind Schedule: : ");
        }elseif($proj->imp_status == 2){
            $sheet->setCellValue("A8", "Implementation Status: (  ) Ahead of Schedule    ( / ) On Schedule     (   ) Behind Schedule: : ");
        }else{
            $sheet->setCellValue("A8", "Implementation Status: (  ) Ahead of Schedule    (   ) On Schedule     ( / ) Behind Schedule: : ");
        }

        $loc_all = "";
        foreach($projlocs as $projloc){
            $loc_all .= $projloc->location;
        }
        
        $sheet->setCellValue("C6", "Location: ".$loc_all);

        $sheet->setCellValue("C7", "Implementing Agency:  ".$agency->UACS_AGY_DSC);

        $sheet->setCellValue("A9", "(1)");
        $sheet->setCellValue("B9", "(2)");
        $sheet->setCellValue("C9", "(3)");
        $sheet->setCellValue("A10", "Findings");
        $sheet->setCellValue("B10", "Possible Reasons/Causes");
        $sheet->setCellValue("C10", "Recommendations");
        $sheet->setCellValue("A11", $form3proj->findings);
        $sheet->setCellValue("B11", $form3proj->possible);
        $sheet->setCellValue("C11", $form3proj->recommendations);

        $sheet->setCellValue("A13", "Prepared by: ");
        $sheet->setCellValue("A14", "Date/Office: ");
        $sheet->setCellValue("A15", "Date: ");

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(70);
        $spreadsheet->getActiveSheet()->mergeCells('A2:C2');
        $spreadsheet->getActiveSheet()->mergeCells('A3:C3');
        $spreadsheet->getActiveSheet()->mergeCells('A4:C4');

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $styleArray2 = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'color' => ['argb' => '000000'],
            ],
        ],
        ];

        $styleArray3 = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '002060'],
            ],
        ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A9:C10')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('A9:C10')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('A11:C11')->applyFromArray($styleArray3);


        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="exportform3.xlsx"');
        $writer->save("php://output");
    } 

    public function exportform4agency($id){
        $form4 = \App\Agency_form4::find($id);
        $form4projs = \App\Agency_form4s_project::leftJoin('projects','projects.id','=','agency_form4s_projects.project_id')->where('agency_form4s_id','=',$form4->id)->get();
        $agency = \App\Agencies::find($form4->agency_id);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("D1", "RPMES FORM 4");
        $sheet->setCellValue("A2", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
        $sheet->setCellValue("A3", "PROJECT RESULTS");
        $sheet->setCellValue("A4", "As of ".$form4->period);
        $sheet->setCellValue("A6", "Implementing Agency:  ");
        $sheet->setCellValue("B6", $agency->UACS_AGY_DSC);

        $sheet->setCellValue("A7", "(1)");
        $sheet->setCellValue("B7", "(2)");
        $sheet->setCellValue("C7", "(3)");
        $sheet->setCellValue("D7", "(4)");
        $sheet->setCellValue("A8", "Name of Project");
        $sheet->setCellValue("B8", "Project Objective/s (Based on LogFrame)");
        $sheet->setCellValue("C8", "Results Indicator/Target (Based on LogFrame)");
        $sheet->setCellValue("D8", "Observed Results");

        $i = 9;

        foreach($form4projs as $form4proj){
            $sheet->setCellValue("A".$i, $form4proj->title); 
            $sheet->setCellValue("B".$i, $form4proj->objectives); 
            $sheet->setCellValue("C".$i, $form4proj->indicator); 
            $sheet->setCellValue("D".$i, $form4proj->results); 

            $i++;
        }

        $footer = $i + 2;
        $sheet->setCellValue("A".$footer, "Submitted by: ");
        $sheet->setCellValue("C".$footer_note = $footer, "Noted by: ".$agency->head);
        $sheet->setCellValue("A".$footer = $footer_note+1, "Date/Office: ");
        $sheet->setCellValue("C".$footer_head = $footer, "Head of the Agency");
        $sheet->setCellValue("A".$footer_date = $footer_head+1, "Date: ");



        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(50);
        $spreadsheet->getActiveSheet()->mergeCells('A2:D2');
        $spreadsheet->getActiveSheet()->mergeCells('A3:D3');
        $spreadsheet->getActiveSheet()->mergeCells('A4:D4');

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $styleArray2 = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'color' => ['argb' => '000000'],
            ],
        ],
        ];

        $styleArray3 = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '002060'],
            ],
        ],
        ];

        $styleArray4 = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A7:D8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $spreadsheet->getActiveSheet()->getStyle('A1:D5')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_head)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_note)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('A7:D8')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('A9:D'.$i)->applyFromArray($styleArray3);


        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="exportform4.xlsx"');
        $writer->save("php://output");
    } 

    public function exportform2agency($id){
        $form2 = \App\Agency_form2::find($id);
        $form2projs = \App\Agency_form2s_project::leftJoin('projects','projects.id','=','agency_form2s_projects.project_id')->where('agency_form2s_id','=',$form2->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->get();
        $agency = \App\Agencies::find($form2->agency_id);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("R1", "RPMES FORM 2");
        $sheet->setCellValue("A2", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
        $sheet->setCellValue("A3", "PHYSICAL AND FINANCIAL ACCOMPLISHMENT REPORT");
        $sheet->setCellValue("A4", "PROJECT RESULTS");
        $sheet->setCellValue("A5", "As of ".$form2->period);
        $sheet->setCellValue("B6", "Implementing Agency: ".$agency->UACS_AGY_DSC);

        $sheet->setCellValue("B7", "(1)");
        $sheet->setCellValue("C7", "(2)");
        $sheet->setCellValue("D7", "(3)");
        $sheet->setCellValue("E7", "(4)");
        $sheet->setCellValue("F7", "(5)");
        $sheet->setCellValue("G7", "(6)");
        $sheet->setCellValue("H7", "(7)");
        $sheet->setCellValue("I7", "(8)");
        $sheet->setCellValue("J7", "(9)");
        $sheet->setCellValue("K7", "(10)");
        $sheet->setCellValue("L7", "(11)");
        $sheet->setCellValue("M7", "(12)");
        $sheet->setCellValue("N7", "(13)");
        $sheet->setCellValue("O7", "(14)");
        $sheet->setCellValue("P7", "(15)");
        $sheet->setCellValue("R7", "(16)");

        $sheet->setCellValue("B8", "(a) Name of Project");
        $sheet->setCellValue("B9", "(b) Date Started");
        $sheet->setCellValue("B10", "(c) Target Completion Date");
        $sheet->setCellValue("B11", "(d) Location");
        $sheet->setCellValue("B12", "(e) Funding Source");

        $sheet->setCellValue("C8", "Financial Status (in PhP M)");
        $sheet->setCellValue("K8", "Physical Status (%)");
        $sheet->setCellValue("P8", "Employment Generated");
        $sheet->setCellValue("R8", "Remarks");

        $sheet->setCellValue("C10", "As of Reporting Period");
        $sheet->setCellValue("D10", "For the Quarter");
        $sheet->setCellValue("E10", "As of Reporting Period");
        $sheet->setCellValue("F10", "For the Quarter");
        $sheet->setCellValue("G10", "As of Reporting Period");
        $sheet->setCellValue("H10", "For the Quarter");
        $sheet->setCellValue("I10", "As of Reporting Period");
        $sheet->setCellValue("J10", "For the Quarter");

        $sheet->setCellValue("C9", "Allocation");
        $sheet->setCellValue("E9", "Releases");
        $sheet->setCellValue("G9", "Obligations");
        $sheet->setCellValue("I9", "Expenditures");

        $sheet->setCellValue("K9", "Output Indicator");
        $sheet->setCellValue("L9", "Target to Date");
        $sheet->setCellValue("M9", "Target for the Quarter");
        $sheet->setCellValue("N9", "Actual to Date");
        $sheet->setCellValue("O9", "Actual for the Quarter");

        $sheet->setCellValue("P10", "(M)");
        $sheet->setCellValue("Q10", "(F)");

        $i = 13;
        $num = 1;

        foreach($form2projs as $form2proj){
            $sheet->setCellValue("A".$i, $num);
            $sheet->setCellValue("B".$i, "(a) ".$form2proj->title); 
            $sheet->setCellValue("C".$i, $form2proj->alloc_asof); 
            $sheet->setCellValue("D".$i, $form2proj->alloc_month); 
            $sheet->setCellValue("E".$i, $form2proj->releases_asof); 
            $sheet->setCellValue("F".$i, $form2proj->releases_month); 
            $sheet->setCellValue("G".$i, $form2proj->obligations_asof); 
            $sheet->setCellValue("H".$i, $form2proj->obligations_month); 
            $sheet->setCellValue("I".$i, $form2proj->expenditures_asof); 
            $sheet->setCellValue("J".$i, $form2proj->expenditures_month);   
            $sheet->setCellValue("K".$i, $form2proj->oi); 
            $sheet->setCellValue("L".$i, $form2proj->ttd); 
            $sheet->setCellValue("M".$i, $form2proj->tftm); 
            $sheet->setCellValue("N".$i, $form2proj->atd); 
            $sheet->setCellValue("O".$i, $form2proj->aftm); 
            $sheet->setCellValue("P".$i, $form2proj->male); 
            $sheet->setCellValue("Q".$i, $form2proj->female); 
            $sheet->setCellValue("R".$i, $form2proj->remarks); 
            $first_i = $i;
            $i++;
            $sheet->setCellValue("B".$i, "(b) ".$form2proj->start); 
            $i++;
            $sheet->setCellValue("B".$i, "(c) ".$form2proj->end); 
            $i++;
            $projlocs = \App\locations::where('proj_id', '=', $form2proj->project_id)->get();

            $loc_all = "";
            foreach($projlocs as $projloc){
                $loc_all .= $projloc->location;
            }
            $sheet->setCellValue("B".$i, "(d) ".$loc_all); 
            $i++;
            $sheet->setCellValue("B".$i, "(e) ".$form2proj->type); 
            $last_i = $i;
            $spreadsheet->getActiveSheet()->mergeCells("A$first_i:A$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("C$first_i:C$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("D$first_i:D$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("E$first_i:E$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("F$first_i:F$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("G$first_i:G$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("H$first_i:H$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("I$first_i:I$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("J$first_i:J$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("K$first_i:K$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("L$first_i:L$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("M$first_i:M$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("N$first_i:N$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("O$first_i:O$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("P$first_i:P$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("Q$first_i:Q$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("R$first_i:R$last_i");
            $i++;
            $num++;
        }

        $footer = $i + 2;
        $sheet->setCellValue("B".$footer, "Submitted by: ");
        $sheet->setCellValue("D".$footer_note = $footer, "Noted by: ".$agency->head);
        $sheet->setCellValue("B".$footer = $footer_note+1, "Date/Office: ");
        $sheet->setCellValue("D".$footer_head = $footer, "Head of the Agency");
        $sheet->setCellValue("B".$footer_date = $footer_head+1, "Date: ");

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $spreadsheet->getActiveSheet()->mergeCells('A2:R2');
        $spreadsheet->getActiveSheet()->mergeCells('A3:R3');
        $spreadsheet->getActiveSheet()->mergeCells('A4:R4');
        $spreadsheet->getActiveSheet()->mergeCells('A5:R5');
        $spreadsheet->getActiveSheet()->mergeCells('C8:J8');
        $spreadsheet->getActiveSheet()->mergeCells('K8:O8');
        $spreadsheet->getActiveSheet()->mergeCells('P8:Q9');
        $spreadsheet->getActiveSheet()->mergeCells('R8:R12');
        $spreadsheet->getActiveSheet()->mergeCells('P7:Q7');

        $spreadsheet->getActiveSheet()->mergeCells('C10:C12');
        $spreadsheet->getActiveSheet()->mergeCells('D10:D12');
        $spreadsheet->getActiveSheet()->mergeCells('E10:E12');
        $spreadsheet->getActiveSheet()->mergeCells('F10:F12');
        $spreadsheet->getActiveSheet()->mergeCells('G10:G12');
        $spreadsheet->getActiveSheet()->mergeCells('H10:H12');
        $spreadsheet->getActiveSheet()->mergeCells('I10:I12');
        $spreadsheet->getActiveSheet()->mergeCells('J10:J12');

        $spreadsheet->getActiveSheet()->mergeCells('C9:D9');
        $spreadsheet->getActiveSheet()->mergeCells('E9:F9');
        $spreadsheet->getActiveSheet()->mergeCells('G9:H9');
        $spreadsheet->getActiveSheet()->mergeCells('I9:J9');

        $spreadsheet->getActiveSheet()->mergeCells('K9:K12');
        $spreadsheet->getActiveSheet()->mergeCells('L9:L12');
        $spreadsheet->getActiveSheet()->mergeCells('M9:M12');
        $spreadsheet->getActiveSheet()->mergeCells('N9:N12');
        $spreadsheet->getActiveSheet()->mergeCells('O9:O12');

        $spreadsheet->getActiveSheet()->mergeCells('P10:P12');
        $spreadsheet->getActiveSheet()->mergeCells('Q10:Q12');

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $styleArray_wrap = [
            'alignment' => [
                'wrapText' => true,
            ],
        ];

        $styleArray2 = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
        ],
        ];

        $styleArray3 = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '002060'],
            ],
            ],
        ];

        $styleArray_left = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],

        ];

        $styleArray4 = [

            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content_center = [
           'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content = [
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $spreadsheet->getActiveSheet()->getStyle('A7:R12')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $spreadsheet->getActiveSheet()->getStyle('A1:R5')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_head)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_note)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('A7:R7')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('A6:R12')->applyFromArray($styleArray_wrap);
        $spreadsheet->getActiveSheet()->getStyle('B8:B12')->applyFromArray($styleArray_left);
        $spreadsheet->getActiveSheet()->getStyle('C8:R12')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle("A13:R$i")->applyFromArray($styleArray_content);
        $spreadsheet->getActiveSheet()->getStyle("C13:R$i")->applyFromArray($styleArray_content_center);
        // $spreadsheet->getActiveSheet()->getStyle('A9:D'.$i)->applyFromArray($styleArray3);


        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="exportform2.xlsx"');
        $writer->save("php://output");
    } 

    public function exportform1agency($id){
        $form1 = \App\Agency_form1::find($id);
        $form1projs = \App\Agency_form1s_project::leftJoin('projects','projects.id','=','agency_form1s_projects.project_id')->where('agency_form1s_id','=',$form1->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->
            leftJoin('sectors','projects.sector','=','sectors.id')->get();
        $agency = \App\Agencies::find($form1->agency_id);


        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("P1", "RPMES FORM 1");
        $sheet->setCellValue("A2", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
        $sheet->setCellValue("A3", "INITIAL PROJECT REPORT");
        $sheet->setCellValue("A4", "As of ".$form1->fy);
        $sheet->setCellValue("B6", "Implementing Agency: ".$agency->UACS_AGY_DSC);

        $sheet->setCellValue("B7", "(1)");
        $sheet->setCellValue("C7", "(2)");
        $sheet->setCellValue("D7", "(3)");
        $sheet->setCellValue("E7", "(4)");
        $sheet->setCellValue("F7", "(5)");
        $sheet->setCellValue("G7", "(6)");
        $sheet->setCellValue("H7", "(7)");
        $sheet->setCellValue("I7", "(8)");
        $sheet->setCellValue("J7", "(9)");
        $sheet->setCellValue("K7", "(10)");
        $sheet->setCellValue("L7", "(11)");
        $sheet->setCellValue("M7", "(12)");
        $sheet->setCellValue("N7", "(13)");
        $sheet->setCellValue("O7", "(14)");
        $sheet->setCellValue("P7", "(15)");

        $sheet->setCellValue("B8", "(a) Name of Project");
        $sheet->setCellValue("B9", "(b) Location");
        $sheet->setCellValue("B10", "(c) Sector/Subsector");
        $sheet->setCellValue("B11", "(d) Funding Source");
        $sheet->setCellValue("B12", "(e) Mode of Implementation");
        $sheet->setCellValue("B13", "(f) Project Schedule");

        $sheet->setCellValue("C8", "Total Project Target");
        $sheet->setCellValue("D8", "JAN");
        $sheet->setCellValue("E8", "FEB");
        $sheet->setCellValue("F8", "MAR");
        $sheet->setCellValue("G8", "APR");
        $sheet->setCellValue("H8", "MAY");
        $sheet->setCellValue("I8", "JUN");
        $sheet->setCellValue("J8", "JUL");
        $sheet->setCellValue("K8", "AUG");
        $sheet->setCellValue("L8", "SEP");
        $sheet->setCellValue("M8", "OCT");
        $sheet->setCellValue("N8", "NOV");
        $sheet->setCellValue("O8", "DEC");
        $sheet->setCellValue("P8", "TOTAL");

        $i = 14;
        $num = 1;

        foreach($form1projs as $form1proj){
            $sheet->setCellValue("A".$i, $num);

            $sheet->setCellValue("B".$i, "(a) ".$form1proj->title); 
            $sheet->setCellValue("C".$i, "FS"); 
            $sheet->setCellValue("D".$i, $form1proj->fs_1); 
            $sheet->setCellValue("E".$i, $form1proj->fs_2); 
            $sheet->setCellValue("F".$i, $form1proj->fs_3); 
            $sheet->setCellValue("G".$i, $form1proj->fs_4); 
            $sheet->setCellValue("H".$i, $form1proj->fs_5); 
            $sheet->setCellValue("I".$i, $form1proj->fs_6); 
            $sheet->setCellValue("J".$i, $form1proj->fs_7);   
            $sheet->setCellValue("K".$i, $form1proj->fs_8); 
            $sheet->setCellValue("L".$i, $form1proj->fs_9); 
            $sheet->setCellValue("M".$i, $form1proj->fs_10); 
            $sheet->setCellValue("N".$i, $form1proj->fs_11); 
            $sheet->setCellValue("O".$i, $form1proj->fs_12); 
            $sheet->setCellValue("P".$i, $form1proj->fs_1 + $form1proj->fs_2 + $form1proj->fs_3 + $form1proj->fs_4 +
                                         $form1proj->fs_5 + $form1proj->fs_6 + $form1proj->fs_7 + $form1proj->fs_8 +
                                         $form1proj->fs_9 + $form1proj->fs_10 + $form1proj->fs_11 + $form1proj->fs_12); 
            $first_i = $i;
            $i++;
            //$projlocs = \App\locations::where('proj_id', '=', $form1proj->project_id)->get();
            $projlocs = \App\locations::where('proj_id', '=', $form1proj->id)->get();

            $loc_all = "";
            foreach($projlocs as $projloc){
                $loc_all .= $projloc->location;
            }
            $sheet->setCellValue("B".$i, "(b) ".$loc_all); 
            $sheet->setCellValue("C".$i, "%"); 
            $sheet->setCellValue("D".$i, $form1proj->pt_1); 
            $sheet->setCellValue("E".$i, $form1proj->pt_2); 
            $sheet->setCellValue("F".$i, $form1proj->pt_3); 
            $sheet->setCellValue("G".$i, $form1proj->pt_4); 
            $sheet->setCellValue("H".$i, $form1proj->pt_5); 
            $sheet->setCellValue("I".$i, $form1proj->pt_6); 
            $sheet->setCellValue("J".$i, $form1proj->pt_7);   
            $sheet->setCellValue("K".$i, $form1proj->pt_8); 
            $sheet->setCellValue("L".$i, $form1proj->pt_9); 
            $sheet->setCellValue("M".$i, $form1proj->pt_10); 
            $sheet->setCellValue("N".$i, $form1proj->pt_11); 
            $sheet->setCellValue("O".$i, $form1proj->pt_12); 
            $sheet->setCellValue("P".$i, $form1proj->pt_1 + $form1proj->pt_2 + $form1proj->pt_3 + $form1proj->pt_4 +
                                         $form1proj->pt_5 + $form1proj->pt_6 + $form1proj->pt_7 + $form1proj->pt_8 +
                                         $form1proj->pt_9 + $form1proj->pt_10 + $form1proj->pt_11 + $form1proj->pt_12); 
            $i++;
            $sheet->setCellValue("B".$i, "(c) ".$form1proj->sector); 
            $sheet->setCellValue("C".$i, "OI"); 
            $sheet->setCellValue("D".$i, $form1proj->oi_1); 
            $sheet->setCellValue("E".$i, $form1proj->oi_2); 
            $sheet->setCellValue("F".$i, $form1proj->oi_3); 
            $sheet->setCellValue("G".$i, $form1proj->oi_4); 
            $sheet->setCellValue("H".$i, $form1proj->oi_5); 
            $sheet->setCellValue("I".$i, $form1proj->oi_6); 
            $sheet->setCellValue("J".$i, $form1proj->oi_7);   
            $sheet->setCellValue("K".$i, $form1proj->oi_8); 
            $sheet->setCellValue("L".$i, $form1proj->oi_9); 
            $sheet->setCellValue("M".$i, $form1proj->oi_10); 
            $sheet->setCellValue("N".$i, $form1proj->oi_11); 
            $sheet->setCellValue("O".$i, $form1proj->oi_12); 
            $three = $i;
            $i++;
            $sheet->setCellValue("B".$i, "(d) ".$form1proj->type); 
            $i++;

            if ($form1proj->mode == 'By contract') {
                $sheet->setCellValue("B".$i, "(e) ".$form1proj->mode . " - " . $form1proj->contractor); 
            } else {
                $sheet->setCellValue("B".$i, "(e) ".$form1proj->mode); 
            }

            
            $five = $i;
            $i++;
            $sheet->setCellValue("B".$i, "(f) ".$form1proj->start . " to " . $form1proj->end); 
            $sheet->setCellValue("C".$i, "EG"); 
            $sheet->setCellValue("D".$i, "Male:".$form1proj->eg_1." Female:".$form1proj->eg_13); 
            $sheet->setCellValue("E".$i, "Male:".$form1proj->eg_2." Female:".$form1proj->eg_14); 
            $sheet->setCellValue("F".$i, "Male:".$form1proj->eg_3." Female:".$form1proj->eg_15);  
            $sheet->setCellValue("G".$i, "Male:".$form1proj->eg_4." Female:".$form1proj->eg_16);  
            $sheet->setCellValue("H".$i, "Male:".$form1proj->eg_5." Female:".$form1proj->eg_17);  
            $sheet->setCellValue("I".$i, "Male:".$form1proj->eg_6." Female:".$form1proj->eg_18);  
            $sheet->setCellValue("J".$i, "Male:".$form1proj->eg_7." Female:".$form1proj->eg_19);    
            $sheet->setCellValue("K".$i, "Male:".$form1proj->eg_8." Female:".$form1proj->eg_20);  
            $sheet->setCellValue("L".$i, "Male:".$form1proj->eg_9." Female:".$form1proj->eg_21);  
            $sheet->setCellValue("M".$i, "Male:".$form1proj->eg_10." Female:".$form1proj->eg_22);  
            $sheet->setCellValue("N".$i, "Male:".$form1proj->eg_11." Female:".$form1proj->eg_23);  
            $sheet->setCellValue("O".$i, "Male:".$form1proj->eg_12." Female:".$form1proj->eg_24);  
            $formmale = $form1proj->eg_1 + $form1proj->eg_2 + $form1proj->eg_3 + $form1proj->eg_4 +
                                         $form1proj->eg_5 + $form1proj->eg_6 + $form1proj->eg_7 + $form1proj->eg_8 +
                                         $form1proj->eg_9 + $form1proj->eg_10 + $form1proj->eg_11 + $form1proj->eg_12;
            $formfemale = $form1proj->eg_13 + $form1proj->eg_14 + $form1proj->eg_15 + $form1proj->eg_16 + $form1proj->eg_17 + $form1proj->eg_18 + $form1proj->eg_19 + $form1proj->eg_20 + $form1proj->eg_21 + $form1proj->eg_22 + $form1proj->eg_22 + $form1proj->eg_24;
            $sheet->setCellValue("P".$i, "Male:".$formmale." Female:".$formfemale);
            $last_i = $i;
            $spreadsheet->getActiveSheet()->mergeCells("A$first_i:A$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("C$three:C$five");
            $spreadsheet->getActiveSheet()->mergeCells("D$three:D$five");
            $spreadsheet->getActiveSheet()->mergeCells("E$three:E$five");
            $spreadsheet->getActiveSheet()->mergeCells("F$three:F$five");
            $spreadsheet->getActiveSheet()->mergeCells("G$three:G$five");
            $spreadsheet->getActiveSheet()->mergeCells("H$three:H$five");
            $spreadsheet->getActiveSheet()->mergeCells("I$three:I$five");
            $spreadsheet->getActiveSheet()->mergeCells("J$three:J$five");
            $spreadsheet->getActiveSheet()->mergeCells("K$three:K$five");
            $spreadsheet->getActiveSheet()->mergeCells("L$three:L$five");
            $spreadsheet->getActiveSheet()->mergeCells("M$three:M$five");
            $spreadsheet->getActiveSheet()->mergeCells("N$three:N$five");
            $spreadsheet->getActiveSheet()->mergeCells("O$three:O$five");
            $spreadsheet->getActiveSheet()->mergeCells("P$three:P$five");
            $i++;
            $num++;
        }

        $footer = $i + 2;
        $sheet->setCellValue("B".$footer, "Submitted by: ");
        $sheet->setCellValue("D".$footer_note = $footer, "Noted by: ".$agency->head);
        $sheet->setCellValue("B".$footer = $footer_note+1, "Date/Office: ");
        $sheet->setCellValue("D".$footer_head = $footer, "Head of the Agency");
        $sheet->setCellValue("B".$footer_date = $footer_head+1, "Date: ");

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $spreadsheet->getActiveSheet()->mergeCells('A2:P2');
        $spreadsheet->getActiveSheet()->mergeCells('A3:P3');
        $spreadsheet->getActiveSheet()->mergeCells('A4:P4');
       
        $spreadsheet->getActiveSheet()->mergeCells('C8:C13');
        $spreadsheet->getActiveSheet()->mergeCells('D8:D13');
        $spreadsheet->getActiveSheet()->mergeCells('E8:E13');
        $spreadsheet->getActiveSheet()->mergeCells('F8:F13');
        $spreadsheet->getActiveSheet()->mergeCells('G8:G13');
        $spreadsheet->getActiveSheet()->mergeCells('H8:H13');
        $spreadsheet->getActiveSheet()->mergeCells('I8:I13');
        $spreadsheet->getActiveSheet()->mergeCells('J8:J13');
        $spreadsheet->getActiveSheet()->mergeCells('K8:K13');
        $spreadsheet->getActiveSheet()->mergeCells('L8:L13');
        $spreadsheet->getActiveSheet()->mergeCells('M8:M13');
        $spreadsheet->getActiveSheet()->mergeCells('N8:N13');
        $spreadsheet->getActiveSheet()->mergeCells('O8:O13');
        $spreadsheet->getActiveSheet()->mergeCells('P8:P13');


        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $styleArray_wrap = [
            'alignment' => [
                'wrapText' => true,
            ],
        ];

        $styleArray2 = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
        ],
        ];

        $styleArray3 = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '002060'],
            ],
            ],
        ];

        $styleArray_left = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],

        ];

        $styleArray4 = [

            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content_center = [
           'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content = [
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $spreadsheet->getActiveSheet()->getStyle('A7:P13')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $spreadsheet->getActiveSheet()->getStyle('A1:P4')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_head)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_note)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('A7:P7')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('A6:P13')->applyFromArray($styleArray_wrap);
        $spreadsheet->getActiveSheet()->getStyle('B8:B13')->applyFromArray($styleArray_left);
        $spreadsheet->getActiveSheet()->getStyle('C8:P12')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle("A13:P$i")->applyFromArray($styleArray_content);
        $spreadsheet->getActiveSheet()->getStyle("C13:P$i")->applyFromArray($styleArray_content_center);
        // $spreadsheet->getActiveSheet()->getStyle('A9:D'.$i)->applyFromArray($styleArray3);


        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="exportform1.xlsx"');
        $writer->save("php://output");
    } 


    public function exportform5($id){
        $form5 = \App\Agency_form5::find($id);
        $form5projs = \App\Agency_form5s_project::leftJoin('projects','projects.id','=','agency_form5s_projects.project_id')->where('agency_form5s_id','=',$form5->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->select('projects.id', 'projects.title', 'agency_form5s_projects.allocation', 'agency_form5s_projects.releases', 'agency_form5s_projects.obligations', 'agency_form5s_projects.oi', 'agency_form5s_projects.expenditures', 'agency_form5s_projects.ttd', 'agency_form5s_projects.atd', 'agency_form5s_projects.male', 'agency_form5s_projects.female', 'agency_form5s_projects.agency_remarks', 'sources.type', 'projects.start', 'projects.end')->get();

        $form5projs_cost = \App\Agency_form5s_project::leftJoin('projects','projects.id','=','agency_form5s_projects.project_id')->where('agency_form5s_id','=',$form5->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->sum('allocation');
        $agency = \App\Agencies::find($form5->agency_id);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("Q1", "RPMES FORM 5");
        $sheet->setCellValue("A2", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
        $sheet->setCellValue("A3", "SUMMARY OF FINANCIAL AND PHYSICAL ACCOMPLISHMENTS INCLUDING PROJECT RESULTS (BY AREA, SECTOR, AGENCY)");
        $sheet->setCellValue("A4", "As of ".$form5->period);
        $sheet->setCellValue("B6", "Implementing Agency: ".$agency->UACS_AGY_DSC);

        $sheet->setCellValue("B7", "(1)");
        $sheet->setCellValue("C7", "(2)");
        $sheet->setCellValue("D7", "(3)");
        $sheet->setCellValue("E7", "(4)");
        $sheet->setCellValue("F7", "(5)");
        $sheet->setCellValue("G7", "(6)");
        $sheet->setCellValue("H7", "(7)");
        // $sheet->setCellValue("I7", "(8)");
        $sheet->setCellValue("I7", "(9)");
        $sheet->setCellValue("J7", "(10)");
        $sheet->setCellValue("K7", "(11)");
        $sheet->setCellValue("L7", "(12)");
        $sheet->setCellValue("M7", "(13)");
        $sheet->setCellValue("N7", "(14)");
        $sheet->setCellValue("P7", "(15)");

        $sheet->setCellValue("B8", "(a) Name of Project");
        $sheet->setCellValue("B9", "(b) Funding Source");
        $sheet->setCellValue("B10", "(c) Project Schedule");
        $sheet->setCellValue("B11", "(d) Location");

        $sheet->setCellValue("D8", "Financial Status (in PhP M)");
        $sheet->setCellValue("N8", "Employment Generated");
        $sheet->setCellValue("P8", "Remarks");

        $sheet->setCellValue("C8", "Output Indicators");
        $sheet->setCellValue("D9", "Allocation");
        $sheet->setCellValue("E9", "Releases");
        $sheet->setCellValue("F9", "Obligations");
        $sheet->setCellValue("G9", "Expenditures");

        $sheet->setCellValue("D10", "As of Reporting Period");
        $sheet->setCellValue("E10", "As of Reporting Period");
        $sheet->setCellValue("F10", "As of Reporting Period");
        $sheet->setCellValue("G10", "As of Reporting Period");
        
        $sheet->setCellValue("H9", "Funding Support (%)");
        $sheet->setCellValue("I9", "Fund Utilization (%)");
        // $sheet->setCellValue("I9", "Weights");
        $sheet->setCellValue("J9", "Target to Date");
        $sheet->setCellValue("K9", "Actual Accomplishment to Date");
        $sheet->setCellValue("L9", "Slippage");
        $sheet->setCellValue("M9", "Performance");

        $sheet->setCellValue("N10", "(M)");
        $sheet->setCellValue("O10", "(F)");

        $i = 13;
        $num = 1;

        foreach($form5projs as $form5proj){
            $sheet->setCellValue("A".$i, $num);
            $sheet->setCellValue("B".$i, "(a) ".$form5proj->title); 
            $sheet->setCellValue("C".$i, $form5proj->oi); 
            $sheet->setCellValue("D".$i, $form5proj->allocation); 
            $sheet->setCellValue("E".$i, $form5proj->releases); 
            $sheet->setCellValue("F".$i, $form5proj->obligations); 
            $sheet->setCellValue("G".$i, $form5proj->expenditures); 
            if($form5proj->allocation == 0.00){
                $funds = "0%";
            }else{
                $funds = round(($form5proj->obligations/$form5proj->allocation)*100, 2);
            }
            $sheet->setCellValue("H".$i, $funds); 
            if($form5proj->releases == 0.00){
                $util = "0%";
            }else{
                $util = round(($form5proj->expenditures/$form5proj->releases)*100, 2);
            }
            $sheet->setCellValue("I".$i, $util); 
            // $sheet->setCellValue("I".$i, $form5proj->allocation/$form5projs_cost); 
            $sheet->setCellValue("J".$i, $form5proj->ttd);   
            $sheet->setCellValue("K".$i, $form5proj->atd); 
            if($form5proj->ttd == 0.00){
                $slip = "0";
            }else{
                $slip = round(($form5proj->atd-$form5proj->ttd), 2);
            }    
            $sheet->setCellValue("L".$i, $slip."%"); 
            if($form5proj->ttd == 0.00){
                $perf = "0";
            }else{
                $perf = round(($form5proj->atd/$form5proj->ttd)*100, 2);
            } 
            $sheet->setCellValue("M".$i, $perf."%"); 
            $sheet->setCellValue("N".$i, $form5proj->male); 
            $sheet->setCellValue("O".$i, $form5proj->female); 
            $sheet->setCellValue("P".$i, $form5proj->agency_remarks); 
            $first_i = $i;
            $i++;
            $sheet->setCellValue("B".$i, "(b) ".$form5proj->type); 
            $i++;
            $sheet->setCellValue("B".$i, "(c) ".$form5proj->start." - ".$form5proj->end); 
            $i++;

            $projlocs = \App\locations::where('proj_id', '=', $form5proj->id)->get();
            $loc_all = "";
            foreach($projlocs as $projloc){
                $loc_all .= $projloc->location;
            }
            $sheet->setCellValue("B".$i, "(d) ".$loc_all); 
            // $i++;
            // $sheet->setCellValue("B".$i, "(e) ".$form5proj->type); 
            $last_i = $i;
            $spreadsheet->getActiveSheet()->mergeCells("A$first_i:A$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("C$first_i:C$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("D$first_i:D$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("E$first_i:E$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("F$first_i:F$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("G$first_i:G$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("H$first_i:H$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("I$first_i:I$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("J$first_i:J$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("K$first_i:K$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("L$first_i:L$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("M$first_i:M$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("N$first_i:N$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("O$first_i:O$last_i");
            $spreadsheet->getActiveSheet()->mergeCells("P$first_i:Q$last_i");
            $i++;
            $num++;
        }

        $footer = $i + 2;
        $sheet->setCellValue("B".$footer, "Prepared by: ");
        $sheet->setCellValue("D".$footer_note = $footer, "Noted by: AGUSTIN C. MENDOZA");
        $sheet->setCellValue("B".$footer = $footer_note+1, "Designation: ");
        $sheet->setCellValue("D".$footer_head = $footer, "Regional Director");
        $sheet->setCellValue("B".$footer_date = $footer_head+1, "Date: ");

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $spreadsheet->getActiveSheet()->mergeCells('A2:Q2');
        $spreadsheet->getActiveSheet()->mergeCells('A3:Q3');
        $spreadsheet->getActiveSheet()->mergeCells('A4:Q4');
        $spreadsheet->getActiveSheet()->mergeCells('A5:Q5');
        $spreadsheet->getActiveSheet()->mergeCells('C8:C12');
        $spreadsheet->getActiveSheet()->mergeCells('D8:G8');
        $spreadsheet->getActiveSheet()->mergeCells('P8:Q12');
        $spreadsheet->getActiveSheet()->mergeCells('P7:Q7');
        $spreadsheet->getActiveSheet()->mergeCells('D10:D12');
        $spreadsheet->getActiveSheet()->mergeCells('E10:E12');
        $spreadsheet->getActiveSheet()->mergeCells('F10:F12');
        $spreadsheet->getActiveSheet()->mergeCells('G10:G12');
        $spreadsheet->getActiveSheet()->mergeCells('H9:H12');
        $spreadsheet->getActiveSheet()->mergeCells('I9:I12');
        $spreadsheet->getActiveSheet()->mergeCells('J9:J12');
        $spreadsheet->getActiveSheet()->mergeCells('K9:K12');
        $spreadsheet->getActiveSheet()->mergeCells('L9:L12');
        $spreadsheet->getActiveSheet()->mergeCells('M9:M12');
        $spreadsheet->getActiveSheet()->mergeCells('N7:O7');
        $spreadsheet->getActiveSheet()->mergeCells('N8:O9');
        $spreadsheet->getActiveSheet()->mergeCells('N10:N12');
        $spreadsheet->getActiveSheet()->mergeCells('O10:O12');
        $spreadsheet->getActiveSheet()->mergeCells('P10:P12');

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $styleArray_wrap = [
            'alignment' => [
                'wrapText' => true,
            ],
        ];

        $styleArray2 = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
        ],
        ];

        $styleArray3 = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '002060'],
            ],
            ],
        ];

        $styleArray_left = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],

        ];

        $styleArray4 = [

            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content_center = [
           'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content = [
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $spreadsheet->getActiveSheet()->getStyle('A7:Q12')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $spreadsheet->getActiveSheet()->getStyle('A1:Q5')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_head)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_note)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('A7:Q7')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('A6:Q12')->applyFromArray($styleArray_wrap);
        $spreadsheet->getActiveSheet()->getStyle('B8:B12')->applyFromArray($styleArray_left);
        $spreadsheet->getActiveSheet()->getStyle('C8:Q12')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle("A13:Q$i")->applyFromArray($styleArray_content);
        $spreadsheet->getActiveSheet()->getStyle("C13:Q$i")->applyFromArray($styleArray_content_center);
        // $spreadsheet->getActiveSheet()->getStyle('A9:D'.$i)->applyFromArray($styleArray3);


        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="exportform5.xlsx"');
        $writer->save("php://output");
    } 

    public function exportform6($id){
        $form6 = \App\Agency_form6::find($id);
        $form6projs = \App\Agency_form6s_project::leftJoin('projects','projects.id','=','agency_form6s_projects.project_id')->where('agency_form6s_id','=',$form6->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->select('projects.id', 'projects.title', 'agency_form6s_projects.releases', 'agency_form6s_projects.expenditures', 'agency_form6s_projects.ttd', 'agency_form6s_projects.atd', 'agency_form6s_projects.issues', 'agency_form6s_projects.source', 'agency_form6s_projects.action')->get();
        $agency = \App\Agencies::find($form6->agency_id);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue("K1", "RPMES FORM 6");
        $sheet->setCellValue("A2", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
        $sheet->setCellValue("A3", "REPORT ON THE STATUS OF PROJECTS ENCOUNTERING IMPLEMENTATION PROBLEMS");
        $sheet->setCellValue("A4", "As of ".$form6->period);
        $sheet->setCellValue("B6", "Implementing Agency: ".$agency->UACS_AGY_DSC);

        $sheet->setCellValue("B7", "Name of Project/Program");
        $sheet->setCellValue("C7", "Location");
        $sheet->setCellValue("D7", "Implementing Agency");
        $sheet->setCellValue("E7", "Funds Utilization");
        $sheet->setCellValue("F7", "Physical Status (%)");
        $sheet->setCellValue("F8", "Target");
        $sheet->setCellValue("G8", "Actual");
        $sheet->setCellValue("H8", "Slippage");
        $sheet->setCellValue("I7", "Issues");
        $sheet->setCellValue("J7", "Source of Information");
        $sheet->setCellValue("K7", "Action Taken/Recommendation");


        $i = 9;
        $num = 1;

        foreach($form6projs as $form5proj){
            $sheet->setCellValue("A".$i, $num);
            $sheet->setCellValue("B".$i, $form5proj->title); 
            if($form5proj->releases == 0.00){
                $funds = "0%";
            }else{
                $funds = round(($form5proj->expenditures/$form5proj->releases)*100, 2);
            }
            $projlocs = \App\locations::where('proj_id', '=', $form5proj->id)->get();

            $loc_all = "";
            foreach($projlocs as $projloc){
                $loc_all .= " ".$projloc->location;
            }
            $sheet->setCellValue("C".$i, $loc_all); 
            $sheet->setCellValue("D".$i, $agency->UACS_AGY_DSC);   
            $sheet->setCellValue("E".$i, $funds."%"); 
            if($form5proj->ttd == 0.00){
                $slip = "0%";
            }else{
                $slip = round(($form5proj->atd-$form5proj->ttd), 2);
            }    
            $sheet->setCellValue("H".$i, $slip."%"); 
            $sheet->setCellValue("F".$i, $form5proj->ttd); 
            $sheet->setCellValue("G".$i, $form5proj->atd); 
            $sheet->setCellValue("I".$i, $form5proj->issues); 
            $sheet->setCellValue("J".$i, $form5proj->source); 
            $sheet->setCellValue("K".$i, $form5proj->action); 
            $last_i = $i;
            // $spreadsheet->getActiveSheet()->mergeCells("A$first_i:A$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("C$first_i:C$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("D$first_i:D$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("E$first_i:E$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("F$first_i:F$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("G$first_i:G$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("H$first_i:H$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("I$first_i:I$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("J$first_i:J$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("K$first_i:K$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("L$first_i:L$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("M$first_i:M$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("N$first_i:N$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("O$first_i:O$last_i");
            // $spreadsheet->getActiveSheet()->mergeCells("P$first_i:Q$last_i");
            $i++;
            $num++;
        }

        $footer = $i + 2;
        $sheet->setCellValue("B".$footer, "Prepared by: ");
        $sheet->setCellValue("D".$footer_note = $footer, "Noted by: AGUSTIN C. MENDOZA");
        $sheet->setCellValue("B".$footer = $footer_note+1, "Designation: ");
        $sheet->setCellValue("D".$footer_head = $footer, "Regional Director");
        $sheet->setCellValue("B".$footer_date = $footer_head+1, "Date: ");

        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $spreadsheet->getActiveSheet()->mergeCells('A2:K2');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A4:K4');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->mergeCells('F7:H7');
        $spreadsheet->getActiveSheet()->mergeCells('A7:A8');
        $spreadsheet->getActiveSheet()->mergeCells('B7:B8');
        $spreadsheet->getActiveSheet()->mergeCells('C7:C8');
        $spreadsheet->getActiveSheet()->mergeCells('D7:D8');
        $spreadsheet->getActiveSheet()->mergeCells('E7:E8');
        $spreadsheet->getActiveSheet()->mergeCells('I7:I8');
        $spreadsheet->getActiveSheet()->mergeCells('J7:J8');
        $spreadsheet->getActiveSheet()->mergeCells('K7:K8');
        // $spreadsheet->getActiveSheet()->mergeCells('A7:A8');
        // $spreadsheet->getActiveSheet()->mergeCells('D10:D12');
        // $spreadsheet->getActiveSheet()->mergeCells('E10:E12');
        // $spreadsheet->getActiveSheet()->mergeCells('F10:F12');
        // $spreadsheet->getActiveSheet()->mergeCells('G9:G12');
        // $spreadsheet->getActiveSheet()->mergeCells('H9:H12');
        // $spreadsheet->getActiveSheet()->mergeCells('I9:I12');
        // $spreadsheet->getActiveSheet()->mergeCells('J9:J12');
        // $spreadsheet->getActiveSheet()->mergeCells('K9:K12');
        // $spreadsheet->getActiveSheet()->mergeCells('L9:L12');
        // $spreadsheet->getActiveSheet()->mergeCells('M9:M12');
        // $spreadsheet->getActiveSheet()->mergeCells('N8:O9');
        // $spreadsheet->getActiveSheet()->mergeCells('N10:N12');
        // $spreadsheet->getActiveSheet()->mergeCells('O10:O12');
        // $spreadsheet->getActiveSheet()->mergeCells('P10:P12');

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],
        ];

        $styleArray_wrap = [
            'alignment' => [
                'wrapText' => true,
            ],
        ];

        $styleArray2 = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
        ],
        ];

        $styleArray3 = [
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '002060'],
            ],
            ],
        ];

        $styleArray_left = [
            'font' => [
                'bold' => true,
                'color' => array('rgb' => 'FFFFFF'),
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'wrapText' => true,
            ],
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],

        ];

        $styleArray4 = [

            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content_center = [
           'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $styleArray_content = [
            'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                'color' => ['argb' => '000000'],
            ],
            ],
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'wrapText' => true,
            ],

        ];

        $spreadsheet->getActiveSheet()->getStyle('A7:K8')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $spreadsheet->getActiveSheet()->getStyle('A1:K5')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_head)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('C'.$footer_note)->applyFromArray($styleArray4);
        $spreadsheet->getActiveSheet()->getStyle('A7:K8')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('A6:K8')->applyFromArray($styleArray_wrap);
        $spreadsheet->getActiveSheet()->getStyle('B8:B1')->applyFromArray($styleArray_left);
        $spreadsheet->getActiveSheet()->getStyle('C8:K8')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle("A8:K$i")->applyFromArray($styleArray_content);
        $spreadsheet->getActiveSheet()->getStyle("C8:K$i")->applyFromArray($styleArray_content_center);
        // $spreadsheet->getActiveSheet()->getStyle('A9:D'.$i)->applyFromArray($styleArray3);


        $writer = new Xlsx($spreadsheet);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="exportform6.xlsx"');
        $writer->save("php://output");
    }


    public function previewform1agency($id){
        $id = decrypt($id);
        $form1 = \App\Agency_form1::find($id);
        $form1projs = \App\Agency_form1s_project::leftJoin('projects','projects.id','=','agency_form1s_projects.project_id')->where('agency_form1s_id','=',$form1->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->
            leftJoin('sectors','projects.sector','=','sectors.id')->get();
        $agency = \App\Agencies::find($form1->agency_id);

        return view('pages.previewform1agency', compact('form1', 'form1projs','agency'));  
    } 

    public function previewform2agency($id){
        $id = decrypt($id);
        $form2 = \App\Agency_form2::find($id);
        $form2projs = \App\Agency_form2s_project::leftJoin('projects','projects.id','=','agency_form2s_projects.project_id')->where('agency_form2s_id','=',$form2->id)->leftJoin('sources','projects.fundingsource','=','sources.id')->get();
        $agency = \App\Agencies::find($form2->agency_id);

        return view('pages.previewform2agency', compact('form2', 'form2projs','agency'));  
    } 

    public function previewform4agency($id){
        $id = decrypt($id);
        $form4 = \App\Agency_form4::find($id);
        $form4projs = \App\Agency_form4s_project::leftJoin('projects','projects.id','=','agency_form4s_projects.project_id')->where('agency_form4s_id','=',$form4->id)->get();
        $agency = \App\Agencies::find($form4->agency_id);

        return view('pages.previewform4agency', compact('form4', 'form4projs','agency')); 
    } 

    public function previewform3agency($id){
        $id = decrypt($id);
        $form3proj = \App\Agency_form3s_project::find($id);
        $form3proj_finding = $form3proj->findings;
        $form3proj_possible = $form3proj->possible;
        $form3proj_recommendations = $form3proj->recommendations;
        return view('pages.previewform3agency', compact('form3proj_finding','form3proj_possible','form3proj_recommendations')); 
    } 


    public function printonepager($id)
    {
        $id = decrypt($id);
        $sectors = \App\Sectors::all();
        $sources = \App\Sources::all();
        $agencies = \App\Agencies::all();
        $categories = \App\Category::all();
        $provinces = \App\Provinces::all();
        $projects = \App\Projects::find($id);
        $locations = \App\Locations::where('proj_id', $id)->get();
        $provincesprojects = \App\Provincesprojects::where('proj_id', $id)->get();

        if (auth()->user()->role_id == 2) {
            $myagency = auth()->user()->agency_id;
        }else{
            $myagency = "";
        }

        return view('pages.printonepager', compact('sectors','sources','agencies','projects','categories','locations','myagency','provinces','provincesprojects'));
    }

}
