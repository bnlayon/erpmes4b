<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Agencies;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $model)
    {
        //


        $this->authorize('manage-users', User::class);

        $agencies = \App\Agencies::orderBy('UACS_AGY_DSC', 'ASC')->get();
        return view('agencies.index', ['users' => $model->with('role')->get()], compact('agencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agencies = \App\Agencies::where("motheragency_id", "=", null)->orderBy('UACS_AGY_DSC', 'ASC')->get();
        return view('agencies.create', compact('agencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agency = new Agencies();
        $agency->UACS_AGY_DSC = $request->UACS_AGY_DSC;
        $agency->Abbreviation = $request->Abbreviation;
        $agency->head = $request->head;
        if ($request->subunit == 1) {
            $agency->motheragency_id = $request->motheragency_id;
        } else {
            $agency->motheragency_id = null;
        }
        $agency->save();
        return redirect()->route('agency.index')->withStatus(__('Agency successfully added.'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Agencies $agency)
    {
        //
        $agencies = \App\Agencies::where("motheragency_id", "=", null)->orderBy('UACS_AGY_DSC', 'ASC')->get();
        return view('agencies.edit', compact(['agencies'], ['agency']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agencies $agency)
    {
        //
        $agency->UACS_AGY_DSC = $request->UACS_AGY_DSC;
        $agency->Abbreviation = $request->Abbreviation;
        $agency->head = $request->head;
        if ($request->subunit == 1) {
            $agency->motheragency_id = $request->motheragency_id;
        } else {
            $agency->motheragency_id = null;
        }
        $agency->save();
        return redirect()->route('agency.index')->withStatus(__('Agency successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy(Agencies $agency)
    {
        $agency->delete();

        return redirect()->route('agency.index')->withStatus(__('Agency successfully deleted.'));
    }
}
