<?php
/*

=========================================================
* Argon Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-pro-laravel
* Copyright 2018 Creative Tim (https://www.creative-tim.com) & UPDIVISION (https://www.updivision.com)

* Coded by www.creative-tim.com & www.updivision.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
namespace App\Http\Controllers;



class HomeController extends Controller


{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {   
        if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3){
            $project_count = 0;
            $getlatestform2submission = \App\Form5::orderBy('period', 'DESC')->first();

        if(empty($getlatestform2submission)){
            $gettotalallocation = 0;
        }else{
            $getidsofforms2 = \App\Agency_form5::select('id')->where('period','=',$getlatestform2submission->period)->get();
            $gettotalallocation = \App\Agency_form5s_project::
            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->where('agency_form5s.period','=', $getlatestform2submission->period)
            ->whereNotNull('projects.implementingagency')
            ->sum('allocation');


            $project_count = \App\Agency_form5s_project::
            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->where('agency_form5s.period','=', $getlatestform2submission->period)
            ->whereNotNull('projects.implementingagency')
            ->count();
        }

        $project_count_male = \App\Agency_form5s_project::
            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->where('agency_form5s.period','=', $getlatestform2submission->period)
            ->whereNotNull('projects.implementingagency')
            ->sum('male');

        $project_count_female = \App\Agency_form5s_project::
            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->where('agency_form5s.period','=', $getlatestform2submission->period)
            ->whereNotNull('projects.implementingagency')
            ->sum('female');

        // $project_count_male = \App\Agency_form2s_project::leftJoin('projects','agency_form2s_projects.project_id','=','projects.id')->sum('male');
        // $project_count_female = \App\Agency_form2s_project::leftJoin('projects','agency_form2s_projects.project_id','=','projects.id')->sum('female');

        // $agencies = \App\Agencies::get();
        $agencies = \App\Agencies::leftJoin('agency_form5s', 'agencies.id', '=', 'agency_form5s.agency_id')->where('agency_form5s.period', \App\Form5::max('period'))->select('agencies.*')->get();

        }elseif (auth()->user()->role_id == 2) {
            $getlatestform2submission = \App\Agency_form2::where('agency_id','=',auth()->user()->agency_id)->orderBy('id', 'DESC')->first();
            // $agencies = \App\Agencies::get();
            $agencies = \App\Agencies::leftJoin('agency_form5s', 'agencies.id', '=', 'agency_form5s.agency_id')->where('agency_form5s.period', \App\Form5::max('period'))->select('agencies.*')->get();
            //PULL SAMPLE
            if(empty($getlatestform2submission)){
                $gettotalallocation = 0;
            }else{
                $gettotalallocation = \App\Agency_form2s_project::where('agency_form2s_id','=',$getlatestform2submission->id)->sum('alloc_asof');
                // dd($getlatestform2submission->id);
            }
        
        
        $project_count = \App\Projects::where('implementingagency','=',auth()->user()->agency_id)->count();
        // $project_cost = \App\Projects::where('implementingagency','=',auth()->user()->agency_id)->sum('cost');
        $project_count_male = \App\Agency_form2s_project::leftJoin('projects','agency_form2s_projects.project_id','=','projects.id')->where('implementingagency','=',auth()->user()->agency_id)->sum('male');
        $project_count_female = \App\Agency_form2s_project::leftJoin('projects','agency_form2s_projects.project_id','=','projects.id')->where('implementingagency','=',auth()->user()->agency_id)->sum('female');
        }

        return view('pages.dashboard', compact('project_count','project_count_male','project_count_female','agencies','gettotalallocation'));
    }

    public function logs()
    {           
        $logs = \App\Logs::get();
        return view('pages.logs', compact('logs'));
    }

    public function manual()
    {           
        return view('pages.manual');
    }

    public function usermanual()
    {           
        return view('pages.usermanual');
    }

    public function about()
    {           
   if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3){
            $reportsadmin = \App\Reports::where('reporttype', '=', 1)->get();
        }elseif(auth()->user()->role_id == 2){
            $reportsadmin = \App\Reports::where('reporttype', '=', 2)->get();
        }else{

        }
        return view('pages.about', compact('reportsadmin'));
    }
}
