<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportGenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  
    public function reportgen(){
        if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3){
            $reportsadmin = \App\Reports::where('reporttype', '=', 1)->where('active', '=', 1)->get();
            $categories = \App\Category::all();
            $spatial = \App\Provinces::all();
            $sectors = \App\Sectors::all();
            $agencies = \App\Agencies::all();
            $sources = \App\Sources::all();
            $form5s = \App\Form5::all();
        }elseif(auth()->user()->role_id == 2){
            $reportsadmin = \App\Reports::where('reporttype', '=', 2)->where('active', '=', 1)->get();
            $categories = \App\Category::all();
            $spatial = \App\Provinces::all();
            $sectors = \App\Sectors::all();
            $agencies = \App\Agencies::all();
            $sources = \App\Sources::all();
            $form5s = \App\Form5::all();
        }else{

        }
        
        return view('pages.reportgen', compact('reportsadmin', 'categories','spatial','sectors','agencies', 'sources', 'form5s'));  
    } 

    public function generatereport(){
        if(request('reporttype') == 1){
            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')->leftJoin('sectors','sectors.id','=','projects.sector')->leftJoin('sources','sources.id','=','projects.fundingsource')->leftJoin('categories','categories.id','=','projects.category')->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')->get();
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlist.xlsx"');
            $writer->save("php://output");
            return back();
        }elseif(request('reporttype') == 2) {
            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')->leftJoin('sectors','sectors.id','=','projects.sector')->leftJoin('sources','sources.id','=','projects.fundingsource')->leftJoin('categories','categories.id','=','projects.category')->where('projects.implementingagency','=',auth()->user()->agency_id)->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')->get();
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlist.xlsx"');
            $writer->save("php://output");
            return back();
        }elseif(request('reporttype') == 3) {
            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')->leftJoin('sectors','sectors.id','=','projects.sector')->leftJoin('sources','sources.id','=','projects.fundingsource')->leftJoin('categories','categories.id','=','projects.category')->where('projects.implementingagency','=',auth()->user()->agency_id)->where('categories.id', '=', request('categoryrep'))->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')->get();

            $category = \App\Category::find(request('categoryrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY CATEGORY "."(".$category->category.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistcategory.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 4) {
            if(request('spatialrep') == 3){
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 3)->get();

            }elseif (request('spatialrep') == 1) {
               $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 1)->get();

            }elseif (request('spatialrep') == 2) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 2)->get();

            }elseif (request('spatialrep') == 5) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 5)->get();

            }elseif (request('spatialrep') == 4) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 4)->get();

            }else{

            }

            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')->leftJoin('sectors','sectors.id','=','projects.sector')->leftJoin('sources','sources.id','=','projects.fundingsource')->leftJoin('categories','categories.id','=','projects.category')->where('projects.implementingagency','=',auth()->user()->agency_id)->whereIn('projects.id', $provinceprojectsids)->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')->get();

            $province = \App\Provinces::find(request('spatialrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY SPATIAL COVERAGE "."(".$province->province.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistprov.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 5) {
            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')->leftJoin('sectors','sectors.id','=','projects.sector')->leftJoin('sources','sources.id','=','projects.fundingsource')->leftJoin('categories','categories.id','=','projects.category')->where('projects.implementingagency','=',auth()->user()->agency_id)->where('projects.sector', '=', request('sectorrep'))->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')->get();

            $sector = \App\Sectors::find(request('sectorrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY SECTOR "."(".$sector->sector.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistsector.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 6) {

                $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')
                ->leftJoin('sectors','sectors.id','=','projects.sector')
                ->leftJoin('sources','sources.id','=','projects.fundingsource')
                ->leftJoin('categories','categories.id','=','projects.category')
                ->where('categories.id', '=', request('categoryrep'))
                ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type',
                'projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category',
                'projects.odatype','projects.others','projects.gaayear', 'projects.rrp', 'projects.lfptype')
                ->get();

            $category = \App\Category::find(request('categoryrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY CATEGORY "."(".$category->category.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);

            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistcategory.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 7 ) {
            if(request('spatialrep') == 3){
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 3)->get();

            }elseif (request('spatialrep') == 1) {
               $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 1)->get();

            }elseif (request('spatialrep') == 2) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 2)->get();

            }elseif (request('spatialrep') == 5) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 5)->get();

            }elseif (request('spatialrep') == 4) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 4)->get();

            }else{

            }
           
                $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')
                ->leftJoin('sectors','sectors.id','=','projects.sector')
                ->leftJoin('sources','sources.id','=','projects.fundingsource')
                ->leftJoin('categories','categories.id','=','projects.category')
                ->whereIn('projects.id', $provinceprojectsids)
                ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status',
                'projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')
                ->get();

            $province = \App\Provinces::find(request('spatialrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY SPATIAL COVERAGE "."(".$province->province.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistprov.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 8) {
           
            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')
            ->leftJoin('sectors','sectors.id','=','projects.sector')
            ->leftJoin('sources','sources.id','=','projects.fundingsource')
            >leftJoin('categories','categories.id','=','projects.category')
            ->where('projects.sector', '=', request('sectorrep'))
            ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode',
            'projects.start','projects.end','projects.status','projects.cost','categories.category',
            'projects.odatype','projects.others','projects.gaayear', 'projects.lfptype')->get();
       
            $sector = \App\Sectors::find(request('sectorrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY SECTOR "."(".$sector->sector.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistsector.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 9) {

            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')
            ->leftJoin('sectors','sectors.id','=','projects.sector')
            ->leftJoin('sources','sources.id','=','projects.fundingsource')
            ->leftJoin('categories','categories.id','=','projects.category')
            ->where('projects.implementingagency', '=', request('agencyrep1'))
            ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost',
            'categories.category','projects.odatype','projects.others','projects.gaayear', 'projects.lfptype')
            ->get();

            $agency = \App\Agencies::find(request('agencyrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            if (request('reporttype') == 9) { $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY AGENCY "."(".$agency->UACS_AGY_DSC.")"); }
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);

            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistagency.xlsx"');
            $writer->save("php://output");
            return back();  
        }
        elseif(request('reporttype') == 10){
                // $temp_form5 = \App\Agency_form5::where('period', \App\Agency_form5::max('period'))->orderBy('period','desc')->pluck('id');
                // $latest_form5 = \App\Agency_form5s_project::whereIn('agency_form5s_id', $temp_form5)->get();               
                

                // $latest_form5 = \App\Agency_form5s_project::leftJoin('agency_form5s', 'agency_form5s.id', '=', 'agency_form5s_projects.agency_form5s_id')
                // ->select('agency_form5s_projects.project_id', 'agency_form5s.period')
                // ->whereIn('agency_form5s.period', request('period'))
                // ->whereIn('agency_form5s.agency_id', request('agencyrep'))
                // ->orderBy('agency_form5s.period','desc')
                // ->pluck('agency_form5s_projects.project_id');              

                // $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')
                // ->leftJoin('sectors','sectors.id','=','projects.sector')
                // ->leftJoin('sources','sources.id','=','projects.fundingsource')
                // ->leftJoin('categories','categories.id','=','projects.category')
                // ->where('categories.id', '=', request('categoryrep'))
                // ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end',
                // 'projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear', 'projects.rrp')
                // ->whereIn('projects.id', $latest_form5)
                // ->get();

                $projs = \App\Agency_form5::leftJoin('agency_form5s_projects','agency_form5s.id','=','agency_form5s_projects.agency_form5s_id')
                ->join('projects','projects.id','=','agency_form5s_projects.project_id')
                ->join('agencies','projects.implementingagency','=','agencies.id')
                ->join('sectors','sectors.id','=','projects.sector')
                ->join('sources','sources.id','=','projects.fundingsource')
                ->join('categories','categories.id','=','projects.category')
                ->whereIn('agency_form5s.period', request('period'))
                ->whereIn('agency_form5s.agency_id', request('agencyrep'))
                ->where('categories.id', '=', request('categoryrep'))
                ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end',
                'projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear', 'projects.rrp', 'projects.lfptype',
                'agency_form5s.period', 'agency_form5s_projects.atd')
                ->orderBy('agency_form5s.period','desc')
                ->get();

            $category = \App\Category::find(request('categoryrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY CATEGORY "."(".$category->category.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:N2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:N4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:N5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "ACTUAL ACCOMPLISHMENT");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "SPECIFICS");
            $sheet->setCellValue("N6", "as of");
            // $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            // $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);

                if($proj->type == 'Official Development Assistance'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->odatype);   
                }elseif($proj->type == 'Local Financing'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->lfptype);
                }elseif($proj->type == 'Others'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->others);
                }else{
                    $sheet->setCellValue("E".$i, $proj->type);
                }                

                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->atd);
                $sheet->setCellValue("K".$i, number_format($proj->cost, 2));
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->rrp);
                $sheet->setCellValue("N".$i, $proj->period);
                // $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:N$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistcategory.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 11) {
            if(request('spatialrep') == 3){
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 3)->get();

            }elseif (request('spatialrep') == 1) {
               $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 1)->get();

            }elseif (request('spatialrep') == 2) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 2)->get();

            }elseif (request('spatialrep') == 5) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 5)->get();

            }elseif (request('spatialrep') == 4) {
                $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 4)->get();

            }else{

            }            
                // $latest_form5 = \App\Agency_form5s_project::leftJoin('agency_form5s', 'agency_form5s.id', '=', 'agency_form5s_projects.agency_form5s_id')
                // ->select('agency_form5s_projects.project_id', 'agency_form5s.period')
                // ->where('agency_form5s.period', \App\Form5::max('period'))
                // ->orderBy('agency_form5s.period','desc')
                // ->pluck('agency_form5s_projects.project_id');

                // $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')
                // ->leftJoin('sectors','sectors.id','=','projects.sector')
                // ->leftJoin('sources','sources.id','=','projects.fundingsource')
                // ->leftJoin('categories','categories.id','=','projects.category')
                // ->whereIn('projects.id', $provinceprojectsids)
                // ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost',
                // 'categories.category','projects.odatype','projects.others','projects.gaayear')->whereIn('projects.id', $latest_form5)->get();

                $projs = \App\Agency_form5::leftJoin('agency_form5s_projects','agency_form5s.id','=','agency_form5s_projects.agency_form5s_id')
                ->join('projects','projects.id','=','agency_form5s_projects.project_id')
                ->join('agencies','projects.implementingagency','=','agencies.id')
                ->join('sectors','sectors.id','=','projects.sector')
                ->join('sources','sources.id','=','projects.fundingsource')
                ->join('categories','categories.id','=','projects.category')
                ->whereIn('agency_form5s.period', request('period'))
                ->whereIn('agency_form5s.agency_id', request('agencyrep'))
                ->whereIn('agency_form5s_projects.project_id', $provinceprojectsids)
                ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end',
                'projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear', 'projects.rrp', 'projects.lfptype',
                'agency_form5s.period', 'agency_form5s_projects.atd')
                ->orderBy('agency_form5s.period','desc')
                ->get();
                       

            $province = \App\Provinces::find(request('spatialrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY SPATIAL COVERAGE "."(".$province->province.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:N2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:N4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:N5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "ACTUAL ACCOMPLISHMENT");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "SPECIFICS");
            $sheet->setCellValue("N6", "as of");
            // $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            // $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);

                if($proj->type == 'Official Development Assistance'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->odatype);   
                }elseif($proj->type == 'Local Financing'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->lfptype);
                }elseif($proj->type == 'Others'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->others);
                }else{
                    $sheet->setCellValue("E".$i, $proj->type);
                }                

                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->atd);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->rrp);
                $sheet->setCellValue("N".$i, $proj->period);
                // $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:N$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistprov.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 12) {              
                $projs = \App\Agency_form5::leftJoin('agency_form5s_projects','agency_form5s.id','=','agency_form5s_projects.agency_form5s_id')
                ->join('projects','projects.id','=','agency_form5s_projects.project_id')
                ->join('agencies','projects.implementingagency','=','agencies.id')
                ->join('sectors','sectors.id','=','projects.sector')
                ->join('sources','sources.id','=','projects.fundingsource')
                ->join('categories','categories.id','=','projects.category')
                ->whereIn('agency_form5s.period', request('period'))
                ->whereIn('agency_form5s.agency_id', request('agencyrep'))
                ->where('projects.sector', '=', request('sectorrep'))
                ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end',
                'projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear', 'projects.rrp', 'projects.lfptype',
                'agency_form5s.period', 'agency_form5s_projects.atd')
                ->orderBy('agency_form5s.period','desc')
                ->get();

            $sector = \App\Sectors::find(request('sectorrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY SECTOR "."(".$sector->sector.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:N2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:N4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:N5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "ACTUAL ACCOMPLISHMENT");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "SPECIFICS");
            $sheet->setCellValue("N6", "as of");
            // $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            // $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);

                if($proj->type == 'Official Development Assistance'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->odatype);   
                }elseif($proj->type == 'Local Financing'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->lfptype);
                }elseif($proj->type == 'Others'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->others);
                }else{
                    $sheet->setCellValue("E".$i, $proj->type);
                }                

                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->atd);
                $sheet->setCellValue("K".$i, number_format($proj->cost, 2));
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->rrp);
                $sheet->setCellValue("N".$i, $proj->period);
                // $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:N$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistsector.xlsx"');
            $writer->save("php://output");
            return back();  

        }elseif(request('reporttype') == 13) {
            
                $projs = \App\Agency_form5::leftJoin('agency_form5s_projects','agency_form5s.id','=','agency_form5s_projects.agency_form5s_id')
                ->join('projects','projects.id','=','agency_form5s_projects.project_id')
                ->join('agencies','projects.implementingagency','=','agencies.id')
                ->join('sectors','sectors.id','=','projects.sector')
                ->join('sources','sources.id','=','projects.fundingsource')
                ->join('categories','categories.id','=','projects.category')
                ->whereIn('agency_form5s.period', request('period'))
                ->whereIn('agency_form5s.agency_id', request('agencyrep'))
                ->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end',
                'projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear', 'projects.rrp', 'projects.lfptype',
                'agency_form5s.period', 'agency_form5s_projects.atd')
                ->orderBy('agency_form5s.period','desc')
                ->get();
                        
            $agency = \App\Agencies::find(request('agencyrep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            if (request('reporttype') == 9) { $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY AGENCY "."(".$agency->UACS_AGY_DSC.")"); }
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:N2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:N4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:N5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "ACTUAL ACCOMPLISHMENT");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "SPECIFICS");
            $sheet->setCellValue("N6", "as of");
            // $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            // $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);

                if($proj->type == 'Official Development Assistance'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->odatype);   
                }elseif($proj->type == 'Local Financing'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->lfptype);
                }elseif($proj->type == 'Others'){
                    $sheet->setCellValue("E".$i, $proj->type .' / '. $proj->others);
                }else{
                    $sheet->setCellValue("E".$i, $proj->type);
                }                

                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->atd);
                $sheet->setCellValue("K".$i, number_format($proj->cost, 2));
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->rrp);
                $sheet->setCellValue("N".$i, $proj->period);
                // $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:N6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:N$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistagency.xlsx"');
            $writer->save("php://output");
            return back();  
        }elseif(request('reporttype') == 14) {
            $latest_form5 = \App\Agency_form5s_project::leftJoin('agency_form5s', 'agency_form5s.id', '=', 'agency_form5s_projects.agency_form5s_id')->select('agency_form5s_projects.project_id', 'agency_form5s.period')->where('agency_form5s.period', \App\Form5::max('period'))->orderBy('agency_form5s.period','desc')->pluck('agency_form5s_projects.project_id');
            $projs = \App\Projects::join('agencies','projects.implementingagency','=','agencies.id')->leftJoin('sectors','sectors.id','=','projects.sector')->leftJoin('sources','sources.id','=','projects.fundingsource')->leftJoin('categories','categories.id','=','projects.category')->where('projects.fundingsource', '=', request('fsourcerep'))->select('projects.id','projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type','projects.mode','projects.start','projects.end','projects.status','projects.cost','categories.category','projects.odatype','projects.others','projects.gaayear')->whereIn('projects.id', $latest_form5)->get();
            
            $fundingsource = \App\Sources::find(request('fsourcerep'));
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue("A2", "NATIONAL ECONOMIC AND DEVELOPMENT AUTHORITY");
            $sheet->setCellValue("A3", "REGIONAL PROJECT MONITORING AND EVALUATION SYSTEM (RPMES)");
            $sheet->setCellValue("A4", "MASTERLIST OF PROJECTS BY FUNDING SOURCE "."(".$fundingsource->type.")");
            $datenow = date("Y-m-d");
            $sheet->setCellValue("A5", "As of ".$datenow);
            $spreadsheet->getActiveSheet()->mergeCells('A2:O2');
            $spreadsheet->getActiveSheet()->mergeCells('A3:O3');
            $spreadsheet->getActiveSheet()->mergeCells('A4:O4');
            $spreadsheet->getActiveSheet()->mergeCells('A5:O5');

            $sheet->setCellValue("B6", "PROJECT TITLE");
            $sheet->setCellValue("C6", "IMPLEMENTING AGENCY");
            $sheet->setCellValue("D6", "SECTOR");
            $sheet->setCellValue("E6", "FUNDING SOURCE");
            $sheet->setCellValue("F6", "MODE OF IMPLEMENTATION");
            $sheet->setCellValue("G6", "START DATE");
            $sheet->setCellValue("H6", "END DATE");
            $sheet->setCellValue("I6", "LOCATION");
            $sheet->setCellValue("J6", "STATUS");
            $sheet->setCellValue("K6", "COST");
            $sheet->setCellValue("L6", "CATEGORY");
            $sheet->setCellValue("M6", "ODA TYPE (if ODA)");
            $sheet->setCellValue("N6", "OTHER FUNDING SOURCE (if Others)");
            $sheet->setCellValue("O6", "GAA YEAR (if GAA)");

            $num = 1;
            $i = 7;
            foreach($projs as $proj){
                $sheet->setCellValue("A".$i, $num);
                $sheet->setCellValue("B".$i, $proj->title);
                $sheet->setCellValue("C".$i, $proj->UACS_AGY_DSC);
                $sheet->setCellValue("D".$i, $proj->sector);
                $sheet->setCellValue("E".$i, $proj->type);
                $sheet->setCellValue("F".$i, $proj->mode);
                $sheet->setCellValue("G".$i, $proj->start);
                $sheet->setCellValue("H".$i, $proj->end);

                $projlocs = \App\locations::where('proj_id', '=', $proj->id)->get();
                $loc_all = "";
                foreach($projlocs as $projloc){
                    $loc_all .= $projloc->location;
                }

                //$sheet->setCellValue("I".$i, "LOCATION");
                $sheet->setCellValue("I".$i, $loc_all);
                $sheet->setCellValue("J".$i, $proj->status);
                $sheet->setCellValue("K".$i, $proj->cost);
                $sheet->setCellValue("L".$i, $proj->category);
                $sheet->setCellValue("M".$i, $proj->odatype);
                $sheet->setCellValue("N".$i, $proj->others);
                $sheet->setCellValue("O".$i, $proj->gaayear);
                $i++;
                $num++;
            }

             $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
            ];

            $styleArray2 = [
                'font' => [
                    'bold' => true,
                    'color' => array('rgb' => 'FFFFFF'),
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'color' => ['argb' => '000000'],
                ],
            ],
            ];

            $styleArray3 = [
                'alignment' => [
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '002060'],
                ],
            ],
            ];
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial Narrow');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $spreadsheet->getActiveSheet()->getStyle('A1:C5')->applyFromArray($styleArray);
            $spreadsheet->getActiveSheet()->getStyle('A6:O6')->applyFromArray($styleArray2);
            $spreadsheet->getActiveSheet()->getStyle("A7:O$i")->applyFromArray($styleArray3);


            $writer = new Xlsx($spreadsheet);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="masterlistfundingsource.xlsx"');
            $writer->save("php://output");
            return back();  
        }
    }
}
