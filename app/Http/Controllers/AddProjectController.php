<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;

class AddProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addproject()
    {

        if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3){
            $projects = \App\Projects::join('agencies', 'projects.implementingagency', '=', 'agencies.id')
            ->leftJoin('sectors', 'projects.sector','=', 'sectors.id')
            ->leftJoin('sources', 'projects.fundingsource','=', 'sources.id')
            ->select('projects.id', 'projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type', 'projects.mode', 'projects.start', 'projects.end', 'projects.project_id', 'projects.modified_by', 'projects.updated_at')
            ->get();
            return view('pages.addproject', compact('projects'));    
        }elseif (auth()->user()->role_id == 2) {
            $projects = \App\Projects::join('agencies', 'projects.implementingagency', '=', 'agencies.id')
            ->leftJoin('sectors', 'projects.sector','=', 'sectors.id')
            ->leftJoin('sources', 'projects.fundingsource','=', 'sources.id')
            ->select('projects.id', 'projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type', 'projects.mode', 'projects.start', 'projects.end', 'projects.project_id', 'projects.modified_by', 'projects.updated_at')
            ->where('projects.implementingagency','=',auth()->user()->agency_id)
            ->get();
            return view('pages.addproject', compact('projects'));  
        }else{

        }
    }

    public function addpage()
    {
        $sectors = \App\Sectors::all();
        $subsectors = \App\Subsectors::all();
        $sources = \App\Sources::all();
        $provinces = \App\Provinces::all();
        $agencies = \App\Agencies::orderBy('UACS_AGY_DSC', 'ASC')->get();
        $categories = \App\Category::all();
        if (auth()->user()->role_id == 2) {
            $myagency = auth()->user()->agency_id;
        }else{
            $myagency = "";
        }


        $classifications = \App\Classification::all();
        $nccl = \App\Classification::select('subsector_id')->distinct()->pluck('subsector_id');

        $ncsubsectors = \App\Subsectors::whereNotIn('id', $nccl)->get();
        $nsubsectors = \App\Subsectors::whereIn('id', $nccl)->get();
        return view('pages.addpage', compact('sectors','sources','agencies','myagency','categories','provinces','subsectors', 'ncsubsectors', 'nsubsectors', 'classifications'));
    }

    public function saveproject()
    {
        $project                     = new \App\Projects();
        $project->title              = request('title');
        $project->implementingagency = request('implementingagency');
        $project->sector             = request('sector');
        // $project->subsector          = request('subsector'.'_'.$project->sector);
        $project->subsector          = request('subsector');
        $project->classification     = request('classification');
        $project->category           = request('category');
        $project->fundingsource      = request('fundingsource');
        $project->mode               = request('mode');
        $project->start              = request('start');
        $project->end                = request('end');
        $project->status             = request('status');
        $project->cost               = request('cost');


        if (request('fundingsource') != 1) {
            $odatype                 = null;
        } else {
            $odatype                 = request('odatype');
        }

        
        if (request('fundingsource') != 4) {
            $others                  = null;
        } else {
            $others                  = request('others');
        }

        //$project->odatype            = request('odatype');
        //$project->others             = request('others');
        $project->odatype            = $odatype;
        $project->others             = $others;
        $project->gaayear            = request('gaayear');
        $project->otherslfp          = request('otherslfp');
        $project->lfptype            = request('lfptype');
        $project->rrp                = request('rrp');
        $project->catothers          = request('catothers');
        $project->contractor         = request('contractor');
        $project->modeothers         = request('modeothers');
        $project->modified_by        = auth()->user()->name;


        if (request('gaayear') == null) {
            if ($project->fundingsource == 1) {
                $gaayear = '0DAF';
            } else if ($project->fundingsource == 3) {
                $gaayear = 'PPPF';
            } else if ($project->fundingsource == 4) {
                $gaayear = 'OTH';
            } else {
                if ($project->lfptype == 'Corporate') {
                    $gaayear = 'CORP';
                } else if ($project->lfptype == '20% Development Fund') {
                    $gaayear = '20DF';
                } else if ($project->lfptype == 'Fund 164') {
                    $gaayear = 'F164';
                } else {
                    $gaayear = 'OTH';
                }
            }
        } else {
            $gaayear = request('gaayear');
        }
        $agency = \App\Agencies::find(request('implementingagency'));
        $t_project_id = $agency->Abbreviation . '-' . $gaayear;
        $t_project = \App\Projects::where('project_id', 'LIKE', $t_project_id . '%')->orderBy('id', 'DESC')->first();

        if ($t_project == null || $t_project->project_id == null) {
            $project_id = $t_project_id . '-' . '0001';
        } else {
            $t_ctr = explode('-', $t_project->project_id);
            $ctr = $t_ctr[count($t_ctr) - 1];
            $ctr++;
            $project_id = $t_project_id . '-' . str_pad($ctr, 4, '0', STR_PAD_LEFT);
        }
        $project->project_id = $project_id;

        $project->save();
        alert()->success($project->title, 'Project Saved!');

        $locations_to_saves = request('location');
        $long_to_saves = request('longitude');
        $la_to_saves = request('latitude');

        if(!empty($locations_to_saves)){
            $count_ids = count(request('location'));
            for($i = 0; $i < $count_ids; $i++){
                $project_loc = new \App\Locations();
                $project_loc->proj_id = $project->id;
                $project_loc->location = $locations_to_saves[$i];
                $project_loc->longitude = $long_to_saves[$i];
                $project_loc->latitude = $la_to_saves[$i];
                $project_loc->save();
            } 
        }

        $provinces_to_saves = request('province');

        if(!empty($provinces_to_saves)){
            $count_ids = count(request('province'));
            for($i = 0; $i < $count_ids; $i++){
                $project_loc = new \App\Provincesprojects();
                $project_loc->proj_id = $project->id;
                $project_loc->province_id = $provinces_to_saves[$i];
                $project_loc->save();
            } 
        }

        $logs                        = new \App\Logs();
        $logs->log                   = "Added Project: ".$project->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back();
    }

    public function editpage($id)
    {
        $id = decrypt($id);
        $sectors = \App\Sectors::all();
        $sources = \App\Sources::all();
        $agencies = \App\Agencies::all();
        $subsectors = \App\Subsectors::all();
        $categories = \App\Category::all();
        $provinces = \App\Provinces::all();
        $projects = \App\Projects::find($id);
        $locations = \App\Locations::where('proj_id', $id)->get();
        $provincesprojects = \App\Provincesprojects::where('proj_id', $id)->get();

        if (auth()->user()->role_id == 2) {
            $myagency = auth()->user()->agency_id;
        }else{
            $myagency = "";
        }

        $classifications = \App\Classification::all();
        $nccl = \App\Classification::select('subsector_id')->distinct()->pluck('subsector_id');

        $ncsubsectors = \App\Subsectors::whereNotIn('id', $nccl)->get();
        $nsubsectors = \App\Subsectors::whereIn('id', $nccl)->get();
        return view('pages.editpage', compact('sectors','sources','agencies','projects','categories','locations','myagency','provinces','provincesprojects','subsectors', 'ncsubsectors', 'nsubsectors', 'classifications'));
    }

    public function editsave(Request $request, $id)
    {
        $project                     = \App\Projects::find($id);
        $project->title              = request('title');
        // $project->location           = request('location');
        $project->implementingagency = request('implementingagency');
        $project->sector             = request('sector');
        // $project->subsector          = request('subsector'.'_'.$project->sector);
        $project->subsector          = request('subsector');
        $project->classification     = request('classification');
        $project->category           = request('category');
        $project->fundingsource      = request('fundingsource');
        $project->mode               = request('mode');
        $project->start              = request('start');
        $project->end                = request('end');
        $project->status             = request('status');
        $project->cost               = request('cost');

        if (request('fundingsource') != 1) {
            $odatype                 = null;
        } else {
            $odatype                 = request('odatype');
        }

        
        if (request('fundingsource') != 4) {
            $others                  = null;
        } else {
            $others                  = request('others');
        }

        //$project->odatype            = request('odatype');
        //$project->others             = request('others');
        $project->odatype            = $odatype;
        $project->others             = $others;
        $project->gaayear            = request('gaayear');
        $project->otherslfp          = request('otherslfp');
        $project->lfptype            = request('lfptype');
        $project->rrp                = request('rrp');
        $project->catothers          = request('catothers');
        $project->contractor         = request('contractor');
        $project->modeothers         = request('modeothers');
        $project->modified_by        = auth()->user()->name;
        $project->save();

        $locations = request('location');
        $long = request('longitude');
        $la = request('latitude');
        $locations_ids = request('locationid');

        $locationprojects = \App\Locations::where('proj_id', $id)->get();
        if(!empty($locations)){
            foreach($locationprojects as $check){
                if(in_array($check->id, $locations_ids)){

                }else{
                    $user = \App\Locations::find($check->id);
                    $user->delete();
                }
            }
        }else{
            \App\Locations::where('proj_id', '=', $id)->delete();
        }

        if(!empty($locations)){
            $count_ids = count(request('location'));
      
            for($i = 0; $i < $count_ids; $i++){
                if(!empty($locations_ids[$i])){
                $project_locations = \App\Locations::find($locations_ids[$i]);
                $project_locations->location = $locations[$i];
                $project_locations->longitude = $long[$i];
                $project_locations->latitude = $la[$i];
                $project_locations->save();     
                }else{
                $project_locations = new \App\Locations();
                $project_locations->proj_id = $project->id;
                $project_locations->location = $locations[$i];
                $project_locations->longitude = $long[$i];
                $project_locations->latitude = $la[$i];
                $project_locations->save();
                }
            } 
        }

        $provinces = array();
            $provinces_to_save = request('province');
            if(!is_null($provinces_to_save)){
                foreach ($provinces_to_save as $province) {

                    if($province!=0)
                        $provinces[] = $province;
                }
            }
        $project->projects_provinces()->sync($provinces);
        $project->save();

        $logs                        = new \App\Logs();
        $logs->log                   = "Edited Project: ".$project->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        alert()->success($project->title, 'Project Saved!');
        return back();
    }

    public function spatial()
    {
        if(auth()->user()->agency_id == 47){
            $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 3)->get();
            // dd($provinceprojectsids);
        }elseif (auth()->user()->agency_id == 48) {
           $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 1)->get();
        }elseif (auth()->user()->agency_id == 49) {
            $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 2)->get();
        }elseif (auth()->user()->agency_id == 50) {
            $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 5)->get();
        }elseif (auth()->user()->agency_id == 51) {
            $provinceprojectsids = \App\Provincesprojects::select('proj_id')->where('province_id', 4)->get();
        }else{

        }
        
            $projects = \App\Projects::join('agencies', 'projects.implementingagency', '=', 'agencies.id')
            ->leftJoin('sectors', 'projects.sector','=', 'sectors.id')
            ->leftJoin('sources', 'projects.fundingsource','=', 'sources.id')
            ->select('projects.id', 'projects.title','agencies.UACS_AGY_DSC', 'sectors.sector', 'sources.type', 'projects.mode', 'projects.start', 'projects.end', 'projects.status')
            ->whereIn('projects.id', $provinceprojectsids)
            ->get();
            return view('pages.spatial', compact('projects'));  

    }

    public function deleteproject($id)
    {
        $id = decrypt($id);
        $project                     = \App\Projects::find($id);
        $project->delete();
    
        $logs                        = new \App\Logs();
        $logs->log                   = "Deleted Project: ".$project->title;
        $logs->username              = auth()->user()->name;
        $logs->save();

        return back()->withStatus(__('Project successfully deleted.'));
    }
}
