<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form3 extends Model
{
    public function getForm3Submission($id){
        $agency_id = auth()->user()->agency_id;
        $checkSubmission = \App\Agency_form3::where('period', $id)->where('agency_id',$agency_id)->get();
        return $checkSubmission;
    }

}
