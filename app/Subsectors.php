<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsectors extends Model
{
    public function sectors()
    {
        return $this->belongsTo(Sectors::class, 'sector_id');
    }
}
