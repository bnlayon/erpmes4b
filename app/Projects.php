<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    public function getChronoDates($id){
        $project_events = \App\Chronodatesprojects::join('projects', 'projects.id', '=', 'chronodatesprojects.proj_id')
            ->select('chronodatesprojects.date','chronodatesprojects.remarks')
            ->where('chronodatesprojects.proj_id', '=', $id)
            ->get();
        return $project_events;
    }

    public function sectors()
    {
        return $this->belongsTo(Sectors::class, 'sector');
    }
    
    public function subsectors()
    {
        return $this->belongsTo(Subsectors::class, 'subsector');
    }
    
    public function classifications()
    {
        return $this->belongsTo(Classification::class, 'classification');
    }

    public function project_locations() {
        return $this->belongsToMany('App\Locations', 'locations', 'proj_id', 'location')->withTimestamps();
    }    

    public function projects_provinces() {
        return $this->belongsToMany('App\Provinces', 'provincesprojects', 'proj_id', 'province_id')->withTimestamps();
    }
}
