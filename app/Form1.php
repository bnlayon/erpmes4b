<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form1 extends Model
{
    public function getForm1Submission($id){
        $agency_id = auth()->user()->agency_id;
        $checkSubmission = \App\Agency_form1::where('fy', $id)->where('agency_id',$agency_id)->get();
        return $checkSubmission;
    }
}
