<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
    public function subsectors()
    {
        return $this->belongsTo(Subsectors::class, 'subsector_id');
    }
}
