<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form2 extends Model
{
    public function getForm2Submission($id){
        $agency_id = auth()->user()->agency_id;
        $checkSubmission = \App\Agency_form2::where('period', $id)->where('agency_id',$agency_id)->get();
        return $checkSubmission;
    }

}
