<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form4 extends Model
{
    public function getForm4Submission($id){
        $agency_id = auth()->user()->agency_id;
        $checkSubmission = \App\Agency_form4::where('period', $id)->where('agency_id',$agency_id)->get();
        return $checkSubmission;
    }

}
