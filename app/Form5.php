<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form5 extends Model
{
    public function getForm5Submission($id){
        $agency_id = auth()->user()->agency_id;
        $checkSubmission = \App\Agency_form5::where('fy', $id)->where('agency_id',$agency_id)->get();
        return $checkSubmission;
    }
}
