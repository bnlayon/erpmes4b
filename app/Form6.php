<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form6 extends Model
{
    public function getForm6Submission($id){
        $agency_id = auth()->user()->agency_id;
        $checkSubmission = \App\Agency_form6::where('fy', $id)->where('agency_id',$agency_id)->get();
        return $checkSubmission;
    }
}
