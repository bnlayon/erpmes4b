<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agencies extends Model
{
    public $timestamps = false;
    function getProjectCount($id) {
    	$getlatestform5submission = \App\Form5::orderBy('period', 'DESC')->first();
    	$getid = \App\Agency_form5::where('agency_id', '=', $id)->orderBy('period', 'DESC')->first();
    	
    	if(empty($getid->id)){

    	}else{
    		$getprojectsfromrpmesform5 = \App\Agency_form5s_project::
            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->whereNotNull('projects.implementingagency')
            ->where('agency_form5s_id', '=', $getid->id)
            ->where('agency_form5s_projects.status', '=', 'Reviewed')
            ->count();
    		return $getprojectsfromrpmesform5;
    	}
    	
    	
    	// if(empty($getid)){
    	// 	return 0;
    	// }else{
    	// 	return $getid->id;
    	// }
    	//$project_count = \App\Projects::leftJoin('')->where('implementingagency','=',$id)->count();
    	
    }

    function getProjectCost($id) {
    	$getlatestform5submission = \App\Form5::orderBy('period', 'DESC')->first();
    	$getid = \App\Agency_form5::where('agency_id', '=', $id)->orderBy('period', 'DESC')->first();
    	
    	if(empty($getid->id)){

    	}else{
    		$getprojectsfromrpmesform5 = \App\Agency_form5s_project::
            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->whereNotNull('projects.implementingagency')
            ->where('agency_form5s_id', '=', $getid->id)
            ->where('agency_form5s_projects.status', '=', 'Reviewed')
            ->where('agency_form5s_id', '=', $getid->id)->sum('allocation');
    		return $getprojectsfromrpmesform5;
    	}
    }

    function getWeight($id) {
    	$getlatestform5submission = \App\Form5::orderBy('period', 'DESC')->first();
    	$getid = \App\Agency_form5::where('agency_id', '=', $id)->orderBy('period', 'DESC')->first();
    	// $getidoftotal = \App\Agency_form5::where('period', '=', $getlatestform5submission->period)->select('id')->get();
    	if(empty($getid)){
    		return 0;
    	}else{
    		$gettotalsumallocation = \App\Agency_form5s_project::
                        leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->whereNotNull('projects.implementingagency')
            ->where('agency_form5s_id', '=', $getid->id)
            ->where('agency_form5s_projects.status', '=', 'Reviewed')
            ->where('agency_form5s_id', '=', $getid->id)->sum('allocation');
	    	// $gettotalsumallocation = round($gettotalsumallocation, 2);

	    	if(empty($getid->id)){
	    		return 0;
	    	}else{
	    		$getprojectsfromrpmesform5s = \App\Agency_form5s_project::
                            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->whereNotNull('projects.implementingagency')
            ->where('agency_form5s_id', '=', $getid->id)
            ->where('agency_form5s_projects.status', '=', 'Reviewed')
                ->where('agency_form5s_id', '=', $getid->id)->get();
	    		$agencyWeight = 0;
	    		foreach($getprojectsfromrpmesform5s as $getprojectsfromrpmesform5){
	    			// $agencyWeight = 0; 
	    			$weight = $getprojectsfromrpmesform5->allocation / $gettotalsumallocation;
	    			$weightAccomplishment = $weight * $getprojectsfromrpmesform5->atd;

	    			$agencyWeight = $agencyWeight + $weightAccomplishment;
	    		}

	    		return round($agencyWeight,2);
	    		// dd(number_format($totalweights, 5));
	    		//return $formattedweight = number_format($totalweights, 2);
	    	}
    	}
    	
    }

    function getSlip($id) {
    	$getlatestform5submission = \App\Form5::orderBy('period', 'DESC')->first();
    	$getid = \App\Agency_form5::where('agency_id', '=', $id)->orderBy('period', 'DESC')->first();
    	// $getidoftotal = \App\Agency_form5::where('period', '=', $getlatestform5submission->period)->select('id')->get();
    	// $gettotalsumatd = \App\Agency_form5s_project::whereIn('agency_form5s_id', $getidoftotal)->sum('atd');
    	// $gettotalsumttd = \App\Agency_form5s_project::whereIn('agency_form5s_id', $getidoftotal)->sum('ttd');
    	// $totalslip = $gettotalsumatd - $gettotalsumttd;

    	if(empty($getid)){
    		return 0;
    	}else{
    		$gettotalsumallocation = \App\Agency_form5s_project::
                        leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->whereNotNull('projects.implementingagency')
            ->where('agency_form5s_id', '=', $getid->id)
            ->where('agency_form5s_projects.status', '=', 'Reviewed')
            ->where('agency_form5s_id', '=', $getid->id)->sum('allocation');
	    	// $gettotalsumallocation = round($gettotalsumallocation, 2);

	    	if(empty($getid->id)){
	    		return 0;
	    	}else{
	    		$getprojectsfromrpmesform5s = \App\Agency_form5s_project::
                            leftJoin('agency_form5s','agency_form5s.id' ,'=', 'agency_form5s_projects.agency_form5s_id')
            ->leftJoin('agencies','agency_form5s.agency_id' ,'=', 'agencies.id')
            ->leftJoin('projects','projects.id' ,'=', 'agency_form5s_projects.project_id')
            ->where('agency_form5s_projects.status','=', 'Reviewed')
            ->whereNotNull('projects.implementingagency')
            ->where('agency_form5s_id', '=', $getid->id)
            ->where('agency_form5s_projects.status', '=', 'Reviewed')
                ->where('agency_form5s_id', '=', $getid->id)->get();
	    		$agencySlip = 0;
	    		foreach($getprojectsfromrpmesform5s as $getprojectsfromrpmesform5){
	    			// $agencyWeight = 0; 
	    			 $weight = $getprojectsfromrpmesform5->allocation / $gettotalsumallocation;

	    			if($getprojectsfromrpmesform5->ttd == 0.00){
		                $slip = "0";
		            }else{
		                $slip = round($getprojectsfromrpmesform5->atd - $getprojectsfromrpmesform5->ttd, 2);
		            }  

	    			 
	    			$weightSlippage = $weight * $slip;

	    		    $agencySlip = $agencySlip + $weightSlippage;
	    			

	    		}

	    		return round($agencySlip,2);
	    		// dd(number_format($totalweights, 5));
	    		//return $formattedweight = number_format($totalweights, 2);
	    	}
    	}
    }
}
