@extends('layouts.app', ['activePage' => 'reportgen', 'menuParent' => 'reportgen', 'titlePage' => __('Report
Generation')])

@section('content')
<div class="content">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">insert_chart</i>
                            </div>
                            <h4 class="card-title">Report Generation</h4>
                        </div>
                        <div class="card-body ">
                            <form id="TypeValidation" class="form-horizontal" action="{{ asset('/generatereport') }}"
                                method="POST">{{ csrf_field() }}
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">Report Type</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link"
                                                id="reporttype" name="reporttype" required="true"
                                                onchange="java_script_:show(this.options[this.selectedIndex].value)">
                                                <option value=""></option>
                                                @foreach ($reportsadmin as $reports)
                                                <option value="{{ $reports->id }}">{{ $reports->report }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="cat" style="display: none;">
                                    <label class="col-sm-2 col-form-label">Choose Category</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link"
                                                id="categoryrep" name="categoryrep">
                                                <option value=""></option>
                                                @foreach ($categories as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->category }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="spatial" style="display: none;">
                                    <label class="col-sm-2 col-form-label">Choose Spatial</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link"
                                                id="spatialrep" name="spatialrep">
                                                <option value=""></option>
                                                @foreach ($spatial as $spa)
                                                <option value="{{ $spa->id }}">{{ $spa->province }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" id="sectori" style="display: none;">
                                    <label class="col-sm-2 col-form-label">Choose Sector</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link"
                                                id="sectorrep" name="sectorrep">
                                                <option value=""></option>
                                                @foreach ($sectors as $sector)
                                                <option value="{{ $sector->id }}">{{ $sector->sector }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                {{-- <div class="row" id="agency1" style="display: none;">
                                    <label class="col-sm-2 col-form-label">Choose Agency Orig</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link"
                                                id="agencyrep1" name="agencyrep1">
                                                <option value=""></option>
                                                @foreach ($agencies as $agency)
                                                <option value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                                                @endforeach
                                            </select>                                           
                                        </div>
                                    </div>
                                </div> --}}

                                <div class="row" id="agencyi" style="display: none;">
                                    <label class="col-sm-2 col-form-label">Choose Agency</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link" multiple="multiple"
                                                id="agencyrep" name="agencyrep[]">
                                                <option value=""></option>
                                                @foreach ($agencies as $agency)
                                                <option value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                                                @endforeach
                                            </select>                                           
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="periodi" style="display: none;">
                                  <label class="col-sm-2 col-form-label">Choose Period</label>
                                  <div class="col-sm-9">
                                      <div class="form-group">
                                          <select class="form-control selectpicker" data-style="btn btn-link" multiple="multiple"
                                              id="period" name="period[]">
                                              <option value=""></option>
                                              @foreach ($form5s as $form5)
                                              <option value="{{ $form5->period }}">{{ $form5->period }}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                  </div>
                              </div>

                                <div class="row" id="fsourcei" style="display: none;">
                                    <label class="col-sm-2 col-form-label">Choose Funding Source</label>
                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <select class="form-control selectpicker" data-style="btn btn-link"
                                                id="fsourcerep" name="fsourcerep">
                                                <option value=""></option>
                                                @foreach ($sources as $source)
                                                <option value="{{ $source->id }}">{{ $source->type }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>                                

                                <script>
                                    function show(reporttype) {
                                        if (reporttype == "3") {
                                            cat.style.display = 'flex';
                                            spatial.style.display = 'none';
                                            sectori.style.display = 'none';
                                            agencyi.style.display = 'none';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'none';
                                        } else if (reporttype == "4") {
                                            cat.style.display = 'none'
                                            spatial.style.display = 'flex';
                                            sectori.style.display = 'none';
                                            agencyi.style.display = 'none';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'none';
                                        } else if (reporttype == "5") {
                                            cat.style.display = 'none'
                                            spatial.style.display = 'none';
                                            sectori.style.display = 'flex';
                                            agencyi.style.display = 'none';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'none';
                                        } 
                                        else if (reporttype == "10") {
                                            cat.style.display = 'flex'
                                            spatial.style.display = 'none';
                                            sectori.style.display = 'none';
                                            agencyi.style.display = 'flex';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'flex';
                                        } 
                                        else if (reporttype == "11") {
                                            cat.style.display = 'none'
                                            spatial.style.display = 'flex';
                                            sectori.style.display = 'none';
                                            agencyi.style.display = 'flex';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'flex';
                                        } 
                                        else if (reporttype == "12") {
                                            cat.style.display = 'none'
                                            spatial.style.display = 'none';
                                            sectori.style.display = 'flex';
                                            agencyi.style.display = 'flex';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'flex';
                                        } 
                                        else if (reporttype == "13") {
                                            cat.style.display = 'none'
                                            spatial.style.display = 'none';
                                            sectori.style.display = 'none';
                                            agencyi.style.display = 'flex';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'flex';
                                        }  else {
                                            cat.style.display = 'none';
                                            spatial.style.display = 'none';
                                            sectori.style.display = 'none';
                                            agencyi.style.display = 'none';
                                            fsourcei.style.display = 'none';
                                            periodi.style.display = 'none';
                                        }
                                    }

                                </script>
                                <div class="row">
                                    <div class="col-sm-10"></div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary btn-fill" type="submit">Generate Report</button>
                                    </div>
                                </div>
                            </form>
                            @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                            <iframe width="1200" height="547"
                                src="https://app.powerbi.com/reportEmbed?reportId=26f6eb6c-4a55-4fc8-ab8d-fb3c602df1ae&autoAuth=true&ctid=52c084cc-cd15-4671-8a57-c19565bcdfc2&config=eyJjbHVzdGVyVXJsIjoiaHR0cHM6Ly93YWJpLXNvdXRoLWVhc3QtYXNpYS1iLXByaW1hcnktcmVkaXJlY3QuYW5hbHlzaXMud2luZG93cy5uZXQvIn0%3D"
                                frameborder="0" allowFullScreen="true"></iframe>
                            <iframe width="1200" height="547"
                                src="https://app.powerbi.com/view?r=eyJrIjoiODE4MjlkZGEtMzY0My00ZGRjLWJlNjgtNjhkYzdiMmVmOTRjIiwidCI6IjUyYzA4NGNjLWNkMTUtNDY3MS04YTU3LWMxOTU2NWJjZGZjMiIsImMiOjEwfQ%3D%3D&pageName=ReportSectionf734b4a00c1379b080e4"
                                frameborder="0" allowFullScreen="true"></iframe>
                            <iframe width="1200" height="547"
                                src="https://app.powerbi.com/view?r=eyJrIjoiYWQ5N2NkZGItZmIzZi00NGE3LTgwOWQtMDhmODUwZDU0N2IxIiwidCI6IjUyYzA4NGNjLWNkMTUtNDY3MS04YTU3LWMxOTU2NWJjZGZjMiIsImMiOjEwfQ%3D%3D"
                                frameborder="0" allowFullScreen="true"></iframe>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $sample = 22; ?>
@endsection

@push('js')
<script>
    $(document).ready(function () {
        // Javascript method's body can be found in assets/js/demos.js
        md.initDashboardPageCharts();

        md.initVectorMap();

    });

</script>
<script>
    $(document).ready(function () {
        var p1 = "<?php echo $sample; ?>";
        // Javascript method's body can be found in assets/assets-for-demo/js/demo.js

        var dataPreferences = {
            labels: [p1 + '%', '32%', '6%'],
            series: [p1, 32, 6]
        };

        var optionsPreferences = {
            height: '230px'
        };

        Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

    });

</script>
@endpush

@push('js')
<script>
    $(document).ready(function () {
        $('#datatables').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }
        });

        var table = $('#datatable').DataTable();
    });

</script>
@endpush
