@extends('layouts.app', ['activePage' => 'form6', 'menuParent' => 'form6', 'titlePage' => __('RPMES Form 6')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form6') }}">RPMES Form 6</a></li>
            <li class="breadcrumb-item"><a href="{{ asset('form6submission') }}/{{ $getagency->period }}">RPMES Form 6 Submission</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $agency->UACS_AGY_DSC }}</li>
          </ol>
        </nav>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            <h4 class="card-title"> RPMES 6 | {{ $agency->UACS_AGY_DSC }} | {{ $getagency->period }}</h4>
          </div>
          <div class="card-body">
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" border="1">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th class="disabled-sorting">Funds Utilization</th>
                    <th class="disabled-sorting">Target</th>
                    <th class="disabled-sorting">Actual</th>
                    <th class="disabled-sorting">Slippage</th>
                    <th class="disabled-sorting">Issues</th>
                    <th class="disabled-sorting">Source of Information</th>
                    <th class="disabled-sorting">Action Taken/Recommendation</th>
                    @if(auth()->user()->role_id == 1)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form6 as $r1project)
                  <tr> 
                    <td>{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    @if($r1project->releases == 0.00)
                      <td>0%</td>
                    @else
                    <td>{{ round(($r1project->expenditures/$r1project->releases)*100, 2) }}%</td>
                    @endif
                    <td>{{ $r1project->ttd }}</td>
                    <td>{{ $r1project->atd }}</td>
                    @if($r1project->ttd == 0.00)
                      <td>0%</td>
                    @else
                    <td>{{ round(($r1project->atd-$r1project->ttd), 2) }}%</td>
                    @endif
                    <td>{{ $r1project->issues }}</td>
                    <td>{{ $r1project->source }}</td>
                    <td>{{ $r1project->action }}</td>
                    @if(auth()->user()->role_id == 1)
                    <td>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalsubmit{{ $r1project->id }}">Review</button>
                    </td>
                    @endif
                  </tr>
                  
                  </tr>

                  <div class="modal fade bd-example-modal-lg modalsubmit{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form action="{{ asset('/form6addsubmit') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h6 class="modal-title" id="exampleModalLongTitle">Review Project</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Issues</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="issues" name="issues" required="true" rows="3">{{ $r1project->issues }}</textarea>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Source of Information</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="source" name="source" required="true" rows="3">{{ $r1project->source }}</textarea>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="action" name="action" required="true" rows="3">{{ $r1project->action }}</textarea>
                                </div>
                              </div>
                            </div>
                          
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                  </div>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 26, 50, -1],
          [10, 26, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush