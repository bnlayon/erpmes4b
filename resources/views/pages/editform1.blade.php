@extends('layouts.app', ['activePage' => 'form1', 'menuParent' => 'form1', 'titlePage' => __('RPMES Form 1')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form1agency') }}">RPMES Form 1</a></li>
            @if($view_only == 0)
            <li class="breadcrumb-item active" aria-current="page">Edit RPMES Form 1</li>
            @else
            <li class="breadcrumb-item active" aria-current="page">View RPMES Form 1</li>
            @endif
          </ol>
        </nav>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            @if($view_only == 0)
            <h4 class="card-title">Edit RPMES 1 | {{ $form1->fy }} | Status: {{ $form1->status }}</h4>
            @else
            <h4 class="card-title">View RPMES 1 | {{ $form1->fy }} | Status: {{ $form1->status }}</h4>
            @endif
          </div>
          <div class="card-body">
            <div class="toolbar" align="right">
            @if($view_only == 0)
            <button type="button"class="btn btn-info btn-round" data-toggle="modal" data-target=".addproject"><i class="material-icons">library_add</i>
                Add Project</button>
            @endif
              <a href="{{ asset('/exportform1agency') }}/{{ $form1->id }}">
                  <button class="btn btn-round btn-success" data-toggle="modal" data-target="#signupModal"><i class="material-icons">import_export</i> Export</button></a>
              <?php $id = encrypt($form1->id); ?>
              <a href="{{ asset('/previewform1agency') }}/{{ $id }}" target="_blank">
                  <button class="btn btn-round btn-warning" data-toggle="modal" data-target="#signupModal"><i class="material-icons">preview</i> Preview</button></a>
            </div>
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th class="disabled-sorting">Details</th>
                    @if($view_only == 0)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form1 as $r1project)
                  <tr> 
                    <td>{{ $r1project->$id }}{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    <td><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">Project Details</button>
<!--                       <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">PHYSICAL TARGETS</button>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modaloi{{ $r1project->id }}">OUTPUT INDICATOR</button>
                      <button class="btn btn-primary btn-fill btn-sm"  type="button" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">EMPLOYMENT GENERATION</button> -->
                    </td>
                    @if($view_only == 0)
                    <td>
                      
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalsubmit{{ $r1project->id }}">ENDORSE</button>
                      <form action="{{ asset('/deleteprojectform1') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                           <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this item?") }}') ? this.parentElement.submit() : ''">
                              <i class="material-icons">close</i>
                              <div class="ripple-container"></div>
                            </button>
                            </form>
                    </td>
                    @endif
                  </tr>
                    <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{ $r1project->title }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form1agency_submitfs') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div id="accordion" role="tablist">
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingOne">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseOne{{ $r1project->id }}" aria-expanded="true" aria-controls="collapseOne">
                                      Financial Schedule(in PhP million)
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>

                                <div id="collapseOne{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                                          <label class="col-sm-2 col-form-label">January</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_1"  value="{{ $r1project->fs_1 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">February</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_2"  value="{{ $r1project->fs_2 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">March</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_3"  value="{{ $r1project->fs_3 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">April</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_4"  value="{{ $r1project->fs_4 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">May</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_5"  value="{{ $r1project->fs_5 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">June</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_6"  value="{{ $r1project->fs_6 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">July</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_7"  value="{{ $r1project->fs_7 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">August</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_8"  value="{{ $r1project->fs_8 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">September</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_9"  value="{{ $r1project->fs_9 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">October</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_10"  value="{{ $r1project->fs_10 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">November</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_11"  value="{{ $r1project->fs_11 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div class="row">
                                                          <label class="col-sm-2 col-form-label">December</label>
                                                          <div class="col-sm-9">
                                                            <div class="form-group">
                                                              <input class="form-control" type="number" name="fs_12"  value="{{ $r1project->fs_12 }}" step="any"/>
                                                            </div>
                                                          </div>
                                                        </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingTwo">
                                  <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseTwo">
                                      Physical Targets
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseTwo{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">January</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_1"  value="{{ $r1project->pt_1 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">February</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_2"  value="{{ $r1project->pt_2 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">March</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_3"  value="{{ $r1project->pt_3 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">April</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_4"  value="{{ $r1project->pt_4 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">May</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_5"  value="{{ $r1project->pt_5 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">June</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_6"  value="{{ $r1project->pt_6 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">July</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_7"  value="{{ $r1project->pt_7 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">August</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_8"  value="{{ $r1project->pt_8 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">September</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_9"  value="{{ $r1project->pt_9 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">October</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_10"  value="{{ $r1project->pt_10 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">November</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_11"  value="{{ $r1project->pt_11 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">December</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="pt_12"  value="{{ $r1project->pt_12 }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingThree">
                                  <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseThree">
                                      Output Indicator
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseThree{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">January</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_1"  value="{{ $r1project->oi_1 }}" />
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">February</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_2"  value="{{ $r1project->oi_2 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">March</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_3"  value="{{ $r1project->oi_3 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">April</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_4"  value="{{ $r1project->oi_4 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">May</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_5"  value="{{ $r1project->oi_5 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">June</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_6"  value="{{ $r1project->oi_6 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">July</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_7"  value="{{ $r1project->oi_7 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">August</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_8"  value="{{ $r1project->oi_8 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">September</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_9"  value="{{ $r1project->oi_9 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">October</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_10"  value="{{ $r1project->oi_10 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">November</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_11"  value="{{ $r1project->oi_11 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">December</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi_12"  value="{{ $r1project->oi_12 }}"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingFour">
                                  <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFour{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseThree">
                                      Employment Generation
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseFour{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingFour" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">January</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_1"  value="{{ $r1project->eg_1 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_13"  value="{{ $r1project->eg_13 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">February</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_2"  value="{{ $r1project->eg_2 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_14"  value="{{ $r1project->eg_14 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">March</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_3"  value="{{ $r1project->eg_3 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_15"  value="{{ $r1project->eg_15 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">April</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_4"  value="{{ $r1project->eg_4 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_16"  value="{{ $r1project->eg_16 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                   <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">May</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_5"  value="{{ $r1project->eg_5 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_17"  value="{{ $r1project->eg_17 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">June</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_6"  value="{{ $r1project->eg_6 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_18"  value="{{ $r1project->eg_18 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">July</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_7"  value="{{ $r1project->eg_7 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_19"  value="{{ $r1project->eg_19 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-2 col-form-label"></label>
                                        <label class="col-sm-1 col-form-label">August</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_8"  value="{{ $r1project->eg_8 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_20"  value="{{ $r1project->eg_20 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-1 col-form-label"></label>
                                        <label class="col-sm-2 col-form-label">Septmeber</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_9"  value="{{ $r1project->eg_9 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_21"  value="{{ $r1project->eg_21 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-1 col-form-label"></label>
                                        <label class="col-sm-2 col-form-label">October</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_10"  value="{{ $r1project->eg_10 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_22"  value="{{ $r1project->eg_22 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-1 col-form-label"></label>
                                        <label class="col-sm-2 col-form-label">November</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_11"  value="{{ $r1project->eg_11 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_23"  value="{{ $r1project->eg_23 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <div class="row">
                                        <label class="col-sm-1 col-form-label"></label>
                                        <label class="col-sm-2 col-form-label">December</label>
                                        <label class="col-sm-1 col-form-label">Male</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_12"  value="{{ $r1project->eg_12 }}" />
                                        <label class="col-sm-1 col-form-label">Female</label>
                                        <input class="form-control col-sm-3" type="number" name="eg_24"  value="{{ $r1project->eg_24 }}" />
                                        <label class="col-sm-1 col-form-label"></label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            @if($view_only == 0)
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>


<!--                     <div class="modal fade bd-example-modal-lg modalpercent{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Physical Targets in %</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form1agency_submitpt') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">January</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_1"  value="{{ $r1project->pt_1 }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">February</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_2"  value="{{ $r1project->pt_2 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">March</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_3"  value="{{ $r1project->pt_3 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">April</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_4"  value="{{ $r1project->pt_4 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">May</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_5"  value="{{ $r1project->pt_5 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">June</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_6"  value="{{ $r1project->pt_6 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">July</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_7"  value="{{ $r1project->pt_7 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">August</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_8"  value="{{ $r1project->pt_8 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">September</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_9"  value="{{ $r1project->pt_9 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">October</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_10"  value="{{ $r1project->pt_10 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">November</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_11"  value="{{ $r1project->pt_11 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">December</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_12"  value="{{ $r1project->pt_12 }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaloi{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Output Indicator</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form1agency_submitoi') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">January</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_1"  value="{{ $r1project->oi_1 }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">February</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_2"  value="{{ $r1project->oi_2 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">March</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_3"  value="{{ $r1project->oi_3 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">April</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_4"  value="{{ $r1project->oi_4 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">May</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_5"  value="{{ $r1project->oi_5 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">June</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_6"  value="{{ $r1project->oi_6 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">July</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_7"  value="{{ $r1project->oi_7 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">August</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_8"  value="{{ $r1project->oi_8 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">September</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_9"  value="{{ $r1project->oi_9 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">October</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_10"  value="{{ $r1project->oi_10 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">November</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_11"  value="{{ $r1project->oi_11 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">December</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi_12"  value="{{ $r1project->oi_12 }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaleg{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Employment Generation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form1agency_submiteg') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">January</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_1"  value="{{ $r1project->eg_1 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_13"  value="{{ $r1project->eg_13 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">February</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_2"  value="{{ $r1project->eg_2 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_14"  value="{{ $r1project->eg_14 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">March</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_3"  value="{{ $r1project->eg_3 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_15"  value="{{ $r1project->eg_15 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">April</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_4"  value="{{ $r1project->eg_4 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_16"  value="{{ $r1project->eg_16 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                           <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">May</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_5"  value="{{ $r1project->eg_5 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_17"  value="{{ $r1project->eg_17 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">June</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_6"  value="{{ $r1project->eg_6 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_18"  value="{{ $r1project->eg_18 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">July</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_7"  value="{{ $r1project->eg_7 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_19"  value="{{ $r1project->eg_19 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-2 col-form-label"></label>
                                <label class="col-sm-1 col-form-label">August</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_8"  value="{{ $r1project->eg_8 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_20"  value="{{ $r1project->eg_20 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-1 col-form-label"></label>
                                <label class="col-sm-2 col-form-label">Septmeber</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_9"  value="{{ $r1project->eg_9 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_21"  value="{{ $r1project->eg_21 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-1 col-form-label"></label>
                                <label class="col-sm-2 col-form-label">October</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_10"  value="{{ $r1project->eg_10 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_22"  value="{{ $r1project->eg_22 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-1 col-form-label"></label>
                                <label class="col-sm-2 col-form-label">November</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_11"  value="{{ $r1project->eg_11 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_23"  value="{{ $r1project->eg_23 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <label class="col-sm-1 col-form-label"></label>
                                <label class="col-sm-2 col-form-label">December</label>
                                <label class="col-sm-1 col-form-label">Male</label>
                                <input class="form-control col-sm-3" type="number" name="eg_12"  value="{{ $r1project->eg_12 }}" />
                                <label class="col-sm-1 col-form-label">Female</label>
                                <input class="form-control col-sm-3" type="number" name="eg_24"  value="{{ $r1project->eg_24 }}" />
                                <label class="col-sm-1 col-form-label"></label>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div> -->

                    <div class="modal fade bd-example-modal-lg modalsubmit{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/endorse') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Endorse Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="agency_remarks" name="agency_remarks" >{{ $r1project->agency_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            @if($view_only == 0)
                            <button type="submit" class="btn btn-primary">Save</button>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

            <div class="modal fade addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Add Project to RPMES Form 1</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="{{ asset('/form1agency_projects_add') }}/{{ $form1->id }}" method="POST">{{ csrf_field() }}
                <div class="modal-body">
                 <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="project"  name="project">
                      
                    @foreach ($projects as $project)
                      <option value="{{ $project->id }}">{{ $project->title }}</option>
                    @endforeach
                </select>
                  </div>
                </div>
                
                <div class="modal-footer">
                  <button class="btn btn-primary btn-fill" type="submit"><i class="material-icons">add</i></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
              </div>
            </div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush