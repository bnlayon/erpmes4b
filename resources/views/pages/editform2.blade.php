@extends('layouts.app', ['activePage' => 'form2', 'menuParent' => 'form2', 'titlePage' => __('RPMES Form 2')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form2agency') }}">RPMES Form 2</a></li>
            @if($view_only == 0)
            <li class="breadcrumb-item active" aria-current="page">Edit RPMES Form 2</li>
            @else
            <li class="breadcrumb-item active" aria-current="page">View RPMES Form 2</li>
            @endif
          </ol>
        </nav>
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            @if($view_only == 0)
            <h4 class="card-title">Edit RPMES 2 | {{ $form2->period }} | Status: {{ $form2->status }}</h4>
            @else
            <h4 class="card-title">View RPMES 2 | {{ $form2->period }} | Status: {{ $form2->status }}</h4>
            @endif
          </div>
          <div class="card-body">
            <div class="toolbar" align="right">
            @if($view_only == 0)
            <button type="button"class="btn btn-info btn-round" data-toggle="modal" data-target=".addproject"><i class="material-icons">library_add</i>
                Add Project</button>
            @endif

              <a href="{{ asset('/exportform2agency') }}/{{ $form2->id }}">
                  <button class="btn btn-round btn-success" data-toggle="modal" data-target="#signupModal"><i class="material-icons">import_export</i> Export</button></a>
                  <?php $id = encrypt($form2->id); ?>
              <a href="{{ asset('/previewform2agency') }}/{{ $id }}" target="_blank">
                  <button class="btn btn-round btn-warning" data-toggle="modal" data-target="#signupModal"><i class="material-icons">preview</i> Preview</button></a>
            </div>
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th class="disabled-sorting">Details</th>
                    @if($view_only == 0)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form2 as $r1project)
                  <tr> 
                    <td>{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    <td><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">Project Details</button>
<!--                       <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">Physical Targets</button>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">Employment Generation</button> -->
                    </td>
                    @if($view_only == 0)
                    <td>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalsubmit{{ $r1project->id }}">ENDORSE</button>
                      <form action="{{ asset('/deleteprojectform2') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                           <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this item?") }}') ? this.parentElement.submit() : ''">
                              <i class="material-icons">close</i>
                              <div class="ripple-container"></div>
                            </button>
                            </form>
                    </td>
                    @endif
                  </tr>
                    <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{ $r1project->title }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form2agency_submitfs') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div id="accordion" role="tablist">
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingOne">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseOne{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseOne">
                                      Financial Status (in PhP M)
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>

                                <div id="collapseOne{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">ALLOCATION</label>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="alloc_asof"  value="{{ $r1project->alloc_asof }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">For the Quarter</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="alloc_month"  value="{{ $r1project->alloc_month }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">RELEASES</label>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="releases_asof"  value="{{ $r1project->releases_asof }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">For the Quarter</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="releases_month"  value="{{ $r1project->releases_month }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">OBLIGATIONS</label>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="obligations_asof"  value="{{ $r1project->obligations_asof }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">For the Quarter</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="obligations_month"  value="{{ $r1project->obligations_month }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">DISBURSEMENT</label>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="expenditures_asof"  value="{{ $r1project->expenditures_asof }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">For the Quarter</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="expenditures_month"  value="{{ $r1project->expenditures_month }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingTwo">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseTwo{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseTwo">
                                      Physical Status
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>

                                <div id="collapseTwo{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Output Indicator</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="text" name="oi"  value="{{ $r1project->oi }}" />
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Target to Date</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="ttd"  value="{{ $r1project->ttd }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Target for the Quarter</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="tftm"  value="{{ $r1project->tftm }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Actual to Date</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="atd"  value="{{ $r1project->atd }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Actual for the Quarter</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="aftm"  value="{{ $r1project->aftm }}" step="any"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingThree">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseThree{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseTwo">
                                      Employment Generation
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>

                                <div id="collapseThree{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Male</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="male"  value="{{ $r1project->male }}" />
                                        </div>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Female</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <input class="form-control" type="number" name="female"  value="{{ $r1project->female }}"/>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingThree">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseFour{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseTwo">
                                      Remarks
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>

                                <div id="collapseFour{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Remarks</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <textarea rows="2" class="form-control" type="text" name="remarks">{{ $r1project->remarks }}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            @if($view_only == 0)
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
<!-- 
                    <div class="modal fade bd-example-modal-lg modalpercent{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Physical Targets in %</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form2agency_submitpt') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Output Indicator</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi"  value="{{ $r1project->oi }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Target to Date</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="ttd"  value="{{ $r1project->ttd }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Target for the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="tftm"  value="{{ $r1project->tftm }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Actual to Date</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="atd"  value="{{ $r1project->atd }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Actual for the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="aftm"  value="{{ $r1project->aftm }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaleg{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Employment Generation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form2agency_submiteg') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Male</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="male"  value="{{ $r1project->male }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Female</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="female"  value="{{ $r1project->female }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div> -->
                    <div class="modal fade bd-example-modal-lg modalsubmit{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/endorse2') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Endorse Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="agency_remarks" name="agency_remarks" >{{ $r1project->agency_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            @if($view_only == 0)
                            <button type="submit" class="btn btn-primary">Save</button>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

            <div class="modal fade addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Add Project to RPMES Form 2</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="{{ asset('/form2agency_projects_add') }}/{{ $form2->id }}" method="POST">{{ csrf_field() }}
                <div class="modal-body">
                 <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="project"  name="project">
                      <option value=""></option>
                    @foreach ($projects as $project)
                      <option value="{{ $project->id }}">{{ $project->title }}</option>
                    @endforeach
                </select>
                  </div>
                </div>
                
                <div class="modal-footer">
                  <button class="btn btn-primary btn-fill" type="submit"><i class="material-icons">add</i></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
              </div>
            </div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush