@extends('layouts.app', [
  'class' => 'off-canvas-sidebar',
  'classPage' => 'login-page',
  'activePage' => '',
  'title' => __('NEDA MIMAROPA'),
  'pageBackground' => asset("material").'/img/mimaropa.jpg'
])

@section('content')
<div class="container" style="height: auto;">
  <div class="row justify-content-center">
      <div class="col-lg-12 col-md-12">
          <h1 class="text-white text-center">National Economic and Development Authority</h1>
      </div>
      <div class="col-lg-12 col-md-12">
        <h2 class="text-white text-center">MIMAROPA Regional Office</h2>
          <h2 class="text-white text-center">{{ __('Electronic Regional Project Monitoring and Evaluation System') }}</h2>
      </div>
  </div>
</div>
@endsection
