<table border="1">
  <tr>
    <th></th>
    <th>(1)</th>
    <th>(2)</th>
    <th>(3)</th>
    <th>(4)</th>
    <th>(5)</th>
    <th>(6)</th>
    <th>(7)</th>
    <th>(8)</th>
    <th>(9)</th>
    <th>(10)</th>
    <th>(11)</th>
    <th>(12)</th>
    <th>(13)</th>
    <th>(14)</th>
    <th>(15)</th>
    <th>(16)</th>
  </tr>
  <tr>
    <th></th>
    <th>
      (a) Name of Project<br>
      (b) Location<br>
      (c) Sector/Subsector<br>
      (d) Funding Source<br>
      (e) Mode of Implementation<br>
      (f) Project Schedule<br>
    </th>
    <th>ALLOCATION - As of Reporting Period</th>
    <th>ALLOCATION - For the Quarter</th>
    <th>RELEASES - As of Reporting Period</th>
    <th>RELEASES - For the Quarter</th>
    <th>OBLIGATIONS - As of Reporting Period</th>
    <th>OBLIGATIONS - For the Quarter</th>
    <th>EXPENDITURES - As of Reporting Period</th>
    <th>EXPENDITURES - For the Quarter</th>
    <th>Output Indicator</th>
    <th>Target to Date</th>
    <th>Target for the Quarter</th>
    <th>Actual to Date</th>
    <th>Actual for the Quarter</th>
    <th>EMPLOYMENT GENERATED - Male</th>
    <th>EMPLOYMENT GENERATED - Female</th>
  </tr>

  <?php         
   $i = 14;
   $num = 1;

    foreach($form2projs as $form2proj){
      echo "<tr>";
        echo "<td>".$num."</td>";
        echo "<td>";
          echo $form2proj->title."<br>";
          echo $form2proj->sector."<br>";
          echo $form2proj->type."<br>";
          echo $form2proj->mode."<br>";
          echo $form2proj->start." - ".$form2proj->end."<br>";
        echo "</td>";
        echo "<td>".$form2proj->alloc_asof."</td>";
        echo "<td>".$form2proj->alloc_month."</td>";
        echo "<td>".$form2proj->releases_asof."</td>";
        echo "<td>".$form2proj->releases_month."</td>";
        echo "<td>".$form2proj->obligations_asof."</td>";
        echo "<td>".$form2proj->obligations_month."</td>";  
        echo "<td>".$form2proj->expenditures_asof."</td>";
        echo "<td>".$form2proj->expenditures_month."</td>"; 
        echo "<td>".$form2proj->oi."</td>";  
        echo "<td>".$form2proj->ttd."</td>";
        echo "<td>".$form2proj->tftm."</td>"; 
        echo "<td>".$form2proj->atd."</td>";
        echo "<td>".$form2proj->aftm."</td>"; 
        echo "<td>".$form2proj->male."</td>";
        echo "<td>".$form2proj->female."</td>"; 
      echo "</tr>";
      $num++;
    }
  ?>
</table>