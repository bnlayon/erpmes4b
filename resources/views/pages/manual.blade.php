@extends('layouts.app', ['activePage' => 'manual', 'menuParent' => 'manual', 'titlePage' => __('Manual')])

@section('content')
<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
            <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-primary card-header-icon">
                  </div>
                  <div class="card-body ">
                    <embed width="100%" height="750" src="{{ asset('material')}}/files/manual.pdf" type="application/pdf" nodownload=1></embed>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      </div>



      
    </div>
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

      md.initVectorMap();

    });
  </script>
@endpush

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush