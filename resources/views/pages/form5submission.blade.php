@extends('layouts.app', ['activePage' => 'form5', 'menuParent' => 'form5', 'titlePage' => __('RPMES Form 5')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form5') }}">RPMES Form 5</a></li>
            <li class="breadcrumb-item active" aria-current="page">RPMES Form 5 Agencies</li>
          </ol>
        </nav>
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">format_list_numbered</i>
            </div>
            <h4 class="card-title">Summary of Financial and Physical Accomplishments Including Project Results (By Area, Sector, Agency)</h4>
          </div>

          <div class="card-body">
            <div class="toolbar" align="right">
              @if(auth()->user()->role_id == 1)
              <button class="btn btn-round" data-toggle="modal" data-target=".add">
                <i class="material-icons">library_add</i>
                Add Agency
              </button>
              @endif
            </div>
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Agency</th>
                    <th>Status</th>
                    <th class="disabled-sorting">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($form5 as $form)
                  <tr>
                    <td>{{ $form->UACS_AGY_DSC }}</td>
                    <td>{{ $form->status }}</td>
                    <td>
                     <!--  <form action="{{ asset('/form5generate') }}/{{ $form->agency_id }}" method="POST">{{ csrf_field() }}
                        <input type="hidden" name="form5s_id" value="{{ $form->id }}">
                        <button type="submit" class="btn btn-primary btn-sm">Generate</button>
                      </form> -->
                      <button type="button" class="btn btn-primary btn-sm" onclick=" window.open('{{ asset('/form5submissionview') }}/{{ $form->id }}','_self')">Review Form</button>
                      <a href="{{ asset('/exportform5') }}/{{ $form->id }}">
                      <button class="btn btn-round btn-success btn-sm" data-toggle="modal" data-target="#signupModal"><i class="material-icons">import_export</i> Export</button>
                      </a>
                      <form action="{{ asset('/deleteprojectform5') }}/{{ $form->id }}" method="POST">{{ csrf_field() }}
                           <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this item?") }}') ? this.parentElement.submit() : ''">
                              <i class="material-icons">close</i>
                              <div class="ripple-container"></div>
                            </button>
                            </form>
                    </td>
                  </tr>

                  @endforeach

                  <div class="modal fade bd-example-modal-lg add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form action="{{ asset('/form5add') }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Add Agency</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                          <input type="hidden" name="period" value="{{ $period }}">
<!--                           <input type="hidden" name="formid" value="{{ $formid }}"> -->
                          <select class="form-control" data-style="btn btn-link" id="agency_id" name="agency_id" required="true">
                          <option value=""></option>
                          @foreach ($agencies as $agency)
                          <option value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                          @endforeach
                          </select> 
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                  </div>
                 
                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush