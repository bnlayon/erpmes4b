<table border="1">
  <tr>
    <th></th>
    <th>(1)<br>Name of Project</th>
    <th>(2)<br>Project Objective/s (Based on LogFrame)</th>
    <th>(3)<br>Results Indicator/Target (Based on LogFrame)</th>
    <th>(4)<br>Observed Results</th>
  </tr>

  <?php        
   $num = 1;

    foreach($form4projs as $form4proj){
      echo "<tr>";
        echo "<td>".$num."</td>";
        echo "<td>";
          echo $form4proj->title."<br>";
        echo "</td>";
        echo "<td>";
          echo $form4proj->objectives."<br>";
        echo "</td>";
        echo "<td>";
          echo $form4proj->indicator."<br>";
        echo "</td>";
        echo "<td>";
          echo $form4proj->results."<br>";
        echo "</td>";
      echo "</tr>";
      $num++;
    }
  ?>
</table>