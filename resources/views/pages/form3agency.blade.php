@extends('layouts.app', ['activePage' => 'form3agency', 'menuParent' => 'form3agency', 'titlePage' => __('RPMES Form 3')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">format_list_numbered</i>
            </div>
            <h4 class="card-title">Project Exception Report</h4>
          </div>

          <div class="card-body">
<!--             <div class="toolbar" align="right">
              <a class="nav-link" href="{{ route('page.addpage') }}">
              <button class="btn btn-round" data-toggle="modal" data-target="#signupModal">
                <i class="material-icons">library_add</i>
                Add RPMES Form 1
              </button>
            </a>
            </div> -->
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Period</th>
                    <th>Status</th>
                    <th>NRO Status of Review</th>
                    <th class="disabled-sorting text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($form3 as $form)
                  <tr>
                    <td>{{ $form->period }}</td>
                    <td>
                      @if(!$form->getForm3Submission($form->period)->isEmpty())
                         @foreach($form->getForm3Submission($form->period) as $entry)
                          {{ $entry->status }}
                         @endforeach
                      @else
                        No Submission
                      @endif
                    </td>
                    <td>
                      @if(!$form->getForm3Submission($form->fy)->isEmpty())
                         @foreach($form->getForm3Submission($form->period) as $entry)
                          {{ $entry->nro_status_review }}
                         @endforeach
                      @else

                      @endif
                    </td>
                    <td>
                      @if($form->is_lock == 1)
                        <!-- <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" ><i class="material-icons">lock</i> Admin Lock</button> -->
                        @foreach($form->getForm3Submission($form->period) as $entry)
                          <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" onclick=" window.open('{{ asset('/editform3') }}/{{ encrypt($entry->id) }}','_self')"><i class="material-icons">lock</i>View Submmision</button>
                        @endforeach
                      @else
                        @if(!$form->getForm3Submission($form->period)->isEmpty())
                           @foreach($form->getForm3Submission($form->period) as $entry)
                           @if($entry->status == 'Submitted')
                            <!-- <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" ><i class="material-icons">lock</i> Form already Submitted/Reviewed</button> -->
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" onclick=" window.open('{{ asset('/editform3') }}/{{ encrypt($entry->id) }}','_self')"><i class="material-icons">lock</i>View Submmision</button>
                           @else
                           <?php $id = encrypt($entry->id); ?>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" onclick=" window.open('{{ asset('/editform3') }}/{{ $id }}','_self')">EDIT</button>
                            <form id="sub3" action="{{ asset('/form3agency_submit') }}/{{ $entry->id }}" method="POST">{{ csrf_field() }}
                            <button class="btn btn-primary btn-fill btn-sm" type="submit">Submit</button>
                            </form>
                          @endif
                        @endforeach
                        @else    
                        <form action="{{ asset('/form3agency_add') }}/{{ $form->period }}" method="POST">{{ csrf_field() }}
                          <button class="btn btn-primary btn-fill btn-sm" type="submit"><i class="material-icons">add</i></button>
                          </form>
                          
                        @endif
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>

<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script>
    document.querySelector('#sub3').addEventListener('submit', function(e) {
      var form = this;
      
      e.preventDefault();
      
      swal({
          title: "Are you sure?",
          text: "You will not be able to edit this file after submission!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            swal({
              title: 'Please wait, form is submitting',
              icon: 'success',
              timer: 2000,
            }).then(function() {
              form.submit();
            });
          } else {
            swal("Cancelled", "", "error");
          }
        });
    });
  </script>
@endpush