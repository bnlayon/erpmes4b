@extends('layouts.app', ['activePage' => 'form6', 'menuParent' => 'form6', 'titlePage' => __('RPMES Form 6')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">format_list_numbered</i>
            </div>
            <h4 class="card-title">Report on the Status of Projects Encountering Implementation Problems</h4>
          </div>

          <div class="card-body">
            <div class="toolbar" align="right">
              <button class="btn btn-info btn-round" data-toggle="modal" data-target=".addform1">
                <i class="material-icons">library_add</i>
                Add RPMES Form 6
              </button>
            </div>
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>FY</th>
                    <th>Status</th>
                    <th class="disabled-sorting text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($form6 as $form)
                  <tr>
                    <td>{{ $form->period }}</td>
                    <td>
                      
                    </td>
                    <td>
                     
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" onclick=" window.open('{{ asset('/form6submission') }}/{{ $form->period }}','_self')">VIEW AGENCIES</button>
                    </td>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

            <div class="modal fade addform1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Add RPMES Form 6</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="{{ asset('/addform6') }}" method="POST">{{ csrf_field() }}
                <div class="modal-body">
                 <div class="form-group">
                    <input class="form-control" type="month" name="period" id="period" required="true" />
                  </div>
                </div>
                
                <div class="modal-footer">
                  <button class="btn btn-primary btn-fill" type="submit"><i class="material-icons">add</i></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
              </div>
            </div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush