@extends('layouts.app', ['activePage' => 'form1', 'menuParent' => 'form1', 'titlePage' => __('RPMES Form 1')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form1') }}">RPMES Form 1</a></li>
            <li class="breadcrumb-item"><a href="{{ asset('form1submission') }}/{{ $getagency->fy }}">RPMES Form 1 Submission</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $agency->UACS_AGY_DSC }}</li>
          </ol>
        </nav>
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            <h4 class="card-title"> RPMES 1 | {{ $agency->UACS_AGY_DSC }} | {{ $getagency->fy }}</h4>
          </div>
          <div class="card-body">
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" border="1">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th>Total Project Target</th>
                    <th>JAN</th>
                    <th>FEB</th>
                    <th>MAR</th>
                    <th>APR</th>
                    <th>MAY</th>
                    <th>JUN</th>
                    <th>JUL</th>
                    <th>AUG</th>
                    <th>SEP</th>
                    <th>OCT</th>
                    <th>NOV</th>
                    <th>DEC</th>
                    <th>TOTAL</th>
                    <!-- <th class="disabled-sorting">Details</th> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form1 as $r1project)
                  <tr> 
                    <td rowspan="4">{{ $r1project->title }}</td>
                    <td rowspan="4">{{ $r1project->modified_by }}</td>
                    <td rowspan="4">{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td rowspan="4">{{ $r1project->status }}</td>
                    <td>FS</td>
                    <?php $t_total = 0; ?>
                    @for ($i = 1; $i < 13; $i++)
                      <td>{{ $r1project->{"fs_$i"} }}</td>
                      <?php $t_total = $t_total + $r1project->{"fs_$i"}; ?>
                    @endfor
                    <td>{{ $t_total }}</td>
                    <!-- <td align="center"><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">FINANCIAL SCHEDULE</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">PHYSICAL TARGETS</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modaloi{{ $r1project->id }}">OUTPUT INDICATOR</button><button class="btn btn-primary btn-fill btn-sm"  type="button" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">EMPLOYMENT GENERATION</button></td> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <td rowspan="4">
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalreview{{ $r1project->id }}">REVIEW</button>
                    </td>
                    @endif
                  </tr>
                  <tr>
                    <td>%</td>
                    <?php $t_total = 0; ?>
                    @for ($i = 1; $i < 13; $i++)
                      <td>{{ $r1project->{"pt_$i"} }}</td>
                      <?php $t_total = $t_total + $r1project->{"pt_$i"}; ?>
                    @endfor
                    <td>{{ $t_total }}</td>
                  </tr>
                  <tr>
                    <td>OI</td>
                    @for ($i = 1; $i < 13; $i++)
                      <td>{{ $r1project->{"oi_$i"} }}</td>
                    @endfor
                    <td></td>
                  </tr>
                  <tr>
                    <td>EG</td>
                    <?php $t_total = 0; ?>
                    @for ($i = 1; $i < 13; $i++)
                      <td>{{ $r1project->{"eg_$i"} }}</td>
                      <?php $t_total = $t_total + $r1project->{"eg_$i"}; ?>
                    @endfor
                    <td>{{ $t_total }}</td>
                  </tr>

                    <!-- <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Financial Schedule</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">January</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_1" name="fs_1" required="true" value="{{ $r1project->fs_1 }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">February</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_2" name="fs_2" required="true" value="{{ $r1project->fs_2 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">March</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_3" name="fs_3" required="true" value="{{ $r1project->fs_3 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">April</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_4" name="fs_4" required="true" value="{{ $r1project->fs_4 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">May</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_5" name="fs_5" required="true" value="{{ $r1project->fs_5 }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">June</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_6" name="fs_6" required="true" value="{{ $r1project->fs_6 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">July</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_7" name="fs_7" required="true" value="{{ $r1project->fs_7 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">August</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_8" name="fs_8" required="true" value="{{ $r1project->fs_8 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">September</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_9" name="fs_9" required="true" value="{{ $r1project->fs_9 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">October</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_10" name="fs_10" required="true" value="{{ $r1project->fs_10 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">November</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_11" name="fs_11" required="true" value="{{ $r1project->fs_11 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">December</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="fs_12" name="fs_12" required="true" value="{{ $r1project->fs_12 }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modalpercent{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Physical Targets in %</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">January</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_1" required="true" value="{{ $r1project->pt_1 }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">February</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_2" required="true" value="{{ $r1project->pt_2 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">March</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_3" required="true" value="{{ $r1project->pt_3 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">April</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_4" required="true" value="{{ $r1project->pt_4 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">May</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_5" required="true" value="{{ $r1project->pt_5 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">June</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_6" required="true" value="{{ $r1project->pt_6 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">July</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_7" required="true" value="{{ $r1project->pt_7 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">August</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_8" required="true" value="{{ $r1project->pt_8 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">September</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_9" required="true" value="{{ $r1project->pt_9 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">October</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_10" required="true" value="{{ $r1project->pt_10 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">November</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_11" required="true" value="{{ $r1project->pt_11 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">December</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="pt_12" required="true" value="{{ $r1project->pt_12 }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaloi{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Output Indicator</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">January</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">February</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">March</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">April</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">May</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">June</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">July</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">August</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">September</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">October</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">November</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">December</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="number" required="true" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaleg{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Employment Generation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">January</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_1" required="true" value="{{ $r1project->eg_1 }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">February</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_2" required="true" value="{{ $r1project->eg_2 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">March</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_3" required="true" value="{{ $r1project->eg_3 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">April</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_4" required="true" value="{{ $r1project->eg_4 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">May</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_5" required="true" value="{{ $r1project->eg_5 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">June</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_6" required="true" value="{{ $r1project->eg_6 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">July</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_7" required="true" value="{{ $r1project->eg_7 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">August</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_8" required="true" value="{{ $r1project->eg_8 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">September</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_9" required="true" value="{{ $r1project->eg_9 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">October</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_10" required="true" value="{{ $r1project->eg_10 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">November</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_11" required="true" value="{{ $r1project->eg_11 }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">December</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="eg_12" required="true" value="{{ $r1project->eg_12 }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div> -->

                     
                    <div class="modal fade bd-example-modal-lg modalreview{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/review') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Review Submission</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="nro_remarks" name="nro_remarks" required="true">{{ $r1project->nro_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>


                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "scrollX": true,
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: false,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush