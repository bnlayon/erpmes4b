@extends('layouts.app', ['activePage' => 'form4', 'menuParent' => 'form4', 'titlePage' => __('RPMES Form 4')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form4') }}">RPMES Form 4</a></li>
            <li class="breadcrumb-item"><a href="{{ asset('form4submission') }}/{{ $getagency->period }}">RPMES Form 4 Submission</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $agency->UACS_AGY_DSC }}</li>
          </ol>
        </nav>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            <h4 class="card-title"> RPMES 4 | {{ $agency->UACS_AGY_DSC }} | {{ $getagency->period }}</h4>
          </div>
          <div class="card-body">
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" border="1">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th>Project Objective/s (Based on LogFrame)</th>
                    <th>Results Indicator/Target (Based on LogFrame)</th>
                    <th>Observed Results</th>
                    <!-- <th class="disabled-sorting">Details</th> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form4 as $r1project)
                  <tr> 
                    <td>{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    <td>{{ $r1project->objectives }}</td>
                    <td>{{ $r1project->indicator }}</td>
                    <td>{{ $r1project->results }}</td>
                    <!-- <td><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}"> Project Objective/s (Based on LogFrame)</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">Results Indicator/Target (Based on LogFrame)</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">Observed Results</button></td> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <td>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalsubmit{{ $r1project->id }}">Review</button>
                    </td>
                    @endif
                  </tr>
                    <!-- <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" >
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form4agency_submitpo') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Project Objective/s</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="objectives" name="objectives" required="true" rows="10">{{ $r1project->objectives }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modalpercent{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form4agency_submitri') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Results Indicator/Target</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="indicator" name="indicator" required="true" rows="10">{{ $r1project->indicator }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaleg{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form4agency_submitor') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Observed Results</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="results" name="results" required="true" rows="10">{{ $r1project->results }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div> -->
                    <div class="modal fade bd-example-modal-lg modalsubmit{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/review4') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Review Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="nro_remarks" name="nro_remarks" required="true">{{ $r1project->nro_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>
                  </tr>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "scrollX": true,
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: false,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush