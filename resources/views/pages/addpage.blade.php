@extends('layouts.app', ['activePage' => 'addproject', 'menuParent' => 'addproject', 'titlePage' => __('Add Project')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('addproject') }}">Projects</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add Project</li>
          </ol>
        </nav>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            <h4 class="card-title">Add Project</h4>
          </div>
          <div class="card-body">
            <form id="TypeValidation" class="form-horizontal" action="{{ asset('/addpage') }}" method="POST">{{ csrf_field() }}
              <div class="row">
                <label class="col-sm-2 col-form-label">Title</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <input class="form-control" type="text" name="title" id="title" required/>
                  </div>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Project Title" data-content="Title of project as found in the approved program of work, loan or grant agreement.">?</a>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">Location</label>
                <div class="col-sm-9">
                              <table class="form-table ui celled table" id="Location">
                                <tr valign="top">
                                  <td colspan="4">
                                    <center>
                                    <a href="javascript:void(0);" class="addLocation">ADD LOCATION</a>
                                    </center>
                                  </td>
                                </tr>
                              </table>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Project Location" data-content="Barangay/Municipality/City/Province/Region where project is implemented.">?</a>

                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <center>
                      <a class="btn btn-sm btn-danger" href="https://www.bing.com/maps/" target="_blank">Check Location Map Here</a>
                  </center>
                </div>
              </div>
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                  <div class="row">
                    <label class="col-sm-2 col-form-label">Implementing Agency</label>
                    <div class="col-sm-9">
                      <div class="form-group">
                        <select class="form-control selectpicker" data-style="btn btn-link" id="implementingagency" name="implementingagency" required="true">
                          <option value=""></option>
                          @foreach ($agencies as $agency)
                          <option value="{{ $agency->id }}">{{ $agency->UACS_AGY_DSC }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                    @elseif(auth()->user()->role_id == 2)
                        <input class="form-control" type="hidden" name="implementingagency" id="implementingagency" required="true" value="{{ $myagency }}" />
                    @else
                    @endif
              <div class="row">
                <label class="col-sm-2 col-form-label">Spatial Coverage</label>
                <div class="col-sm-9">
                  @foreach ($provinces as $province)
                  <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="province[]" value="{{ $province->id }}">
                        {{ $province->province }}
                        <span class="form-check-sign">
                            <span class="check"></span>
                        </span>
                    </label>
                  </div>
                  @endforeach
                </div>
              </div>



              <div class="row">
                <label class="col-sm-2 col-form-label">Classification</label>
                <div class="col-sm-9">
                  <select onChange="viewSectorAndSubsector()" class="form-control selectpicker" data-live-search="true" data-style="btn btn-link" id="classification" required="true" name="classification">
                    <option value=""></option>
                    @foreach ($nsubsectors as $subsectors_)
                      <optgroup label="{{ $subsectors_->subsector }}">
                      @foreach ($classifications as $classification)
                        @if ($classification->subsector_id == $subsectors_->id)
                          <option data-sectorId="{{ $subsectors_->sector_id }}" data-sectorName="{{ $subsectors_->sectors->sector }}" data-subsectorId="{{ $subsectors_->id }}" data-subsectorName="{{ $subsectors_->subsector }}" value="{{ $classification->id }}">{{ $classification->classification }}</option>
                        @endif
                      @endforeach
                      </optgroup>
                    @endforeach
                    @foreach ($ncsubsectors as $subsectors_)
                      <option data-sectorId="{{ $subsectors_->sector_id }}" data-sectorName="{{ $subsectors_->sectors->sector }}" data-subsectorId="{{ $subsectors_->id }}" data-subsectorName="{{ $subsectors_->subsector }}" value="{{ $subsectors_->id }}">{{ $subsectors_->subsector }}</option>
                    @endforeach
                  </select>
                </div>
              </div>


              <div class="row" id="divSubSector" hidden>
                <label class="col-sm-2 col-form-label">Subsector</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="subsector" required="true" name="subsector">
                    </select>
                  </div>
                </div>
              </div>

              <div class="row" id="divSector" hidden>
                <label class="col-sm-2 col-form-label">Sector</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="sector" required="true" name="sector">
                    </select>
                  </div>
                </div>
              </div>


              <!-- <div class="row">
                <label class="col-sm-2 col-form-label">Sector</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="sector" required="true" name="sector" onchange="java_script_:showSubsector(this.options[this.selectedIndex].value)">
                      <option value=""></option>
                    @foreach ($sectors as $sector)
                      <option value="{{ $sector->id }}">{{ $sector->sector }}</option>
                    @endforeach
                </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">Subsector</label>
                <div class="col-sm-9">
                <?php for($i=1;$i<=4;$i++){ ?>
                  <div id="sector_{{ $i }}" style="display: none;"> 
                    <select class="form-control selectpicker" data-style="btn btn-link" id="subsector" name="subsector_{{ $i }}">
                      <option value=""></option>
                    @foreach ($subsectors as $subsectors_)
                      @if ($subsectors_->sector_id == $i)
                        <option value="{{ $subsectors_->id }}">{{ $subsectors_->subsector }}</option>
                      @endif
                    @endforeach
                    </select>
                    </div>
                    <?php } ?>
                    
                </div>
              </div> -->
              <script type="text/javascript">

                function viewSectorAndSubsector() {
                  if (document.getElementById('classification').value == '' || document.getElementById('classification').value == null) {
                    $("#sector").empty();
                    $("#subsector").empty();
                    document.getElementById('divSector').hidden = true;
                    document.getElementById('divSubSector').hidden = true;
                  } else {
                    document.getElementById('divSector').hidden = false;
                    document.getElementById('divSubSector').hidden = false;
                    $("#sector").empty();
                    $("#subsector").empty();

                    $("#sector").append('<option value="' + $("#classification option:selected").attr("data-sectorId") + '">' + $("#classification option:selected").attr("data-sectorName") + '</option>');
                    $('#sector').val($("#classification option:selected").attr("data-sectorId"));
                    $("#sector").selectpicker("refresh");

                    $("#subsector").append('<option value="' + $("#classification option:selected").attr("data-subsectorId") + '">' + $("#classification option:selected").attr("data-subsectorName") + '</option>');
                    $('#subsector').val($("#classification option:selected").attr("data-subsectorId"));
                    $("#subsector").selectpicker("refresh");
                  }
                }

                function showSubsector(sector) {
                  if (sector=="1") {
                    sector_1.style.display='block';
                    sector_2.style.display='none';
                    sector_3.style.display='none';
                    sector_4.style.display='none';
                  } else if (sector=="2") {
                    sector_1.style.display='none';
                    sector_2.style.display='block';
                    sector_3.style.display='none';
                    sector_4.style.display='none';
                  } else if (sector=="3") {
                    sector_1.style.display='none';
                    sector_2.style.display='none';
                    sector_3.style.display='block';
                    sector_4.style.display='none';
                  } else if (sector=="4") {
                    sector_1.style.display='none';
                    sector_2.style.display='none';
                    sector_3.style.display='none';
                    sector_4.style.display='block';
                  }
                }
              </script>
              <div class="row">
                <label class="col-sm-2 col-form-label">Funding Source</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="fundingsource" required="true" name="fundingsource" onchange="java_script_:show(this.options[this.selectedIndex].value)">
                      <option value=""></option>
                    @foreach ($sources as $source)
                      <option value="{{ $source->id }}">{{ $source->type }}</option>
                    @endforeach
                </select>
                  </div>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Funding Source" data-content="Indicate source of fund for the project (e.g., ODA loan or grant; GAA; calamity fund, etc.).">?</a>
                </div>
              </div>
              <div class="row" id="lfp" style="display: none;">
                <label class="col-sm-2 col-form-label">LFP Type</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" type="text" name="lfptype" id="lfptype" onchange="java_script_:show2(this.options[this.selectedIndex].value)"/>
                      <option value="" disabled="" selected=""></option>
                      <option value="GAA">GAA</option>
                      <option value="Corporate">Corporate</option>
                      <option value="20% Development Fund">20% Development Fund</option>
                      <option value="Fund 164">Fund 164 (Special Trust Fund)</option>
                      <option value="Others">Others</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
              <div class="row" id="gaalabel" style="display: none;">
                <label class="col-sm-2 col-form-label">GAA Year</label>
                <div class="col-sm-8">
                  <div class="form-group">
                      <!--
                    <input class="form-control" type="date" name="gaayear" id="gaayear" />
                    -->
                   <select class="form-control selectpicker" data-style="btn btn-link" id="gaayear" name="gaayear">
                                <option value=""></option>
                                @for ($i = now()->year; $i > 2015; $i--)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                   </select>
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
              
              <div class="row" id="otherslfplabel" style="display: none;">
                <label class="col-sm-2 col-form-label">Others</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input class="form-control" type="text" name="otherslfp" id="otherslfp" />
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
              <div class="row" id="oda" style="display: none;">
                <label class="col-sm-2 col-form-label">ODA Type</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" type="text" name="odatype" id="odatype"/>
                      <option value="" disabled="" selected=""></option>
                      <option value="Grant">Grant</option>
                      <option value="Loan">Loan</option>
                      <option value="GrantLoan">Grant and Loan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>
              <div class="row" id="otherslabel" style="display: none;">
                <label class="col-sm-2 col-form-label">Others</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input class="form-control" type="text" name="others" id="others" />
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>


              <script>
                function show(fundingsource) {
                  if(fundingsource == "1"){
                    oda.style.display='flex';
                    otherslabel.style.display='none';
                    lfp.style.display='none';
                    gaalabel.style.display='none';
                    otherslfplabel.style.display='none';
                  } else if (fundingsource == "2") {
                    oda.style.display='none';
                    otherslabel.style.display='none';
                    lfp.style.display='flex';
                  } else if (fundingsource == "4") {
                    oda.style.display='none';
                    otherslabel.style.display='flex';
                    lfp.style.display='none';
                    gaalabel.style.display='none';
                    otherslfplabel.style.display='none';
                  } else if (fundingsource == "5") {
                    oda.style.display='none';
                    otherslabel.style.display='none';
                    gaalabel.style.display='flex';
                    lfp.style.display='none';
                    gaalabel.style.display='none';
                    otherslfplabel.style.display='none';
                  } else {
                    oda.style.display='none';
                    otherslabel.style.display='none';
                    lfp.style.display='none';
                    gaalabel.style.display='none';
                    otherslfplabel.style.display='none';
                  }
                }
              </script>

              <script>
                function show2(lfptype) {
                  if(lfptype == "GAA"){
                    gaalabel.style.display='flex';
                    otherslfplabel.style.display='none';
                  } else if (lfptype == "Others") {
                    gaalabel.style.display='none';
                    otherslfplabel.style.display='flex';
                  } else {
                    gaalabel.style.display='none';
                    otherslfplabel.style.display='none';
                  }
                }
              </script>

              <div class="row">
                <label class="col-sm-2 col-form-label">Mode of Implementation</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" type="text" name="mode" id="mode" onchange="java_script_:showMode(this.options[this.selectedIndex].value)"/>
                      <option value="" disabled="" selected=""></option>
                      <option value="By contract">By contract</option>
                      <option value="By admin">By admin</option>
                      <option value="Others">Others</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Mode of Implementation" data-content="Indicate how the project will be implemented. If by contract, state name of contractor.">?</a>
                </div>
              </div>

              <div class="row" id="contractlabel" style="display: none;">
                <label class="col-sm-2 col-form-label">Specify contractor</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input class="form-control" type="text" name="contractor" id="contractor" />
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>

              <div class="row" id="othersmodelabel" style="display: none;">
                <label class="col-sm-2 col-form-label">Others</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input class="form-control" type="text" name="modeothers" id="modeothers" />
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">Project Schedule - Start</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <input class="form-control" type="date" name="start" id="start" required="true" />
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">Project Schedule - End</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <input class="form-control" type="date" name="end" id="end" required="true" />
                  </div>
                </div>
              </div>

              <div class="row">
                <label class="col-sm-2 col-form-label">Project Category</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="category" required="true" name="category"  onchange="java_script_:show3(this.options[this.selectedIndex].value)">
                      <option value=""></option>
                    @foreach ($categories as $category)
                      <option value="{{ $category->id }}">{{ $category->category }}</option>
                    @endforeach
                </select>
                  </div>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Project Category" data-content="">?</a>
                </div>
              </div>

              <div class="row" id="rrplabel" style="display: none;">
                <label class="col-sm-2 col-form-label">Specify RRP (Yolanda, COVID19, Tisoy, Ursula, etc.)</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input class="form-control" type="text" name="rrp" id="rrp" />
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>

              <div class="row" id="otherscatlabel" style="display: none;">
                <label class="col-sm-2 col-form-label">Others</label>
                <div class="col-sm-8">
                  <div class="form-group">
                    <input class="form-control" type="text" name="catothers" id="catothers" />
                  </div>
                </div>
                <div class="col-sm-1"></div>
              </div>

              <script>
                function show3(category) {
                  if(category == '3'){
                    rrplabel.style.display='flex';
                    otherscatlabel.style.display='none';
                  } else if (category == '8') {
                    rrplabel.style.display='none';
                    otherscatlabel.style.display='flex';
                  } else {
                    rrplabel.style.display='none';
                    otherscatlabel.style.display='none';
                  }
                }
              </script>

              <script>
                function showMode(mode) {
                  if(mode == "By contract"){
                    contractlabel.style.display='flex';
                    othersmodelabel.style.display='none';
                  } else if (mode == "Others") {
                    contractlabel.style.display='none';
                    othersmodelabel.style.display='flex';
                  } else {
                    contractlabel.style.display='none';
                    othersmodelabel.style.display='none';
                  }
                }
              </script>

              <div class="row">
                <label class="col-sm-2 col-form-label">Project Status</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="status" required="true" name="status">
                      <option value=""></option>
                      <option value="Ongoing">Ongoing</option>
                      <option value="Completed">Completed</option>
                </select>
                  </div>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Project Category" data-content="">?</a>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">Project Cost</label>
                <div class="col-sm-9">
                  <div class="form-group">
                    <input class="form-control" name="cost" id="cost" required="true" step="any"/>
                  </div>
                </div>
                <div class="col-sm-1">
                <a tabindex="0" class="btn btn-sm btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Project Category" data-content="">?</a>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-sm-10"></div>
                <div class="col-sm-2">
                <button class="btn btn-primary" id="btnfrmsubmit" type="submit" hidden></button>
                <button class="btn btn-primary" id="addproject" type="button">Add Projects</button>
              </div>
              </div>
        </form>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>
@endsection

@push('js')
  <script>
    function setFormValidation(id) {
      $(id).validate({
        highlight: function(element) {
          $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
          $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
        },
        success: function(element) {
          $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
          $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
        },
        errorPlacement: function(error, element) {
          $(element).closest('.form-group').append(error);
        },
      });
    }

    $(document).ready(function() {
      setFormValidation('#AddProjectValidation');
    });
  </script>
    <script>
    $(document).ready(function(){
      $(".addLocation").click(function(){
        $("#Location").append('<tr valign="top"><td>Location: <input class="form-control" id="location" type="text" name="location[]" required="true" /></td><td>Longitude: <input class="form-control" id="longitude" type="number" step="any" name="longitude[]" required="true" /></td><td>Latitude: <input class="form-control" id="latitude" type="number" step="any" name="latitude[]" required="true" /></td><td><a href="javascript:void(0);" class="remLocation">REMOVE</a></td></tr>');
      });
        $("#Location").on('click','.remLocation',function(){
            $(this).parent().parent().remove();
        });
    });
  </script>
@endpush

@push('js')
  <script>
    $(document).ready(function() {
      // initialise Datetimepicker and Sliders
      md.initFormExtendedDatetimepickers();
      if ($('.slider').length != 0) {
        md.initSliders();
      }
    });




    $('#cost').keyup(function(event) {
      if(event.which >= 37 && event.which <= 40){
       event.preventDefault();
      }

      $(this).val(function(index, value) {
          value = value.replace(/,/g,'');
          return numberWithCommas(value);
      });
    });

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    $("#addproject").click(function() {
      var t_int = document.getElementById("cost").value;
      t_int = t_int.replace(/\,/g,'');
      t_int = t_int * 1;
      if (Number.isFinite(t_int) == true) {
        document.getElementById("cost").value = t_int.toFixed(2);
        // document.getElementById("TypeValidation").submit();
        document.getElementById("btnfrmsubmit").click();
      } else {
        $.notify({
          icon: "error",
          message: "Invalid Project Cost"
        }, {
          type: 'danger',
          timer: 2000,
          placement: {
            from: 'top',
            align: 'right'
          }
        });
      }
    });
  </script>
@endpush
