@extends('layouts.app', ['activePage' => 'about', 'menuParent' => 'about', 'titlePage' => __('About ProyekTanglaw')])

@section('content')
<div class="content">
  <div class="content">
    <div class="container-fluid">

          <div class="row">
            <div class="col-md-12">
                    <div class="card card-product">
                      <div class="card-body">
                        <center><img class="img" src="{{ asset('material')}}/img/proyek.png" width="40%"></center>
                        <h4 class="card-title">
                          About the logo
                        </h4>
                        <div class="card-description">
                          The logo depicts an “eye” with a lighthouse as its eyeball.
                          It means to watch or to guard which reflects the main objective of project monitoring – which is to ensure that projects are properly implemented.
                          The lighthouse suggests the archipelagic configuration of the MIMAROPA Region. It also symbolizes guidance which reflects the essence of project facilitation and problem-solving sessions.
                          The binary code at the bottom represents the digital platform and database of the eRPMES. 
                        </div>
                      </div>
                      <div class="card-footer">
                        <div class="stats">
                          <p class="card-category"><i class="material-icons">place</i> MIMAROPA</p>
                        </div>
                      </div>
                    </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <iframe width="1280px" height= "1500px" src= "https://forms.office.com/Pages/ResponsePage.aspx?id=zITAUhXNcUaKV8GVZbzfworvUYQVE7xLqGFAO7LHcTtUNEY0NUhaV1JEUUJEQjQ5T003MkhMR1ZNSC4u&embed=true" frameborder= "0" marginwidth= "0" marginheight= "0" style= "border: none; max-width:100%; max-height:100vh" allowfullscreen webkitallowfullscreen mozallowfullscreen msallowfullscreen> </iframe>
            </div>
          </div>
    </div>
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

      md.initVectorMap();

    });
  </script>
@endpush

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush