@extends('layouts.app', ['activePage' => 'addproject', 'menuParent' => 'addproject', 'titlePage' => __('Add Project')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">List of Projects</h4>
          </div>

          <div class="card-body"> 
            <div class="toolbar" align="right">
              <a href="{{ route('page.addpage') }}">
              <button type="button"class="btn btn-info btn-round" data-toggle="modal" data-target="#signupModal">
                <i class="material-icons">library_add</i>
                Add Project
              </button>
              </a>
<a href="{{ asset('material')}}/files/project_template.xlsx" target="_blank">
              <button class="btn btn-round btn-success" data-toggle="modal" data-target="#signupModal">
                <i class="material-icons">cloud_download</i>
                Download Project Import Template   
              </button>
            </a>
            <a href="{{ asset('material')}}/files/location_template.xlsx" target="_blank">
              <button class="btn btn-round btn-success" data-toggle="modal" data-target="#signupModal">
                <i class="material-icons">cloud_download</i>
                Download Location Import Template   
              </button>
            </a>

            </div>
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Project ID</th>
                    <th>Title</th>
                    <th>Funding Source</th>
                    <th>Mode of Implementation</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <th>Implementing Agency</th>
                    @endif
                    <th class="disabled-sorting text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects as $project)
                    <tr>
                        @if ($project->project_id == null)
                          <td>{{ $project->id }}</td>
                        @else
                          <td>{{ $project->project_id }}</td>
                        @endif
                        <td>{{ $project->title }}</td>
                        <td>{{ $project->type }}</td>
                        <td>{{ $project->mode }}</td>
                        <td>{{ $project->modified_by }}</td>
                        <td>{{ $project->updated_at->format('Y-m-d') }}</td>
                        @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                        <td>{{ $project->UACS_AGY_DSC }}</td>
                        @endif
                        <td class="text-right">
                          <?php $id = encrypt($project->id); ?>
                           <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg{{ $project->id }}"><i class="material-icons">pageview</i></button>
                           <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" onclick=" window.open('{{ asset('/editpage') }}/{{ $id }}','_self')"><i class="material-icons">dvr</i></button>
                           <a href="{{ asset('/printonepager') }}/{{$id}}" onclick="w = window.open(this.href);w.print();return false;w.close();"><button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="material-icons">print</i></button></a>
                           <!-- <form action="{{ asset('/deleteproject') }}/{{ $id }}" method="POST">{{ csrf_field() }}
                           <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this item?") }}') ? this.parentElement.submit() : ''">
                              <i class="material-icons">close</i>
                              <div class="ripple-container"></div>
                            </button>
                            </form> -->
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          @foreach ($projects as $project)
          <div class="modal fade bd-example-modal-lg{{ $project->id }}" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">{{ $project->title }}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Sector: {{ $project->sector }}<br>
                  Mode of Implementation: {{ $project->mode }}<br>
                  Project Schedule: {{ $project->start }} - {{ $project->end }}<br>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush