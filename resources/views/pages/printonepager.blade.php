<html>
  <head>
        <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('material') }}/img/apple-icon.png">
  <link rel="icon" type="image/png" href="{{ asset('material') }}/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    One Pager Project Report
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{ asset('material') }}/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{ asset('material') }}/demo/demo.css" rel="stylesheet" />
  </head>

  <body>
    <table border="1" width="100%">
      <tr>
        <td colspan="2"><h1>{{ $projects->title }}</h1></td>
      </tr>
      <tr>
        <td width="25%"><h5>Project Location: </h5></td>
        <td><h5>
          @foreach ($locations as $loc)
            {{ $loc->location }} : {{ $loc->longitude }} , {{ $loc->latitude }}
            <br>
          @endforeach
        </h5></td>
      </tr>
      <tr>
        <td width="25%"><h5>Implementing Agency: </h5></td>
        <td>
        <h5>
              @foreach ($agencies as $agency)
                @if ($agency->id == $projects->implementingagency)
                  {{ $agency->UACS_AGY_DSC }}
                @endif
              @endforeach
        </h5>
        </td>
      </tr>
      <tr>
        <td width="25%"><h5>Spatial Coverage: </h5></td>
        <td>
        <h5>
          @foreach ($provinces as $province)
                        @foreach($provincesprojects as $provincesproject)
                          @if($provincesproject->province_id == $province->id)
                            {{ $province->province }}
                          @endif
                        @endforeach
          @endforeach
        </h5>
        </td>
      </tr>
      <tr>
        <td width="25%"><h5>Sector: </h5></td>
        <td>
        <h5>
          @foreach ($sectors as $sector)

                        @if ($sector->id == $projects->sector)
                          {{ $sector->sector }}
                        @endif
                        
                    @endforeach
        </h5>
        </td>
      </tr>
      <tr>
        <td width="25%"><h5>Funding Source: </h5></td>
        <td>
        <h5>
@foreach ($sources as $source)
                        @if ($source->id == $projects->fundingsource)
                          {{ $source->type }}
                        @endif
                        
                    @endforeach
        </h5>
        </td>
      </tr>
            <tr>
        <td width="25%"><h5>Mode of Implementation: </h5></td>
        <td>
        <h5>
@if ($projects->mode == "By contract")
                          By contract
                        @endif
@if ($projects->mode == "By admin")
                          By admin
                        @endif
@if ($projects->mode == "Others")
                          Others
                        @endif
        </h5>
        </td>
      </tr>
       <tr>
        <td width="25%"><h5>Project Schedule: </h5></td>
        <td>
        <h5>
          {{ $projects->start }} - {{ $projects->end }}
        </h5>
        </td>
      </tr>
      <tr>
        <td width="25%"><h5>Project Category: </h5></td>
        <td>
        <h5>
@foreach ($categories as $category)
                        @if ($category->id == $projects->category)
                          {{ $category->category }}
                        @endif
                    @endforeach
        </h5>
        </td>
      </tr>
      <tr>
        <td width="25%"><h5>Project Status: </h5></td>
        <td>
        <h5>
@if ($projects->status == "Ongoing")
                          Ongoing
                        @endif
@if ($projects->status == "Completed")
                          Completed
                        @endif
        </h5>
        </td>
      </tr>
      <tr>
        <td width="25%"><h5>Project Cost: </h5></td>
        <td>
        <h5>
          {{ $projects->cost }}
        </h5>
        </td>
      </tr>
    </table>
  </body>
</html>
                   