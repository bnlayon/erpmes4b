@extends('layouts.app', ['activePage' => 'form2', 'menuParent' => 'form1', 'titlePage' => __('RPMES Form 2')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form2') }}">RPMES Form 2</a></li>
            <li class="breadcrumb-item"><a href="{{ asset('form2submission') }}/{{ $getagency->period }}">RPMES Form 2 Submission</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $agency->UACS_AGY_DSC }}</li>
          </ol>
        </nav>
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            <h4 class="card-title"> RPMES 2 | {{ $agency->UACS_AGY_DSC }} | {{ $getagency->period }}</h4>
          </div>
          <div class="card-body">
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" border="1">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th>Allocation as of reporting period</th>
                    <th>Releases as of reporting period</th>
                    <th>Obligations as of reporting period</th>
                    <th>Expenditures as of reporting period</th>
                    <th>Output Indicator</th>
                    <th>Target to Date</th>
                    <th>Actual Accomplishment to Date</th>
                    <th>Male</th>
                    <th>Female</th>
                    <th>Remarks</th>
                    <!-- <th class="disabled-sorting">Details</th> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form2 as $r1project)
                  <tr> 
                    <td>{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    <td>{{ $r1project->alloc_asof }}</td>
                    <td>{{ $r1project->releases_asof }}</td>
                    <td>{{ $r1project->obligations_asof }}</td>
                    <td>{{ $r1project->expenditures_asof }}</td>
                    <td>{{ $r1project->oi }}</td>
                    <td>{{ $r1project->ttd }}</td>
                    <td>{{ $r1project->atd }}</td>
                    <td>{{ $r1project->male }}</td>
                    <td>{{ $r1project->female }}</td>
                    <td>{{ $r1project->nro_remarks }}</td>
                    <!-- <td><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">Financial Status</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">Physical Status</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">Employment Generation</button></td> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <td>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalreview2{{ $r1project->id }}">REVIEW</button>
                    </td>
                    @endif
                  </tr>
                    <!-- <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Financial Status (in PhP M)</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body" style="  overflow: scroll;  height: 500px;">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">ALLOCATION</label>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="alloc_asof" required="true" value="{{ $r1project->alloc_asof }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">For the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="alloc_month" required="true" value="{{ $r1project->alloc_month }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">RELEASES</label>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="releases_asof" required="true" value="{{ $r1project->releases_asof }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">For the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="releases_month" required="true" value="{{ $r1project->releases_month }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">OBLIGATIONS</label>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="obligations_asof" required="true" value="{{ $r1project->obligations_asof }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">For the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="obligations_month" required="true" value="{{ $r1project->obligations_month }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">EXPENDITURES</label>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">As of Reporting Period</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="expenditures_asof" required="true" value="{{ $r1project->expenditures_asof }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">For the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="expenditures_month" required="true" value="{{ $r1project->expenditures_month }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modalpercent{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Physical Status in %</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Output Indicator</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="text" name="oi" required="true" value="{{ $r1project->oi }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Target to Date</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="ttd" required="true" value="{{ $r1project->ttd }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Target for the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="tftm" required="true" value="{{ $r1project->tftm }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Actual to Date</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="atd" required="true" value="{{ $r1project->atd }}"/>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Actual for the Month</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="aftm" required="true" value="{{ $r1project->aftm }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaleg{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Employment Generation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Male</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="male" required="true" value="{{ $r1project->male }}" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Female</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <input class="form-control" type="number" name="female" required="true" value="{{ $r1project->female }}"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div> -->
                    <div class="modal fade bd-example-modal-lg modalreview2{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/review2') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Review Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="nro_remarks" name="nro_remarks" required="true">{{ $r1project->nro_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                  <!-- </tr> -->



                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "scrollX": true,
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: false,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush