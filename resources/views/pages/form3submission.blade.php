@extends('layouts.app', ['activePage' => 'form2agency', 'menuParent' => 'form2agency', 'titlePage' => __('RPMES Form 3')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form3') }}">RPMES Form 3</a></li>
            <li class="breadcrumb-item active" aria-current="page">RPMES Form 3 Submission</li>
          </ol>
        </nav>
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">format_list_numbered</i>
            </div>
            <h4 class="card-title">Project Exception Report | {{ $period }} Submissions</h4>
          </div>

          <div class="card-body">
<!--             <div class="toolbar" align="right">
              <a class="nav-link" href="{{ route('page.addpage') }}">
              <button class="btn btn-round" data-toggle="modal" data-target="#signupModal">
                <i class="material-icons">library_add</i>
                Add RPMES Form 1
              </button>
            </a>
            </div> -->
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Agency</th>
                    <th>Status of Agency Submission</th>
                    <th>NRO Status of Review</th>
                    <th class="disabled-sorting text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($form3 as $form)
                  <tr>
                    <td>{{ $form->UACS_AGY_DSC }}</td>
                    <td>{{ $form->status }}</td>
                    <td>{{ $form->nro_status_review }}</td>
                    <td>
                      @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                      <button type="button" class="btn btn-primary btn-sm" onclick=" window.open('{{ asset('/form3submissionview') }}/{{ $form->id }}','_self')">Review Projects</button>
                      @if($form->status == 'Submitted')
                        <form action="{{ asset('/form3revert') }}/{{ $form->id }}" method="POST">{{ csrf_field() }}
                          <button type="submit" class="btn btn-warning btn-sm">Revert Submission</button>
                        </form>
                        @endif
                      @elseif(auth()->user()->role_id == 3)
                      <button type="button" class="btn btn-primary btn-sm" onclick=" window.open('{{ asset('/form3submissionview') }}/{{ $form->id }}','_self')">View Projects</button>
                      @else
                      @endif
                      @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                      <button class="btn btn-danger btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalreview{{ $form->id }}">Review Submission</button>
                      @endif
<!--                       <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg">Export</button> -->
                    </td>
                  </tr>
                  <div class="modal fade bd-example-modal-lg modalreview{{ $form->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form action="{{ asset('/form3review') }}/{{ $form->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Review Submission</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="nro_remarks" name="nro_remarks" required="true">{{ $form->nro_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>
                  @endforeach


                 
                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush