@extends('layouts.app', ['activePage' => 'form4', 'menuParent' => 'form4', 'titlePage' => __('RPMES Form 4')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form4agency') }}">RPMES Form 4</a></li>
            @if($view_only == 0)
            <li class="breadcrumb-item active" aria-current="page">Edit RPMES Form 4</li>
            @else
            <li class="breadcrumb-item active" aria-current="page">View RPMES Form 4</li>
            @endif
          </ol>
        </nav>
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            @if($view_only == 0)
            <h4 class="card-title">Edit RPMES 4 | {{ $form4->period }} | Status: {{ $form4->status }}</h4>
            @else
            <h4 class="card-title">View RPMES 4 | {{ $form4->period }} | Status: {{ $form4->status }}</h4>
            @endif
          </div>
          <div class="card-body">
            <div class="toolbar" align="right">
            @if($view_only == 0)
            <button type="button"class="btn btn-info btn-round" data-toggle="modal" data-target=".addproject"><i class="material-icons">library_add</i>
                Add Project</button>
            @endif
              <a href="{{ asset('/exportform4agency') }}/{{ $form4->id }}">
                  <button class="btn btn-round btn-success" data-toggle="modal" data-target="#signupModal"><i class="material-icons">import_export</i> Export</button></a>
              <?php $id = encrypt($form4->id); ?>
              <a href="{{ asset('/previewform4agency') }}/{{ $id }}" target="_blank">
                  <button class="btn btn-round btn-warning" data-toggle="modal" data-target="#signupModal"><i class="material-icons">preview</i> Preview</button></a>
            </div>
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th class="disabled-sorting">Details</th>
                    @if($view_only == 0)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form4 as $r1project)
                  <tr> 
                    <td>{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    <td><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}"> Project Details</button></td>
                    @if($view_only == 0)
                    <td>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalsubmit{{ $r1project->id }}">ENDORSE</button>
                      <form action="{{ asset('/deleteprojectform4') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                           <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this item?") }}') ? this.parentElement.submit() : ''">
                              <i class="material-icons">close</i>
                              <div class="ripple-container"></div>
                            </button>
                            </form>
                    </td>
                    @endif
                  </tr>
                    <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">{{ $r1project->title }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form4agency_submitpo') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div id="accordion" role="tablist">
                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingOne">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseOne{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseOne">
                                      Project Objective/s
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseOne{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Project Objective/s</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <textarea class="form-control" id="objectives" name="objectives" required="true" rows="10">{{ $r1project->objectives }}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingTwo">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseTwo{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseTwo">
                                      Results Indicator/Target
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseTwo{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Results Indicator/Target</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <textarea class="form-control" id="indicator" name="indicator" required="true" rows="10">{{ $r1project->indicator }}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="card card-collapse">
                                <div class="card-header" role="tab" id="headingThree">
                                  <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseThree{{ $r1project->id }}" aria-expanded="false" aria-controls="collapseThree">
                                      Observed Results
                                      <i class="material-icons">keyboard_arrow_down</i>
                                    </a>
                                  </h5>
                                </div>
                                <div id="collapseThree{{ $r1project->id }}" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                  <div class="card-body">
                                    <div class="row">
                                      <label class="col-sm-2 col-form-label">Observed Results</label>
                                      <div class="col-sm-9">
                                        <div class="form-group">
                                          <textarea class="form-control" id="results" name="results" required="true" rows="10">{{ $r1project->results }}</textarea>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            @if($view_only == 0)
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modalsubmit{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/endorse4') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Endorse Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="agency_remarks" name="agency_remarks" required="true">{{ $r1project->agency_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            @if($view_only == 0)
                            <button type="submit" class="btn btn-primary">Save</button>
                            @endif
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

            <div class="modal fade addproject" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Add Project to RPMES Form 4</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form action="{{ asset('/form4agency_projects_add') }}/{{ $form4->id }}" method="POST">{{ csrf_field() }}
                <div class="modal-body">
                 <div class="form-group">
                    <select class="form-control selectpicker" data-style="btn btn-link" id="project" required="true" name="project">
                      <option value=""></option>
                    @foreach ($projects as $project)
                      <option value="{{ $project->id }}">{{ $project->title }}</option>
                    @endforeach
                    </select>
                  </div>
                </div>
                
                <div class="modal-footer">
                  <button class="btn btn-primary btn-fill" type="submit"><i class="material-icons">add</i></button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
                </div>
              </div>
            </div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush