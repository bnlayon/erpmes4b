@extends('layouts.app', ['activePage' => 'spatial', 'menuParent' => 'spatial', 'titlePage' => __('Spatial Projects')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">List of Projects</h4>
          </div>

          <div class="card-body">
            <div class="toolbar" align="right">
              <!-- <a class="nav-link" href="{{ route('page.addpage') }}">
              <button class="btn btn-round" data-toggle="modal" data-target="#signupModal">
                <i class="material-icons">library_add</i>
                Add Project
              </button>
            </a> -->
            </div>
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Funding Source</th>
                    <th>Mode of Implementation</th>
                    
                    <th>Implementing Agency</th>
                    <th>Status</th>
                    <th class="disabled-sorting text-right">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects as $project)
                    <tr>
                        <td>{{ $project->title }}</td>
                        <td>{{ $project->type }}</td>
                        <td>{{ $project->mode }}</td>
                       
                        <td>{{ $project->UACS_AGY_DSC }}</td>
                        <td>{{ $project->status }}</td>
                        <td class="text-right">
                          <?php $id = encrypt($project->id); ?>
                           <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg{{ $project->id }}"><i class="material-icons">pageview</i></button>
                           <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".bd-example-modal-lg" onclick=" window.open('{{ asset('/editpage') }}/{{ $id }}','_self')"><i class="material-icons">dvr</i></button>
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          @foreach ($projects as $project)
          <div class="modal fade bd-example-modal-lg{{ $project->id }}" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">{{ $project->title }}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  Sector: {{ $project->sector }}<br>
                  Mode of Implementation: {{ $project->mode }}<br>
                  Project Schedule: {{ $project->start }} - {{ $project->end }}<br>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatable').DataTable();
    });
  </script>
@endpush