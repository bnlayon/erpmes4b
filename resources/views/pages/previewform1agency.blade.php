<table border="1">
  <tr>
    <th></th>
    <th>(1)</th>
    <th>(2)</th>
    <th>(3)</th>
    <th>(4)</th>
    <th>(5)</th>
    <th>(6)</th>
    <th>(7)</th>
    <th>(8)</th>
    <th>(9)</th>
    <th>(10)</th>
    <th>(11)</th>
    <th>(12)</th>
    <th>(13)</th>
    <th>(14)</th>
    <th>(15)</th>
  </tr>
  <tr>
    <th></th>
    <th>
      (a) Name of Project<br>
      (b) Location<br>
      (c) Sector/Subsector<br>
      (d) Funding Source<br>
      (e) Mode of Implementation<br>
      (f) Project Schedule<br>
    </th>
    <th>Total Project Target</th>
    <th>JAN</th>
    <th>FEB</th>
    <th>MAR</th>
    <th>APR</th>
    <th>MAY</th>
    <th>JUN</th>
    <th>JUL</th>
    <th>AUG</th>
    <th>SEP</th>
    <th>OCT</th>
    <th>NOV</th>
    <th>DEC</th>
    <th>TOTAL</th>
  </tr>

  <?php         
   $i = 14;
   $num = 1;

    foreach($form1projs as $form1proj){
      echo "<tr>";
        echo "<td>".$num."</td>";
        echo "<td>";
          echo $form1proj->title."<br>";
          echo $form1proj->sector."<br>";
          echo $form1proj->type."<br>";
          echo $form1proj->mode."<br>";
          echo $form1proj->start." - ".$form1proj->end."<br>";
        echo "</td>";
        echo "<td>";
          echo "FS"."<br>";
          echo "%"."<br>";
          echo "OI"."<br>";
          echo "EG"."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_1."<br>";
          echo $form1proj->pt_1."<br>";
          echo $form1proj->oi_1."<br>";
          echo "Male: ".$form1proj->eg_1." Female: ".$form1proj->eg_13."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_2."<br>";
          echo $form1proj->pt_2."<br>";
          echo $form1proj->oi_2."<br>";
          echo "Male: ".$form1proj->eg_2." Female: ".$form1proj->eg_14."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_3."<br>";
          echo $form1proj->pt_3."<br>";
          echo $form1proj->oi_3."<br>";
          echo "Male: ".$form1proj->eg_3." Female: ".$form1proj->eg_15."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_4."<br>";
          echo $form1proj->pt_4."<br>";
          echo $form1proj->oi_4."<br>";
          echo "Male: ".$form1proj->eg_4." Female: ".$form1proj->eg_16."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_5."<br>";
          echo $form1proj->pt_5."<br>";
          echo $form1proj->oi_5."<br>";
          echo "Male: ".$form1proj->eg_5." Female: ".$form1proj->eg_17."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_6."<br>";
          echo $form1proj->pt_6."<br>";
          echo $form1proj->oi_6."<br>";
          echo "Male: ".$form1proj->eg_6." Female: ".$form1proj->eg_18."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_7."<br>";
          echo $form1proj->pt_7."<br>";
          echo $form1proj->oi_7."<br>";
          echo "Male: ".$form1proj->eg_7." Female: ".$form1proj->eg_19."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_8."<br>";
          echo $form1proj->pt_8."<br>";
          echo $form1proj->oi_8."<br>";
          echo "Male: ".$form1proj->eg_8." Female: ".$form1proj->eg_20."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_9."<br>";
          echo $form1proj->pt_9."<br>";
          echo $form1proj->oi_9."<br>";
          echo "Male: ".$form1proj->eg_9." Female: ".$form1proj->eg_21."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_10."<br>";
          echo $form1proj->pt_10."<br>";
          echo $form1proj->oi_10."<br>";
          echo "Male: ".$form1proj->eg_10." Female: ".$form1proj->eg_22."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_11."<br>";
          echo $form1proj->pt_11."<br>";
          echo $form1proj->oi_11."<br>";
          echo "Male: ".$form1proj->eg_11." Female: ".$form1proj->eg_23."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_12."<br>";
          echo $form1proj->pt_12."<br>";
          echo $form1proj->oi_12."<br>";
          echo "Male: ".$form1proj->eg_12." Female: ".$form1proj->eg_24."<br>";
        echo "</td>";
        echo "<td>";
          echo $form1proj->fs_1 + $form1proj->fs_2 + $form1proj->fs_3 + $form1proj->fs_4 + $form1proj->fs_5 + $form1proj->fs_6 + $form1proj->fs_7 + $form1proj->fs_8 + $form1proj->fs_9 + $form1proj->fs_10 + $form1proj->fs_11 + $form1proj->fs_12."<br>";
          echo $form1proj->pt_1 + $form1proj->pt_2 + $form1proj->pt_3 + $form1proj->pt_4 + $form1proj->pt_5 + $form1proj->pt_6 + $form1proj->pt_7 + $form1proj->pt_8 + $form1proj->pt_9 + $form1proj->pt_10 + $form1proj->pt_11 + $form1proj->pt_12."<br><br>";
            $male = $form1proj->eg_1 + $form1proj->eg_2 + $form1proj->eg_3 + $form1proj->eg_4 + $form1proj->eg_5 + $form1proj->eg_6 + $form1proj->eg_7 + $form1proj->eg_8 + $form1proj->eg_9 + $form1proj->eg_10 + $form1proj->eg_11 + $form1proj->eg_12;
          echo "Male: ".$male."<br>";
          $female = $form1proj->eg_13 + $form1proj->eg_14 + $form1proj->eg_15 + $form1proj->eg_16 + $form1proj->eg_17 + $form1proj->eg_18 + $form1proj->eg_19 + $form1proj->eg_20 + $form1proj->eg_21 + $form1proj->eg_22 + $form1proj->eg_22 + $form1proj->eg_24;
          echo "Female: ".$female."<br>";
        echo "</td>";
      echo "</tr>";
      $num++;
    }
  ?>
</table>