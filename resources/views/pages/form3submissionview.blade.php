@extends('layouts.app', ['activePage' => 'form3', 'menuParent' => 'form3', 'titlePage' => __('RPMES Form 3')])


@section('content')


<div class="content">
  <div class="container-fluid">
    <div class="row">
      <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ asset('form3') }}">RPMES Form 3</a></li>
            <li class="breadcrumb-item"><a href="{{ asset('form3submission') }}/{{ $getagency->period }}">RPMES Form 3 Submission</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $agency->UACS_AGY_DSC }}</li>
          </ol>
        </nav>
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">library_add</i>
            </div>
            <h4 class="card-title"> RPMES 3 | {{ $agency->UACS_AGY_DSC }} | {{ $getagency->period }}</h4>
          </div>
          <div class="card-body">
            <div class="material-datatables">
              <table id="datatables2" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%" border="1">
                <thead>
                  <tr>
                    <th>Title</th>
                    <th>Modified By</th>
                    <th>Modified Date</th>
                    <th>Status</th>
                    <th>Findings</th>
                    <th>Possible Causes</th>
                    <th>Recommendations</th>
                    <!-- <th class="disabled-sorting">Details</th> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <th class="disabled-sorting">Actions</th>
                    @endif
                  </tr>
                </thead>
                <tbody>
                  @foreach ($projects_in_form3 as $r1project)
                  <tr> 
                    <td>{{ $r1project->title }}</td>
                    <td>{{ $r1project->modified_by }}</td>
                    <td>{{ $r1project->updated_at->format('Y-m-d') }}</td>
                    <td>{{ $r1project->status }}</td>
                    <td>
                      @if ($r1project->findings == null || $r1project->findings == '' || $r1project->findings == '-')
                      <a href="#" class="link-primary text-info" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">[Edit]</a>
                      @else
                      <a href="#" class="link-primary text-info" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">{{ $r1project->findings }}</a>
                      @endif
                    </td>
                    <td>
                      @if ($r1project->possible == null || $r1project->possible == '' || $r1project->possible == '-')
                      <a href="#" class="link-primary text-info" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">[Edit]</a>
                      @else
                      <a href="#" class="link-primary text-info" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">{{ $r1project->possible }}</a>
                      @endif
                    </td>
                    <td>
                      @if ($r1project->recommendations == null || $r1project->recommendations == '' || $r1project->recommendations == '-')
                      <a href="#" class="link-primary text-info" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">[Edit]</a>
                      @else
                      <a href="#" class="link-primary text-info" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">{{ $r1project->recommendations }}</a>
                      @endif
                    </td>
                    <!-- <td><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalfs{{ $r1project->id }}">Findings</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalpercent{{ $r1project->id }}">Possible Reasons/Causes</button><button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modaleg{{ $r1project->id }}">Recommendations</button></td> -->
                    @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
                    <td>
                      <button class="btn btn-primary btn-fill btn-sm" type="button" data-toggle="modal" data-target=".modalsubmit{{ $r1project->id }}">Review</button>
                    </td>
                    @endif
                  </tr>
                    <div class="modal fade bd-example-modal-lg modalfs{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form3agency_submitfs') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Findings</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="findings" name="findings" required="true">{{ $r1project->findings }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modalpercent{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form3agency_submitpt') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Possible Reasons/Causes</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="possible" name="possible" required="true">{{ $r1project->possible }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modaleg{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="{{ asset('/form3agency_submiteg') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Recommendations</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="recommendations" name="recommendations" required="true">{{ $r1project->recommendations }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button class="btn btn-primary btn-fill" type="submit">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    <div class="modal fade bd-example-modal-lg modalsubmit{{ $r1project->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <form class="form-horizontal" action="{{ asset('/review3') }}/{{ $r1project->id }}" method="POST">{{ csrf_field() }}
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Review Project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <label class="col-sm-2 col-form-label">Remarks</label>
                              <div class="col-sm-9">
                                <div class="form-group">
                                  <textarea class="form-control" id="nro_remarks" name="nro_remarks" required="true">{{ $r1project->nro_remarks }}</textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                      </form>
                    </div>
                  </tr>
                  @endforeach


                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->
  </div>
</div>

@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables2').DataTable({
        "pagingType": "full_numbers",
        "scrollX": true,
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: false,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });

      var table = $('#datatables2').DataTable();
    });
  </script>
@endpush