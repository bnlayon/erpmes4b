<footer class="footer">
  <div class="container">
    <nav class="float-left">
    <ul>
        
        <li>
        <a href="http://mimaropa.neda.gov.ph/" target="_blank">
            {{ __('About Us') }}
        </a>
        </li>
    </ul>
    </nav>
    <div class="copyright float-right">
    &copy;
    <script>
      document.write(new Date().getFullYear())
    </script> Information and Communications Technology Staff, BNL
    </div>
  </div>
</footer>