<div class="sidebar" data-color="azure" data-background-color="black" data-image="{{ asset('material') }}/img/mimaropa.jpg">
  <!--
    Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

    Tip 2: you can also add an image using data-image tag
-->
  <div class="logo">
    <a href="https://www.neda.gov.ph/" class="simple-text logo-mini">
      <img class="img" src="{{ asset('material')}}/img/neda-logo.png" height="30">
    </a>
    <a href="https://www.neda.gov.ph/" class="simple-text logo-normal">
      {{ __('ProyekTanglaw') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <div class="user">
      <div class="photo">
        <img src="{{ auth()->user()->profilePicture() }}" />
      </div>
      <div class="user-info">
        <a data-toggle="collapse" href="#collapseExample" class="username">
          <span>
            {{ auth()->user()->name }}
            <b class="caret"></b>
          </span>
        </a>
        <div class="collapse" id="collapseExample">
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> MP </span>
                <span class="sidebar-normal"> My Profile </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      @if(auth()->user()->role_id == 1)
      <li class="nav-item {{ ($menuParent == 'laravel' || $activePage == 'dashboard') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" {{ ($menuParent == 'laravel' || $activePage == 'dashboard') ? ' aria-expanded="true"' : '' }}>
          <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>
          <p>{{ __('Administration') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse {{ ($menuParent == 'dashboard' || $menuParent == 'laravel') ? ' show' : '' }}" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li>
            @can('manage-users', App\User::class)
              <li class="nav-item{{ $activePage == 'role-management' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('role.index') }}">
                  <span class="sidebar-mini"> RM </span>
                  <span class="sidebar-normal"> {{ __('Role Management') }} </span>
                </a>
              </li>
            @endcan
            @can('manage-users', App\User::class)
              <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('user.index') }}">
                  <span class="sidebar-mini"> UM </span>
                  <span class="sidebar-normal"> {{ __('User Management') }} </span>
                </a>
              </li>
            @endcan
            @can('manage-users', App\User::class)
              <li class="nav-item{{ $activePage == 'agency-management' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('agency.index') }}">
                  <span class="sidebar-mini"> AM </span>
                  <span class="sidebar-normal"> {{ __('Agency Management') }} </span>
                </a>
              </li>
            @endcan
            @can('manage-users', App\User::class)
              <li class="nav-item{{ $activePage == 'logs' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('page.logs') }}">
                  <span class="sidebar-mini"> SL </span>
                  <span class="sidebar-normal"> {{ __('System Logs') }} </span>
                </a>
              </li>
            @endcan
          </ul>
        </div>
      </li>
      @endif
      <li class="nav-item{{ $activePage == 'addproject' ? ' active' : '' }} ">
        <a class="nav-link" href="{{ route('page.addproject') }}">
          <i class="material-icons">storage</i>
          <p> Projects </p>
        </a>
      </li>
      <li class="nav-item {{ $menuParent == 'forms' ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#formsExamples" {{ $menuParent == 'forms' ? 'aria-expanded="true"' : '' }}>
          <i class="material-icons">content_paste</i>
          <p> eRPMES
            <b class="caret"></b>
          </p>
        </a>
        @if(auth()->user()->role_id == 1 || auth()->user()->role_id == 3)
        <div class="collapse {{ $menuParent == 'forms' ? 'show' : '' }}" id="formsExamples">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'form1' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form1') }}">
                <span class="sidebar-mini"> 1 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 1') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form2' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form2') }}">
                <span class="sidebar-mini"> 2 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 2') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form3' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form3') }}">
                <span class="sidebar-mini"> 3 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 3') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form4' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form4') }}">
                <span class="sidebar-mini"> 4 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 4') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form5' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form5') }}">
                <span class="sidebar-mini"> 5 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 5') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form6' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form6') }}">
                <span class="sidebar-mini"> 6 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 6') }} </span>
              </a>
            </li>
          </ul>
        </div>
        @elseif(auth()->user()->role_id == 2)
        <div class="collapse {{ $menuParent == 'forms' ? 'show' : '' }}" id="formsExamples">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'form1agency' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form1agency') }}">
                <span class="sidebar-mini"> 1 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 1') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form2agency' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form2agency') }}">
                <span class="sidebar-mini"> 2 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 2') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form3agency' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form3agency') }}">
                <span class="sidebar-mini"> 3 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 3') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'form4agency' ? ' active' : '' }} ">
              <a class="nav-link" href="{{ route('page.form4agency') }}">
                <span class="sidebar-mini"> 4 </span>
                <span class="sidebar-normal"> {{ __('RPMES Form 4') }} </span>
              </a>
            </li>
          </ul>
        </div>
        @endif
      </li>
      @if(auth()->user()->agency_id == 47 || auth()->user()->agency_id == 48 || auth()->user()->agency_id == 49 || auth()->user()->agency_id == 50 || auth()->user()->agency_id == 51)
          <li class="nav-item{{ $activePage == 'spatial' ? ' active' : '' }} ">
            <a class="nav-link" href="{{ route('page.spatial') }}">
              <i class="material-icons">storage</i>
              <p> Spatial Projects </p>
            </a>
          </li>
      @endif
      <li class="nav-item{{ $activePage == 'reportgen' ? ' active' : '' }} ">
        <a class="nav-link" href="{{ route('page.reportgen') }}">
          <i class="material-icons">insert_chart</i>
          <p> Report Generation </p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'about' ? ' active' : '' }} ">
        <a class="nav-link" href="{{ route('page.about') }}">
          <i class="material-icons">remove_red_eye</i>
          <p> About ProyekTanglaw </p>
        </a>
      </li>
    </ul>
  </div>
</div>
