@extends('layouts.app', ['activePage' => 'agency-management', 'menuParent' => 'laravel', 'titlePage' => __('Agency Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">account_balance</i>
                </div>
                <h4 class="card-title">{{ __('Agencies') }}</h4>
              </div>
              <div class="card-body">
                @can('manage-users', App\User::class)
                  <div class="row">
                    <div class="col-12 text-right">
                      <a href="{{ route('agency.create') }}" class="btn btn-sm btn-rose">{{ __('Add agency') }}</a>
                    </div>
                  </div>
                @endcan
                <div class="table-responsive">
                  <table id="datatables" class="table table-striped table-no-bordered table-hover" style="display:none">
                    <thead class="text-primary">
                      <th>
                          {{ __('Abbreviation') }}
                      </th>
                      <th>
                        {{ __('Name') }}
                      </th>
                      <th>
                        {{ __('Head') }}
                      </th>
                      @can('manage-users', App\User::class)
                        <th class="text-right">
                          {{ __('Actions') }}
                        </th>
                      @endcan
                    </thead>
                    <tbody>
                      @foreach($agencies as $agency)
                        <tr>
                          <td>
                            {{ $agency->Abbreviation }}
                          </td>
                          <td>
                            {{ $agency->UACS_AGY_DSC }}
                          </td>
                          <td>
                            {{ $agency->head }}
                          </td>
                          @can('manage-users', App\User::class)
                            <td class="td-actions text-right">
                              <form action="{{ route('agency.destroy', $agency) }}" method="post">
                                @csrf
                                @method('delete')
                              <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('agency.edit', $agency) }}" data-original-title="" title="">
                                  <i class="material-icons">edit</i>
                                  <div class="ripple-container"></div>
                                </a>
                              
                                <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this agency?") }}') ? this.parentElement.submit() : ''">
                                  <i class="material-icons">close</i>
                                  <div class="ripple-container"></div>
                                </button>
                              </form>
                            </td>
                          @endcan
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    $('#datatables').fadeIn(1100);
    $('#datatables').DataTable({
      "pagingType": "full_numbers",
      "lengthMenu": [
        [10, 25, 50, -1],
        [10, 25, 50, "All"]
      ],
      responsive: true,
      language: {
        search: "_INPUT_",
        searchPlaceholder: "Search agencies",
      },
      "columnDefs": [
        { "orderable": false, "targets": 3 },
      ],        
      "order": [[ 1, "asc" ]],
    });
  });
</script>
@endpush