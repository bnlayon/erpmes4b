@extends('layouts.app', ['activePage' => 'agency-management', 'menuParent' => 'laravel', 'titlePage' => __('Agency Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('agency.update', $agency) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')

            <div class="card ">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="material-icons">account_balance</i>
                </div>
                <h4 class="card-title">{{ __('Edit Agency') }}</h4>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('agency.index') }}" class="btn btn-sm btn-rose">{{ __('Back to list') }}</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('UACS_AGY_DSC') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('UACS_AGY_DSC') ? ' is-invalid' : '' }}" name="UACS_AGY_DSC" id="input-UACS_AGY_DSC" type="text" placeholder="{{ __('Name') }}" value="{{ old('name', $agency->UACS_AGY_DSC) }}" required="true" aria-required="true"/>
                      @include('alerts.feedback', ['field' => 'UACS_AGY_DSC'])
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Abbreviation') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('Abbreviation') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('Abbreviation') ? ' is-invalid' : '' }}" name="Abbreviation" id="input-Abbreviation" type="text" placeholder="{{ __('Abbreviation') }}" value="{{ old('Abbreviation', $agency->Abbreviation) }}" required="true" aria-required="true"/>
                      @include('alerts.feedback', ['field' => 'Abbreviation'])
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <label class="col-sm-2 col-form-label label-checkbox">Is it a sub-unit?</label>
                  <div class="col-sm-10 checkbox-radios">
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="subunit" onChange="loadMontherAgency()" value="1" id="subunitYes" @if($agency->motheragency_id != null) checked @endif>Yes
                        <span class="circle">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="subunit" onChange="loadMontherAgency()" value="0" id="subunitNo" @if($agency->motheragency_id == null) checked @endif>No
                        <span class="circle">
                          <span class="check"></span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>

                <div class="row" id="divmotheragency" @if($agency->motheragency_id == null) hidden @endif>
                  <label class="col-sm-2 col-form-label">{{ __('Mother Agency') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('motheragency_id') ? ' has-danger' : '' }}">
                      <select class="selectpicker col-sm-12" name="motheragency_id" data-live-search="true" data-style="select-with-transition" title="Select Mother Agency" data-size="12">
                        @foreach ($agencies as $magency)
                          @if ($magency->id != $agency->id)
                            <option value="{{ $magency->id }}" @if(old('motheragency_id', $magency->id) == $agency->motheragency_id) selected="selected" @endif>{{ $magency->UACS_AGY_DSC }}</option>
                          @endif
                        @endforeach
                      </select>
                      @include('alerts.feedback', ['field' => 'motheragency_id'])
                    </div>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Head') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('head') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('head') ? ' is-invalid' : '' }}" name="head" id="input-head" type="text" placeholder="{{ __('Name') }}" value="{{ old('head', $agency->head) }}"/>
                      @include('alerts.feedback', ['field' => 'head'])
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-rose">{{ __('Save') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection


@push('js')
  <script>
    function loadMontherAgency() {
      if (document.getElementById('subunitYes').checked == true) {
        document.getElementById('divmotheragency').hidden = false;
      } else {
        document.getElementById('divmotheragency').hidden = true;
      }
    }
  </script>
@endpush