@extends('layouts.app', [
  'class' => 'off-canvas-sidebar',
  'classPage' => 'login-page',
  'activePage' => '',
  'title' => __('NEDA MIMAROPA'),
  'pageBackground' => asset("material").'/img/mimaropa.jpg'
])

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12 ml-auto mr-auto">
      <div class="card card-signup">
        <h2 class="card-title text-center"><br>{{ __('ProyekTanglaw Dashboard') }}</h2>
        <div class="card-body">
          <center>
            <iframe width="1024" height="530" src="https://app.powerbi.com/view?r=eyJrIjoiYTkwNWQyNmYtOTk0YS00YmIxLTg1NTQtMGU0NjgzOGI3ODJiIiwidCI6IjUyYzA4NGNjLWNkMTUtNDY3MS04YTU3LWMxOTU2NWJjZGZjMiIsImMiOjEwfQ%3D%3D&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe>
          </center>
          <br><br>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      md.checkFullPageBackgroundImage();
    });
  </script>
@endpush
