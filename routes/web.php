<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('pages.welcome');
})->name('welcome');

Route::get('public', function () {
  return view('auth.public');
})->name('public');

Auth::routes();

Route::get('home', 'HomeController@index')->name('home');
Route::get('dashboard', 'HomeController@index')->name('home');
Route::get('logs', 'HomeController@logs')->name('page.logs');
Route::get('pricing', 'ExamplePagesController@pricing')->name('page.pricing');
Route::get('lock', 'ExamplePagesController@lock')->name('page.lock');
Route::get('error', ['as' => 'page.error', 'uses' => 'ExamplePagesController@error']);

Route::group(['middleware' => 'auth'], function () {
    Route::resource('category', 'CategoryController', ['except' => ['show']]);
    Route::resource('tag', 'TagController', ['except' => ['show']]);
    Route::resource('item', 'ItemController', ['except' => ['show']]);
    Route::resource('role', 'RoleController', ['except' => ['show', 'destroy']]);
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::resource('agency', 'AgencyController', ['except' => ['show']]);
    
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    
    Route::get('rtl-support', ['as' => 'page.rtl-support', 'uses' => 'ExamplePagesController@rtlSupport']);
    Route::get('timeline', ['as' => 'page.timeline', 'uses' => 'ExamplePagesController@timeline']);
    Route::get('widgets', ['as' => 'page.widgets', 'uses' => 'ExamplePagesController@widgets']);
    Route::get('charts', ['as' => 'page.charts', 'uses' => 'ExamplePagesController@charts']);
    Route::get('calendar', ['as' => 'page.calendar', 'uses' => 'ExamplePagesController@calendar']);

    Route::get('buttons', ['as' => 'page.buttons', 'uses' => 'ComponentPagesController@buttons']);
    Route::get('grid-system', ['as' => 'page.grid', 'uses' => 'ComponentPagesController@grid']);
    Route::get('panels', ['as' => 'page.panels', 'uses' => 'ComponentPagesController@panels']);
    Route::get('sweet-alert', ['as' => 'page.sweet-alert', 'uses' => 'ComponentPagesController@sweetAlert']);
    Route::get('notifications', ['as' => 'page.notifications', 'uses' => 'ComponentPagesController@notifications']);
    Route::get('icons', ['as' => 'page.icons', 'uses' => 'ComponentPagesController@icons']);
    Route::get('typography', ['as' => 'page.typography', 'uses' => 'ComponentPagesController@typography']);
    
    Route::get('regular-tables', ['as' => 'page.regular_tables', 'uses' => 'TablePagesController@regularTables']);
    Route::get('extended-tables', ['as' => 'page.extended_tables', 'uses' => 'TablePagesController@extendedTables']);
    Route::get('datatable-tables', ['as' => 'page.datatable_tables', 'uses' => 'TablePagesController@datatableTables']);

    Route::get('regular-form', ['as' => 'page.regular_forms', 'uses' => 'FormPagesController@regularForms']);
    Route::get('extended-form', ['as' => 'page.extended_forms', 'uses' => 'FormPagesController@extendedForms']);
    Route::get('validation-form', ['as' => 'page.validation_forms', 'uses' => 'FormPagesController@validationForms']);
    Route::get('wizard-form', ['as' => 'page.wizard_forms', 'uses' => 'FormPagesController@wizardForms']);

    Route::get('google-maps', ['as' => 'page.google_maps', 'uses' => 'MapPagesController@googleMaps']);
    Route::get('fullscreen-maps', ['as' => 'page.fullscreen_maps', 'uses' => 'MapPagesController@fullscreenMaps']);
    Route::get('vector-maps', ['as' => 'page.vector_maps', 'uses' => 'MapPagesController@vectorMaps']);

    Route::get('manual', ['as' => 'page.manual', 'uses' => 'HomeController@manual']);
    Route::get('usermanual', ['as' => 'page.usermanual', 'uses' => 'HomeController@usermanual']);
    //NEW
    Route::get('addproject', ['as' => 'page.addproject', 'uses' => 'AddProjectController@addproject']);
    Route::get('addpage', ['as' => 'page.addpage', 'uses' => 'AddProjectController@addpage']);
    Route::post('addpage', ['as' => 'page.addpage', 'uses' => 'AddProjectController@saveproject']);
    Route::get('editpage/{id}', ['as' => 'page.editpage', 'uses' => 'AddProjectController@editpage']);
    Route::post('editpage/{id}', ['as' => 'page.editpage', 'uses' => 'AddProjectController@editsave']);
    Route::get('reportgen', ['as' => 'page.reportgen', 'uses' => 'ReportGenController@reportgen']);
    Route::get('spatial', ['as' => 'page.spatial', 'uses' => 'AddProjectController@spatial']);
    Route::post('deleteproject/{id}', ['as' => 'page.deleteproject', 'uses' => 'AddProjectController@deleteproject']);
    //Form 1
    Route::get('form1agency', ['as' => 'page.form1agency', 'uses' => 'EFormsController@form1agency']);
    Route::post('form1agency_add/{id}', ['as' => 'page.form1agency_add', 'uses' => 'EFormsController@form1agency_add']);
    Route::get('editform1/{id}', ['as' => 'page.editform1', 'uses' => 'EFormsController@editform1']);
    Route::post('form1agency_projects_add/{id}', ['as' => 'page.form1agency_projects_add', 'uses' => 'EFormsController@form1agency_projects_add']);
    Route::post('endorse/{id}', ['as' => 'page.endorse', 'uses' => 'EFormsController@endorse']); 
    Route::post('form1agency_submit/{id}', ['as' => 'page.form1agency_submit', 'uses' => 'EFormsController@form1agency_submit']); 
    Route::post('form1agency_submitfs/{id}', ['as' => 'page.form1agency_submitfs', 'uses' => 'EFormsController@form1agency_submitfs']); 
    Route::post('form1agency_submitpt/{id}', ['as' => 'page.form1agency_submitpt', 'uses' => 'EFormsController@form1agency_submitpt']); 
    Route::post('form1agency_submitoi/{id}', ['as' => 'page.form1agency_submitoi', 'uses' => 'EFormsController@form1agency_submitoi']);
    Route::post('form1agency_submiteg/{id}', ['as' => 'page.form1agency_submiteg', 'uses' => 'EFormsController@form1agency_submiteg']); 
    Route::post('deleteprojectform1/{id}', ['as' => 'page.deleteprojectform1', 'uses' => 'EFormsController@deleteprojectform1']);
    Route::post('deleteprojectform2/{id}', ['as' => 'page.deleteprojectform2', 'uses' => 'EFormsController@deleteprojectform2']);
    Route::post('deleteprojectform3/{id}', ['as' => 'page.deleteprojectform3', 'uses' => 'EFormsController@deleteprojectform3']);
    Route::post('deleteprojectform4/{id}', ['as' => 'page.deleteprojectform4', 'uses' => 'EFormsController@deleteprojectform4']);
    Route::post('deleteprojectform5/{id}', ['as' => 'page.deleteprojectform5', 'uses' => 'EFormsController@deleteprojectform5']);
    Route::post('deleteprojectform6/{id}', ['as' => 'page.deleteprojectform6', 'uses' => 'EFormsController@deleteprojectform6']);
    //Form 1 MIMAROPA
    Route::get('form1', ['as' => 'page.form1', 'uses' => 'EFormsControllerAdmin@form1']);
    Route::post('addform1', ['as' => 'page.addform1', 'uses' => 'EFormsControllerAdmin@addform1']);
    Route::post('lockform1/{id}', ['as' => 'page.lockform1', 'uses' => 'EFormsControllerAdmin@lockform1']);
    Route::post('unlock/{id}', ['as' => 'page.unlock', 'uses' => 'EFormsControllerAdmin@unlock']);
    Route::get('form1submission/{id}', ['as' => 'page.form1submission', 'uses' => 'EFormsControllerAdmin@form1submission']);
    Route::get('form1submissionview/{id}', ['as' => 'page.form1submissionview', 'uses' => 'EFormsControllerAdmin@form1submissionview']);
    Route::post('review/{id}', ['as' => 'page.review', 'uses' => 'EFormsControllerAdmin@review']);
    Route::post('formreview/{id}', ['as' => 'page.formreview', 'uses' => 'EFormsControllerAdmin@formreview']);
    // Route::get('/addproject', 'AddProjectController@addproject')->name('addproject');
//SAMPLE PUSH TO LANCE
    //Form 2 MIMAROPA
    Route::get('form2', ['as' => 'page.form2', 'uses' => 'EFormsControllerAdmin@form2']);
    Route::post('addform2', ['as' => 'page.addform2', 'uses' => 'EFormsControllerAdmin@addform2']);
    Route::post('lockform2/{id}', ['as' => 'page.lockform2', 'uses' => 'EFormsControllerAdmin@lockform2']);
    Route::post('unlock2/{id}', ['as' => 'page.unlock2', 'uses' => 'EFormsControllerAdmin@unlock2']);
    Route::get('form2submission/{id}', ['as' => 'page.form2submission', 'uses' => 'EFormsControllerAdmin@form2submission']);
    Route::post('form2review/{id}', ['as' => 'page.form2review', 'uses' => 'EFormsControllerAdmin@form2review']);   
    Route::get('form2submissionview/{id}', ['as' => 'page.form2submissionview', 'uses' => 'EFormsControllerAdmin@form2submissionview']);
    Route::post('review2/{id}', ['as' => 'page.review2', 'uses' => 'EFormsControllerAdmin@review2']); 

    //Form 2 Agency
    Route::get('form2agency', ['as' => 'page.form2agency', 'uses' => 'EFormsController@form2agency']);
    Route::post('form2agency_add/{id}', ['as' => 'page.form2agency_add', 'uses' => 'EFormsController@form2agency_add']);
    Route::get('editform2/{id}', ['as' => 'page.editform2', 'uses' => 'EFormsController@editform2']);
    Route::post('form2agency_submit/{id}', ['as' => 'page.form2agency_submit', 'uses' => 'EFormsController@form2agency_submit']); 
    Route::post('form2agency_projects_add/{id}', ['as' => 'page.form2agency_projects_add', 'uses' => 'EFormsController@form2agency_projects_add']);
    Route::post('endorse2/{id}', ['as' => 'page.endorse2', 'uses' => 'EFormsController@endorse2']); 
    Route::post('form2agency_submitfs/{id}', ['as' => 'page.form2agency_submitfs', 'uses' => 'EFormsController@form2agency_submitfs']); 
    Route::post('form2agency_submiteg/{id}', ['as' => 'page.form2agency_submiteg', 'uses' => 'EFormsController@form2agency_submiteg']); 
    Route::post('form2agency_submitpt/{id}', ['as' => 'page.form2agency_submitpt', 'uses' => 'EFormsController@form2agency_submitpt']);

    //Form 3 MIMAROPA
    Route::get('form3', ['as' => 'page.form3', 'uses' => 'EFormsControllerAdmin@form3']);
    Route::post('addform3', ['as' => 'page.addform3', 'uses' => 'EFormsControllerAdmin@addform3']);
    Route::post('lockform3/{id}', ['as' => 'page.lockform3', 'uses' => 'EFormsControllerAdmin@lockform3']);
    Route::post('unlock3/{id}', ['as' => 'page.unlock3', 'uses' => 'EFormsControllerAdmin@unlock3']);
    Route::get('form3submission/{id}', ['as' => 'page.form3submission', 'uses' => 'EFormsControllerAdmin@form3submission']);
    Route::get('form3submissionview/{id}', ['as' => 'page.form3submissionview', 'uses' => 'EFormsControllerAdmin@form3submissionview']);
    Route::post('review3/{id}', ['as' => 'page.review3', 'uses' => 'EFormsControllerAdmin@review3']);
    Route::post('form3review/{id}', ['as' => 'page.form3review', 'uses' => 'EFormsControllerAdmin@form3review']);   

    //Form 3 Agency
    Route::get('form3agency', ['as' => 'page.form3agency', 'uses' => 'EFormsController@form3agency']);
    Route::post('form3agency_add/{id}', ['as' => 'page.form3agency_add', 'uses' => 'EFormsController@form3agency_add']);
    Route::get('editform3/{id}', ['as' => 'page.editform3', 'uses' => 'EFormsController@editform3']);
    Route::post('form3agency_projects_add/{id}', ['as' => 'page.form3agency_projects_add', 'uses' => 'EFormsController@form3agency_projects_add']);
    Route::post('form3agency_submitfs/{id}', ['as' => 'page.form3agency_submitfs', 'uses' => 'EFormsController@form3agency_submitfs']); 
    Route::post('form3agency_submiteg/{id}', ['as' => 'page.form3agency_submiteg', 'uses' => 'EFormsController@form3agency_submiteg']); 
    Route::post('form3agency_submitpt/{id}', ['as' => 'page.form3agency_submitpt', 'uses' => 'EFormsController@form3agency_submitpt']);
    Route::post('endorse3/{id}', ['as' => 'page.endorse3', 'uses' => 'EFormsController@endorse3']); 
    Route::post('form3agency_submit/{id}', ['as' => 'page.form3agency_submit', 'uses' => 'EFormsController@form3agency_submit']); 
 

    //Form 4 MIMAROPA
    Route::get('form4', ['as' => 'page.form4', 'uses' => 'EFormsControllerAdmin@form4']);
    Route::post('addform4', ['as' => 'page.addform4', 'uses' => 'EFormsControllerAdmin@addform4']);
    Route::post('lockform4/{id}', ['as' => 'page.lockform4', 'uses' => 'EFormsControllerAdmin@lockform4']);
    Route::post('unlock4/{id}', ['as' => 'page.unlock4', 'uses' => 'EFormsControllerAdmin@unlock4']);
    Route::get('form4submission/{id}', ['as' => 'page.form4submission', 'uses' => 'EFormsControllerAdmin@form4submission']);
    Route::get('form4submissionview/{id}', ['as' => 'page.form4submissionview', 'uses' => 'EFormsControllerAdmin@form4submissionview']);
    Route::post('review4/{id}', ['as' => 'page.review4', 'uses' => 'EFormsControllerAdmin@review4']); 
    Route::post('form4review/{id}', ['as' => 'page.form4review', 'uses' => 'EFormsControllerAdmin@form4review']);   

    //Form 4 Agency
    Route::get('form4agency', ['as' => 'page.form4agency', 'uses' => 'EFormsController@form4agency']);
    Route::post('form4agency_add/{id}', ['as' => 'page.form4agency_add', 'uses' => 'EFormsController@form4agency_add']);
    Route::get('editform4/{id}', ['as' => 'page.editform4', 'uses' => 'EFormsController@editform4']);
    Route::post('form4agency_projects_add/{id}', ['as' => 'page.form4agency_projects_add', 'uses' => 'EFormsController@form4agency_projects_add']);
    Route::post('form4agency_submitpo/{id}', ['as' => 'page.form4agency_submitpo', 'uses' => 'EFormsController@form4agency_submitpo']); 
    Route::post('form4agency_submitri/{id}', ['as' => 'page.form4agency_submitri', 'uses' => 'EFormsController@form4agency_submitri']); 
    Route::post('form4agency_submitor/{id}', ['as' => 'page.form4agency_submitor', 'uses' => 'EFormsController@form4agency_submitor']); 
    Route::post('endorse4/{id}', ['as' => 'page.endorse4', 'uses' => 'EFormsController@endorse4']);   
    Route::post('form4agency_submit/{id}', ['as' => 'page.form4agency_submit', 'uses' => 'EFormsController@form4agency_submit']); 

Route::get('form5', ['as' => 'page.form5', 'uses' => 'EFormsControllerAdmin@form5']);
Route::post('addform5', ['as' => 'page.addform5', 'uses' => 'EFormsControllerAdmin@addform5']);
Route::get('form5submission/{id}', ['as' => 'page.form5submission', 'uses' => 'EFormsControllerAdmin@form5submission']);
Route::post('form5add', ['as' => 'page.form5add', 'uses' => 'EFormsControllerAdmin@form5add']);

Route::get('form5submissionview/{id}', ['as' => 'page.form5submissionview', 'uses' => 'EFormsControllerAdmin@form5submissionview']);
Route::post('form5generate/{id}', ['as' => 'page.form5generate', 'uses' => 'EFormsControllerAdmin@form5generate']);
Route::post('form5addsubmit/{id}', ['as' => 'page.form5addsubmit', 'uses' => 'EFormsControllerAdmin@form5addsubmit']);
Route::get('exportform5/{id}', ['as' => 'page.exportform5', 'uses' => 'ReportController@exportform5']);

Route::get('form6', ['as' => 'page.form6', 'uses' => 'EFormsControllerAdmin@form6']);
Route::post('addform6', ['as' => 'page.addform6', 'uses' => 'EFormsControllerAdmin@addform6']);
Route::get('form6submission/{id}', ['as' => 'page.form6submission', 'uses' => 'EFormsControllerAdmin@form6submission']);
Route::post('form6add', ['as' => 'page.form6add', 'uses' => 'EFormsControllerAdmin@form6add']);
Route::get('form6submissionview/{id}', ['as' => 'page.form6submissionview', 'uses' => 'EFormsControllerAdmin@form6submissionview']);
Route::post('form6addsubmit/{id}', ['as' => 'page.form6addsubmit', 'uses' => 'EFormsControllerAdmin@form6addsubmit']);
Route::get('exportform6/{id}', ['as' => 'page.exportform6', 'uses' => 'ReportController@exportform6']);
    // Report Generation
    Route::get('exportform3agency/{id}', ['as' => 'page.exportform3agency', 'uses' => 'ReportController@exportform3agency']);
    Route::get('exportform4agency/{id}', ['as' => 'page.exportform4agency', 'uses' => 'ReportController@exportform4agency']);
    Route::get('exportform2agency/{id}', ['as' => 'page.exportform2agency', 'uses' => 'ReportController@exportform2agency']);
    Route::get('exportform1agency/{id}', ['as' => 'page.exportform1agency', 'uses' => 'ReportController@exportform1agency']);

    Route::get('previewform1agency/{id}', ['as' => 'page.previewform1agency', 'uses' => 'ReportController@previewform1agency']);
    Route::get('previewform4agency/{id}', ['as' => 'page.previewform4agency', 'uses' => 'ReportController@previewform4agency']);
    Route::get('previewform3agency/{id}', ['as' => 'page.previewform3agency', 'uses' => 'ReportController@previewform3agency']);
    Route::get('previewform2agency/{id}', ['as' => 'page.previewform2agency', 'uses' => 'ReportController@previewform2agency']);

    Route::post('generatereport', ['as' => 'page.generatereport', 'uses' => 'ReportGenController@generatereport']);
    Route::get('about', ['as' => 'page.about', 'uses' => 'HomeController@about']);

    Route::get('printonepager/{id}', ['as' => 'page.printonepager', 'uses' => 'ReportController@printonepager']);

    Route::post('form1revert/{id}', ['as' => 'page.form1revert', 'uses' => 'EFormsControllerAdmin@form1revert']);
    Route::post('form2revert/{id}', ['as' => 'page.form2revert', 'uses' => 'EFormsControllerAdmin@form2revert']);
    Route::post('form3revert/{id}', ['as' => 'page.form3revert', 'uses' => 'EFormsControllerAdmin@form3revert']);
    Route::post('form4revert/{id}', ['as' => 'page.form4revert', 'uses' => 'EFormsControllerAdmin@form4revert']);


  });


