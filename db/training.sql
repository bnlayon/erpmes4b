/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.14-MariaDB : Database - erpmes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erpmes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `erpmes`;

/*Table structure for table `agencies` */

DROP TABLE IF EXISTS `agencies`;

CREATE TABLE `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) DEFAULT NULL,
  `motheragency_id` int(11) DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) DEFAULT NULL,
  `Category` varchar(10) DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) DEFAULT NULL,
  `head` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `agencies` */

insert  into `agencies`(`id`,`UACS_DPT_ID`,`motheragency_id`,`UACS_AGY_ID`,`Category`,`UACS_DPT_DSC`,`Abbreviation`,`UACS_AGY_DSC`,`head`) values 
(2,NULL,NULL,NULL,NULL,NULL,'PA2','2ND INFANTRY DIVISION, PHILIPPINE ARMY ',NULL),
(3,NULL,NULL,NULL,NULL,NULL,'BFAR','BUREAU OF FISHERIES AND AQUATIC RESOURCES MIMAROPA',NULL),
(4,NULL,NULL,NULL,NULL,NULL,'CAAP','CIVIL AVIATION AUTHORITY OF THE PHILIPPINES',NULL),
(5,NULL,NULL,NULL,NULL,NULL,'CHED','COMMISSION ON HIGHER EDUCATION MIMAROPA',NULL),
(6,NULL,NULL,NULL,NULL,NULL,'CPD','COMMISSION ON POPULATION AND DEVELOPMENT MIMAROPA',NULL),
(7,NULL,NULL,NULL,NULL,NULL,'DAR','DEPARTMENT OF AGRARIAN REFORM MIMAROPA',NULL),
(8,NULL,NULL,NULL,NULL,NULL,'DA','DEPARTMENT OF AGRICULTRE MIMAROPA',NULL),
(9,NULL,NULL,NULL,NULL,NULL,'DEPED','DEPARTMENT OF EDUCATION MIMAROPA',NULL),
(10,NULL,NULL,NULL,NULL,NULL,'DOE','DEPARTMENT OF ENERGY',NULL),
(11,NULL,NULL,NULL,NULL,NULL,'DENR','DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES MIMAROPA',NULL),
(12,NULL,NULL,NULL,NULL,NULL,'DOH','DEPARTMENT OF HEALTH MIMAROPA',NULL),
(13,NULL,NULL,NULL,NULL,NULL,'DICT','DEPARTMENT OF INFORMATION AND COMMUNICATIONS TECHNOLOGY - LUZON CLUSTER 3',NULL),
(14,NULL,NULL,NULL,NULL,NULL,'DOLE','DEPARTMENT OF LABOR AND EMPLOYMENT MIMAROPA',NULL),
(15,NULL,NULL,NULL,NULL,NULL,'DPWH','DEPARTMENT OF PUBLIC WORKS AND HIGHWAYS MIMAROPA',NULL),
(16,NULL,NULL,NULL,NULL,NULL,'DOST','DEPARTMENT OF SCIENCE AND TECHNOLOGY MIMAROPA',NULL),
(17,NULL,NULL,NULL,NULL,NULL,'DSWD','DEPARTMENT OF SOCIAL WELFARE AND DEVELOPMENT MIMAROPA',NULL),
(18,NULL,NULL,NULL,NULL,NULL,'DILG','DEPARTMENT OF THE INTERIOR AND LOCAL GOVERNMENT MIMAROPA',NULL),
(19,NULL,NULL,NULL,NULL,NULL,'DOT','DEPARTMENT OF TOURISM MIMAROPA',NULL),
(20,NULL,NULL,NULL,NULL,NULL,'DTI','DEPARTMENT OF TRADE AND INDUSTRY MIMAROPA',NULL),
(21,NULL,NULL,NULL,NULL,NULL,'DOTR','DEPARTMENT OF TRANSPORTATION',NULL),
(22,NULL,NULL,NULL,NULL,NULL,'LWUA','LOCAL WATER UTILITIES ADMINISTRATION',NULL),
(23,NULL,NULL,NULL,NULL,NULL,'NCCA','NATIONAL COMMISSION FOR CULTURE AND THE ARTS',NULL),
(24,NULL,NULL,NULL,NULL,NULL,'NCIP','NATIONAL COMMISSION ON INDIGENOUS PEOPLES MIMAROPA',NULL),
(25,NULL,NULL,NULL,NULL,NULL,'NEA','NATIONAL ELECTRIFICATION ADMINISTRATION',NULL),
(26,NULL,NULL,NULL,NULL,NULL,'NHA','NATIONAL HOUSING AUTHORITY REGION 4',NULL),
(27,NULL,NULL,NULL,NULL,NULL,'NIA','NATIONAL IRRIGATION ADMINISTRATION MIMAROPA',NULL),
(28,NULL,NULL,NULL,NULL,NULL,'NNC','NATIONAL NUTRITION COUNCIL MIMAROPA',NULL),
(29,NULL,NULL,NULL,NULL,NULL,'NAPOLCOM','NATIONAL POLICE COMMISSION MIMAROPA',NULL),
(30,NULL,NULL,NULL,NULL,NULL,'NPC','NATIONAL POWER CORPORATION',NULL),
(31,NULL,NULL,NULL,NULL,NULL,'OCD','OFFICE OF CIVIL DEFENSE MIMAROPA',NULL),
(32,NULL,NULL,NULL,NULL,NULL,'PCA','PHILIPPINE COCONUT AUTHORITY MIMAROPA ',NULL),
(33,NULL,NULL,NULL,NULL,NULL,'PDEA','PHILIPPINE DRUG ENFORCEMENT AGENCY MIMAROPA',NULL),
(34,NULL,NULL,NULL,NULL,NULL,'PFIDA','PHILIPPINE FIBER INDUSTRY DEVELOPMENT AUTHORITY 4',NULL),
(35,NULL,NULL,NULL,NULL,NULL,'PIA','PHILIPPINE INFORMATION AGENCY MIMAROPA',NULL),
(36,NULL,NULL,NULL,NULL,NULL,'PNP','PHILIPPINE NATIONAL POLICE MIMAROPA',NULL),
(37,NULL,NULL,NULL,NULL,NULL,'PPA','PHILIPPINE PORTS AUTHORITY',NULL),
(38,NULL,NULL,NULL,NULL,NULL,'PSA','PHILIPPINE STATISTICS AUTHORITY MIMAROPA',NULL),
(39,NULL,NULL,NULL,NULL,NULL,'TESDA','TECHNICAL EDUCATION AND SKILLS DEVELOPMENT AUTHORITY MIMAROPA',NULL),
(40,NULL,NULL,NULL,NULL,NULL,'PAWC','WESTERN COMMAND, PHILIPPINE ARMY',NULL),
(41,NULL,NULL,NULL,NULL,NULL,'MSC','MARINDUQUE STATE COLLEGE',NULL),
(42,NULL,NULL,NULL,NULL,NULL,'MSCAT','MINDORO STATE COLLEGE OF AGRICULTURE AND TECHNOLOGY',NULL),
(43,NULL,NULL,NULL,NULL,NULL,'OMSC','OCCIDENTAL MINDORO STATE COLLEGE',NULL),
(44,NULL,NULL,NULL,NULL,NULL,'PSU','PALAWAN STATE UNIVERSITY',NULL),
(45,NULL,NULL,NULL,NULL,NULL,'RSU','ROMBLON STATE UNIVERSITY',NULL),
(46,NULL,NULL,NULL,NULL,NULL,'WPU','WESTERN PHILIPPINES UNIVERSITY',NULL),
(47,NULL,NULL,NULL,NULL,NULL,'MAR','PROVINCIAL GOVERNMENT OF MARINDUQUE',NULL),
(48,NULL,NULL,NULL,NULL,NULL,'OCM','PROVINCIAL GOVERNMENT OF OCCIDENTAL MINDORO',NULL),
(49,NULL,NULL,NULL,NULL,NULL,'ORM','PROVINCIAL GOVERNMENT OF ORIENTAL MINDORO',NULL),
(50,NULL,NULL,NULL,NULL,NULL,'PAL','PROVINCIAL GOVERNMENT OF PALAWAN',NULL),
(51,NULL,NULL,NULL,NULL,NULL,'ROM','PROVINCIAL GOVERNMENT OF ROMBLON',NULL),
(52,NULL,NULL,NULL,NULL,NULL,'CAL','CITY GOVERNMENT OF CALAPAN ',NULL),
(53,NULL,NULL,NULL,NULL,NULL,'PP','CITY GOVERNMENT OF PUERTO PRINCESA ',NULL),
(276,'24',0,'001','HEAD','National Economic and Development Authority (NEDA)','NEDA','National Economic and Development Authority (NEDA)','Acting Secretary Karl Kendrick T. Chua'),
(278,NULL,NULL,NULL,NULL,NULL,NULL,'POPULATION COMMISSION',NULL),
(279,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE PORTS AUTHORITY - MINDORO',NULL),
(280,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE PORTS AUTHORITY - PALAWAN',NULL);

/*Table structure for table `agency_form1s` */

DROP TABLE IF EXISTS `agency_form1s`;

CREATE TABLE `agency_form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `fy` int(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form1s` */

/*Table structure for table `agency_form1s_projects` */

DROP TABLE IF EXISTS `agency_form1s_projects`;

CREATE TABLE `agency_form1s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form1s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `fs_1` decimal(10,2) DEFAULT 0.00,
  `fs_2` decimal(10,2) DEFAULT 0.00,
  `fs_3` decimal(10,2) DEFAULT 0.00,
  `fs_4` decimal(10,2) DEFAULT 0.00,
  `fs_5` decimal(10,2) DEFAULT 0.00,
  `fs_6` decimal(10,2) DEFAULT 0.00,
  `fs_7` decimal(10,2) DEFAULT 0.00,
  `fs_8` decimal(10,2) DEFAULT 0.00,
  `fs_9` decimal(10,2) DEFAULT 0.00,
  `fs_10` decimal(10,2) DEFAULT 0.00,
  `fs_11` decimal(10,2) DEFAULT 0.00,
  `fs_12` decimal(10,2) DEFAULT 0.00,
  `pt_1` decimal(10,2) DEFAULT 0.00,
  `pt_2` decimal(10,2) DEFAULT 0.00,
  `pt_3` decimal(10,2) DEFAULT 0.00,
  `pt_4` decimal(10,2) DEFAULT 0.00,
  `pt_5` decimal(10,2) DEFAULT 0.00,
  `pt_6` decimal(10,2) DEFAULT 0.00,
  `pt_7` decimal(10,2) DEFAULT 0.00,
  `pt_8` decimal(10,2) DEFAULT 0.00,
  `pt_9` decimal(10,2) DEFAULT 0.00,
  `pt_10` decimal(10,2) DEFAULT 0.00,
  `pt_11` decimal(10,2) DEFAULT 0.00,
  `pt_12` decimal(10,2) DEFAULT 0.00,
  `oi_1` varchar(250) DEFAULT '-',
  `oi_2` varchar(250) DEFAULT '-',
  `oi_3` varchar(250) DEFAULT '-',
  `oi_4` varchar(250) DEFAULT '-',
  `oi_5` varchar(250) DEFAULT '-',
  `oi_6` varchar(250) DEFAULT '-',
  `oi_7` varchar(250) DEFAULT '-',
  `oi_8` varchar(250) DEFAULT '-',
  `oi_9` varchar(250) DEFAULT '-',
  `oi_10` varchar(250) DEFAULT '-',
  `oi_11` varchar(250) DEFAULT '-',
  `oi_12` varchar(250) DEFAULT '-',
  `eg_1` int(11) DEFAULT 0,
  `eg_2` int(11) DEFAULT 0,
  `eg_3` int(11) DEFAULT 0,
  `eg_4` int(11) DEFAULT 0,
  `eg_5` int(11) DEFAULT 0,
  `eg_6` int(11) DEFAULT 0,
  `eg_7` int(11) DEFAULT 0,
  `eg_8` int(11) DEFAULT 0,
  `eg_9` int(11) DEFAULT 0,
  `eg_10` int(11) DEFAULT 0,
  `eg_11` int(11) DEFAULT 0,
  `eg_12` int(11) DEFAULT 0,
  `eg_13` int(11) DEFAULT 0,
  `eg_14` int(11) DEFAULT 0,
  `eg_15` int(11) DEFAULT 0,
  `eg_16` int(11) DEFAULT 0,
  `eg_17` int(11) DEFAULT 0,
  `eg_18` int(11) DEFAULT 0,
  `eg_19` int(11) DEFAULT 0,
  `eg_20` int(11) DEFAULT 0,
  `eg_21` int(11) DEFAULT 0,
  `eg_22` int(11) DEFAULT 0,
  `eg_23` int(11) DEFAULT 0,
  `eg_24` int(11) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form1s_projects` */

/*Table structure for table `agency_form2s` */

DROP TABLE IF EXISTS `agency_form2s`;

CREATE TABLE `agency_form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form2s` */

/*Table structure for table `agency_form2s_projects` */

DROP TABLE IF EXISTS `agency_form2s_projects`;

CREATE TABLE `agency_form2s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form2s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `alloc_asof` decimal(20,2) DEFAULT 0.00,
  `alloc_month` decimal(20,2) DEFAULT 0.00,
  `releases_asof` decimal(20,2) DEFAULT 0.00,
  `releases_month` decimal(20,2) DEFAULT 0.00,
  `obligations_asof` decimal(20,2) DEFAULT 0.00,
  `obligations_month` decimal(20,2) DEFAULT 0.00,
  `expenditures_asof` decimal(20,2) DEFAULT 0.00,
  `expenditures_month` decimal(20,2) DEFAULT 0.00,
  `male` int(11) DEFAULT 0,
  `female` int(11) DEFAULT 0,
  `oi` text DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT 0.00,
  `tftm` decimal(20,2) DEFAULT 0.00,
  `atd` decimal(20,2) DEFAULT 0.00,
  `aftm` decimal(20,2) DEFAULT 0.00,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form2s_projects` */

/*Table structure for table `agency_form3s` */

DROP TABLE IF EXISTS `agency_form3s`;

CREATE TABLE `agency_form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form3s` */

/*Table structure for table `agency_form3s_projects` */

DROP TABLE IF EXISTS `agency_form3s_projects`;

CREATE TABLE `agency_form3s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form3s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `findings` text DEFAULT NULL,
  `possible` text DEFAULT NULL,
  `recommendations` text DEFAULT NULL,
  `impstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form3s_projects` */

/*Table structure for table `agency_form4s` */

DROP TABLE IF EXISTS `agency_form4s`;

CREATE TABLE `agency_form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form4s` */

/*Table structure for table `agency_form4s_projects` */

DROP TABLE IF EXISTS `agency_form4s_projects`;

CREATE TABLE `agency_form4s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form4s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `objectives` text DEFAULT NULL,
  `indicator` text DEFAULT NULL,
  `results` text DEFAULT NULL,
  `impstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form4s_projects` */

/*Table structure for table `agency_form5s` */

DROP TABLE IF EXISTS `agency_form5s`;

CREATE TABLE `agency_form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form5s` */

/*Table structure for table `agency_form5s_projects` */

DROP TABLE IF EXISTS `agency_form5s_projects`;

CREATE TABLE `agency_form5s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form5s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `allocation` decimal(20,2) DEFAULT NULL,
  `releases` decimal(20,2) DEFAULT NULL,
  `obligations` decimal(20,2) DEFAULT NULL,
  `expenditures` decimal(20,2) DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT NULL,
  `atd` decimal(20,2) DEFAULT NULL,
  `male` int(11) DEFAULT NULL,
  `female` int(11) DEFAULT NULL,
  `negativeslippage` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form5s_projects` */

/*Table structure for table `agency_form6s` */

DROP TABLE IF EXISTS `agency_form6s`;

CREATE TABLE `agency_form6s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form6s` */

/*Table structure for table `agency_form6s_projects` */

DROP TABLE IF EXISTS `agency_form6s_projects`;

CREATE TABLE `agency_form6s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form6s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `releases` decimal(20,2) DEFAULT NULL,
  `expenditures` decimal(20,2) DEFAULT NULL,
  `atd` decimal(20,2) DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT NULL,
  `issues` text DEFAULT NULL,
  `source` text DEFAULT NULL,
  `action` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form6s_projects` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `categories_name_unique` (`category`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category`,`created_at`,`updated_at`) values 
(1,'ELCAC','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Balik Probinsya Program (BP2)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Rehab and Recovery Projects (RRP)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(6,'Official Development Assistance (ODA)',NULL,NULL),
(7,'Regular',NULL,NULL),
(8,'Others',NULL,NULL);

/*Table structure for table `form1s` */

DROP TABLE IF EXISTS `form1s`;

CREATE TABLE `form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fy` int(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form1s` */

/*Table structure for table `form2s` */

DROP TABLE IF EXISTS `form2s`;

CREATE TABLE `form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form2s` */

/*Table structure for table `form3s` */

DROP TABLE IF EXISTS `form3s`;

CREATE TABLE `form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form3s` */

/*Table structure for table `form4s` */

DROP TABLE IF EXISTS `form4s`;

CREATE TABLE `form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form4s` */

/*Table structure for table `form5s` */

DROP TABLE IF EXISTS `form5s`;

CREATE TABLE `form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form5s` */

/*Table structure for table `form6s` */

DROP TABLE IF EXISTS `form6s`;

CREATE TABLE `form6s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form6s` */

/*Table structure for table `impstatuses` */

DROP TABLE IF EXISTS `impstatuses`;

CREATE TABLE `impstatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `impstatuses` */

insert  into `impstatuses`(`id`,`type`) values 
(1,'Ahead of Schedule'),
(2,'On Schedule'),
(3,'Behind Schedule');

/*Table structure for table `item_tag` */

DROP TABLE IF EXISTS `item_tag`;

CREATE TABLE `item_tag` (
  `item_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `item_tag_item_id_foreign` (`item_id`) USING BTREE,
  KEY `item_tag_tag_id_foreign` (`tag_id`) USING BTREE,
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `item_tag` */

insert  into `item_tag`(`item_id`,`tag_id`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(3,1);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `items_name_unique` (`name`) USING BTREE,
  KEY `items_category_id_foreign` (`category_id`) USING BTREE,
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`description`,`picture`,`category_id`,`status`,`date`,`show_on_homepage`,`options`,`created_at`,`updated_at`) values 
(1,'5 citybreak ideas for this year','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,1,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Top 10 restaurants in Italy','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Cocktail ideas for your birthday party','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `longitude` float(20,7) DEFAULT NULL,
  `latitude` float(20,7) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `locations` */

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `logs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_100000_create_password_resets_table',1),
(2,'2019_01_15_100000_create_roles_table',1),
(3,'2019_01_15_110000_create_users_table',1),
(4,'2019_01_17_121504_create_categories_table',1),
(5,'2019_01_21_130422_create_tags_table',1),
(6,'2019_01_21_163402_create_items_table',1),
(7,'2019_01_21_163414_create_item_tag_table',1),
(8,'2019_03_06_132557_add_photo_column_to_users_table',1),
(9,'2019_03_06_143255_add_fields_to_items_table',1),
(10,'2019_03_20_090438_add_color_tags_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `password_resets` */

insert  into `password_resets`(`email`,`token`,`created_at`) values 
('bnlayon@neda.gov.ph','$2y$10$WUp4C7xUrQOe9M/IldHV3OhDkvjrJHAoiHbvIfgFu74qN52jaO3Cy','2020-11-09 06:00:27');

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `implementingagency` int(11) NOT NULL,
  `sector` int(11) NOT NULL,
  `fundingsource` int(11) NOT NULL,
  `mode` varchar(500) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `category` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `cost` int(50) NOT NULL,
  `odatype` varchar(10) DEFAULT NULL,
  `others` varchar(200) DEFAULT NULL,
  `gaayear` date DEFAULT NULL,
  `lfptype` varchar(50) DEFAULT NULL,
  `otherslfp` varchar(100) DEFAULT NULL,
  `rrp` varchar(100) DEFAULT NULL,
  `catothers` varchar(100) DEFAULT NULL,
  `contractor` varchar(250) DEFAULT NULL,
  `modeothers` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `projects` */

/*Table structure for table `provinces` */

DROP TABLE IF EXISTS `provinces`;

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `provinces` */

insert  into `provinces`(`id`,`province`) values 
(1,'Occidental Mindoro'),
(2,'Oriental Mindoro'),
(3,'Marinduque'),
(4,'Romblon'),
(5,'Palawan');

/*Table structure for table `provincesprojects` */

DROP TABLE IF EXISTS `provincesprojects`;

CREATE TABLE `provincesprojects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `province_id` int(1) DEFAULT NULL,
  `proj_id` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `provincesprojects` */

insert  into `provincesprojects`(`id`,`province_id`,`proj_id`,`created_at`,`updated_at`) values 
(1,1,100,'2021-01-12 01:47:37','2021-01-12 01:47:37'),
(2,3,100,'2021-01-12 01:47:37','2021-01-12 01:47:37'),
(3,5,1,'2021-01-12 01:47:37','2021-01-12 01:47:37');

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` varchar(100) DEFAULT NULL,
  `reporttype` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `reports` */

insert  into `reports`(`id`,`report`,`reporttype`) values 
(1,'Masterlist of Projects',1),
(2,'Masterlist of Agency Projects',2),
(3,'Sample Chart',1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `roles_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'Admin','This is the administration role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Agency','This is the implementing agency role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Member','This is the member role','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `sectors` */

DROP TABLE IF EXISTS `sectors`;

CREATE TABLE `sectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sectors` */

insert  into `sectors`(`id`,`sector`) values 
(1,'Economic'),
(2,'Infrastructure'),
(3,'Social'),
(4,'Government Institutional Development'),
(5,'Others');

/*Table structure for table `sources` */

DROP TABLE IF EXISTS `sources`;

CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sources` */

insert  into `sources`(`id`,`type`) values 
(1,'Official Development Assistance'),
(2,'Local Financing'),
(3,'Public-Private Partnership'),
(4,'Others');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `tags_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`color`,`created_at`,`updated_at`) values 
(1,'Hot','#f44336','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Trending','#9c27b0','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'New','#00bcd4','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `agency_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  KEY `users_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`picture`,`role_id`,`agency_id`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Administrator','bnlayon@neda.gov.ph','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i','profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg',1,0,'essj1EZFvoOheM9PmVvNWYGA9YR5ltSPjzb3MHekBxvJIBCB52KmDSCE4VCn','2020-07-24 06:35:23','2020-11-03 05:24:35'),
(276,'Roy A. Dimayuga','radimayuga@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(277,'Amalia S. Tuppil','astuppil@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(278,'Francis Bayani M. de Guzman','fmdeguzman@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/TEWaxWKNw3digbXGGpTbzaD8XFNiV2jBZh2FkzSF.jpeg',1,0,NULL,'0000-00-00 00:00:00','2021-01-15 08:09:02'),
(279,'Loreto H. Castillo','lhcastillo@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(280,'Jojo R. Datinguinoo','jrdatinguinoo@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(281,'Keizher Marasigan','ktmarasigan@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00','2021-01-14 06:19:58'),
(282,'Joel Paule','jmpaule@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:11:04'),
(283,'Kenneth Joy Arteza','ktarteza@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:14:21'),
(284,'Ruther John Col-long','rbcol-long@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:12:12'),
(285,'Jhunjun Fajutagana','jffajutagana@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:12:58'),
(286,'Nepo Jerome Benter','ngbenter@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:15:06'),
(288,'Chara Lois T. Eje','cteje@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:10:40'),
(289,'Beltisezara Garcia','bagarcia@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:15:33'),
(290,'RD Susan A. Sumbeling','sasumbeling@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:15:55'),
(291,'ARD Bernardino A. Atienza, Jr.','baatienza@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:16:27'),
(292,'Jelyn B. Gabuco','CAAPAREA4Gabuco',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,4,NULL,'0000-00-00 00:00:00',NULL),
(293,'Elsa V. Morales','CPDOCALAPANCITYMorales',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,52,NULL,'0000-00-00 00:00:00',NULL),
(294,'Merlie Jaye R. Dignadice','CPDO PUERTOPRINCESACITYDignadice',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,53,NULL,'0000-00-00 00:00:00',NULL),
(295,'Melvin T. Tirao','DARTirao',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,7,NULL,'0000-00-00 00:00:00',NULL),
(296,'Jonas Paolo M. Saludo','DENRSaludo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,11,NULL,'0000-00-00 00:00:00',NULL),
(297,'Laurente A. Samala','DepEdSamala',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,9,NULL,'0000-00-00 00:00:00',NULL),
(298,'Rusella G. Muñoz','DICTMuñoz',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,13,NULL,'0000-00-00 00:00:00','2021-01-15 03:56:22'),
(299,'Lorenzo F. Suarez','DILGSuarez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,18,NULL,'0000-00-00 00:00:00',NULL),
(301,'Jelyn E. Doctor','DOSTDoctor',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(302,'Gladys A. Quesea','DOTQuesea',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,19,NULL,'0000-00-00 00:00:00',NULL),
(303,'Dennis M. Albano','DOTrAlbano',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,21,NULL,'0000-00-00 00:00:00',NULL),
(304,'Engr. Marilou M. Lawas','DPWHLawas',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,15,NULL,'0000-00-00 00:00:00',NULL),
(305,'Antonio C. Cirilo','DSWDCirilo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(306,'Mary Ann M. Hernandez','MinSCATHernandez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,42,NULL,'0000-00-00 00:00:00',NULL),
(307,'Engr. Robert N. Lamonte','MSCLamonte',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,41,NULL,'0000-00-00 00:00:00',NULL),
(308,'Mary Joy A. Sta. Ana','NAPOLCOMAna',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,29,NULL,'0000-00-00 00:00:00',NULL),
(309,'James B. Panado','NEAPanado',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,25,NULL,'0000-00-00 00:00:00',NULL),
(310,'Lowell L. Lozano','NIALozano',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,27,NULL,'0000-00-00 00:00:00',NULL),
(311,'Maria Camille Louise C. Chen','NNCChen',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,28,NULL,'0000-00-00 00:00:00',NULL),
(312,'Roger M. Lived','NPCLived',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,30,NULL,'0000-00-00 00:00:00',NULL),
(313,'Emelita G. Agpalo','PPDOOccidentalMindoroAgpalo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,48,NULL,'0000-00-00 00:00:00',NULL),
(314,'Edmin L. Distajo','PPDOOrientalMindoroDistajo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,49,NULL,'0000-00-00 00:00:00',NULL),
(315,'Sherry Rose M. Peralta','PPDOPalawanPeralta',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,50,NULL,'0000-00-00 00:00:00',NULL),
(316,'Amabel S Liao','PSULiao',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,44,NULL,'0000-00-00 00:00:00',NULL),
(317,'Dr. Reynaldo P. Ramos','RSURamos',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,45,NULL,'0000-00-00 00:00:00',NULL),
(318,'Mel Angelo B. Estoque','TESDAEstoque',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,39,NULL,'0000-00-00 00:00:00',NULL),
(319,'Ms. Gene Michelle S. Paduga','WPUPaduga',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,46,NULL,'0000-00-00 00:00:00',NULL),
(320,'Jean E. Tirante','DATirante',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,8,NULL,'0000-00-00 00:00:00',NULL),
(321,'Gloren R. Hinlo','POPCOMHinlo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,278,NULL,'0000-00-00 00:00:00',NULL),
(322,'Ssg Roy A Malaque (CAV) PA','2IDPA',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,2,NULL,'0000-00-00 00:00:00',NULL),
(323,'Dwane Darcy D. Cayonte','DOECayonte',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,10,NULL,'0000-00-00 00:00:00',NULL),
(324,'Engr. Benidicto B. Allago','DOHAllago',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,12,NULL,'0000-00-00 00:00:00',NULL),
(325,'Carnilo Orevillo','LWUAOrevillo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,22,NULL,'0000-00-00 00:00:00',NULL),
(326,'Joemer M. Samong','NCIPSamong',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,24,NULL,'0000-00-00 00:00:00',NULL),
(327,'Allinah Janzen O Magnaye','OCDMagnaye',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,31,NULL,'0000-00-00 00:00:00',NULL),
(328,'Wenceslao M. Paguia, Jr., Phd','OMSCPhd',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,43,NULL,'0000-00-00 00:00:00',NULL),
(329,'Jovito B. Bumatay','PFIDABumatay',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,34,NULL,'0000-00-00 00:00:00',NULL),
(330,'Melanie Ronquillo','PIAMIMAROPARonquillo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,35,NULL,'0000-00-00 00:00:00',NULL),
(331,'Margarito P. Dimaiilig','PPAPMOMindoroDimaiilig',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,279,NULL,'0000-00-00 00:00:00',NULL),
(332,'Engr. Relly W. Madarcos','PPAPMOPalawanMadarcos',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,280,NULL,'0000-00-00 00:00:00',NULL),
(333,'Emerson V. Caisip','PSACaisip',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,38,NULL,'0000-00-00 00:00:00',NULL),
(334,'Engr. Christine Mendoza','PPDOMarinduqueMendoza',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,47,NULL,'0000-00-00 00:00:00',NULL),
(335,'Kathlaine May B. Esteban','CAAPAREA4Esteban',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,4,NULL,'0000-00-00 00:00:00',NULL),
(336,'Aristeo Joseph C. Genil','CPDOCALAPANCITYGenil',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,52,NULL,'0000-00-00 00:00:00',NULL),
(337,'Michael Rio L. Daquer','CPDO PUERTOPRINCESACITYDaquer',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,53,NULL,'0000-00-00 00:00:00',NULL),
(338,'Melissa D. Punsalan','DARPunsalan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,7,NULL,'0000-00-00 00:00:00',NULL),
(339,'John Philip M. Merced','DENRMerced',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,11,NULL,'0000-00-00 00:00:00',NULL),
(340,'Merleen B. Abante','DepEdAbante',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,9,NULL,'0000-00-00 00:00:00',NULL),
(341,'Raiza Joyce A. Mendoza','DICTMendoza',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,13,NULL,'0000-00-00 00:00:00',NULL),
(342,'Michael Casto A. Ras','DILGRas',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,18,NULL,'0000-00-00 00:00:00',NULL),
(343,'Kimberly D. Olicia','DOLEOlicia',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,14,NULL,'0000-00-00 00:00:00',NULL),
(344,'Zenchin Geri G. Pormento','DOSTPormento',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(345,'Faye Angeli A. Reyes','DOTReyes',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,19,NULL,'0000-00-00 00:00:00',NULL),
(346,'Luxmi F. De Leon','DOTrLeon',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,21,NULL,'0000-00-00 00:00:00',NULL),
(347,'Engr. Anacorita R. Mercader','DPWHMercader',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,15,NULL,'0000-00-00 00:00:00',NULL),
(348,'Jhun Mark G. Villaroza','DSWDVillaroza',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(349,'Engr. Jennie T. Fernando','MinSCATFernando',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,42,NULL,'0000-00-00 00:00:00',NULL),
(350,'Mr. Romell L. Morales','MSCMorales',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,41,NULL,'0000-00-00 00:00:00',NULL),
(351,'Lorlaine F. Dolendo','NAPOLCOMDolendo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,29,NULL,'0000-00-00 00:00:00',NULL),
(352,'Rodolfo D. Evangelista','NEAEvangelista',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,25,NULL,'0000-00-00 00:00:00',NULL),
(353,'Maria Victoria O. Malenab','NIAMalenab',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,27,NULL,'0000-00-00 00:00:00',NULL),
(354,'Bianca Louise Veronica M. Estrella','NNCEstrella',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,28,NULL,'0000-00-00 00:00:00',NULL),
(355,'Marilou O. Oribiana','NPCOribiana',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,30,NULL,'0000-00-00 00:00:00',NULL),
(356,'Bernadette T. Salgado','PPDOOccidentalMindoroSalgado',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,48,NULL,'0000-00-00 00:00:00',NULL),
(357,'Maria Margarita V. Lopez','PPDOOrientalMindoroLopez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,49,NULL,'0000-00-00 00:00:00',NULL),
(358,'Mr. Matthew Micah Lamitar','PPDOPalawanLamitar',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,50,NULL,'0000-00-00 00:00:00',NULL),
(359,'Jose G. Buenconsejo','PSUBuenconsejo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,44,NULL,'0000-00-00 00:00:00',NULL),
(360,'Engr. Jason F. Rufon','RSURufon',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,45,NULL,'0000-00-00 00:00:00',NULL),
(361,'Eljun Francel A. Angeles','TESDAAngeles',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,39,NULL,'0000-00-00 00:00:00',NULL),
(362,'Engr. Ryan Jay V. Balcarcel','WPUBalcarcel',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,46,NULL,'0000-00-00 00:00:00',NULL),
(363,'Rolibeth G. Morillo ','DAMorillo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,8,NULL,'0000-00-00 00:00:00',NULL),
(364,'Sharmaine C. Ricardo','POPCOMRicardo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,278,NULL,'0000-00-00 00:00:00',NULL),
(366,'Roland Victor A. Aguilar','DOEAguilar',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,10,NULL,'0000-00-00 00:00:00',NULL),
(367,'Engr. Stephanie S. Sioco','DOHSioco',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,12,NULL,'0000-00-00 00:00:00',NULL),
(368,'Punzalan','LWUAPunzalan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,22,NULL,'0000-00-00 00:00:00',NULL),
(369,'Mary Ann L. Delos Santos','NCIPSantos',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,24,NULL,'0000-00-00 00:00:00',NULL),
(370,'Jefrie G Rodriguez','OCDRodriguez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,31,NULL,'0000-00-00 00:00:00',NULL),
(371,'Frances Darlyn S. Pangilinan, Ce','OMSCCe',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,43,NULL,'0000-00-00 00:00:00',NULL),
(372,'Zellica Mae C. Bautista','PFIDABautista',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,34,NULL,'0000-00-00 00:00:00',NULL),
(373,'Madonna Punzalan','PIAMIMAROPAPunzalan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,35,NULL,'0000-00-00 00:00:00',NULL),
(374,'Ronald O. Matibag','PPAPMOMindoroMatibag',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,279,NULL,'0000-00-00 00:00:00',NULL),
(375,'Juna M. Sanchez','PPAPMOPalawanSanchez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,280,NULL,'0000-00-00 00:00:00',NULL),
(376,'Rachel L. Vasquez','PSAVasquez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,38,NULL,'0000-00-00 00:00:00',NULL),
(377,'Anna Christina Ibahan','PPDOMarinduqueIbahan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,47,NULL,'0000-00-00 00:00:00',NULL),
(379,'Sara J. Marasigan','sjmarasigan@neda.gov.ph',NULL,'$2y$10$8QeuvZMQB3oZyF6rbatzautC5bamaZZy3PaANp0WHkeZx7Cg6cJH.',NULL,3,276,NULL,'2021-01-14 08:58:41','2021-01-14 08:58:41'),
(380,'NEDASAMPLE','neda@neda.gov.ph',NULL,'$2y$10$PE8pHr00G7fxdOOjA7HOo.ZpypPXdRRi/UJP68oBH/bp2KaskQ7I2','profile/q5oxHYnK6PAnUVMkgIv7l88s4mqpgKuaUNcNJeFa.jpeg',2,276,NULL,'2021-01-14 09:11:23','2021-01-18 00:37:29'),
(381,'Kim Neko C. Baña','DOLEBaña',NULL,'$2y$10$FcVwoUY15Kh0HPIdmb49EOgjtyEfFPCDBiUTsjqEwFVv/plZyooXO',NULL,2,14,NULL,'2021-01-14 09:18:50','2021-01-15 03:58:55'),
(382,'Gemma Etis','PPDORomblonEtis',NULL,'$2y$10$.2AD5yvsQjibz7bNZv.zH.xrSy6e/i0fS4j3tJPog1PKfuhTSDvFy',NULL,2,49,NULL,'2021-01-15 03:49:47','2021-01-15 03:49:47'),
(383,'Victorino N. Benedicto, Jr.','PPDORomblonBenedicto',NULL,'$2y$10$5RbcXfnQYOmwQeu796gR6eIBx9kqmysRkRUaRQaP1xWLPZDtbshhm',NULL,2,51,NULL,'2021-01-15 03:54:53','2021-01-15 03:54:53');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
