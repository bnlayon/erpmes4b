

INSERT INTO `agencies` (`id`, `UACS_DPT_ID`, `motheragency_id`, `UACS_AGY_ID`, `Category`, `UACS_DPT_DSC`, `Abbreviation`, `UACS_AGY_DSC`, `head`) VALUES
(282, NULL, 15, NULL, NULL, NULL, 'MOrDEO', 'Mindoro Oriental DEO', NULL),
(283, NULL, 15, NULL, NULL, NULL, 'MOccDeo', 'Mindoro Occidental DEO', NULL),
(284, NULL, 15, NULL, NULL, NULL, 'MarDEO', 'Marinduque DEO', NULL),
(285, NULL, 15, NULL, NULL, NULL, 'RomDEO', 'Romblon DEO', NULL),
(286, NULL, 15, NULL, NULL, NULL, 'Pal1DEO', 'Palawan 1 DEO', NULL),
(287, NULL, 15, NULL, NULL, NULL, 'Pal2DEO', 'Palawan 2 DEO', NULL),
(288, NULL, 15, NULL, NULL, NULL, 'Pal3DEO', 'Palawan 3 DEO', NULL),
(289, NULL, 15, NULL, NULL, NULL, 'SMDEO', 'Southern Mindoro DEO', NULL),
(290, NULL, 4, NULL, NULL, NULL, 'CAAPHO', 'CAAP - Central Head Office', NULL),
(291, NULL, 4, NULL, NULL, NULL, 'CAAPA3', 'CAAP - Area 3', NULL),
(292, NULL, 37, NULL, NULL, NULL, 'PPA-HO', 'PHILIPPINE PORTS AUTHORITY - Central Head Office', NULL),
(293, NULL, 37, NULL, NULL, NULL, 'PPA-Bat', 'PHILIPPINE PORTS AUTHORITY - PMO Batangas', NULL),
(294, NULL, 37, NULL, NULL, NULL, 'PPAMarQuez', 'PHILIPPINE PORTS AUTHORITY - PMO MarQuez', NULL);

UPDATE `agencies` SET `UACS_AGY_DSC` = 'Mindoro State University' WHERE `id` = '42';
