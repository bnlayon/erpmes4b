/*
 Navicat MySQL Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : erpmes

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 09/11/2020 17:53:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for agencies
-- ----------------------------
DROP TABLE IF EXISTS `agencies`;
CREATE TABLE `agencies`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `motheragency_id` int NULL DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Category` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `Abbreviation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `head` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 278 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of agencies
-- ----------------------------
INSERT INTO `agencies` VALUES (2, NULL, NULL, NULL, NULL, NULL, 'PA2', '2ND INFANTRY DIVISION, PHILIPPINE ARMY ', NULL);
INSERT INTO `agencies` VALUES (3, NULL, NULL, NULL, NULL, NULL, 'BFAR', 'BUREAU OF FISHERIES AND AQUATIC RESOURCES MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (4, NULL, NULL, NULL, NULL, NULL, 'CAAP', 'CIVIL AVIATION AUTHORITY OF THE PHILIPPINES', NULL);
INSERT INTO `agencies` VALUES (5, NULL, NULL, NULL, NULL, NULL, 'CHED', 'COMMISSION ON HIGHER EDUCATION MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (6, NULL, NULL, NULL, NULL, NULL, 'CPD', 'COMMISSION ON POPULATION AND DEVELOPMENT MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (7, NULL, NULL, NULL, NULL, NULL, 'DAR', 'DEPARTMENT OF AGRARIAN REFORM MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (8, NULL, NULL, NULL, NULL, NULL, 'DA', 'DEPARTMENT OF AGRICULTRE MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (9, NULL, NULL, NULL, NULL, NULL, 'DEPED', 'DEPARTMENT OF EDUCATION MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (10, NULL, NULL, NULL, NULL, NULL, 'DOE', 'DEPARTMENT OF ENERGY', NULL);
INSERT INTO `agencies` VALUES (11, NULL, NULL, NULL, NULL, NULL, 'DENR', 'DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (12, NULL, NULL, NULL, NULL, NULL, 'DOH', 'DEPARTMENT OF HEALTH MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (13, NULL, NULL, NULL, NULL, NULL, 'DICT', 'DEPARTMENT OF INFORMATION AND COMMUNICATIONS TECHNOLOGY - LUZON CLUSTER 3', NULL);
INSERT INTO `agencies` VALUES (14, NULL, NULL, NULL, NULL, NULL, 'DOLE', 'DEPARTMENT OF LABOR AND EMPLOYMENT MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (15, NULL, NULL, NULL, NULL, NULL, 'DPWH', 'DEPARTMENT OF PUBLIC WORKS AND HIGHWAYS MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (16, NULL, NULL, NULL, NULL, NULL, 'DOST', 'DEPARTMENT OF SCIENCE AND TECHNOLOGY MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (17, NULL, NULL, NULL, NULL, NULL, 'DSWD', 'DEPARTMENT OF SOCIAL WELFARE AND DEVELOPMENT MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (18, NULL, NULL, NULL, NULL, NULL, 'DILG', 'DEPARTMENT OF THE INTERIOR AND LOCAL GOVERNMENT MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (19, NULL, NULL, NULL, NULL, NULL, 'DOT', 'DEPARTMENT OF TOURISM MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (20, NULL, NULL, NULL, NULL, NULL, 'DTI', 'DEPARTMENT OF TRADE AND INDUSTRY MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (21, NULL, NULL, NULL, NULL, NULL, 'DOTR', 'DEPARTMENT OF TRANSPORTATION', NULL);
INSERT INTO `agencies` VALUES (22, NULL, NULL, NULL, NULL, NULL, 'LWUA', 'LOCAL WATER UTILITIES ADMINISTRATION', NULL);
INSERT INTO `agencies` VALUES (23, NULL, NULL, NULL, NULL, NULL, 'NCCA', 'NATIONAL COMMISSION FOR CULTURE AND THE ARTS', NULL);
INSERT INTO `agencies` VALUES (24, NULL, NULL, NULL, NULL, NULL, 'NCIP', 'NATIONAL COMMISSION ON INDIGENOUS PEOPLES MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (25, NULL, NULL, NULL, NULL, NULL, 'NEA', 'NATIONAL ELECTRIFICATION ADMINISTRATION', NULL);
INSERT INTO `agencies` VALUES (26, NULL, NULL, NULL, NULL, NULL, 'NHA', 'NATIONAL HOUSING AUTHORITY REGION 4', NULL);
INSERT INTO `agencies` VALUES (27, NULL, NULL, NULL, NULL, NULL, 'NIA', 'NATIONAL IRRIGATION ADMINISTRATION MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (28, NULL, NULL, NULL, NULL, NULL, 'NNC', 'NATIONAL NUTRITION COUNCIL MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (29, NULL, NULL, NULL, NULL, NULL, 'NAPOLCOM', 'NATIONAL POLICE COMMISSION MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (30, NULL, NULL, NULL, NULL, NULL, 'NPC', 'NATIONAL POWER CORPORATION', NULL);
INSERT INTO `agencies` VALUES (31, NULL, NULL, NULL, NULL, NULL, 'OCD', 'OFFICE OF CIVIL DEFENSE MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (32, NULL, NULL, NULL, NULL, NULL, 'PCA', 'PHILIPPINE COCONUT AUTHORITY MIMAROPA ', NULL);
INSERT INTO `agencies` VALUES (33, NULL, NULL, NULL, NULL, NULL, 'PDEA', 'PHILIPPINE DRUG ENFORCEMENT AGENCY MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (34, NULL, NULL, NULL, NULL, NULL, 'PFIDA', 'PHILIPPINE FIBER INDUSTRY DEVELOPMENT AUTHORITY 4', NULL);
INSERT INTO `agencies` VALUES (35, NULL, NULL, NULL, NULL, NULL, 'PIA', 'PHILIPPINE INFORMATION AGENCY MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (36, NULL, NULL, NULL, NULL, NULL, 'PNP', 'PHILIPPINE NATIONAL POLICE MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (37, NULL, NULL, NULL, NULL, NULL, 'PPA', 'PHILIPPINE PORTS AUTHORITY', NULL);
INSERT INTO `agencies` VALUES (38, NULL, NULL, NULL, NULL, NULL, 'PSA', 'PHILIPPINE STATISTICS AUTHORITY MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (39, NULL, NULL, NULL, NULL, NULL, 'TESDA', 'TECHNICAL EDUCATION AND SKILLS DEVELOPMENT AUTHORITY MIMAROPA', NULL);
INSERT INTO `agencies` VALUES (40, NULL, NULL, NULL, NULL, NULL, 'PAWC', 'WESTERN COMMAND, PHILIPPINE ARMY', NULL);
INSERT INTO `agencies` VALUES (41, NULL, NULL, NULL, NULL, NULL, 'MSC', 'MARINDUQUE STATE COLLEGE', NULL);
INSERT INTO `agencies` VALUES (42, NULL, NULL, NULL, NULL, NULL, 'MSCAT', 'MINDORO STATE COLLEGE OF AGRICULTURE AND TECHNOLOGY', NULL);
INSERT INTO `agencies` VALUES (43, NULL, NULL, NULL, NULL, NULL, 'OMSC', 'OCCIDENTAL MINDORO STATE COLLEGE', NULL);
INSERT INTO `agencies` VALUES (44, NULL, NULL, NULL, NULL, NULL, 'PSU', 'PALAWAN STATE UNIVERSITY', NULL);
INSERT INTO `agencies` VALUES (45, NULL, NULL, NULL, NULL, NULL, 'RSU', 'ROMBLON STATE UNIVERSITY', NULL);
INSERT INTO `agencies` VALUES (46, NULL, NULL, NULL, NULL, NULL, 'WPU', 'WESTERN PHILIPPINES UNIVERSITY', NULL);
INSERT INTO `agencies` VALUES (47, NULL, NULL, NULL, NULL, NULL, 'MAR', 'PROVINCIAL GOVERNMENT OF MARINDUQUE', NULL);
INSERT INTO `agencies` VALUES (48, NULL, NULL, NULL, NULL, NULL, 'OCM', 'PROVINCIAL GOVERNMENT OF OCCIDENTAL MINDORO', NULL);
INSERT INTO `agencies` VALUES (49, NULL, NULL, NULL, NULL, NULL, 'ORM', 'PROVINCIAL GOVERNMENT OF ORIENTAL MINDORO', NULL);
INSERT INTO `agencies` VALUES (50, NULL, NULL, NULL, NULL, NULL, 'PAL', 'PROVINCIAL GOVERNMENT OF PALAWAN', NULL);
INSERT INTO `agencies` VALUES (51, NULL, NULL, NULL, NULL, NULL, 'ROM', 'PROVINCIAL GOVERNMENT OF ROMBLON', NULL);
INSERT INTO `agencies` VALUES (52, NULL, NULL, NULL, NULL, NULL, 'CAL', 'CITY GOVERNMENT OF CALAPAN ', NULL);
INSERT INTO `agencies` VALUES (53, NULL, NULL, NULL, NULL, NULL, 'PP', 'CITY GOVERNMENT OF PUERTO PRINCESA ', NULL);
INSERT INTO `agencies` VALUES (276, '24', 0, '001', 'HEAD', 'National Economic and Development Authority (NEDA)', 'NEDA', 'National Economic and Development Authority (NEDA)', 'Acting Secretary Karl Kendrick T. Chua');

-- ----------------------------
-- Table structure for agency_form1s
-- ----------------------------
DROP TABLE IF EXISTS `agency_form1s`;
CREATE TABLE `agency_form1s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `fy` int NULL DEFAULT NULL,
  `agency_id` int NULL DEFAULT NULL,
  `nro_status_review` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form1s
-- ----------------------------
INSERT INTO `agency_form1s` VALUES (5, 'Draft', '2020-08-03', '2020-10-30', 2020, 276, '', 'SAMPLE Rev');
INSERT INTO `agency_form1s` VALUES (7, 'Draft', '2020-08-07', '2020-09-01', 2020, 219, '', 'SAMPLE REMARKS');

-- ----------------------------
-- Table structure for agency_form1s_projects
-- ----------------------------
DROP TABLE IF EXISTS `agency_form1s_projects`;
CREATE TABLE `agency_form1s_projects`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form1s_id` int NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `agency_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `fs_1` decimal(10, 2) NULL DEFAULT 0,
  `fs_2` decimal(10, 2) NULL DEFAULT 0,
  `fs_3` decimal(10, 2) NULL DEFAULT 0,
  `fs_4` decimal(10, 2) NULL DEFAULT 0,
  `fs_5` decimal(10, 2) NULL DEFAULT 0,
  `fs_6` decimal(10, 2) NULL DEFAULT 0,
  `fs_7` decimal(10, 2) NULL DEFAULT 0,
  `fs_8` decimal(10, 2) NULL DEFAULT 0,
  `fs_9` decimal(10, 2) NULL DEFAULT 0,
  `fs_10` decimal(10, 2) NULL DEFAULT 0,
  `fs_11` decimal(10, 2) NULL DEFAULT 0,
  `fs_12` decimal(10, 2) NULL DEFAULT 0,
  `pt_1` decimal(10, 2) NULL DEFAULT 0,
  `pt_2` decimal(10, 2) NULL DEFAULT 0,
  `pt_3` decimal(10, 2) NULL DEFAULT 0,
  `pt_4` decimal(10, 2) NULL DEFAULT 0,
  `pt_5` decimal(10, 2) NULL DEFAULT 0,
  `pt_6` decimal(10, 2) NULL DEFAULT 0,
  `pt_7` decimal(10, 2) NULL DEFAULT 0,
  `pt_8` decimal(10, 2) NULL DEFAULT 0,
  `pt_9` decimal(10, 2) NULL DEFAULT 0,
  `pt_10` decimal(10, 2) NULL DEFAULT 0,
  `pt_11` decimal(10, 2) NULL DEFAULT 0,
  `pt_12` decimal(10, 2) NULL DEFAULT 0,
  `oi_1` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_2` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_3` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_4` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_5` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_6` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_7` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_8` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_9` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_10` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_11` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `oi_12` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-',
  `eg_1` int NULL DEFAULT 0,
  `eg_2` int NULL DEFAULT 0,
  `eg_3` int NULL DEFAULT 0,
  `eg_4` int NULL DEFAULT 0,
  `eg_5` int NULL DEFAULT 0,
  `eg_6` int NULL DEFAULT 0,
  `eg_7` int NULL DEFAULT 0,
  `eg_8` int NULL DEFAULT 0,
  `eg_9` int NULL DEFAULT 0,
  `eg_10` int NULL DEFAULT 0,
  `eg_11` int NULL DEFAULT 0,
  `eg_12` int NULL DEFAULT 0,
  `eg_13` int NULL DEFAULT 0,
  `eg_14` int NULL DEFAULT 0,
  `eg_15` int NULL DEFAULT 0,
  `eg_16` int NULL DEFAULT 0,
  `eg_17` int NULL DEFAULT 0,
  `eg_18` int NULL DEFAULT 0,
  `eg_19` int NULL DEFAULT 0,
  `eg_20` int NULL DEFAULT 0,
  `eg_21` int NULL DEFAULT 0,
  `eg_22` int NULL DEFAULT 0,
  `eg_23` int NULL DEFAULT 0,
  `eg_24` int NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form1s_projects
-- ----------------------------
INSERT INTO `agency_form1s_projects` VALUES (1, 10, 'Reviewed', '2020-11-03', '2020-11-05', 5, 'SAMPLE REVIEWS', 'SAMPLE REMARKS', 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 100000.00, 100000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 12.00, 0.00, 0.00, 0.00, 0.00, 0.00, 50.00, 25.00, 5.00, '-', '-', '-', '-', '-', '-', '-', '-', '-', 'Sample Output Indicator', 'Sample Output Indicator', 'Sample Output Indicator', 12, 12, 12, 12, 12, 12, 21, 12, 12, 12, 12, 12, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for agency_form2s
-- ----------------------------
DROP TABLE IF EXISTS `agency_form2s`;
CREATE TABLE `agency_form2s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agency_id` int NULL DEFAULT NULL,
  `nro_status_review` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form2s
-- ----------------------------
INSERT INTO `agency_form2s` VALUES (1, 'Draft', '2020-08-26', '2020-10-30', '2020-07', 276, '', 'SA');

-- ----------------------------
-- Table structure for agency_form2s_projects
-- ----------------------------
DROP TABLE IF EXISTS `agency_form2s_projects`;
CREATE TABLE `agency_form2s_projects`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form2s_id` int NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `agency_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `alloc_asof` decimal(20, 2) NULL DEFAULT 0,
  `alloc_month` decimal(20, 2) NULL DEFAULT 0,
  `releases_asof` decimal(20, 2) NULL DEFAULT 0,
  `releases_month` decimal(20, 2) NULL DEFAULT 0,
  `obligations_asof` decimal(20, 2) NULL DEFAULT 0,
  `obligations_month` decimal(20, 2) NULL DEFAULT 0,
  `expenditures_asof` decimal(20, 2) NULL DEFAULT 0,
  `expenditures_month` decimal(20, 2) NULL DEFAULT 0,
  `male` int NULL DEFAULT 0,
  `female` int NULL DEFAULT 0,
  `oi` decimal(20, 2) NULL DEFAULT 0,
  `ttd` decimal(20, 2) NULL DEFAULT 0,
  `tftm` decimal(20, 2) NULL DEFAULT 0,
  `atd` decimal(20, 2) NULL DEFAULT 0,
  `aftm` decimal(20, 2) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form2s_projects
-- ----------------------------
INSERT INTO `agency_form2s_projects` VALUES (1, 10, 'Endorsed', '2020-11-03', '2020-11-09', 1, NULL, 'SAMPLE', 90.00, 0.00, 60.00, 0.00, 50.00, 0.00, 70.00, 0.00, 23, 1, 50.00, 50.00, 50.00, 50.00, 50.00);
INSERT INTO `agency_form2s_projects` VALUES (2, 11, 'Endorsed', '2020-11-09', '2020-11-09', 1, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 34, 56, 0.00, 0.00, 0.00, 0.00, 0.00);

-- ----------------------------
-- Table structure for agency_form3s
-- ----------------------------
DROP TABLE IF EXISTS `agency_form3s`;
CREATE TABLE `agency_form3s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agency_id` int NULL DEFAULT NULL,
  `nro_status_review` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form3s
-- ----------------------------
INSERT INTO `agency_form3s` VALUES (22, 'Draft', '2020-09-14', '2020-10-30', '2020-09', 276, NULL, NULL);

-- ----------------------------
-- Table structure for agency_form3s_projects
-- ----------------------------
DROP TABLE IF EXISTS `agency_form3s_projects`;
CREATE TABLE `agency_form3s_projects`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form3s_id` int NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `agency_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `findings` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `possible` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `recommendations` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `impstatus` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form3s_projects
-- ----------------------------
INSERT INTO `agency_form3s_projects` VALUES (1, 10, 'Endorsed', '2020-11-03', '2020-11-03', 22, NULL, 'SAMPLE', NULL, NULL, NULL, 1);

-- ----------------------------
-- Table structure for agency_form4s
-- ----------------------------
DROP TABLE IF EXISTS `agency_form4s`;
CREATE TABLE `agency_form4s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agency_id` int NULL DEFAULT NULL,
  `nro_status_review` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form4s
-- ----------------------------
INSERT INTO `agency_form4s` VALUES (1, 'Draft', '2020-09-28', '2020-10-06', '2020-09', 276, '', 'SAMPLE REMARKS');

-- ----------------------------
-- Table structure for agency_form4s_projects
-- ----------------------------
DROP TABLE IF EXISTS `agency_form4s_projects`;
CREATE TABLE `agency_form4s_projects`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form4s_id` int NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `agency_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `objectives` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `indicator` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `results` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `impstatus` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form4s_projects
-- ----------------------------
INSERT INTO `agency_form4s_projects` VALUES (1, 10, 'Endorsed', '2020-11-03', '2020-11-03', 1, NULL, 'SAMPLE', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for agency_form5s
-- ----------------------------
DROP TABLE IF EXISTS `agency_form5s`;
CREATE TABLE `agency_form5s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agency_id` int NULL DEFAULT NULL,
  `nro_status_review` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form5s
-- ----------------------------
INSERT INTO `agency_form5s` VALUES (2, 'Draft', '2020-11-03', '2020-11-03', '2020-11', 276, NULL, NULL);

-- ----------------------------
-- Table structure for agency_form5s_projects
-- ----------------------------
DROP TABLE IF EXISTS `agency_form5s_projects`;
CREATE TABLE `agency_form5s_projects`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NULL DEFAULT NULL,
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form5s_id` int NULL DEFAULT NULL,
  `nro_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `agency_remarks` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `allocation` decimal(20, 2) NULL DEFAULT NULL,
  `releases` decimal(20, 2) NULL DEFAULT NULL,
  `obligations` decimal(20, 2) NULL DEFAULT NULL,
  `expenditures` decimal(20, 2) NULL DEFAULT NULL,
  `ttd` decimal(20, 2) NULL DEFAULT NULL,
  `atd` decimal(20, 2) NULL DEFAULT NULL,
  `male` int NULL DEFAULT NULL,
  `female` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of agency_form5s_projects
-- ----------------------------
INSERT INTO `agency_form5s_projects` VALUES (12, 10, NULL, '2020-11-09', '2020-11-09', 2, NULL, NULL, 90.00, 60.00, 50.00, 70.00, 100.00, 70.43, 23, 1);
INSERT INTO `agency_form5s_projects` VALUES (13, 11, NULL, '2020-11-09', '2020-11-09', 2, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 34, 56);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `categories_name_unique`(`category`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'ELCAC', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `categories` VALUES (2, 'Balik Probinsya Program (BP2)', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `categories` VALUES (3, 'Rehab and Recovery Projects (RRP)', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `categories` VALUES (6, 'Official Development Assistance (ODA)', NULL, NULL);

-- ----------------------------
-- Table structure for form1s
-- ----------------------------
DROP TABLE IF EXISTS `form1s`;
CREATE TABLE `form1s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `fy` int NOT NULL,
  `is_lock` int NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of form1s
-- ----------------------------
INSERT INTO `form1s` VALUES (1, 2020, 0, '2020-07-31', '2020-10-12');

-- ----------------------------
-- Table structure for form2s
-- ----------------------------
DROP TABLE IF EXISTS `form2s`;
CREATE TABLE `form2s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_lock` int NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of form2s
-- ----------------------------
INSERT INTO `form2s` VALUES (2, '2020-07', 0, '2020-08-26', '2020-09-28');

-- ----------------------------
-- Table structure for form3s
-- ----------------------------
DROP TABLE IF EXISTS `form3s`;
CREATE TABLE `form3s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_lock` int NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of form3s
-- ----------------------------
INSERT INTO `form3s` VALUES (1, '2020-09', 0, '2020-09-14', '2020-09-28');

-- ----------------------------
-- Table structure for form4s
-- ----------------------------
DROP TABLE IF EXISTS `form4s`;
CREATE TABLE `form4s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_lock` int NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of form4s
-- ----------------------------
INSERT INTO `form4s` VALUES (1, '2020-09', 0, '2020-09-28', '2020-10-02');

-- ----------------------------
-- Table structure for form5s
-- ----------------------------
DROP TABLE IF EXISTS `form5s`;
CREATE TABLE `form5s`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `period` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_lock` int NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of form5s
-- ----------------------------
INSERT INTO `form5s` VALUES (2, '2020-11', 1, '2020-11-03', '2020-11-03');

-- ----------------------------
-- Table structure for impstatuses
-- ----------------------------
DROP TABLE IF EXISTS `impstatuses`;
CREATE TABLE `impstatuses`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of impstatuses
-- ----------------------------
INSERT INTO `impstatuses` VALUES (1, 'Ahead of Schedule');
INSERT INTO `impstatuses` VALUES (2, 'On Schedule');
INSERT INTO `impstatuses` VALUES (3, 'Behind Schedule');

-- ----------------------------
-- Table structure for item_tag
-- ----------------------------
DROP TABLE IF EXISTS `item_tag`;
CREATE TABLE `item_tag`  (
  `item_id` int UNSIGNED NOT NULL,
  `tag_id` int UNSIGNED NOT NULL,
  INDEX `item_tag_item_id_foreign`(`item_id`) USING BTREE,
  INDEX `item_tag_tag_id_foreign`(`tag_id`) USING BTREE,
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of item_tag
-- ----------------------------
INSERT INTO `item_tag` VALUES (1, 1);
INSERT INTO `item_tag` VALUES (1, 2);
INSERT INTO `item_tag` VALUES (1, 3);
INSERT INTO `item_tag` VALUES (2, 1);
INSERT INTO `item_tag` VALUES (3, 1);

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `category_id` int UNSIGNED NOT NULL,
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `items_name_unique`(`name`) USING BTREE,
  INDEX `items_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES (1, '5 citybreak ideas for this year', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.', NULL, 1, 'published', '2020-07-24', 1, '[\"0\",\"1\"]', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `items` VALUES (2, 'Top 10 restaurants in Italy', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.', NULL, 2, 'published', '2020-07-24', 1, '[\"0\",\"1\"]', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `items` VALUES (3, 'Cocktail ideas for your birthday party', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.', NULL, 2, 'published', '2020-07-24', 1, '[\"0\",\"1\"]', '2020-07-24 06:35:23', '2020-07-24 06:35:23');

-- ----------------------------
-- Table structure for locations
-- ----------------------------
DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `proj_id` int NOT NULL,
  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  `longitude` float(20, 7) NULL DEFAULT NULL,
  `latitude` float(20, 7) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of locations
-- ----------------------------
INSERT INTO `locations` VALUES (3, 10, 'SAMPLE LOCATION PROJECT 1', '2020-08-06', '2020-10-27', 118.5593262, 9.7312298);
INSERT INTO `locations` VALUES (5, 10, 'SAMPLE LOCATION PROJECT 2', '2020-08-07', '2020-10-27', 122.0398026, 12.4104605);
INSERT INTO `locations` VALUES (6, 14, 'PASIG CITY', '2020-08-07', '2020-10-06', 118.5593262, 9.7312298);
INSERT INTO `locations` VALUES (7, 14, 'MANDALUYONG CITY', '2020-10-06', '2020-10-06', 118.5593262, 9.7312298);
INSERT INTO `locations` VALUES (8, 16, 'SAMPLE LOCATION 1', '2020-11-03', '2020-11-03', 12.8902998, 12.8902998);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (2, '2019_01_15_100000_create_roles_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_01_15_110000_create_users_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_01_17_121504_create_categories_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_01_21_130422_create_tags_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_01_21_163402_create_items_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_01_21_163414_create_item_tag_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_03_06_132557_add_photo_column_to_users_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_03_06_143255_add_fields_to_items_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_03_20_090438_add_color_tags_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('bnlayon@neda.gov.ph', '$2y$10$WUp4C7xUrQOe9M/IldHV3OhDkvjrJHAoiHbvIfgFu74qN52jaO3Cy', '2020-11-09 06:00:27');

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `implementingagency` int NOT NULL,
  `sector` int NOT NULL,
  `fundingsource` int NOT NULL,
  `mode` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `category` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES (10, 'SAMPLE NEDA PROJECT 1', 276, 5, 2, 'SAMPLE MODE OF IMPLEMENTATION', '2020-08-06', '2020-08-06', '2020-08-06', '2020-08-07', 3);
INSERT INTO `projects` VALUES (11, 'SAMPLE NEDA PROJECT 2', 276, 1, 2, 'SAMPLE MODE OF IMPLEMENTATION', '2020-08-07', '2020-08-07', '2020-08-07', '2020-08-07', 1);
INSERT INTO `projects` VALUES (16, 'SAMPLE NEDA PROJECT 3', 276, 4, 1, 'SAMPLE MODE OF IMPLEMENTATION', '2020-11-03', '2020-11-30', '2020-11-03', '2020-11-03', 2);
INSERT INTO `projects` VALUES (17, 'SAMPLE', 276, 5, 3, 'sdsdsd', '2020-11-05', '2020-11-05', '2020-11-05', '2020-11-05', 6);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Admin', 'This is the administration role', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `roles` VALUES (2, 'Agency', 'This is the implementing agency role', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `roles` VALUES (3, 'Member', 'This is the member role', '2020-07-24 06:35:23', '2020-07-24 06:35:23');

-- ----------------------------
-- Table structure for sectors
-- ----------------------------
DROP TABLE IF EXISTS `sectors`;
CREATE TABLE `sectors`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sectors
-- ----------------------------
INSERT INTO `sectors` VALUES (1, 'Economic');
INSERT INTO `sectors` VALUES (2, 'Infrastructure');
INSERT INTO `sectors` VALUES (3, 'Social');
INSERT INTO `sectors` VALUES (4, 'Government Institutional Development');
INSERT INTO `sectors` VALUES (5, 'Others');

-- ----------------------------
-- Table structure for sources
-- ----------------------------
DROP TABLE IF EXISTS `sources`;
CREATE TABLE `sources`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sources
-- ----------------------------
INSERT INTO `sources` VALUES (1, 'Official Development Assistance');
INSERT INTO `sources` VALUES (2, 'Local Financing');
INSERT INTO `sources` VALUES (3, 'Public-Private Partnership');
INSERT INTO `sources` VALUES (4, 'Other (please specify)');

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tags_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO `tags` VALUES (1, 'Hot', '#f44336', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `tags` VALUES (2, 'Trending', '#9c27b0', '2020-07-24 06:35:23', '2020-07-24 06:35:23');
INSERT INTO `tags` VALUES (3, 'New', '#00bcd4', '2020-07-24 06:35:23', '2020-07-24 06:35:23');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `role_id` int UNSIGNED NOT NULL,
  `agency_id` int NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE,
  INDEX `users_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 270 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Administrator', 'bnlayon@neda.gov.ph', '2020-07-24 06:35:23', '$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 1, 0, 'RibQufeGkvIgBXwkYL9DeYLVSfpFGOc7vBo26aHFtZx2BPUJDXbwX1RZWl91', '2020-07-24 06:35:23', '2020-11-03 05:24:35');
INSERT INTO `users` VALUES (2, 'NEDA', 'neda@neda.gov.ph', '2020-07-24 06:35:23', '$2y$10$7ve/VFhRY0auWpHhi/QgoOoIPY3/k92oBbnGSuE4wC0e.gHE.tgWm', 'profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpeg', 2, 276, 'TBI2ZqdOnZUJ48OJLydqsPOwiNKtO51XHwLRPKQ3DHbTkMjjir23zNTiqPrY', '2020-07-24 06:35:23', '2020-11-03 05:32:45');
INSERT INTO `users` VALUES (217, 'PA2', 'pa2@pa2.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 2, NULL, NULL, NULL);
INSERT INTO `users` VALUES (218, 'BFAR', 'bfar@bfar.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 3, NULL, NULL, NULL);
INSERT INTO `users` VALUES (219, 'CAAP', 'caap@caap.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 4, NULL, NULL, NULL);
INSERT INTO `users` VALUES (220, 'CHED', 'ched@ched.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 5, NULL, NULL, NULL);
INSERT INTO `users` VALUES (221, 'CPD', 'cpd@cpd.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 6, NULL, NULL, NULL);
INSERT INTO `users` VALUES (222, 'DAR', 'dar@dar.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 7, NULL, NULL, NULL);
INSERT INTO `users` VALUES (223, 'DA', 'da@da.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 8, NULL, NULL, NULL);
INSERT INTO `users` VALUES (224, 'DEPED', 'deped@deped.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 9, NULL, NULL, NULL);
INSERT INTO `users` VALUES (225, 'DOE', 'doe@doe.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 10, NULL, NULL, NULL);
INSERT INTO `users` VALUES (226, 'DENR', 'denr@denr.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 11, NULL, NULL, NULL);
INSERT INTO `users` VALUES (227, 'DOH', 'doh@doh.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 12, NULL, NULL, NULL);
INSERT INTO `users` VALUES (228, 'DICT', 'dict@dict.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 13, NULL, NULL, NULL);
INSERT INTO `users` VALUES (229, 'DOLE', 'dole@dole.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 14, NULL, NULL, NULL);
INSERT INTO `users` VALUES (230, 'DPWH', 'dpwh@dpwh.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 15, NULL, NULL, NULL);
INSERT INTO `users` VALUES (231, 'DOST', 'dost@dost.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 16, NULL, NULL, NULL);
INSERT INTO `users` VALUES (232, 'DSWD', 'dswd@dswd.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 17, NULL, NULL, NULL);
INSERT INTO `users` VALUES (233, 'DILG', 'dilg@dilg.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 18, NULL, NULL, NULL);
INSERT INTO `users` VALUES (234, 'DOT', 'dot@dot.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 19, NULL, NULL, NULL);
INSERT INTO `users` VALUES (235, 'DTI', 'dti@dti.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 20, NULL, NULL, NULL);
INSERT INTO `users` VALUES (236, 'DOTR', 'dotr@dotr.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 21, NULL, NULL, NULL);
INSERT INTO `users` VALUES (237, 'LWUA', 'lwua@lwua.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 22, NULL, NULL, NULL);
INSERT INTO `users` VALUES (238, 'NCCA', 'ncca@ncca.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 23, NULL, NULL, NULL);
INSERT INTO `users` VALUES (239, 'NCIP', 'ncip@ncip.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 24, NULL, NULL, NULL);
INSERT INTO `users` VALUES (240, 'NEA', 'nea@nea.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 25, NULL, NULL, NULL);
INSERT INTO `users` VALUES (241, 'NHA', 'nha@nha.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 26, NULL, NULL, NULL);
INSERT INTO `users` VALUES (242, 'NIA', 'nia@nia.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 27, NULL, NULL, NULL);
INSERT INTO `users` VALUES (243, 'NNC', 'nnc@nnc.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 28, NULL, NULL, NULL);
INSERT INTO `users` VALUES (244, 'NAPOLCOM', 'napolcom@napolcom.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 29, NULL, NULL, NULL);
INSERT INTO `users` VALUES (245, 'NPC', 'npc@npc.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 30, NULL, NULL, NULL);
INSERT INTO `users` VALUES (246, 'OCD', 'ocd@ocd.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 31, NULL, NULL, NULL);
INSERT INTO `users` VALUES (247, 'PCA', 'pca@pca.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 32, NULL, NULL, NULL);
INSERT INTO `users` VALUES (248, 'PDEA', 'pdea@pdea.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 33, NULL, NULL, NULL);
INSERT INTO `users` VALUES (249, 'PFIDA', 'pfida@pfida.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 34, NULL, NULL, NULL);
INSERT INTO `users` VALUES (250, 'PIA', 'pia@pia.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 35, NULL, NULL, NULL);
INSERT INTO `users` VALUES (251, 'PNP', 'pnp@pnp.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 36, NULL, NULL, NULL);
INSERT INTO `users` VALUES (252, 'PPA', 'ppa@ppa.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 37, NULL, NULL, NULL);
INSERT INTO `users` VALUES (253, 'PSA', 'psa@psa.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 38, NULL, NULL, NULL);
INSERT INTO `users` VALUES (254, 'TESDA', 'tesda@tesda.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 39, NULL, NULL, NULL);
INSERT INTO `users` VALUES (255, 'PAWC', 'pawc@pawc.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 40, NULL, NULL, NULL);
INSERT INTO `users` VALUES (256, 'MSC', 'msc@msc.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 41, NULL, NULL, NULL);
INSERT INTO `users` VALUES (257, 'MSCAT', 'mscat@mscat.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 42, NULL, NULL, NULL);
INSERT INTO `users` VALUES (258, 'OMSC', 'omsc@omsc.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 43, NULL, NULL, NULL);
INSERT INTO `users` VALUES (259, 'PSU', 'psu@psu.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 44, NULL, NULL, NULL);
INSERT INTO `users` VALUES (260, 'RSU', 'rsu@rsu.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 45, NULL, NULL, NULL);
INSERT INTO `users` VALUES (261, 'WPU', 'wpu@wpu.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 46, NULL, NULL, NULL);
INSERT INTO `users` VALUES (262, 'MAR', 'mar@mar.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 47, NULL, NULL, NULL);
INSERT INTO `users` VALUES (263, 'OCM', 'ocm@ocm.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 48, NULL, NULL, NULL);
INSERT INTO `users` VALUES (264, 'ORM', 'orm@orm.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 49, NULL, NULL, NULL);
INSERT INTO `users` VALUES (265, 'PAL', 'pal@pal.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 50, NULL, NULL, NULL);
INSERT INTO `users` VALUES (266, 'ROM', 'rom@rom.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 51, NULL, NULL, NULL);
INSERT INTO `users` VALUES (267, 'CAL', 'cal@cal.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 52, NULL, NULL, NULL);
INSERT INTO `users` VALUES (268, 'PP', 'pp@pp.gov.ph', NULL, '$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.', 'profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg', 2, 53, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
