DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`127.0.0.1` SQL SECURITY DEFINER VIEW `latest rpmes form 5 v2` AS (
SELECT
  `agency_form5s_projects`.`project_id` AS `project_id`,
  `agency_form5s_projects`.`status`     AS `status`,
  `agency_form5s_projects`.`allocation` AS `allocation`,
  `agency_form5s_projects`.`male`       AS `male`,
  `agency_form5s_projects`.`female`     AS `female`,
  `projects`.`sector`                   AS `sector`,
  `projects`.`category`                 AS `category`,
  `agency_form5s`.`period`              AS `PERIOD`,
  `agencies`.`Abbreviation`             AS `agency`,
  `agencies`.`id`                       AS `agency_id`
FROM (((`agency_form5s_projects`
     JOIN `agency_form5s`)
    JOIN `projects`)
   JOIN `agencies`)
WHERE `agency_form5s`.`id` = `agency_form5s_projects`.`agency_form5s_id`
    AND `projects`.`id` = `agency_form5s_projects`.`project_id`
    AND `agency_form5s_projects`.`status` = 'Reviewed'
    AND `agency_form5s`.`period` = '2021-03'
    AND `projects`.`implementingagency` = `agencies`.`id`)$$

DELIMITER ;