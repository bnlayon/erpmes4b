/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.11-MariaDB : Database - erpmes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erpmes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `erpmes`;

/*Table structure for table `agencies` */

DROP TABLE IF EXISTS `agencies`;

CREATE TABLE `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) DEFAULT NULL,
  `motheragency_id` int(11) DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) DEFAULT NULL,
  `Category` varchar(10) DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) DEFAULT NULL,
  `head` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `agencies` */

insert  into `agencies`(`id`,`UACS_DPT_ID`,`motheragency_id`,`UACS_AGY_ID`,`Category`,`UACS_DPT_DSC`,`Abbreviation`,`UACS_AGY_DSC`,`head`) values 
(2,NULL,NULL,NULL,NULL,NULL,NULL,'2ND INFANTRY DIVISION, PHILIPPINE ARMY ',NULL),
(3,NULL,NULL,NULL,NULL,NULL,NULL,'BUREAU OF FISHERIES AND AQUATIC RESOURCES MIMAROPA',NULL),
(4,NULL,NULL,NULL,NULL,NULL,NULL,'CIVIL AVIATION AUTHORITY OF THE PHILIPPINES',NULL),
(5,NULL,NULL,NULL,NULL,NULL,NULL,'COMMISSION ON HIGHER EDUCATION MIMAROPA',NULL),
(6,NULL,NULL,NULL,NULL,NULL,NULL,'COMMISSION ON POPULATION AND DEVELOPMENT MIMAROPA',NULL),
(7,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF AGRARIAN REFORM MIMAROPA',NULL),
(8,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF AGRICULTRE MIMAROPA',NULL),
(9,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF EDUCATION MIMAROPA',NULL),
(10,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF ENERGY',NULL),
(11,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES MIMAROPA',NULL),
(12,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF HEALTH MIMAROPA',NULL),
(13,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF INFORMATION AND COMMUNICATIONS TECHNOLOGY - LUZON CLUSTER 3',NULL),
(14,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF LABOR AND EMPLOYMENT MIMAROPA',NULL),
(15,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF PUBLIC WORKS AND HIGHWAYS MIMAROPA',NULL),
(16,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF SCIENCE AND TECHNOLOGY MIMAROPA',NULL),
(17,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF SOCIAL WELFARE AND DEVELOPMENT MIMAROPA',NULL),
(18,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF THE INTERIOR AND LOCAL GOVERNMENT MIMAROPA',NULL),
(19,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF TOURISM MIMAROPA',NULL),
(20,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF TRADE AND INDUSTRY MIMAROPA',NULL),
(21,NULL,NULL,NULL,NULL,NULL,NULL,'DEPARTMENT OF TRANSPORTATION',NULL),
(22,NULL,NULL,NULL,NULL,NULL,NULL,'LOCAL WATER UTILITIES ADMINISTRATION',NULL),
(23,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL COMMISSION FOR CULTURE AND THE ARTS',NULL),
(24,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL COMMISSION ON INDIGENOUS PEOPLES MIMAROPA',NULL),
(25,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL ELECTRIFICATION ADMINISTRATION',NULL),
(26,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL HOUSING AUTHORITY REGION 4',NULL),
(27,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL IRRIGATION ADMINISTRATION MIMAROPA',NULL),
(28,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL NUTRITION COUNCIL MIMAROPA',NULL),
(29,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL POLICE COMMISSION MIMAROPA',NULL),
(30,NULL,NULL,NULL,NULL,NULL,NULL,'NATIONAL POWER CORPORATION',NULL),
(31,NULL,NULL,NULL,NULL,NULL,NULL,'OFFICE OF CIVIL DEFENSE MIMAROPA',NULL),
(32,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE COCONUT AUTHORITY MIMAROPA ',NULL),
(33,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE DRUG ENFORCEMENT AGENCY MIMAROPA',NULL),
(34,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE FIBER INDUSTRY DEVELOPMENT AUTHORITY 4',NULL),
(35,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE INFORMATION AGENCY MIMAROPA',NULL),
(36,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE NATIONAL POLICE MIMAROPA',NULL),
(37,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE PORTS AUTHORITY',NULL),
(38,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE STATISTICS AUTHORITY MIMAROPA',NULL),
(39,NULL,NULL,NULL,NULL,NULL,NULL,'TECHNICAL EDUCATION AND SKILLS DEVELOPMENT AUTHORITY MIMAROPA',NULL),
(40,NULL,NULL,NULL,NULL,NULL,NULL,'WESTERN COMMAND, PHILIPPINE ARMY',NULL),
(41,NULL,NULL,NULL,NULL,NULL,NULL,'MARINDUQUE STATE COLLEGE',NULL),
(42,NULL,NULL,NULL,NULL,NULL,NULL,'MINDORO STATE COLLEGE OF AGRICULTURE AND TECHNOLOGY',NULL),
(43,NULL,NULL,NULL,NULL,NULL,NULL,'OCCIDENTAL MINDORO STATE COLLEGE',NULL),
(44,NULL,NULL,NULL,NULL,NULL,NULL,'PALAWAN STATE UNIVERSITY',NULL),
(45,NULL,NULL,NULL,NULL,NULL,NULL,'ROMBLON STATE UNIVERSITY',NULL),
(46,NULL,NULL,NULL,NULL,NULL,NULL,'WESTERN PHILIPPINES UNIVERSITY',NULL),
(47,NULL,NULL,NULL,NULL,NULL,NULL,'PROVINCIAL GOVERNMENT OF MARINDUQUE',NULL),
(48,NULL,NULL,NULL,NULL,NULL,NULL,'PROVINCIAL GOVERNMENT OF OCCIDENTAL MINDORO',NULL),
(49,NULL,NULL,NULL,NULL,NULL,NULL,'PROVINCIAL GOVERNMENT OF ORIENTAL MINDORO',NULL),
(50,NULL,NULL,NULL,NULL,NULL,NULL,'PROVINCIAL GOVERNMENT OF PALAWAN',NULL),
(51,NULL,NULL,NULL,NULL,NULL,NULL,'PROVINCIAL GOVERNMENT OF ROMBLON',NULL),
(52,NULL,NULL,NULL,NULL,NULL,NULL,'CITY GOVERNMENT OF CALAPAN ',NULL),
(53,NULL,NULL,NULL,NULL,NULL,NULL,'CITY GOVERNMENT OF PUERTO PRINCESA ',NULL),
(276,'24',0,'001','HEAD','National Economic and Development Authority (NEDA)','NEDA','National Economic and Development Authority (NEDA)','Acting Secretary Karl Kendrick T. Chua');

/*Table structure for table `agency_form1s` */

DROP TABLE IF EXISTS `agency_form1s`;

CREATE TABLE `agency_form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `fy` int(4) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form1s` */

insert  into `agency_form1s`(`id`,`status`,`created_at`,`updated_at`,`fy`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(5,'Draft','2020-08-03','2020-10-30',2020,276,'','SAMPLE Rev'),
(7,'Draft','2020-08-07','2020-09-01',2020,219,'','SAMPLE REMARKS');

/*Table structure for table `agency_form1s_projects` */

DROP TABLE IF EXISTS `agency_form1s_projects`;

CREATE TABLE `agency_form1s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form1s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `fs_1` decimal(10,0) DEFAULT 0,
  `fs_2` decimal(10,0) DEFAULT 0,
  `fs_3` decimal(10,0) DEFAULT 0,
  `fs_4` decimal(10,0) DEFAULT 0,
  `fs_5` decimal(10,0) DEFAULT 0,
  `fs_6` decimal(10,0) DEFAULT 0,
  `fs_7` decimal(10,0) DEFAULT 0,
  `fs_8` decimal(10,0) DEFAULT 0,
  `fs_9` decimal(10,0) DEFAULT 0,
  `fs_10` decimal(10,0) DEFAULT 0,
  `fs_11` decimal(10,0) DEFAULT 0,
  `fs_12` decimal(10,0) DEFAULT 0,
  `pt_1` decimal(10,0) DEFAULT 0,
  `pt_2` decimal(10,0) DEFAULT 0,
  `pt_3` decimal(10,0) DEFAULT 0,
  `pt_4` decimal(10,0) DEFAULT 0,
  `pt_5` decimal(10,0) DEFAULT 0,
  `pt_6` decimal(10,0) DEFAULT 0,
  `pt_7` decimal(10,0) DEFAULT 0,
  `pt_8` decimal(10,0) DEFAULT 0,
  `pt_9` decimal(10,0) DEFAULT 0,
  `pt_10` decimal(10,0) DEFAULT 0,
  `pt_11` decimal(10,0) DEFAULT 0,
  `pt_12` decimal(10,0) DEFAULT 0,
  `oi_1` varchar(250) DEFAULT '-',
  `oi_2` varchar(250) DEFAULT '-',
  `oi_3` varchar(250) DEFAULT '-',
  `oi_4` varchar(250) DEFAULT '-',
  `oi_5` varchar(250) DEFAULT '-',
  `oi_6` varchar(250) DEFAULT '-',
  `oi_7` varchar(250) DEFAULT '-',
  `oi_8` varchar(250) DEFAULT '-',
  `oi_9` varchar(250) DEFAULT '-',
  `oi_10` varchar(250) DEFAULT '-',
  `oi_11` varchar(250) DEFAULT '-',
  `oi_12` varchar(250) DEFAULT '-',
  `eg_1` int(11) DEFAULT 0,
  `eg_2` int(11) DEFAULT 0,
  `eg_3` int(11) DEFAULT 0,
  `eg_4` int(11) DEFAULT 0,
  `eg_5` int(11) DEFAULT 0,
  `eg_6` int(11) DEFAULT 0,
  `eg_7` int(11) DEFAULT 0,
  `eg_8` int(11) DEFAULT 0,
  `eg_9` int(11) DEFAULT 0,
  `eg_10` int(11) DEFAULT 0,
  `eg_11` int(11) DEFAULT 0,
  `eg_12` int(11) DEFAULT 0,
  `eg_13` int(11) DEFAULT 0,
  `eg_14` int(11) DEFAULT 0,
  `eg_15` int(11) DEFAULT 0,
  `eg_16` int(11) DEFAULT 0,
  `eg_17` int(11) DEFAULT 0,
  `eg_18` int(11) DEFAULT 0,
  `eg_19` int(11) DEFAULT 0,
  `eg_20` int(11) DEFAULT 0,
  `eg_21` int(11) DEFAULT 0,
  `eg_22` int(11) DEFAULT 0,
  `eg_23` int(11) DEFAULT 0,
  `eg_24` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form1s_projects` */

insert  into `agency_form1s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form1s_id`,`nro_remarks`,`agency_remarks`,`fs_1`,`fs_2`,`fs_3`,`fs_4`,`fs_5`,`fs_6`,`fs_7`,`fs_8`,`fs_9`,`fs_10`,`fs_11`,`fs_12`,`pt_1`,`pt_2`,`pt_3`,`pt_4`,`pt_5`,`pt_6`,`pt_7`,`pt_8`,`pt_9`,`pt_10`,`pt_11`,`pt_12`,`oi_1`,`oi_2`,`oi_3`,`oi_4`,`oi_5`,`oi_6`,`oi_7`,`oi_8`,`oi_9`,`oi_10`,`oi_11`,`oi_12`,`eg_1`,`eg_2`,`eg_3`,`eg_4`,`eg_5`,`eg_6`,`eg_7`,`eg_8`,`eg_9`,`eg_10`,`eg_11`,`eg_12`,`eg_13`,`eg_14`,`eg_15`,`eg_16`,`eg_17`,`eg_18`,`eg_19`,`eg_20`,`eg_21`,`eg_22`,`eg_23`,`eg_24`) values 
(1,10,'Reviewed','2020-11-03','2020-11-05',5,'SAMPLE REVIEWS','SAMPLE REMARKS',0,0,0,0,0,0,0,0,100000,100000,0,0,0,0,0,12,0,0,0,0,0,50,25,5,'-','-','-','-','-','-','-','-','-','Sample Output Indicator','Sample Output Indicator','Sample Output Indicator',12,12,12,12,12,12,21,12,12,12,12,12,1,0,0,0,0,0,0,0,0,0,0,0);

/*Table structure for table `agency_form2s` */

DROP TABLE IF EXISTS `agency_form2s`;

CREATE TABLE `agency_form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form2s` */

insert  into `agency_form2s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2020-08-26','2020-10-30','2020-07',276,'','SA');

/*Table structure for table `agency_form2s_projects` */

DROP TABLE IF EXISTS `agency_form2s_projects`;

CREATE TABLE `agency_form2s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form2s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `alloc_asof` decimal(20,0) DEFAULT 0,
  `alloc_month` decimal(20,0) DEFAULT 0,
  `releases_asof` decimal(20,0) DEFAULT 0,
  `releases_month` decimal(20,0) DEFAULT 0,
  `obligations_asof` decimal(20,0) DEFAULT 0,
  `obligations_month` decimal(20,0) DEFAULT 0,
  `expenditures_asof` decimal(20,0) DEFAULT 0,
  `expenditures_month` decimal(20,0) DEFAULT 0,
  `male` int(5) DEFAULT 0,
  `female` int(5) DEFAULT 0,
  `oi` decimal(20,0) DEFAULT 0,
  `ttd` decimal(20,0) DEFAULT 0,
  `tftm` decimal(20,0) DEFAULT 0,
  `atd` decimal(20,0) DEFAULT 0,
  `aftm` decimal(20,0) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form2s_projects` */

insert  into `agency_form2s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form2s_id`,`nro_remarks`,`agency_remarks`,`alloc_asof`,`alloc_month`,`releases_asof`,`releases_month`,`obligations_asof`,`obligations_month`,`expenditures_asof`,`expenditures_month`,`male`,`female`,`oi`,`ttd`,`tftm`,`atd`,`aftm`) values 
(1,10,'Endorsed','2020-11-03','2020-11-03',1,NULL,'SAMPLE',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

/*Table structure for table `agency_form3s` */

DROP TABLE IF EXISTS `agency_form3s`;

CREATE TABLE `agency_form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form3s` */

insert  into `agency_form3s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(22,'Draft','2020-09-14','2020-10-30','2020-09',276,NULL,NULL);

/*Table structure for table `agency_form3s_projects` */

DROP TABLE IF EXISTS `agency_form3s_projects`;

CREATE TABLE `agency_form3s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form3s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `findings` text DEFAULT NULL,
  `possible` text DEFAULT NULL,
  `recommendations` text DEFAULT NULL,
  `impstatus` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form3s_projects` */

insert  into `agency_form3s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form3s_id`,`nro_remarks`,`agency_remarks`,`findings`,`possible`,`recommendations`,`impstatus`) values 
(1,10,'Endorsed','2020-11-03','2020-11-03',22,NULL,'SAMPLE',NULL,NULL,NULL,1);

/*Table structure for table `agency_form4s` */

DROP TABLE IF EXISTS `agency_form4s`;

CREATE TABLE `agency_form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form4s` */

insert  into `agency_form4s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2020-09-28','2020-10-06','2020-09',276,'','SAMPLE REMARKS');

/*Table structure for table `agency_form4s_projects` */

DROP TABLE IF EXISTS `agency_form4s_projects`;

CREATE TABLE `agency_form4s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form4s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `objectives` text DEFAULT NULL,
  `indicator` text DEFAULT NULL,
  `results` text DEFAULT NULL,
  `impstatus` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form4s_projects` */

insert  into `agency_form4s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form4s_id`,`nro_remarks`,`agency_remarks`,`objectives`,`indicator`,`results`,`impstatus`) values 
(1,10,'Endorsed','2020-11-03','2020-11-03',1,NULL,'SAMPLE',NULL,NULL,NULL,NULL);

/*Table structure for table `agency_form5s` */

DROP TABLE IF EXISTS `agency_form5s`;

CREATE TABLE `agency_form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form5s` */

insert  into `agency_form5s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2020-11-03','2020-11-03','2020-11',195,NULL,NULL),
(2,'Draft','2020-11-03','2020-11-03','2020-11',276,NULL,NULL);

/*Table structure for table `agency_form5s_projects` */

DROP TABLE IF EXISTS `agency_form5s_projects`;

CREATE TABLE `agency_form5s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form5s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `allocation` decimal(20,0) DEFAULT NULL,
  `releases` decimal(20,0) DEFAULT NULL,
  `obligations` decimal(20,0) DEFAULT NULL,
  `expenditures` decimal(20,0) DEFAULT NULL,
  `tod` decimal(20,0) DEFAULT NULL,
  `atd` decimal(20,0) DEFAULT NULL,
  `male` int(10) DEFAULT NULL,
  `female` int(10) DEFAULT NULL,
  `remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form5s_projects` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category`,`created_at`,`updated_at`) values 
(1,'ELCAC','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Balik Probinsya Program (BP2)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Rehab and Recovery Projects (RRP)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(6,'Official Development Assistance (ODA)',NULL,NULL);

/*Table structure for table `form1s` */

DROP TABLE IF EXISTS `form1s`;

CREATE TABLE `form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fy` int(4) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form1s` */

insert  into `form1s`(`id`,`fy`,`is_lock`,`created_at`,`updated_at`) values 
(1,2020,0,'2020-07-31','2020-10-12');

/*Table structure for table `form2s` */

DROP TABLE IF EXISTS `form2s`;

CREATE TABLE `form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form2s` */

insert  into `form2s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(2,'2020-07',0,'2020-08-26','2020-09-28');

/*Table structure for table `form3s` */

DROP TABLE IF EXISTS `form3s`;

CREATE TABLE `form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form3s` */

insert  into `form3s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-09',0,'2020-09-14','2020-09-28');

/*Table structure for table `form4s` */

DROP TABLE IF EXISTS `form4s`;

CREATE TABLE `form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form4s` */

insert  into `form4s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-09',0,'2020-09-28','2020-10-02');

/*Table structure for table `form5s` */

DROP TABLE IF EXISTS `form5s`;

CREATE TABLE `form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form5s` */

insert  into `form5s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(2,'2020-11',1,'2020-11-03','2020-11-03');

/*Table structure for table `impstatuses` */

DROP TABLE IF EXISTS `impstatuses`;

CREATE TABLE `impstatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `impstatuses` */

insert  into `impstatuses`(`id`,`type`) values 
(1,'Ahead of Schedule'),
(2,'On Schedule'),
(3,'Behind Schedule');

/*Table structure for table `item_tag` */

DROP TABLE IF EXISTS `item_tag`;

CREATE TABLE `item_tag` (
  `item_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `item_tag_item_id_foreign` (`item_id`),
  KEY `item_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `item_tag` */

insert  into `item_tag`(`item_id`,`tag_id`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(3,1);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `items_name_unique` (`name`),
  KEY `items_category_id_foreign` (`category_id`),
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`description`,`picture`,`category_id`,`status`,`date`,`show_on_homepage`,`options`,`created_at`,`updated_at`) values 
(1,'5 citybreak ideas for this year','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,1,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Top 10 restaurants in Italy','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Cocktail ideas for your birthday party','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(10) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `longitude` float(20,7) DEFAULT NULL,
  `latitude` float(20,7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

/*Data for the table `locations` */

insert  into `locations`(`id`,`proj_id`,`location`,`created_at`,`updated_at`,`longitude`,`latitude`) values 
(3,10,'SAMPLE LOCATION PROJECT 1','2020-08-06','2020-10-27',118.5593262,9.7312298),
(5,10,'SAMPLE LOCATION PROJECT 2','2020-08-07','2020-10-27',122.0398026,12.4104605),
(6,14,'PASIG CITY','2020-08-07','2020-10-06',118.5593262,9.7312298),
(7,14,'MANDALUYONG CITY','2020-10-06','2020-10-06',118.5593262,9.7312298),
(8,16,'SAMPLE LOCATION 1','2020-11-03','2020-11-03',12.8902998,12.8902998);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_100000_create_password_resets_table',1),
(2,'2019_01_15_100000_create_roles_table',1),
(3,'2019_01_15_110000_create_users_table',1),
(4,'2019_01_17_121504_create_categories_table',1),
(5,'2019_01_21_130422_create_tags_table',1),
(6,'2019_01_21_163402_create_items_table',1),
(7,'2019_01_21_163414_create_item_tag_table',1),
(8,'2019_03_06_132557_add_photo_column_to_users_table',1),
(9,'2019_03_06_143255_add_fields_to_items_table',1),
(10,'2019_03_20_090438_add_color_tags_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `implementingagency` int(3) NOT NULL,
  `sector` int(1) NOT NULL,
  `fundingsource` int(1) NOT NULL,
  `mode` varchar(500) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `category` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `projects` */

insert  into `projects`(`id`,`title`,`implementingagency`,`sector`,`fundingsource`,`mode`,`start`,`end`,`created_at`,`updated_at`,`category`) values 
(10,'SAMPLE NEDA PROJECT 1',276,5,2,'SAMPLE MODE OF IMPLEMENTATION','2020-08-06','2020-08-06','2020-08-06','2020-08-07',3),
(11,'SAMPLE NEDA PROJECT 2',276,1,2,'SAMPLE MODE OF IMPLEMENTATION','2020-08-07','2020-08-07','2020-08-07','2020-08-07',1),
(16,'SAMPLE NEDA PROJECT 3',276,4,1,'SAMPLE MODE OF IMPLEMENTATION','2020-11-03','2020-11-30','2020-11-03','2020-11-03',2),
(17,'SAMPLE',276,5,3,'sdsdsd','2020-11-05','2020-11-05','2020-11-05','2020-11-05',6);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'Admin','This is the administration role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Agency','This is the implementing agency role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Member','This is the member role','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `sectors` */

DROP TABLE IF EXISTS `sectors`;

CREATE TABLE `sectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sectors` */

insert  into `sectors`(`id`,`sector`) values 
(1,'Economic'),
(2,'Infrastructure'),
(3,'Social'),
(4,'Government Institutional Development'),
(5,'Others');

/*Table structure for table `sources` */

DROP TABLE IF EXISTS `sources`;

CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sources` */

insert  into `sources`(`id`,`type`) values 
(1,'Official Development Assistance'),
(2,'Local Financing'),
(3,'Public-Private Partnership'),
(4,'Other (please specify)');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`color`,`created_at`,`updated_at`) values 
(1,'Hot','#f44336','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Trending','#9c27b0','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'New','#00bcd4','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `agency_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`picture`,`role_id`,`agency_id`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Administrator','bnlayon@neda.gov.ph','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i','profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg',1,0,'xtQvtDcTm02WgMvqtVpmH8q4xZduJZ5d0FGPNGeK9tDhH46l5TeeIjbvrVMB','2020-07-24 06:35:23','2020-11-03 05:24:35'),
(2,'NEDA','neda@neda.gov.ph','2020-07-24 06:35:23','$2y$10$7ve/VFhRY0auWpHhi/QgoOoIPY3/k92oBbnGSuE4wC0e.gHE.tgWm','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpeg',2,276,'ysbEpboNC5XEWTHJfcOBtUSGHc4q8vY2PlGHzerb2x0NoJNa1nwdj5FFGwPO','2020-07-24 06:35:23','2020-11-03 05:32:45'),
(4,'DPWH','dpwh@dpwh.gov.ph',NULL,'$2y$10$T1Gq701Lb7POFc8jGSDSUOBKgRygzP6hFnESDlpP6ZdVV6fZuXWP.','profile/xrL9qABk32IzgNNV3Jt7zdSM4CyKKWEVLVKXXHzn.jpeg',2,2,NULL,'2020-10-30 03:35:18','2020-11-03 05:32:33');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
