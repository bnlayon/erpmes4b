/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.11-MariaDB : Database - iams
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`iams` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `iams`;

/*Table structure for table `administrations` */

DROP TABLE IF EXISTS `administrations`;

CREATE TABLE `administrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tasks` varchar(200) DEFAULT NULL,
  `is_lock` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `administrations` */

insert  into `administrations`(`id`,`tasks`,`is_lock`,`created_at`,`updated_at`) values 
(1,'Risk Register Submission',1,NULL,'2020-10-05');

/*Table structure for table `areabudgets` */

DROP TABLE IF EXISTS `areabudgets`;

CREATE TABLE `areabudgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditarea_id` int(11) DEFAULT NULL,
  `particular` varchar(1000) DEFAULT NULL,
  `region` varchar(1000) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `areabudgets` */

insert  into `areabudgets`(`id`,`auditarea_id`,`particular`,`region`,`amount`,`created_at`,`updated_at`) values 
(1,22,'Per diem (P800 x 3 pax/ NRO x 5 days x 6 NROs)','CAR, 1, 3,4A',72000,'2020-10-16','2020-10-16'),
(2,22,'Per diem (P800 x 6 pax x 5 days)','2',24000,'2020-10-16','2020-10-16'),
(3,22,'Taxi Fare (500 x 2 x 7 NROs)','All Regions',7000,'2020-10-16','2020-10-16');

/*Table structure for table `areacriterias` */

DROP TABLE IF EXISTS `areacriterias`;

CREATE TABLE `areacriterias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditarea_id` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `areacriterias` */

/*Table structure for table `areamethodologies` */

DROP TABLE IF EXISTS `areamethodologies`;

CREATE TABLE `areamethodologies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditarea_id` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `areamethodologies` */

/*Table structure for table `areaobjs` */

DROP TABLE IF EXISTS `areaobjs`;

CREATE TABLE `areaobjs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditarea_id` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `areaobjs` */

/*Table structure for table `areascopes` */

DROP TABLE IF EXISTS `areascopes`;

CREATE TABLE `areascopes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditarea_id` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `areascopes` */

insert  into `areascopes`(`id`,`auditarea_id`,`description`,`created_at`,`updated_at`) values 
(1,21,'sample scope','2020-10-16','2020-10-16'),
(4,22,'Pillar I (Compliance with the Legislative and Regulatory Framework) - looks at compliance with key legal requirements under RA 9184 as it relates to competitiveness;','2020-10-16','2020-10-16'),
(5,22,'Pillar II (Agency Institutional Framework and Management Capacity) - looks at how the procurement system is operational through the management systems in the agency;','2020-10-16','2020-10-16'),
(6,22,'Pillar Ill (Procurement Operations and market Practices)  - looks at the operational effectiveness and efficiency of the procurement system at the agency level; and','2020-10-16','2020-10-16'),
(7,22,'Pillar IV (Integrity and Transparency of the Agency Procurement System) - looks at indicators of the procurement system that contribute to integrity and transparency','2020-10-16','2020-10-16');

/*Table structure for table `areaworks` */

DROP TABLE IF EXISTS `areaworks`;

CREATE TABLE `areaworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditarea_id` varchar(11) DEFAULT NULL,
  `title` varchar(1000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `report_date` date DEFAULT NULL,
  `final_report` date DEFAULT NULL,
  `team_leader_id` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `areaworks` */

insert  into `areaworks`(`id`,`auditarea_id`,`title`,`start_date`,`end_date`,`report_date`,`final_report`,`team_leader_id`,`created_at`,`updated_at`) values 
(4,'20','sample work area 2','2020-10-19','2020-10-23','2020-10-26','2020-10-30',8,'2020-10-16','2020-10-16'),
(5,'22','NCO Procurement Management Division','2020-10-15','2020-10-15','2020-10-15','2020-10-15',10,'2020-10-16','2020-10-16'),
(6,'22','Region 4A, 4B and 5','2020-11-09','2020-11-20','2020-11-23','2020-11-30',10,'2020-10-16','2020-10-16'),
(7,'22','CAR, REgion 1 and Region 3','2020-12-07','2020-12-11','2020-12-14','2020-12-25',8,'2020-10-16','2020-10-16'),
(8,'22','Region 2','2021-01-04','2021-01-08','2021-01-11','2021-01-29',8,'2020-10-16','2020-10-16');

/*Table structure for table `assessments` */

DROP TABLE IF EXISTS `assessments`;

CREATE TABLE `assessments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `risk_id` int(1) DEFAULT NULL,
  `impact` int(1) DEFAULT NULL,
  `likelihood` int(1) DEFAULT NULL,
  `date_assess` date DEFAULT NULL,
  `user_assess` int(2) DEFAULT NULL,
  `ias_assess` varchar(1) DEFAULT NULL,
  `ias_remarks` text DEFAULT NULL,
  `riskreg_id` int(3) DEFAULT NULL,
  `is_priority` int(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

/*Data for the table `assessments` */

insert  into `assessments`(`id`,`created_at`,`updated_at`,`risk_id`,`impact`,`likelihood`,`date_assess`,`user_assess`,`ias_assess`,`ias_remarks`,`riskreg_id`,`is_priority`) values 
(16,'2020-10-07','2020-10-19',2,2,5,'2020-10-07',2,'Y','YES',1,0);

/*Table structure for table `auditareas` */

DROP TABLE IF EXISTS `auditareas`;

CREATE TABLE `auditareas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) DEFAULT NULL,
  `audit_type` varchar(1000) DEFAULT NULL,
  `assurance_type` varchar(1000) DEFAULT NULL,
  `cp_type` varchar(1000) DEFAULT NULL,
  `risk_id` varchar(1000) DEFAULT NULL,
  `objective` varchar(1000) DEFAULT NULL,
  `scope` varchar(1000) DEFAULT NULL,
  `responsible_div` varchar(1000) DEFAULT NULL,
  `init_type` varchar(1000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

/*Data for the table `auditareas` */

insert  into `auditareas`(`id`,`title`,`audit_type`,`assurance_type`,`cp_type`,`risk_id`,`objective`,`scope`,`responsible_div`,`init_type`,`start_date`,`end_date`,`created_at`,`updated_at`) values 
(21,'Property and Supply Management System','ASSURANCE','CONTROL PROCESSES','OPERATIONS',NULL,'To examine the possible causes of recurring Commission on Audit (COA) findings on NEDA’s reports on physical count of property, plant and equipment (RPCPPE) from CY 2015 to 2019;','NEDA CY 2015 to 2019 RPCPPEs; internal policies and procedures instituted by NEDA and its Administrative Staff (AdS) in relation to property and supply management, and other relevant documents of the AdS Asset Management Division.','MAD','BASED ON ANNUAL AUDIT PLAN','2020-10-30','2020-12-31','2020-10-15','2020-10-15'),
(22,'Review of the Adoption and Implementation of the Agency  Procurement Compliance and Performance Indicators as a Standard Procurement and Monitoring and Assessment Tool at the NEDA Central Office and Selected Regional Offices','ASSURANCE','CONTROL PROCESSES','OPERATIONS',NULL,'Verify the extent of implementation an6 compliance of concerned officials and staff of the NEDA CO and NROs in the assessment of its respective procurement activities usirlg the APCPI, as required by GPPB Resolution No. 10-2012, and ?ther pertinent government procurement laws, rules and regulations;','Pillar I (Compliance with the Legislative and Regulatory Framework): looks at compliance to key legal requi rments under RA 9184 as it relates to competitiveness;','MAD','BASED ON ANNUAL AUDIT PLAN','2020-10-19','2020-12-30','2020-10-15','2020-10-15');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`name`,`color`,`description`,`created_at`,`updated_at`) values 
(1,'Travel',NULL,'Travel ideas for everyone','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(2,'Food',NULL,'Our favourite recipes','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(3,'Home',NULL,'The latest trends in home decorations','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(4,'Fashion',NULL,'Stay in touch with the latest trends','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(5,'Health',NULL,'An apple a day keeps the doctor away','2020-08-11 02:11:37','2020-08-11 02:11:37');

/*Table structure for table `clusters` */

DROP TABLE IF EXISTS `clusters`;

CREATE TABLE `clusters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `cluster` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `clusters` */

insert  into `clusters`(`id`,`created_at`,`updated_at`,`cluster`) values 
(1,NULL,NULL,'GAS/STO'),
(2,NULL,NULL,'MFO1'),
(3,NULL,NULL,'MFO2'),
(4,NULL,NULL,'MFO3');

/*Table structure for table `divisions` */

DROP TABLE IF EXISTS `divisions`;

CREATE TABLE `divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `staff_id` int(2) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `divisions` */

insert  into `divisions`(`id`,`created_at`,`updated_at`,`staff_id`,`name`) values 
(1,NULL,NULL,1,'ICT Network Division'),
(2,NULL,NULL,1,'Information Systems Development Division');

/*Table structure for table `item_tag` */

DROP TABLE IF EXISTS `item_tag`;

CREATE TABLE `item_tag` (
  `item_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `item_tag_item_id_foreign` (`item_id`),
  KEY `item_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `item_tag` */

insert  into `item_tag`(`item_id`,`tag_id`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(3,1);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `items_name_unique` (`name`),
  KEY `items_category_id_foreign` (`category_id`),
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`description`,`picture`,`category_id`,`status`,`date`,`show_on_homepage`,`options`,`created_at`,`updated_at`) values 
(1,'5 citybreak ideas for this year','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.</p><p>Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.</p>',NULL,1,'published','2020-08-11',1,'[\"0\",\"1\"]','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(2,'Top 10 restaurants in Italy','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.</p><p>Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.</p>',NULL,2,'published','2020-08-11',1,'[\"0\",\"1\"]','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(3,'Cocktail ideas for your birthday party','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.</p><p>Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.</p>',NULL,2,'published','2020-08-11',1,'[\"0\",\"1\"]','2020-08-11 02:11:37','2020-08-11 02:11:37');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_100000_create_password_resets_table',1),
(2,'2019_01_15_100000_create_roles_table',1),
(3,'2019_01_15_110000_create_users_table',1),
(4,'2019_01_17_121504_create_categories_table',1),
(5,'2019_01_21_130422_create_tags_table',1),
(6,'2019_01_21_163402_create_items_table',1),
(7,'2019_01_21_163414_create_item_tag_table',1),
(8,'2019_03_06_132557_add_photo_column_to_users_table',1),
(9,'2019_03_20_090438_add_color_tags_table',1);

/*Table structure for table `objectives` */

DROP TABLE IF EXISTS `objectives`;

CREATE TABLE `objectives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective` text DEFAULT NULL,
  `staff_id` int(2) DEFAULT NULL,
  `is_active` int(1) DEFAULT 1,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `positive` text DEFAULT NULL,
  `negative` text DEFAULT NULL,
  `division` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `objectives` */

insert  into `objectives`(`id`,`objective`,`staff_id`,`is_active`,`created_at`,`updated_at`,`positive`,`negative`,`division`) values 
(15,'90% Available and functional network and hardware services',1,1,'2020-08-27','2020-08-27','Stable internet connection; All ICT equipments are working properly','Loss internent connection; Issues on ICT equipments',1),
(16,'90% Accessible and functional existing information systems',1,1,'2020-08-27','2020-08-27','High percentage of uptime in all information systems','Internet Service, Communication and Other Service Provider Issues',2),
(17,'90% Accessible and functional existing ICT Equipment',1,1,'2020-08-27','2020-10-07','ICT equipment functioning normal','Poor performance of ICT equipment',1),
(18,'100% Updated Disaster Recovery Plan',1,1,'2020-08-27','2020-08-27','Clear instructions and guidelines for data back up and plan in case of disaster','Incomplete details on how to restore system in case of back up',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `processes` */

DROP TABLE IF EXISTS `processes`;

CREATE TABLE `processes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `process` varchar(1000) DEFAULT NULL,
  `staff_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `processes` */

insert  into `processes`(`id`,`created_at`,`updated_at`,`process`,`staff_id`) values 
(1,NULL,NULL,'Information and Communications Technology',1);

/*Table structure for table `riskregisters` */

DROP TABLE IF EXISTS `riskregisters`;

CREATE TABLE `riskregisters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riskregister` year(4) DEFAULT NULL,
  `is_lock` int(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `riskregisters` */

insert  into `riskregisters`(`id`,`riskregister`,`is_lock`,`created_at`,`updated_at`) values 
(1,2020,0,NULL,'2020-10-19');

/*Table structure for table `riskregistersubmissions` */

DROP TABLE IF EXISTS `riskregistersubmissions`;

CREATE TABLE `riskregistersubmissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(2) DEFAULT NULL,
  `riskreg_id` int(3) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `riskregistersubmissions` */

insert  into `riskregistersubmissions`(`id`,`staff_id`,`riskreg_id`,`status`,`created_at`,`updated_at`) values 
(2,1,1,'For Clearance','2020-10-07','2020-10-07');

/*Table structure for table `risks` */

DROP TABLE IF EXISTS `risks`;

CREATE TABLE `risks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `objective_id` int(3) DEFAULT NULL,
  `definition` text DEFAULT NULL,
  `potential` text DEFAULT NULL,
  `control` text DEFAULT NULL,
  `cluster` int(1) DEFAULT NULL,
  `process_id` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `risks` */

insert  into `risks`(`id`,`created_at`,`updated_at`,`objective_id`,`definition`,`potential`,`control`,`cluster`,`process_id`) values 
(2,'2020-08-28','2020-08-28',17,'Unavailability and expired security applications','Deliberate Software Attacks and Malwares','Updated Anti-virus and anti malware software',1,1),
(3,'2020-08-28','2020-08-28',17,'Users violate security policies in the use of ICT resources','Disclosure of classified data, entry of erroneous data, accidental deletion or modification of data, storage of data in unprotected areas and failure to protect information','Implementation of ICT Guidelines',2,1),
(4,'2020-08-28','2020-08-28',17,'Nonperformance of regular maintenance measures of PCs and Laptops','Prone/Abuse to Software Attacks and Malwares; Breakdown of PCs and laptops','Regular maintenance check of PCs and Laptops',1,1),
(5,'2020-10-07','2020-10-07',16,'SAMPLE','SAMPLE','SAMPLE',1,1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'Admin','This is the administration role','2020-08-11 02:11:36','2020-08-11 02:11:36'),
(2,'Creator','This is the creator role','2020-08-11 02:11:36','2020-08-11 02:11:36'),
(3,'Member','This is the member role','2020-08-11 02:11:36','2020-08-11 02:11:36'),
(4,'IAS DC',NULL,NULL,NULL),
(5,'IAS Director',NULL,NULL,NULL),
(6,'IAS Auditor',NULL,NULL,NULL);

/*Table structure for table `staffs` */

DROP TABLE IF EXISTS `staffs`;

CREATE TABLE `staffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff` varchar(100) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `staffs` */

insert  into `staffs`(`id`,`staff`,`name`) values 
(1,'ICTS','Information and Communications Technology Staff'),
(2,'AS','Administrative Staff'),
(3,'DIS','Development Information Staff'),
(4,'FPMS','Financial, Planning, and Management Staff'),
(5,'IS','Infrastructure Staff'),
(6,'PIS','Public Investment Staff'),
(7,'SDS','Social Development Staff'),
(8,'MES','Monitoring and Evaluation Staff'),
(9,'RO 1','Regional Office 1'),
(10,'RO 2','Regional Office 2');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`color`,`created_at`,`updated_at`) values 
(1,'Hot','#f44336','2020-08-11 02:11:36','2020-08-11 02:11:36'),
(2,'Trending','#9c27b0','2020-08-11 02:11:37','2020-08-11 02:11:37'),
(3,'New','#00bcd4','2020-08-11 02:11:37','2020-08-11 02:11:37');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `staff_id` int(2) DEFAULT NULL,
  `is_director` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`picture`,`role_id`,`remember_token`,`created_at`,`updated_at`,`staff_id`,`is_director`) values 
(1,'Admin','admin@neda.gov.ph','2020-08-11 02:11:36','$2y$10$9JENsczxYp9O8e2HzARLXuwb6qxmCAs1CN8Q7FMJJ3/mNVhZTFF76',NULL,1,'xDAz324u5N0wmVL8bpMQ0mUpQwB8qZdbk9MHJ9PKG9x3g9Q277QIBHvlI3bd','2020-08-11 02:11:36','2020-08-11 02:11:36',99,NULL),
(2,'ICTS User','icts@neda.gov.ph','2020-08-11 02:11:36','$2y$10$9JENsczxYp9O8e2HzARLXuwb6qxmCAs1CN8Q7FMJJ3/mNVhZTFF76',NULL,2,'WFZID8OzUPgIFWsmpsqxf5DFkX32iTXsjgotT9Rc9JkRTSgpBbDTcQkqEs4k','2020-08-11 02:11:36','2020-08-11 02:11:36',1,NULL),
(3,'ICTS Dir','ictsdir@neda.gov.ph','2020-08-11 02:11:36','$2y$10$9JENsczxYp9O8e2HzARLXuwb6qxmCAs1CN8Q7FMJJ3/mNVhZTFF76',NULL,3,'sTOyLkcZYKfFIVVt1nfQQvrY3AuxlmSIJeZ9VuMFEUJtEgAH95zIDSisVPc6','2020-08-11 02:11:36','2020-08-11 02:11:36',1,1),
(5,'IAS DC','iasdc@neda.gov.ph',NULL,'$2y$10$RUQdGGQrFlYJUNLwt4B3KeT9oLfH1qXRzpY.23o5TplhlrvWz26oe',NULL,4,NULL,'2020-10-14 08:51:33','2020-10-14 08:51:33',NULL,NULL),
(6,'IAS Director','iasdirector@neda.gov.ph',NULL,'$2y$10$pZbKgejSWWsShluIpqf58eZUSRs2pIyM5cb9XcIM54BtLuDq7gluu',NULL,5,NULL,'2020-10-14 08:52:56','2020-10-14 08:52:56',NULL,NULL),
(7,'IAS Auditor','auditor@neda.gov.ph',NULL,'$2y$10$G.mscHcwIxqQuMrsNGT/j.wK6dj0b0Zdecfn4UWk4YFxavBQkjXt2',NULL,6,NULL,'2020-10-14 08:53:43','2020-10-14 08:53:43',NULL,NULL),
(8,'Arceli Divina','arceli@neda.gov.ph',NULL,'$2y$10$HNkMrLR5fGliSD6lHBbQp.JVsGj8R7m654eIXMyMCNtC9K1YqPFhO',NULL,6,NULL,'2020-10-15 09:24:47','2020-10-15 09:24:47',NULL,NULL),
(9,'Michelle Mangibin','michelle@neda.gov.ph',NULL,'$2y$10$jZQ9sC7QbTfXS/o53.DQPezQrgWd4t6/tUhdvOUgy/dOgVE7CiN72',NULL,6,NULL,'2020-10-15 09:25:39','2020-10-15 09:25:39',NULL,NULL),
(10,'Juan Angelo Rocamora','juan@neda.gov.ph',NULL,'$2y$10$NNGIouaxnIkLa4tXArLl.On9vyUEDY.kiRJXfRLD3O6Qt7YLce5fO',NULL,6,NULL,'2020-10-15 09:26:58','2020-10-15 09:26:58',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
