/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.11-MariaDB : Database - erpmes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erpmes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `erpmes`;

/*Table structure for table `agencies` */

DROP TABLE IF EXISTS `agencies`;

CREATE TABLE `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) DEFAULT NULL,
  `motheragency_id` int(11) DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) DEFAULT NULL,
  `Category` varchar(10) DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) DEFAULT NULL,
  `head` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=441 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `agencies` */

insert  into `agencies`(`id`,`UACS_DPT_ID`,`motheragency_id`,`UACS_AGY_ID`,`Category`,`UACS_DPT_DSC`,`Abbreviation`,`UACS_AGY_DSC`,`head`) values 
(1,'01',0,'003','HEAD','Commission on Appointments','CA','Commission on Appointments',NULL),
(2,'01',0,'004','HEAD','House of Representatives','HOR','House of Representatives',NULL),
(3,'01',0,'005','HEAD','House of Representatives Electoral Tribunal','HOR-ET','House of Representatives Electoral Tribunal',NULL),
(4,'01',0,'001','HEAD','Senate of the Philippines','Senate','Senate of the Philippines',NULL),
(5,'01',0,'002','HEAD','Senate Electoral Tribunal','SET','Senate Electoral Tribunal',NULL),
(6,'02',0,'003','ATT','Office of Cabinet Secretary','OCS','Office of the Cabinet Secretary',NULL),
(7,'02',0,'001','HEAD','Office of the President','OP','Office of the President',NULL),
(8,'03',0,'000','HEAD','Office of the Vice-President','OVP','Office of the Vice-President',NULL),
(9,'04',0,'001','HEAD','Department of Agrarian Reform','DAR','Department of Agrarian Reform',NULL),
(10,'05',19,'014','ATT','Department of Agriculture (DA)','CODA','Cotton Development Administration',NULL),
(11,'05',19,'015','ATT','Department of Agriculture (DA)','LDC','Livestock Development Council',NULL),
(12,'05',19,'002','ATT','Department of Agriculture (DA)','ACPC','Agricultural Credit Policy Council',NULL),
(13,'05',19,'003','ATT','Department of Agriculture (DA)','BFAR','Bureau of Fisheries and Aquatic Resources',NULL),
(14,'05',19,'009','ATT','Department of Agriculture (DA)','NMIS','National Meat Inspection Service',NULL),
(15,'05',19,'010','ATT','Department of Agriculture (DA)','PCC','Philippine Carabao Center',NULL),
(16,'05',19,'011','ATT','Department of Agriculture (DA)','PHILMECH','Philippine Center for Post-Harvest Development and Mechanization',NULL),
(17,'05',19,'013','ATT','Department of Agriculture (DA)','PCAD','Philippine Council for Agriculture and Fisheries  ',NULL),
(18,'05',19,'012','ATT','Department of Agriculture (DA)','PFIDA','Philippine Fiber Industry Development Authority',NULL),
(19,'05',0,'001','HEAD','Department of Agriculture (DA)','DA','Department of Agriculture (DA)',NULL),
(20,'06',23,'004','ATT','Department of Budget and Management (DBM)','PhilGEPS','Philippine Government Electronic Procurement System ',NULL),
(21,'06',23,'003','ATT','Department of Budget and Management (DBM)','DBM-PS','Procurement Service',NULL),
(22,'06',23,'002','ATT','Department of Budget and Management (DBM)','GPPB','Government Procurement Policy Board-Technical Support Office',NULL),
(23,'06',0,'001','HEAD','Department of Budget and Management (DBM)','DBM','Department of Budget and Management (DBM)',NULL),
(24,'07',29,'006','ATT','Department of Education (DepEd)','ECCDC','Early Childhood Care and Development Council',NULL),
(25,'07',29,'002','ATT','Department of Education (DepEd)','NBDB','National Book Development Board',NULL),
(26,'07',29,'003','ATT','Department of Education (DepEd)','NCCT','National Council for Children\'s Television',NULL),
(27,'07',29,'004','ATT','Department of Education (DepEd)','NM','National Museum',NULL),
(28,'07',29,'005','ATT','Department of Education (DepEd)','PHSA','Philippine High School for the Arts',NULL),
(29,'07',0,'001','HEAD','Department of Education (DepEd)','DepEd','Department of Education (DepEd)',NULL),
(148,'09',0,'001','HEAD','Department of Energy (DOE)','DOE','Department of Energy (DOE)',NULL),
(149,'10',159,'007','ATT','Department of Environment and Natural Resources (DENR)','BMB','Biodiversity Management Bureau',NULL),
(150,'10',159,'008','ATT','Department of Environment and Natural Resources (DENR)','ERDB','Ecosystems Research and Development Bureau',NULL),
(151,'10',159,'010','ATT','Department of Environment and Natural Resources (DENR)','LMB','Land Management Bureau',NULL),
(152,'10',159,'011','ATT','Department of Environment and Natural Resources (DENR)','PAWB','Protected Areas and Wildlife Bureau',NULL),
(153,'10',159,'009','ATT','Department of Environment and Natural Resources (DENR)','FMB','Forest Management Bureau ',NULL),
(154,'10',159,'002','ATT','Department of Environment and Natural Resources (DENR)','EMB','Environmental Management Bureau',NULL),
(155,'10',159,'003','ATT','Department of Environment and Natural Resources (DENR)','MGB','Mines and Geo-Sciences Bureau',NULL),
(156,'10',159,'004','ATT','Department of Environment and Natural Resources (DENR)','NAMRIA','National Mapping and Resource Information Authority',NULL),
(157,'10',159,'005','ATT','Department of Environment and Natural Resources (DENR)','NWRB','National Water Resources Board',NULL),
(158,'10',159,'006','ATT','Department of Environment and Natural Resources (DENR)','PCSD','Palawan Council for Sustainable Development Staff',NULL),
(159,'10',0,'001','HEAD','Department of Environment and Natural Resources (DENR)','DENR','Department of Environment and Natural Resources (DENR)',NULL),
(160,'11',169,'002','ATT','Department of Finance (DOF)','BOC','Bureau of Customs',NULL),
(161,'11',169,'003','ATT','Department of Finance (DOF)','BIR','Bureau of Internal Revenue',NULL),
(162,'11',169,'004','ATT','Department of Finance (DOF)','BLGF','Bureau of Local Government Finance',NULL),
(163,'11',169,'005','ATT','Department of Finance (DOF)','BT','Bureau of the Treasury',NULL),
(164,'11',169,'006','ATT','Department of Finance (DOF)','CBAA','Central Board of Assessment Appeals',NULL),
(165,'11',169,'008','ATT','Department of Finance (DOF)','IC','Insurance Commission',NULL),
(166,'11',169,'009','ATT','Department of Finance (DOF)','NTRC','National Tax Research Center',NULL),
(167,'11',169,'010','ATT','Department of Finance (DOF)','PMO','Privatization and Management Office',NULL),
(168,'11',169,'011','ATT','Department of Finance (DOF)','SEC','Securities and Exchange Commission',NULL),
(169,'11',0,'001','HEAD','Department of Finance (DOF)','DOF','Department of Finance (DOF)',NULL),
(170,'12',173,'002','ATT','Department of Foreign Affairs (DFA)','FSI','Foreign Service Institute',NULL),
(171,'12',173,'003','ATT','Department of Foreign Affairs (DFA)','TCCP','Technical Cooperation Council of the Philippines',NULL),
(172,'12',173,'004','ATT','Department of Foreign Affairs (DFA)','UNACOM','UNESCO National Commission of the Philippines',NULL),
(173,'12',0,'001','HEAD','Department of Foreign Affairs (DFA)','DFA','Department of Foreign Affairs (DFA)',NULL),
(174,'13',177,'003','ATT','Department of Health (DOH)','PNAC','Philippine National AIDS Council',NULL),
(175,'13',276,'002','ATT','National Economic and Development Authority (NEDA)','POPCOM','Commission on Population',NULL),
(176,'13',177,'003','ATT','Department of Health (DOH)','NNC','National Nutrition Council',NULL),
(177,'13',0,'001','HEAD','Department of Health (DOH)','DOH','Department of Health (DOH)',NULL),
(178,'14',184,'002','ATT','Department of the Interior and Local Government (DILG)','BFP','Bureau of Fire Protection',NULL),
(179,'14',184,'003','ATT','Department of the Interior and Local Government (DILG)','BJMP','Bureau of Jail Management and Penology',NULL),
(180,'14',184,'004','ATT','Department of the Interior and Local Government (DILG)','LGA','Local Government Academy',NULL),
(181,'14',184,'005','ATT','Department of the Interior and Local Government (DILG)','NAPOLCOM','National Police Commission',NULL),
(182,'14',184,'006','ATT','Department of the Interior and Local Government (DILG)','PNP','Philippine National Police',NULL),
(183,'14',184,'007','ATT','Department of the Interior and Local Government (DILG)','PPSC','Philippine Public Safety College',NULL),
(184,'14',0,'001','HEAD','Department of the Interior and Local Government (DILG)','DILG','Department of the Interior and Local Government (DILG)',NULL),
(185,'15',195,'011','ATT','Department of Justice (DOJ)','OADR','Office for Alternative Dispute Resolution',NULL),
(186,'15',195,'002','ATT','Department of Justice (DOJ)','BUCOR','Bureau of Corrections',NULL),
(187,'15',195,'003','ATT','Department of Justice (DOJ)','BI','Bureau of Immigration',NULL),
(188,'15',195,'004','ATT','Department of Justice (DOJ)','LRA','Land Registration Authority',NULL),
(189,'15',195,'005','ATT','Department of Justice (DOJ)','NBI','National Bureau of Investigation',NULL),
(190,'15',195,'006','ATT','Department of Justice (DOJ)','OGCC','Office of the Government Corporate Counsel',NULL),
(191,'15',195,'007','ATT','Department of Justice (DOJ)','OSG','Office of the Solicitor General',NULL),
(192,'15',195,'008','ATT','Department of Justice (DOJ)','PAPA','Parole and Probation Administration',NULL),
(193,'15',195,'009','ATT','Department of Justice (DOJ)','PCGG','Presidential Commission on Good Government',NULL),
(194,'15',195,'010','ATT','Department of Justice (DOJ)','PAO','Public Attorney\'s Office',NULL),
(195,'15',0,'001','HEAD','Department of Justice (DOJ)','DOJ','Department of Justice (DOJ)',NULL),
(196,'16',197,'011','ATT','Department of Labor and Employment (DOLE)','OSHC','Occupational Safety and Health Center',NULL),
(197,'16',0,'001','HEAD','Department of Labor and Employment (DOLE)','DOLE','Department of Labor and Employment (DOLE)',NULL),
(198,'16',197,'002','ATT','Department of Labor and Employment (DOLE)','ILS','Institute for Labor Studies',NULL),
(199,'16',197,'003','ATT','Department of Labor and Employment (DOLE)','NCMB','National Conciliation and Mediation Board',NULL),
(200,'16',197,'004','ATT','Department of Labor and Employment (DOLE)','NLRC','National Labor Relations Commission',NULL),
(201,'16',197,'005','ATT','Department of Labor and Employment (DOLE)','NMP','National Maritime Polytechnic',NULL),
(202,'16',197,'006','ATT','Department of Labor and Employment (DOLE)','NWPC','National Wages and Productivity Commission',NULL),
(203,'16',197,'007','ATT','Department of Labor and Employment (DOLE)','POEA','Philippine Overseas Employment Administration',NULL),
(204,'16',197,'008','ATT','Department of Labor and Employment (DOLE)','PRC','Professional Regulation Commission',NULL),
(205,'16',197,'010','ATT','Department of Labor and Employment (DOLE)','OWWA','Overseas Workers Welfare Administration',NULL),
(206,'17',218,'011','ATT','Department of National Defense (DND)','NDRRMC','National Disaster Risk Reduction and Management Council',NULL),
(207,'17',218,'013','ATT','Department of National Defense (DND)','PMA','Philippine Military Academy',NULL),
(208,'17',218,'014','ATT','Department of National Defense (DND)','PSG','Presidential Security Group',NULL),
(209,'17',218,'010','ATT','Department of National Defense (DND)','AFP','Armed Forces of the Philippines',NULL),
(210,'17',218,'002','ATT','Department of National Defense (DND)','GA','Government Arsenal',NULL),
(211,'17',218,'003','ATT','Department of National Defense (DND)','NDCP','National Defense College of the Philippines',NULL),
(212,'17',218,'004','ATT','Department of National Defense (DND)','OCD','Office of Civil Defense',NULL),
(213,'17',218,'008','ATT','Department of National Defense (DND)','PAF','Philippine Air Force ( Air Forces )  ',NULL),
(214,'17',218,'007','ATT','Department of National Defense (DND)','PA','Philippine Army ( Land Forces )',NULL),
(215,'17',218,'009','ATT','Department of National Defense (DND)','PN','Philippine Navy ( Naval Forces )  ',NULL),
(216,'17',218,'005','ATT','Department of National Defense (DND)','PVAO','Philippine Veterans Affairs Office (PVAO)',NULL),
(217,'17',218,'006','ATT','Department of National Defense (DND)','VMMC','Veterans Memorial Medical Center',NULL),
(218,'17',0,'001','HEAD','Department of National Defense (DND)','DND','Department of National Defense (DND)',NULL),
(219,'18',0,'001','HEAD','Department of Public Works and Highways (DPWH)','DPWH','Department of Public Works and Highways (DPWH)',NULL),
(220,'19',239,'021','ATT','Department of Science and Technology (DOST)','ICTO','Information and Communications technology Office',NULL),
(221,'19',239,'002','ATT','Department of Science and Technology (DOST)','ASTI','Advanced Science and Technology Institute',NULL),
(222,'19',239,'003','ATT','Department of Science and Technology (DOST)','FNRI','Food and Nutrition Research Institute',NULL),
(223,'19',239,'004','ATT','Department of Science and Technology (DOST)','FPRDI','Forest Products Research and Development Institute',NULL),
(224,'19',239,'005','ATT','Department of Science and Technology (DOST)','ITDI','Industrial Technology Development Institute',NULL),
(225,'19',239,'007','ATT','Department of Science and Technology (DOST)','MIRDC','Metals Industry Research and Development Center',NULL),
(226,'19',239,'008','ATT','Department of Science and Technology (DOST)','NAST','National Academy of Science and Technology',NULL),
(227,'19',239,'009','ATT','Department of Science and Technology (DOST)','NRCP','National Research Council of the Philippines',NULL),
(228,'19',239,'010','ATT','Department of Science and Technology (DOST)','PAGASA','Philippine Atmospheric, Geophysical and Astronomical Services Administration',NULL),
(229,'19',239,'011','ATT','Department of Science and Technology (DOST)','PCAARRD','Philippine Council for Agriculture, Aquatic and Natural Resources Research and Development',NULL),
(230,'19',239,'012','ATT','Department of Science and Technology (DOST)','PCHRD','Philippine Council for Health Research and Development',NULL),
(231,'19',239,'013','ATT','Department of Science and Technology (DOST)','PCIEERD','Philippine Council for Industry, Energy and Emerging Technology Research and Development (PCIEERD)',NULL),
(232,'19',239,'014','ATT','Department of Science and Technology (DOST)','PHIVOLCS','Philippine Institute of Volcanology and Seismology',NULL),
(233,'19',239,'015','ATT','Department of Science and Technology (DOST)','PNRI','Philippine Nuclear Research Institute',NULL),
(234,'19',239,'016','ATT','Department of Science and Technology (DOST)','PSHS','Philippine Science High School',NULL),
(235,'19',239,'017','ATT','Department of Science and Technology (DOST)','PTRI','Philippine Textile Research Institute',NULL),
(236,'19',239,'019','ATT','Department of Science and Technology (DOST)','STII','Science and Technology Information Institute',NULL),
(237,'19',239,'018','ATT','Department of Science and Technology (DOST)','SEI','Science Education Institute',NULL),
(238,'19',239,'020','ATT','Department of Science and Technology (DOST)','TAPI','Technology Application and Promotion Institute',NULL),
(239,'19',0,'001','HEAD','Department of Science and Technology (DOST)','DOST','Department of Science and Technology (DOST)',NULL),
(240,'20',244,'002','ATT','Department of Social Welfare and Development (DSWD)','CWC','Council for the Welfare of Children',NULL),
(241,'20',244,'003','ATT','Department of Social Welfare and Development (DSWD)','ICAB','Inter-Country Adoption Board',NULL),
(242,'20',244,'006','ATT','Department of Social Welfare and Development (DSWD)','JJWC','Juvenile Justice and Welfare Council',NULL),
(243,'20',244,'004','ATT','Department of Social Welfare and Development (DSWD)','NCDA','National Council on Disability Affairs',NULL),
(244,'20',0,'001','HEAD','Department of Social Welfare and Development (DSWD)','DSWD','Department of Social Welfare and Development (DSWD)',NULL),
(245,'21',249,'004','ATT','Department of Tourism (DOT)','PCSSD','Philippine Commission on Sports Scuba Diving',NULL),
(246,'21',249,'005','ATT','Department of Tourism (DOT)','Philippine','Philippine Retirement Authority',NULL),
(247,'21',249,'002','ATT','Department of Tourism (DOT)','IA','Intramuros Administration',NULL),
(248,'21',249,'003','ATT','Department of Tourism (DOT)','NPDC','National Parks Development Committee',NULL),
(249,'21',0,'001','HEAD','Department of Tourism (DOT)','DOT','Department of Tourism (DOT)',NULL),
(250,'22',255,'008','ATT','Department of Trade and Industry (DTI)','IPOPHL','Intellectual Property Office of the Philippines (IPOPHL)',NULL),
(251,'22',255,'002','ATT','Department of Trade and Industry (DTI)','BOI','Board of Investments',NULL),
(252,'22',255,'007','ATT','Department of Trade and Industry (DTI)','CIAP','Construction Industry Authority of the Philippines (CIAP)',NULL),
(253,'22',255,'006','ATT','Department of Trade and Industry (DTI)','DCP','Design Center of the Philippines',NULL),
(254,'22',255,'005','ATT','Department of Trade and Industry (DTI)','PTTC','Philippine Trade Training Center',NULL),
(255,'22',0,'001','HEAD','Department of Trade and Industry (DTI)','DTI','Department of Trade and Industry (DTI)',NULL),
(256,'24',276,'008','ATT','National Economic and Development Authority (NEDA)','NRO Caraga','NEDA Regional Office Caraga',NULL),
(257,'24',276,'009','ATT','National Economic and Development Authority (NEDA)','NRO CAR','NEDA Regional Office Cordillera Administrative Region',NULL),
(258,'24',276,'010','ATT','National Economic and Development Authority (NEDA)','NRO1','NEDA Regional Office I',NULL),
(259,'24',276,'011','ATT','National Economic and Development Authority (NEDA)','NRO2','NEDA Regional Office II',NULL),
(260,'24',276,'012','ATT','National Economic and Development Authority (NEDA)','NRO3','NEDA Regional Office III',NULL),
(261,'24',276,'013','ATT','National Economic and Development Authority (NEDA)','NRO4A','NEDA Regional Office IVA',NULL),
(262,'24',276,'014','ATT','National Economic and Development Authority (NEDA)','NRO4B','NEDA Regional Office IVB',NULL),
(263,'24',276,'015','ATT','National Economic and Development Authority (NEDA)','NRO9','NEDA Regional Office IX',NULL),
(264,'24',276,'016','ATT','National Economic and Development Authority (NEDA)','NRO5','NEDA Regional Office V',NULL),
(265,'24',276,'017','ATT','National Economic and Development Authority (NEDA)','NRO6','NEDA Regional Office VI',NULL),
(266,'24',276,'018','ATT','National Economic and Development Authority (NEDA)','NRO7','NEDA Regional Office VII',NULL),
(267,'24',276,'019','ATT','National Economic and Development Authority (NEDA)','NRO8','NEDA Regional Office VIII',NULL),
(268,'24',276,'020','ATT','National Economic and Development Authority (NEDA)','NRO10','NEDA Regional Office X',NULL),
(269,'24',276,'021','ATT','National Economic and Development Authority (NEDA)','NRO11','NEDA Regional Office XI',NULL),
(270,'24',276,'022','ATT','National Economic and Development Authority (NEDA)','NRO12','NEDA Regional Office XII',NULL),
(271,'24',276,'004','ATT','National Economic and Development Authority (NEDA)','PNVSCA','Philippine National Volunteer Service Coordinating Agency',NULL),
(272,'24',276,'006','ATT','National Economic and Development Authority (NEDA)','PSRTI','Philippine Statistical Research and Training Institute (formerly Statistical Research and Training C',NULL),
(273,'24',276,'008','ATT','National Economic and Development Authority (NEDA)','PSA','Philippine Statistics Authority',NULL),
(274,'24',276,'005','ATT','National Economic and Development Authority (NEDA)','PPPC','Public-Private Partnership Center',NULL),
(275,'24',276,'007','ATT','National Economic and Development Authority (NEDA)','TC','Tariff Commission',NULL),
(276,'24',0,'001','HEAD','National Economic and Development Authority (NEDA)','NEDA','National Economic and Development Authority (NEDA)','Acting Secretary Karl Kendrick Chua'),
(277,'25',283,'002','ATT','Presidential Communications Operations Office (PCOO)','BBS','Bureau of Broadcast Services',NULL),
(278,'25',283,'003','ATT','Presidential Communications Operations Office (PCOO)','BCS','Bureau of Communications Services',NULL),
(279,'25',283,'004','ATT','Presidential Communications Operations Office (PCOO)','NPO','National Printing Office',NULL),
(280,'25',283,'005','ATT','Presidential Communications Operations Office (PCOO)','NIB','News and Information Bureau',NULL),
(281,'25',283,'006','ATT','Presidential Communications Operations Office (PCOO)','PIA','Philippine Information Agency',NULL),
(282,'25',283,'007','ATT','Presidential Communications Operations Office (PCOO)','PBS/RTVM','Presidential Broadcast Staff (RTVM)',NULL),
(283,'25',0,'001','HEAD','Presidential Communications Operations Office (PCOO)','PCOO','Presidential Communications Operations Office (PCOO)',NULL),
(284,'26',0,'043','HEAD','Presidential Communication Development and Strategic Planning Office','PCDSPO','Presidential Communication Development and Strategic Planning Office',NULL),
(285,'26',159,'044','ATT','Department of Environment and Natural Resources (DENR)','PRA','Philippine Reclamation Authority',NULL),
(286,'26',0,'045','HEAD','National Solid Waste Management Council','NSWMC','National Solid Waste Management Council',NULL),
(287,'26',148,'046','ATT','Department of Energy (DOE)','TransCo','National Transmission Corporation',NULL),
(288,'26',6,'047','ATT','Office of the Cabinet Secretary ','OPPAC','Office of the President-Presidential Action Center',NULL),
(289,'26',148,'048','ATT','Department of Energy (DOE)','PSALM','Power Sector Assets and Liabilities Management Corporation',NULL),
(290,'26',0,'001','HEAD','Anti-Money Laundering Council','AMLC','Anti-Money Laundering Council',NULL),
(291,'26',0,'002','HEAD','Climate Change Commission','CCC','Climate Change Commission',NULL),
(292,'26',0,'003','HEAD','Commission on Filipinos Overseas','CFO','Commission on Filipinos Overseas',NULL),
(293,'26',0,'004','HEAD','Commission on Higher Education','CHED','Commission on Higher Education',NULL),
(294,'26',0,'005','HEAD','Commission on the Filipino Language','CFL','Commission on the Filipino Language',NULL),
(295,'26',0,'006','HEAD','Dangerous Drugs Board  ','DDB','Dangerous Drugs Board  ',NULL),
(296,'26',148,'008','ATT','Department of Energy (DOE)','ERC','Energy Regulatory Commission',NULL),
(297,'26',0,'009','HEAD','Film Development Council of the Philippines','FDCP','Film Development Council of the Philippines',NULL),
(298,'26',0,'010','HEAD','Games and Amusements Board','GAB','Games and Amusements Board',NULL),
(299,'26',7,'011','ATT','Office of the President (OP)','GCG','Governance Commission for Government-Owned or Controlled Corporations',NULL),
(300,'26',301,'012','ATT','Housing and Urban Development Coordinating Council','HLURB','Housing and Land Use Regulatory Board',NULL),
(301,'26',7,'013','ATT','Office of the President (OP)','DHSUD','Department of Human Settlements and Urban Development',NULL),
(302,'26',0,'014','HEAD','Mindanao Development Authority','MinDA','Mindanao Development Authority',NULL),
(303,'26',0,'015','HEAD','Movie and Television Review and Classification Board','MTRCB','Movie and Television Review and Classification Board',NULL),
(304,'26',6,'016','ATT','Office of the Cabinet Secretary ','NAPC','National Anti-Poverty Commission',NULL),
(305,'26',0,'017','HEAD','National Commission for Culture and the Arts','NCCA','National Commission for Culture and the Arts',NULL),
(306,'26',0,'018','HEAD','National Historical Commission of the Philippines','NHCP','National Historical Commission of the Philippines',NULL),
(307,'26',0,'019','HEAD','National Library of the Philippines','NLP','National Library of the Philippines',NULL),
(308,'26',0,'020','HEAD','National Archives of the Philippines','NAP','National Archives of the Philippines',NULL),
(309,'26',6,'021','ATT','Office of the Cabinet Secretary ','NCIP','National Commission on Indigenous Peoples',NULL),
(310,'26',6,'022','ATT','Office of the Cabinet Secretary ','NCMF','National Commission on Muslim Filipinos',NULL),
(311,'26',0,'023','HEAD','National Intelligence Coordinating Agency','NICA','National Intelligence Coordinating Agency',NULL),
(312,'26',0,'024','HEAD','National Security Council','NSC','National Security Council',NULL),
(313,'26',0,'026','HEAD','Office of the Presidential Adviser on the Peace Process','OPAPP','Office of the Presidential Adviser on the Peace Process',NULL),
(314,'26',0,'027','HEAD','Optical Media Board','OMB','Optical Media Board',NULL),
(315,'26',0,'028','HEAD','Pasig River Rehabilitation Commission','PRRC','Pasig River Rehabilitation Commission',NULL),
(316,'26',6,'029','ATT','Office of Cabinet Secretary','PCW','Philippine Commission on Women (National Commission on the Role of Filipino Women)',NULL),
(317,'26',0,'030','HEAD','Philippine Drug Enforcement Agency','PDEA','Philippine Drug Enforcement Agency',NULL),
(318,'26',0,'031','HEAD','Philippine Racing Commission','PHILRACOM','Philippine Racing Commission',NULL),
(319,'26',0,'032','HEAD','Philippine Sports Commission','PSC','Philippine Sports Commission',NULL),
(320,'26',6,'033','ATT','Office of the Cabinet Secretary','PCUP','Presidential Commission on the Urban Poor',NULL),
(321,'26',0,'035','HEAD','Presidential Legislative Liaison Office','PLLO','Presidential Legislative Liaison Office',NULL),
(322,'26',0,'036','HEAD','Presidential Management Staff','PMS','Presidential Management Staff',NULL),
(323,'26',19,'037','ATT','Department of Agriculture (DA)','FPA','Fertilizer and Pesticide Authority',NULL),
(324,'26',0,'038','HEAD','Philippine Competition Commission','PHCC','Philippine Competition Commission',NULL),
(325,'26',6,'040','ATT','Office of the Cabinet Secretary ','NYC','National Youth Commission',NULL),
(326,'26',6,'041','ATT','Office of the Cabinet Secretary ','TESDA','Technical Education and Skills Development Authority',NULL),
(327,'26',255,'042','ATT','Department of Trade and Industry (DTI)','CDA','Cooperative Development Authority',NULL),
(328,'27',329,'002','ATT','Autonomous Region in Muslim Mindanao (ARMM)','ARMMDPWH','ARMM - Department of Public Works and Highways',NULL),
(329,'27',0,'001','HEAD','Autonomous Region in Muslim Mindanao (ARMM)','ARMM','Autonomous Region in Muslim Mindanao (ARMM)',NULL),
(330,'28',0,'001','HEAD','Legislative-Executive Development Advisory Council','LEDAC','Legislative-Executive Development Advisory Council',NULL),
(331,'29',335,'004','ATT','Supreme Court of the Philippines','CA-SC','Court of Appeals',NULL),
(332,'29',335,'005','ATT','Supreme Court of the Philippines','CTA','Court of Tax Appeals',NULL),
(333,'29',335,'002','ATT','Supreme Court of the Philippines','PET','Presidential Electoral Tribunal',NULL),
(334,'29',335,'003','ATT','Supreme Court of the Philippines','SB','Sandiganbayan',NULL),
(335,'29',0,'001','HEAD','Supreme Court of the Philippines','SC','Supreme Court of the Philippines',NULL),
(336,'30',337,'002','ATT','Civil Service Commission (CSC)','CESB','Career Executive Service Board',NULL),
(337,'30',0,'001','HEAD','Civil Service Commission (CSC)','CSC','Civil Service Commission (CSC)',NULL),
(338,'31',0,'000','HEAD','Commission on Audit (COA)','COA','Commission on Audit (COA)',NULL),
(339,'32',0,'000','HEAD','Commission on Elections (COMELEC)','COMELEC','Commission on Elections (COMELEC)',NULL),
(340,'33',0,'000','HEAD','Office of the Ombudsman','Ombudsman','Office of the Ombudsman',NULL),
(341,'34',342,'003','ATT','Commission on Human Rights (CHR)','HRVVMC','Human Rights Violations Victims\' Memorial Commission',NULL),
(342,'34',0,'001','HEAD','Commission on Human Rights (CHR)','CHR','Commission on Human Rights (CHR)',NULL),
(343,'35',20,'092','ATT_GOCC','Department of Agriculture (DA)','QUEDANCOR','Quedan and Rural Credit Guarantee Corporation',NULL),
(344,'35',0,'087','GOCC','Clark Development Corporation','CDC','Clark Development Corporation',NULL),
(345,'35',0,'088','GOCC','Cagayan Economic Zone Authority','CEZA','Cagayan Economic Zone Authority',NULL),
(346,'35',301,'089','ATT_GOCC','Housing and Urban Development Coordinating Council','HMDF','Home Development Mutual Fund',NULL),
(347,'35',438,'090','ATT_GOCC','Department of Transportation','Northrail','North Luzon Railways Corporation',NULL),
(348,'35',0,'091','GOCC','People\'s Credit Finance Corporation','PCFC','People\'s Credit Finance Corporation',NULL),
(349,'35',438,'092','ATT_GOCC','Department of Transportation','CPA','Cebu Ports Authority',NULL),
(350,'35',159,'093','ATT_GOCC','Department of Environment and Natural Resources (DENR)','LLDA','Laguna Lake Development Authority',NULL),
(351,'35',159,'094','ATT_GOCC','Department of Environment and Natural Resources (DENR)','NRDC','Natural Resources Development Corporation',NULL),
(352,'35',219,'095','ATT_GOCC','Department of National Defense (DND)','AFP-CES','AFP Commissary and Exchange Service',NULL),
(353,'35',148,'096','ATT_GOCC','Department of Energy (DOE)','PNOC','Philippine National Oil Company',NULL),
(354,'35',169,'097','ATT_GOCC','Department of Finance (DOF)','PDIC','Philippine Deposit Insurance Corporation',NULL),
(355,'35',169,'098','ATT_GOCC','Department of Finance (DOF)','PHILEXIM','Philippine Export-Import Credit Agency',NULL),
(356,'35',0,'099','GOCC','Social Security System','SSS','Social Security System',NULL),
(357,'35',240,'100','ATT_GOCC','Department of Science and Technology (DOST)','TRC','Technology Resource Center',NULL),
(358,'35',249,'101','ATT_GOCC','Department of Tourism (DOT)','DFPC','Duty Free Philippines Corporation',NULL),
(359,'35',249,'102','ATT_GOCC','Department of Tourism (DOT)','PTA','Philippine Tourism Authority',NULL),
(360,'35',438,'103','ATT_GOCC','Department of Transportation','CAAP','Civil Aviation Authority of the Philippines',NULL),
(361,'35',438,'104','ATT_GOCC','Department of Transportation','MIAA','Manila International Airport Authority',NULL),
(362,'35',438,'105','ATT_GOCC','Department of Transportation','MRT3','Metro Rail Transit 3',NULL),
(363,'35',438,'106','ATT_GOCC','Department of Transportation','PPA','Philippine Ports Authority',NULL),
(364,'35',255,'107','ATT_GOCC','Department of Trade and Industry (DTI)','CITC','Cottage Industry Technology Center',NULL),
(365,'35',255,'108','ATT_GOCC','Department of Trade and Industry (DTI)','NDC','National Development Company',NULL),
(366,'35',255,'109','ATT_GOCC','Department of Trade and Industry (DTI)','SBGFCTRC','Small Business Guarantee and Finance Corporation Technology Resource Center',NULL),
(367,'35',283,'110','ATT_GOCC','Presidential Communications Operations Office (PCOO)','IBC','Intercontinental Broadcasting Corporation',NULL),
(368,'35',411,'111','ATT_GOCC','Bases Conversion and Development Authority','CIAC','Clark International Airport Corporation',NULL),
(369,'35',197,'112','ATT_GOCC','Department of Labor and Employment (DOLE)','ECC','Employees\' Compensation Commission',NULL),
(370,'35',438,'113','ATT_GOCC','Department of Transportation','MCIAA','Mactan-Cebu International Airport Authority',NULL),
(371,'35',219,'114','ATT_GOCC','Department of Public Works and Highways (DPWH)','MWSS','Metropolitan Waterworks and Sewerage System',NULL),
(372,'35',148,'115','ATT_GOCC','Department of Energy (DOE)','PNOC-EC','Philippine National Oil Company - Exploration Corporation',NULL),
(373,'35',148,'116','ATT_GOCC','Department of Energy (DOE)','PNOC-RC','Philippine National Oil Company - Renewables Corporation',NULL),
(374,'35',255,'120','ATT_GOCC','Department of Trade and Industry (DTI)','PITC','Philippine International Trading Corporation (PITC)',NULL),
(375,'35',438,'122','ATT_GOCC','Department of Transportation','PADC','Philippine Aerospace Development Corporation',NULL),
(376,'35',255,'121','ATT_GOCC','Department of Trade and Industry (DTI)','PPPI','Philippine Pharma Procurement Incorporated (PPPI)',NULL),
(377,'35',159,'119','ATT_GOCC','Department of Environment and Natural Resources (DENR)','PMDC','Philippine Mining Development Corporation',NULL),
(378,'35',148,'118','ATT_GOCC','Department of Energy (DOE)','PEMC','Philippine Electricity Market Corporation',NULL),
(379,'35',0,'117','GOCC','Bangko Sentral ng Pilipinas','BSP','Bangko Sentral ng Pilipinas',NULL),
(380,'35',20,'003','ATT_GOCC','Department of Agriculture (DA)','NDA','National Dairy Authority',NULL),
(381,'35',20,'007','ATT_GOCC','Department of Agriculture (DA)','NTA','National Tobacco Administration',NULL),
(382,'35',0,'001','GOCC','Land Bank of the Philippines','LBP','Land Bank of the Philippines',NULL),
(383,'35',20,'009','ATT_GOCC','Department of Agriculture (DA)','PCIC','Philippine Crop Insurance Corporation',NULL),
(384,'35',19,'004','ATT','Department of Agriculture (DA)','NFA','National Food Authority',NULL),
(385,'35',6,'005','ATT_GOCC','Office of the Cabinet Secretary ','NIA','National Irrigation Administration',NULL),
(386,'35',19,'008','ATT','Department of Agriculture (DA)','PCA','Philippine Coconut Authority',NULL),
(387,'35',20,'010','ATT_GOCC','Department of Agriculture (DA)','PFDA','Philippine Fisheries Development Authority',NULL),
(388,'35',20,'011','ATT_GOCC','Department of Agriculture (DA)','PhilRice','Philippine Rice Research Institute',NULL),
(389,'35',20,'012','ATT_GOCC','Department of Agriculture (DA)','PhilSuCor','Philippine Sugar Corporation',NULL),
(390,'35',20,'014','ATT_GOCC','Department of Agriculture (DA)','SRA','Sugar Regulatory Administration',NULL),
(391,'35',148,'015','ATT_GOCC','Department of Energy (DOE)','NEA','National Electrification Administration',NULL),
(392,'35',148,'016','ATT_GOCC','Department of Energy (DOE)','NPC','National Power Corporation',NULL),
(393,'35',177,'025','ATT_GOCC','Department of Health (DOH)','LCP','Lung Center of the Philippines',NULL),
(394,'35',177,'026','ATT_GOCC','Department of Health (DOH)','NKTI','National Kidney and Transplant Institute',NULL),
(395,'35',177,'027','ATT_GOCC','Department of Health (DOH)','PCMC','Philippine Children’s Medical Center',NULL),
(396,'35',177,'028','ATT_GOCC','Department of Health (DOH)','PHIC','Philippine Health Insurance Corporation',NULL),
(397,'35',177,'029','ATT_GOCC','Department of Health (DOH)','PHC','Philippine Heart Center',NULL),
(398,'35',177,'030','ATT_GOCC','Department of Health (DOH)','PITAHC','Philippine Institute of Traditional and Alternative Health Care',NULL),
(399,'35',219,'034','ATT_GOCC','Department of Public Works and Highways (DPWH)','LWUA','Local Water Utilities Administration',NULL),
(400,'35',249,'040','ATT_GOCC','Department of Tourism (DOT)','TIEZA','Tourism Infrastructure and Enterprise Zone Authority',NULL),
(401,'35',249,'041','ATT_GOCC','Department of Tourism (DOT)','TPB','Tourism Promotions Board',NULL),
(402,'35',0,'042','GOCC','Aurora Pacific Economic Zone and Freeport Authority','APE','Aurora Pacific Economic Zone and Freeport Authority',NULL),
(403,'35',255,'043','ATT_GOCC','Department of Trade and Industry (DTI)','CITEM','Center for International Trade Expositions and Missions',NULL),
(404,'35',255,'046','ATT_GOCC','Department of Trade and Industry (DTI)','PEZA','Philippine Economic Zone Authority',NULL),
(405,'35',255,'049','ATT_GOCC','Department of Trade and Industry (DTI)','SBC','Small Business Corporation',NULL),
(406,'35',438,'052','ATT_GOCC','Department of Transportation','LRTA','Light Rail Transit Authority',NULL),
(407,'35',438,'056','ATT_GOCC','Department of Transportation','PNR','Philippine National Railways',NULL),
(408,'35',276,'058','ATT_GOCC','National Economic and Development Authority (NEDA)','PIDS','Philippine Institute for Development Studies',NULL),
(409,'35',283,'059','ATT_GOCC','Presidential Communications Operations Office (PCOO)','PTNI','People\'s Television Network, Inc.',NULL),
(410,'35',0,'061','GOCC','Authority of the Freeport Area of Bataan','AFAB','Authority of the Freeport Area of Bataan',NULL),
(411,'35',0,'063','GOCC','Bases Conversion and Development Authority','BCDA','Bases Conversion and Development Authority',NULL),
(412,'35',0,'066','GOCC','Credit Information Corporation','CIC','Credit Information Corporation',NULL),
(413,'35',305,'067','ATT_GOCC','National Commission on Culture and the Arts ','CCP','Cultural Center of the Philippines',NULL),
(414,'35',0,'068','GOCC','Development Academy of the Philippines','DAP','Development Academy of the Philippines',NULL),
(415,'35',0,'069','GOCC','Development Bank of the Philippines','DBP','Development Bank of the Philippines',NULL),
(416,'35',0,'072','GOCC','Philippine Guarantee Corporation','PGC','Philippine Guarantee Corporation',NULL),
(417,'35',301,'073','ATT_GOCC','Housing and Urban Development Coordinating Council','NHMFC','National Home Mortgage Finance Corporation',NULL),
(418,'35',301,'074','ATT_GOCC','Housing and Urban Development Coordinating Council','NHA','National Housing Authority',NULL),
(419,'35',0,'078','GOCC','Philippine Center for Economic Development','PCED','Philippine Center for Economic Development',NULL),
(420,'35',0,'080','GOCC','Philippine Postal Corporation','PHLPOST','Philippine Postal Corporation',NULL),
(421,'35',0,'082','GOCC','Southern Philippines Development Authority','SPDA','Southern Philippines Development Authority',NULL),
(422,'35',0,'083','GOCC','Subic Bay Metropolitan Authority','SBMA','Subic Bay Metropolitan Authority',NULL),
(423,'35',0,'084','GOCC','Zamboanga City Special Economic Zone Authority','ZCSEZA','Zamboanga City Special Economic Zone Authority',NULL),
(424,'35',301,'086','ATT_GOCC','Housing and Urban Development Coordinating Council','SHFC','Social Housing Finance Corporation',NULL),
(425,'36',0,'001','HEAD','Metropolitan Manila Development Authority','MMDA','Metropolitan Manila Development Authority',NULL),
(426,'37',429,'004','ATT','Department of Information and Communications Technology','CICC','Cybercrime Investigation and Coordination Center',NULL),
(427,'37',429,'003','ATT','Department of Information and Communications Technology','NPrivaC','National Privacy Commission',NULL),
(428,'37',429,'002','ATT','Department of Information and Communications Technology','NTC','National Telecommunications Commission',NULL),
(429,'37',0,'001','HEAD','Department of Information and Communications Technology','DICT','Department of Information and Communications Technology',NULL),
(430,'38',438,'008','ATT','Department of Transportation','LTFRB','Land Transportation Franchising & Regulatory Board',NULL),
(431,'38',438,'009','ATT','Department of Transportation','LTO','Land Transportation Office',NULL),
(432,'38',438,'002','ATT','Department of Transportation','CAB','Civil Aeronautics Board',NULL),
(433,'38',438,'003','ATT','Department of Transportation','MARINA','Maritime Industry Authority (MARINA)',NULL),
(434,'38',438,'005','ATT','Department of Transportation','OTS','Office for Transportation Security',NULL),
(435,'38',438,'004','ATT','Department of Transportation','OTC','Office of Transport Cooperatives',NULL),
(436,'38',438,'006','ATT','Department of Transportation','PCG','Philippine Coast Guard',NULL),
(437,'38',438,'007','ATT','Department of Transportation','TRB','Toll Regulatory Board',NULL),
(438,'38',0,'001','HEAD','Department of Transportation','DOTr','Department of Transportation',NULL),
(440,'20',0,'001','HEAD','Department of Social Welfare and Development (DSWD)','DSWD','Department of Social Welfare and Development (DSWD)',NULL);

/*Table structure for table `agency_form1s` */

DROP TABLE IF EXISTS `agency_form1s`;

CREATE TABLE `agency_form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `fy` int(4) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form1s` */

insert  into `agency_form1s`(`id`,`status`,`created_at`,`updated_at`,`fy`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(5,'Draft','2020-08-03','2020-10-06',2020,276,'','SAMPLE Rev'),
(7,'Draft','2020-08-07','2020-09-01',2020,219,'','SAMPLE REMARKS');

/*Table structure for table `agency_form1s_projects` */

DROP TABLE IF EXISTS `agency_form1s_projects`;

CREATE TABLE `agency_form1s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form1s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `fs_1` decimal(10,0) DEFAULT 0,
  `fs_2` decimal(10,0) DEFAULT 0,
  `fs_3` decimal(10,0) DEFAULT 0,
  `fs_4` decimal(10,0) DEFAULT 0,
  `fs_5` decimal(10,0) DEFAULT 0,
  `fs_6` decimal(10,0) DEFAULT 0,
  `fs_7` decimal(10,0) DEFAULT 0,
  `fs_8` decimal(10,0) DEFAULT 0,
  `fs_9` decimal(10,0) DEFAULT 0,
  `fs_10` decimal(10,0) DEFAULT 0,
  `fs_11` decimal(10,0) DEFAULT 0,
  `fs_12` decimal(10,0) DEFAULT 0,
  `pt_1` decimal(10,0) DEFAULT 0,
  `pt_2` decimal(10,0) DEFAULT 0,
  `pt_3` decimal(10,0) DEFAULT 0,
  `pt_4` decimal(10,0) DEFAULT 0,
  `pt_5` decimal(10,0) DEFAULT 0,
  `pt_6` decimal(10,0) DEFAULT 0,
  `pt_7` decimal(10,0) DEFAULT 0,
  `pt_8` decimal(10,0) DEFAULT 0,
  `pt_9` decimal(10,0) DEFAULT 0,
  `pt_10` decimal(10,0) DEFAULT 0,
  `pt_11` decimal(10,0) DEFAULT 0,
  `pt_12` decimal(10,0) DEFAULT 0,
  `oi_1` varchar(250) DEFAULT NULL,
  `oi_2` varchar(250) DEFAULT NULL,
  `oi_3` varchar(250) DEFAULT NULL,
  `oi_4` varchar(250) DEFAULT NULL,
  `oi_5` varchar(250) DEFAULT NULL,
  `oi_6` varchar(250) DEFAULT NULL,
  `oi_7` varchar(250) DEFAULT NULL,
  `oi_8` varchar(250) DEFAULT NULL,
  `oi_9` varchar(250) DEFAULT NULL,
  `oi_10` varchar(250) DEFAULT NULL,
  `oi_11` varchar(250) DEFAULT NULL,
  `oi_12` varchar(250) DEFAULT NULL,
  `eg_1` int(11) DEFAULT NULL,
  `eg_2` int(11) DEFAULT NULL,
  `eg_3` int(11) DEFAULT NULL,
  `eg_4` int(11) DEFAULT NULL,
  `eg_5` int(11) DEFAULT NULL,
  `eg_6` int(11) DEFAULT NULL,
  `eg_7` int(11) DEFAULT NULL,
  `eg_8` int(11) DEFAULT NULL,
  `eg_9` int(11) DEFAULT NULL,
  `eg_10` int(11) DEFAULT NULL,
  `eg_11` int(11) DEFAULT NULL,
  `eg_12` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form1s_projects` */

insert  into `agency_form1s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form1s_id`,`nro_remarks`,`agency_remarks`,`fs_1`,`fs_2`,`fs_3`,`fs_4`,`fs_5`,`fs_6`,`fs_7`,`fs_8`,`fs_9`,`fs_10`,`fs_11`,`fs_12`,`pt_1`,`pt_2`,`pt_3`,`pt_4`,`pt_5`,`pt_6`,`pt_7`,`pt_8`,`pt_9`,`pt_10`,`pt_11`,`pt_12`,`oi_1`,`oi_2`,`oi_3`,`oi_4`,`oi_5`,`oi_6`,`oi_7`,`oi_8`,`oi_9`,`oi_10`,`oi_11`,`oi_12`,`eg_1`,`eg_2`,`eg_3`,`eg_4`,`eg_5`,`eg_6`,`eg_7`,`eg_8`,`eg_9`,`eg_10`,`eg_11`,`eg_12`) values 
(4,10,'Endorsed','2020-08-06','2020-10-06',5,'BERNARD REVIEW OK OK','SAMPLE PROJECT',1,112,122112,121,1211,12145,7,90,78,89,1,8,78,90,89,90,90,90,90,90,78,0,0,90,'SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE','SAMPLE',12,3,5,6,7,9,0,1,12,32,4,5);

/*Table structure for table `agency_form2s` */

DROP TABLE IF EXISTS `agency_form2s`;

CREATE TABLE `agency_form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form2s` */

insert  into `agency_form2s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2020-08-26','2020-09-14','2020-07',276,'','SA');

/*Table structure for table `agency_form2s_projects` */

DROP TABLE IF EXISTS `agency_form2s_projects`;

CREATE TABLE `agency_form2s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form2s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `alloc_asof` decimal(20,0) DEFAULT 0,
  `alloc_month` decimal(20,0) DEFAULT 0,
  `releases_asof` decimal(20,0) DEFAULT 0,
  `releases_month` decimal(20,0) DEFAULT 0,
  `obligations_asof` decimal(20,0) DEFAULT 0,
  `obligations_month` decimal(20,0) DEFAULT 0,
  `expenditures_asof` decimal(20,0) DEFAULT 0,
  `expenditures_month` decimal(20,0) DEFAULT 0,
  `male` int(5) DEFAULT 0,
  `female` int(5) DEFAULT 0,
  `oi` decimal(20,0) DEFAULT 0,
  `ttd` decimal(20,0) DEFAULT 0,
  `tftm` decimal(20,0) DEFAULT 0,
  `atd` decimal(20,0) DEFAULT 0,
  `aftm` decimal(20,0) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form2s_projects` */

insert  into `agency_form2s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form2s_id`,`nro_remarks`,`agency_remarks`,`alloc_asof`,`alloc_month`,`releases_asof`,`releases_month`,`obligations_asof`,`obligations_month`,`expenditures_asof`,`expenditures_month`,`male`,`female`,`oi`,`ttd`,`tftm`,`atd`,`aftm`) values 
(1,10,'Reviewed','2020-08-28','2020-09-01',1,'ssdsdsdsdsd','ENDORSE',1223,12312,123123123,123123123123,123123123123,23123123,123123123,23232323,11,33,1,2,3,4,5);

/*Table structure for table `agency_form3s` */

DROP TABLE IF EXISTS `agency_form3s`;

CREATE TABLE `agency_form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form3s` */

insert  into `agency_form3s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(22,'Draft','2020-09-14','2020-09-14','2020-09',276,NULL,NULL);

/*Table structure for table `agency_form3s_projects` */

DROP TABLE IF EXISTS `agency_form3s_projects`;

CREATE TABLE `agency_form3s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form3s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `findings` text DEFAULT NULL,
  `possible` text DEFAULT NULL,
  `recommendations` text DEFAULT NULL,
  `impstatus` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form3s_projects` */

insert  into `agency_form3s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form3s_id`,`nro_remarks`,`agency_remarks`,`findings`,`possible`,`recommendations`,`impstatus`) values 
(1,10,'Endorsed','2020-09-14','2020-09-28',22,NULL,'SAMPLE ENDORSE','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc id fermentum est. Nullam luctus, ipsum sit amet eleifend luctus, sapien arcu molestie felis, quis pellentesque ante augue eget sem. Mauris porttitor fringilla semper. Cras accumsan vulputate lacus eu congue. Quisque rutrum, urna at consectetur sollicitudin, odio ligula molestie ex, sit amet egestas ipsum elit eget libero. Cras ac odio eu orci molestie viverra eu vitae quam. Duis non varius ex. Nullam vehicula, elit sed consequat placerat, tellus ligula dapibus leo, non congue elit est id lorem. Nunc pulvinar finibus arcu, non commodo tellus porttitor eu. \r\n\r\nEtiam facilisis orci at arcu efficitur, in aliquet diam malesuada. Phasellus non est vel velit convallis lacinia. Duis consequat, quam eget consequat egestas, purus neque ornare velit, eu suscipit nulla neque sit amet urna. Ut vitae ex dui. Ut sit amet est in odio convallis luctus.','Integer placerat nisi in placerat tempus. Proin sit amet porttitor neque. Nullam ante risus, ullamcorper eu sodales a, semper eget lacus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Cras consequat, velit placerat scelerisque porttitor, sapien felis ultricies magna, vel dapibus magna metus eget ante. Nullam iaculis ultricies leo, facilisis luctus risus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras maximus diam dolor, ut scelerisque dui condimentum eu. Etiam elementum velit quis blandit tempor. Donec ultricies turpis nec ligula vestibulum euismod. Sed sed vulputate nisi. Sed et turpis a massa facilisis condimentum. Fusce hendrerit eget diam ut auctor. Pellentesque sed risus accumsan, sollicitudin purus sagittis, consectetur massa. Duis et enim eu elit efficitur tincidunt sit amet in dui. Sed nunc risus, aliquet sit amet augue ac, pretium ullamcorper massa.','Nulla sapien dui, lobortis et nisi et, maximus tincidunt lacus. Sed et lacinia ante. Integer risus ante, tincidunt a ultricies quis, vulputate vitae eros. Nulla ullamcorper interdum ipsum at mollis. Phasellus ultrices mi elit, condimentum tempus tortor varius sit amet. Ut condimentum sodales sapien, et rhoncus lorem pellentesque vitae. Nullam in blandit est, vitae facilisis odio. Curabitur porta viverra turpis nec dictum. Etiam in porttitor augue, vitae lobortis arcu.',2);

/*Table structure for table `agency_form4s` */

DROP TABLE IF EXISTS `agency_form4s`;

CREATE TABLE `agency_form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(3) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form4s` */

insert  into `agency_form4s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2020-09-28','2020-10-06','2020-09',276,'','SAMPLE REMARKS');

/*Table structure for table `agency_form4s_projects` */

DROP TABLE IF EXISTS `agency_form4s_projects`;

CREATE TABLE `agency_form4s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(5) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form4s_id` int(5) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `objectives` text DEFAULT NULL,
  `indicator` text DEFAULT NULL,
  `results` text DEFAULT NULL,
  `impstatus` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `agency_form4s_projects` */

insert  into `agency_form4s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form4s_id`,`nro_remarks`,`agency_remarks`,`objectives`,`indicator`,`results`,`impstatus`) values 
(1,10,'Draft','2020-09-28','2020-10-02',1,'FSsfsfsf',NULL,'SAMPLE OBJECTIVES','SAMPLE RESULTS','SAMPLE RESULTS',NULL);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category`,`created_at`,`updated_at`) values 
(1,'ELCAC','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Balik Probinsya Program (BP2)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Rehab and Recovery Projects (RRP)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(6,'Official Development Assistance (ODA)',NULL,NULL);

/*Table structure for table `form1s` */

DROP TABLE IF EXISTS `form1s`;

CREATE TABLE `form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fy` int(4) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form1s` */

insert  into `form1s`(`id`,`fy`,`is_lock`,`created_at`,`updated_at`) values 
(1,2020,0,'2020-07-31','2020-10-12');

/*Table structure for table `form2s` */

DROP TABLE IF EXISTS `form2s`;

CREATE TABLE `form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form2s` */

insert  into `form2s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(2,'2020-07',0,'2020-08-26','2020-09-28');

/*Table structure for table `form3s` */

DROP TABLE IF EXISTS `form3s`;

CREATE TABLE `form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form3s` */

insert  into `form3s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-09',0,'2020-09-14','2020-09-28');

/*Table structure for table `form4s` */

DROP TABLE IF EXISTS `form4s`;

CREATE TABLE `form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `form4s` */

insert  into `form4s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-09',0,'2020-09-28','2020-10-02');

/*Table structure for table `impstatuses` */

DROP TABLE IF EXISTS `impstatuses`;

CREATE TABLE `impstatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `impstatuses` */

insert  into `impstatuses`(`id`,`type`) values 
(1,'Ahead of Schedule'),
(2,'On Schedule'),
(3,'Behind Schedule');

/*Table structure for table `item_tag` */

DROP TABLE IF EXISTS `item_tag`;

CREATE TABLE `item_tag` (
  `item_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `item_tag_item_id_foreign` (`item_id`),
  KEY `item_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `item_tag` */

insert  into `item_tag`(`item_id`,`tag_id`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(3,1);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `items_name_unique` (`name`),
  KEY `items_category_id_foreign` (`category_id`),
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`description`,`picture`,`category_id`,`status`,`date`,`show_on_homepage`,`options`,`created_at`,`updated_at`) values 
(1,'5 citybreak ideas for this year','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,1,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Top 10 restaurants in Italy','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Cocktail ideas for your birthday party','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(10) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `longitude` float(20,7) DEFAULT NULL,
  `latitude` float(20,7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `locations` */

insert  into `locations`(`id`,`proj_id`,`location`,`created_at`,`updated_at`,`longitude`,`latitude`) values 
(3,10,'SAMPLE LOCATION PROJECT 1','2020-08-06','2020-08-07',NULL,NULL),
(5,10,'SAMPLE LOCATION PROJECT 2','2020-08-07','2020-08-07',NULL,NULL),
(6,14,'PASIG CITY','2020-08-07','2020-10-06',1222.8902588,32.8903008),
(7,14,'MANDALUYONG CITY','2020-10-06','2020-10-06',122.8902969,3322.8903809);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_100000_create_password_resets_table',1),
(2,'2019_01_15_100000_create_roles_table',1),
(3,'2019_01_15_110000_create_users_table',1),
(4,'2019_01_17_121504_create_categories_table',1),
(5,'2019_01_21_130422_create_tags_table',1),
(6,'2019_01_21_163402_create_items_table',1),
(7,'2019_01_21_163414_create_item_tag_table',1),
(8,'2019_03_06_132557_add_photo_column_to_users_table',1),
(9,'2019_03_06_143255_add_fields_to_items_table',1),
(10,'2019_03_20_090438_add_color_tags_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `implementingagency` int(3) NOT NULL,
  `sector` int(1) NOT NULL,
  `fundingsource` int(1) NOT NULL,
  `mode` varchar(500) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `category` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

/*Data for the table `projects` */

insert  into `projects`(`id`,`title`,`implementingagency`,`sector`,`fundingsource`,`mode`,`start`,`end`,`created_at`,`updated_at`,`category`) values 
(10,'SAMPLE NEDA PROJECT',276,5,2,'SAMPLE PROJECT','2020-08-06','2020-08-06','2020-08-06','2020-08-07',3),
(11,'DPWH PROJECT',219,1,2,'SAMPLE MODE OF IMPLEMENTATION','2020-08-07','2020-08-07','2020-08-07','2020-08-07',1),
(14,'SAMPLE DOH PROJECTS',177,5,2,'SAMPLE MODE','2020-10-06','2020-10-31','2020-10-06','2020-10-06',1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'Admin','This is the administration role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Agency','This is the implementing agency role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Member','This is the member role','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `sectors` */

DROP TABLE IF EXISTS `sectors`;

CREATE TABLE `sectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sectors` */

insert  into `sectors`(`id`,`sector`) values 
(1,'Economic'),
(2,'Infrastructure'),
(3,'Social'),
(4,'Government Institutional Development'),
(5,'Others');

/*Table structure for table `sources` */

DROP TABLE IF EXISTS `sources`;

CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `sources` */

insert  into `sources`(`id`,`type`) values 
(1,'Official Development Assistance'),
(2,'Local Financing'),
(3,'Public-Private Partnership'),
(4,'Other (please specify)');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`color`,`created_at`,`updated_at`) values 
(1,'Hot','#f44336','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Trending','#9c27b0','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'New','#00bcd4','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `agency_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`picture`,`role_id`,`agency_id`,`remember_token`,`created_at`,`updated_at`) values 
(1,'BNL Admin','admin@material.com','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i','profile/8FxgfOdSMojZ5wXqAvxYYabg3xtZgu1T8BRCpdEf.png',1,0,'YicldokxY762PYzs98MWPvN6H05Gc6QAO4KaYXcEfjYFqMTamtbWylFeBXBR','2020-07-24 06:35:23','2020-07-31 10:29:58'),
(2,'Sample Agency','creator@material.com','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i','profile/5Z0a92oYiGM7pxyuhLpCCX8J1z8KaOjkBBO5hZ0g.png',2,276,'eip74eTgp0I1kX1pMoGLNiiVoQX5JAV6AuWwySrJm7Hq7ARmjEZ7VRPJPTGx','2020-07-24 06:35:23','2020-07-31 10:54:09'),
(3,'Member','member@material.com','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i',NULL,3,0,'wZCTlNF4qE','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
