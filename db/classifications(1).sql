-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2021 at 11:11 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erpmes`
--

-- --------------------------------------------------------

--
-- Table structure for table `classifications`
--

CREATE TABLE `classifications` (
  `id` int(11) NOT NULL,
  `subsector_id` int(11) NOT NULL,
  `classification` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classifications`
--

INSERT INTO `classifications` (`id`, `subsector_id`, `classification`) VALUES
(24, 1, 'Crop Production'),
(25, 1, 'Livestock and Poultry'),
(26, 1, 'Aquatic and Marine Production'),
(27, 1, 'Post Harvest Facilities/Equipmentand Services'),
(28, 1, 'Agri-Related Trainings'),
(29, 1, 'Research and Development(R&D)'),
(30, 2, 'Land Distribution/Land Tenure Improvement'),
(31, 2, 'Support Services to ARCs'),
(32, 3, 'Upland Resource Management'),
(33, 3, 'Lowland Resource Management'),
(34, 3, 'Coastal Resource Management'),
(35, 5, 'Manufacturing'),
(36, 5, 'Mining and Quarrying'),
(37, 6, 'Trainings on Cooperativism'),
(38, 6, 'Credit Facilities and Services'),
(39, 10, 'Resettlement'),
(40, 10, 'Socialized Housing'),
(41, 10, 'Economic Housing'),
(42, 12, 'Health and Nutrition'),
(43, 12, 'Reproductive Health'),
(44, 12, 'Sanitation'),
(45, 14, 'Roads'),
(46, 14, 'Bridges'),
(47, 14, 'SeaPorts'),
(48, 14, 'Airports'),
(49, 15, 'Irrigation'),
(50, 15, 'Water Supply'),
(51, 15, 'Flood Control'),
(52, 16, 'Generation'),
(53, 16, 'Transmission'),
(54, 16, 'Distribution'),
(55, 17, 'Telecommunication'),
(56, 17, 'Postal'),
(57, 17, 'Internet'),
(58, 18, 'School Buildings'),
(59, 18, 'Health Centers'),
(60, 18, 'Other Community Infrastructure'),
(61, 18, 'Other Government Buildings/Offices');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classifications`
--
ALTER TABLE `classifications`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `classifications_subsector_id_foreign` (`subsector_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classifications`
--
ALTER TABLE `classifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classifications`
--
ALTER TABLE `classifications`
  ADD CONSTRAINT `classifications_subsector_id_foreign` FOREIGN KEY (`subsector_id`) REFERENCES `subsectors` (`id`) ON DELETE CASCADE;
  
ALTER TABLE `projects`
ADD COLUMN `classification` int(11) DEFAULT NULL AFTER `subsector`;
  
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
