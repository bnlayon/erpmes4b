/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.14-MariaDB : Database - erpmes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erpmes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `erpmes`;

/*Table structure for table `agencies` */

DROP TABLE IF EXISTS `agencies`;

CREATE TABLE `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) DEFAULT NULL,
  `motheragency_id` int(11) DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) DEFAULT NULL,
  `Category` varchar(10) DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) DEFAULT NULL,
  `head` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=281 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `agencies` */

insert  into `agencies`(`id`,`UACS_DPT_ID`,`motheragency_id`,`UACS_AGY_ID`,`Category`,`UACS_DPT_DSC`,`Abbreviation`,`UACS_AGY_DSC`,`head`) values 
(2,NULL,NULL,NULL,NULL,NULL,'PA2','2ND INFANTRY DIVISION, PHILIPPINE ARMY ',NULL),
(3,NULL,NULL,NULL,NULL,NULL,'BFAR','BUREAU OF FISHERIES AND AQUATIC RESOURCES MIMAROPA',NULL),
(4,NULL,NULL,NULL,NULL,NULL,'CAAP','CIVIL AVIATION AUTHORITY OF THE PHILIPPINES',NULL),
(5,NULL,NULL,NULL,NULL,NULL,'CHED','COMMISSION ON HIGHER EDUCATION MIMAROPA',NULL),
(6,NULL,NULL,NULL,NULL,NULL,'CPD','COMMISSION ON POPULATION AND DEVELOPMENT MIMAROPA',NULL),
(7,NULL,NULL,NULL,NULL,NULL,'DAR','DEPARTMENT OF AGRARIAN REFORM MIMAROPA',NULL),
(8,NULL,NULL,NULL,NULL,NULL,'DA','DEPARTMENT OF AGRICULTRE MIMAROPA',NULL),
(9,NULL,NULL,NULL,NULL,NULL,'DEPED','DEPARTMENT OF EDUCATION MIMAROPA',NULL),
(10,NULL,NULL,NULL,NULL,NULL,'DOE','DEPARTMENT OF ENERGY',NULL),
(11,NULL,NULL,NULL,NULL,NULL,'DENR','DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES MIMAROPA',NULL),
(12,NULL,NULL,NULL,NULL,NULL,'DOH','DEPARTMENT OF HEALTH MIMAROPA',NULL),
(13,NULL,NULL,NULL,NULL,NULL,'DICT','DEPARTMENT OF INFORMATION AND COMMUNICATIONS TECHNOLOGY - LUZON CLUSTER 3',NULL),
(14,NULL,NULL,NULL,NULL,NULL,'DOLE','DEPARTMENT OF LABOR AND EMPLOYMENT MIMAROPA',NULL),
(15,NULL,NULL,NULL,NULL,NULL,'DPWH','DEPARTMENT OF PUBLIC WORKS AND HIGHWAYS MIMAROPA',NULL),
(16,NULL,NULL,NULL,NULL,NULL,'DOST','DEPARTMENT OF SCIENCE AND TECHNOLOGY MIMAROPA',NULL),
(17,NULL,NULL,NULL,NULL,NULL,'DSWD','DEPARTMENT OF SOCIAL WELFARE AND DEVELOPMENT MIMAROPA',NULL),
(18,NULL,NULL,NULL,NULL,NULL,'DILG','DEPARTMENT OF THE INTERIOR AND LOCAL GOVERNMENT MIMAROPA',NULL),
(19,NULL,NULL,NULL,NULL,NULL,'DOT','DEPARTMENT OF TOURISM MIMAROPA',NULL),
(20,NULL,NULL,NULL,NULL,NULL,'DTI','DEPARTMENT OF TRADE AND INDUSTRY MIMAROPA',NULL),
(21,NULL,NULL,NULL,NULL,NULL,'DOTR','DEPARTMENT OF TRANSPORTATION',NULL),
(22,NULL,NULL,NULL,NULL,NULL,'LWUA','LOCAL WATER UTILITIES ADMINISTRATION',NULL),
(23,NULL,NULL,NULL,NULL,NULL,'NCCA','NATIONAL COMMISSION FOR CULTURE AND THE ARTS',NULL),
(24,NULL,NULL,NULL,NULL,NULL,'NCIP','NATIONAL COMMISSION ON INDIGENOUS PEOPLES MIMAROPA',NULL),
(25,NULL,NULL,NULL,NULL,NULL,'NEA','NATIONAL ELECTRIFICATION ADMINISTRATION',NULL),
(26,NULL,NULL,NULL,NULL,NULL,'NHA','NATIONAL HOUSING AUTHORITY REGION 4',NULL),
(27,NULL,NULL,NULL,NULL,NULL,'NIA','NATIONAL IRRIGATION ADMINISTRATION MIMAROPA',NULL),
(28,NULL,NULL,NULL,NULL,NULL,'NNC','NATIONAL NUTRITION COUNCIL MIMAROPA',NULL),
(29,NULL,NULL,NULL,NULL,NULL,'NAPOLCOM','NATIONAL POLICE COMMISSION MIMAROPA',NULL),
(30,NULL,NULL,NULL,NULL,NULL,'NPC','NATIONAL POWER CORPORATION',NULL),
(31,NULL,NULL,NULL,NULL,NULL,'OCD','OFFICE OF CIVIL DEFENSE MIMAROPA',NULL),
(32,NULL,NULL,NULL,NULL,NULL,'PCA','PHILIPPINE COCONUT AUTHORITY MIMAROPA ',NULL),
(33,NULL,NULL,NULL,NULL,NULL,'PDEA','PHILIPPINE DRUG ENFORCEMENT AGENCY MIMAROPA',NULL),
(34,NULL,NULL,NULL,NULL,NULL,'PFIDA','PHILIPPINE FIBER INDUSTRY DEVELOPMENT AUTHORITY 4',NULL),
(35,NULL,NULL,NULL,NULL,NULL,'PIA','PHILIPPINE INFORMATION AGENCY MIMAROPA',NULL),
(36,NULL,NULL,NULL,NULL,NULL,'PNP','PHILIPPINE NATIONAL POLICE MIMAROPA',NULL),
(37,NULL,NULL,NULL,NULL,NULL,'PPA','PHILIPPINE PORTS AUTHORITY',NULL),
(38,NULL,NULL,NULL,NULL,NULL,'PSA','PHILIPPINE STATISTICS AUTHORITY MIMAROPA',NULL),
(39,NULL,NULL,NULL,NULL,NULL,'TESDA','TECHNICAL EDUCATION AND SKILLS DEVELOPMENT AUTHORITY MIMAROPA',NULL),
(40,NULL,NULL,NULL,NULL,NULL,'PAWC','WESTERN COMMAND, PHILIPPINE ARMY',NULL),
(41,NULL,NULL,NULL,NULL,NULL,'MSC','MARINDUQUE STATE COLLEGE',NULL),
(42,NULL,NULL,NULL,NULL,NULL,'MSCAT','MINDORO STATE COLLEGE OF AGRICULTURE AND TECHNOLOGY',NULL),
(43,NULL,NULL,NULL,NULL,NULL,'OMSC','OCCIDENTAL MINDORO STATE COLLEGE',NULL),
(44,NULL,NULL,NULL,NULL,NULL,'PSU','PALAWAN STATE UNIVERSITY',NULL),
(45,NULL,NULL,NULL,NULL,NULL,'RSU','ROMBLON STATE UNIVERSITY',NULL),
(46,NULL,NULL,NULL,NULL,NULL,'WPU','WESTERN PHILIPPINES UNIVERSITY',NULL),
(47,NULL,NULL,NULL,NULL,NULL,'MAR','PROVINCIAL GOVERNMENT OF MARINDUQUE',NULL),
(48,NULL,NULL,NULL,NULL,NULL,'OCM','PROVINCIAL GOVERNMENT OF OCCIDENTAL MINDORO',NULL),
(49,NULL,NULL,NULL,NULL,NULL,'ORM','PROVINCIAL GOVERNMENT OF ORIENTAL MINDORO',NULL),
(50,NULL,NULL,NULL,NULL,NULL,'PAL','PROVINCIAL GOVERNMENT OF PALAWAN',NULL),
(51,NULL,NULL,NULL,NULL,NULL,'ROM','PROVINCIAL GOVERNMENT OF ROMBLON',NULL),
(52,NULL,NULL,NULL,NULL,NULL,'CAL','CITY GOVERNMENT OF CALAPAN ',NULL),
(53,NULL,NULL,NULL,NULL,NULL,'PP','CITY GOVERNMENT OF PUERTO PRINCESA ',NULL),
(276,'24',0,'001','HEAD','National Economic and Development Authority (NEDA)','NEDA','National Economic and Development Authority (NEDA)','Acting Secretary Karl Kendrick T. Chua'),
(278,NULL,NULL,NULL,NULL,NULL,NULL,'POPULATION COMMISSION',NULL),
(279,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE PORTS AUTHORITY - MINDORO',NULL),
(280,NULL,NULL,NULL,NULL,NULL,NULL,'PHILIPPINE PORTS AUTHORITY - PALAWAN',NULL);

/*Table structure for table `agency_form1s` */

DROP TABLE IF EXISTS `agency_form1s`;

CREATE TABLE `agency_form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `fy` int(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form1s` */

insert  into `agency_form1s`(`id`,`status`,`created_at`,`updated_at`,`fy`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2021-01-18','2021-01-18',2020,27,NULL,NULL),
(2,'Draft','2021-01-18','2021-01-18',2020,276,NULL,NULL),
(3,'Draft','2021-01-18','2021-01-18',2020,46,NULL,NULL),
(5,'Draft','2021-01-18','2021-01-18',2020,48,NULL,NULL),
(6,'Draft','2021-01-18','2021-01-18',2020,14,NULL,NULL),
(7,'Draft','2021-01-18','2021-01-18',2020,47,NULL,NULL),
(8,'Draft','2021-01-18','2021-01-18',2020,11,NULL,NULL),
(9,'Draft','2021-01-18','2021-01-18',2020,51,NULL,NULL),
(10,'Draft','2021-01-18','2021-01-18',2020,51,NULL,NULL),
(11,'Draft','2021-01-18','2021-01-18',2020,41,NULL,NULL),
(12,'Draft','2021-01-18','2021-01-18',2020,39,NULL,NULL),
(13,'Draft','2021-01-18','2021-01-18',2020,29,NULL,NULL),
(14,'Draft','2021-01-18','2021-01-18',2020,52,NULL,NULL),
(15,'Draft','2021-01-18','2021-01-18',2020,49,NULL,NULL),
(16,'Draft','2021-01-18','2021-01-18',2020,50,NULL,NULL),
(17,'Draft','2021-01-18','2021-01-18',2020,8,NULL,NULL),
(18,'Draft','2021-01-18','2021-01-18',2020,13,NULL,NULL),
(19,'Submitted','2021-01-18','2021-01-19',2020,22,NULL,NULL),
(20,'Draft','2021-01-18','2021-01-18',2020,19,NULL,NULL),
(21,'Draft','2021-01-18','2021-01-18',2020,45,NULL,NULL),
(22,'Draft','2021-01-19','2021-01-19',2020,43,NULL,NULL),
(23,'Draft','2021-01-19','2021-01-19',2020,278,NULL,NULL),
(24,'Draft','2021-01-19','2021-01-19',2020,15,NULL,NULL);

/*Table structure for table `agency_form1s_projects` */

DROP TABLE IF EXISTS `agency_form1s_projects`;

CREATE TABLE `agency_form1s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form1s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `fs_1` decimal(10,2) DEFAULT 0.00,
  `fs_2` decimal(10,2) DEFAULT 0.00,
  `fs_3` decimal(10,2) DEFAULT 0.00,
  `fs_4` decimal(10,2) DEFAULT 0.00,
  `fs_5` decimal(10,2) DEFAULT 0.00,
  `fs_6` decimal(10,2) DEFAULT 0.00,
  `fs_7` decimal(10,2) DEFAULT 0.00,
  `fs_8` decimal(10,2) DEFAULT 0.00,
  `fs_9` decimal(10,2) DEFAULT 0.00,
  `fs_10` decimal(10,2) DEFAULT 0.00,
  `fs_11` decimal(10,2) DEFAULT 0.00,
  `fs_12` decimal(10,2) DEFAULT 0.00,
  `pt_1` decimal(10,2) DEFAULT 0.00,
  `pt_2` decimal(10,2) DEFAULT 0.00,
  `pt_3` decimal(10,2) DEFAULT 0.00,
  `pt_4` decimal(10,2) DEFAULT 0.00,
  `pt_5` decimal(10,2) DEFAULT 0.00,
  `pt_6` decimal(10,2) DEFAULT 0.00,
  `pt_7` decimal(10,2) DEFAULT 0.00,
  `pt_8` decimal(10,2) DEFAULT 0.00,
  `pt_9` decimal(10,2) DEFAULT 0.00,
  `pt_10` decimal(10,2) DEFAULT 0.00,
  `pt_11` decimal(10,2) DEFAULT 0.00,
  `pt_12` decimal(10,2) DEFAULT 0.00,
  `oi_1` varchar(250) DEFAULT '-',
  `oi_2` varchar(250) DEFAULT '-',
  `oi_3` varchar(250) DEFAULT '-',
  `oi_4` varchar(250) DEFAULT '-',
  `oi_5` varchar(250) DEFAULT '-',
  `oi_6` varchar(250) DEFAULT '-',
  `oi_7` varchar(250) DEFAULT '-',
  `oi_8` varchar(250) DEFAULT '-',
  `oi_9` varchar(250) DEFAULT '-',
  `oi_10` varchar(250) DEFAULT '-',
  `oi_11` varchar(250) DEFAULT '-',
  `oi_12` varchar(250) DEFAULT '-',
  `eg_1` int(11) DEFAULT 0,
  `eg_2` int(11) DEFAULT 0,
  `eg_3` int(11) DEFAULT 0,
  `eg_4` int(11) DEFAULT 0,
  `eg_5` int(11) DEFAULT 0,
  `eg_6` int(11) DEFAULT 0,
  `eg_7` int(11) DEFAULT 0,
  `eg_8` int(11) DEFAULT 0,
  `eg_9` int(11) DEFAULT 0,
  `eg_10` int(11) DEFAULT 0,
  `eg_11` int(11) DEFAULT 0,
  `eg_12` int(11) DEFAULT 0,
  `eg_13` int(11) DEFAULT 0,
  `eg_14` int(11) DEFAULT 0,
  `eg_15` int(11) DEFAULT 0,
  `eg_16` int(11) DEFAULT 0,
  `eg_17` int(11) DEFAULT 0,
  `eg_18` int(11) DEFAULT 0,
  `eg_19` int(11) DEFAULT 0,
  `eg_20` int(11) DEFAULT 0,
  `eg_21` int(11) DEFAULT 0,
  `eg_22` int(11) DEFAULT 0,
  `eg_23` int(11) DEFAULT 0,
  `eg_24` int(11) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form1s_projects` */

insert  into `agency_form1s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form1s_id`,`nro_remarks`,`agency_remarks`,`fs_1`,`fs_2`,`fs_3`,`fs_4`,`fs_5`,`fs_6`,`fs_7`,`fs_8`,`fs_9`,`fs_10`,`fs_11`,`fs_12`,`pt_1`,`pt_2`,`pt_3`,`pt_4`,`pt_5`,`pt_6`,`pt_7`,`pt_8`,`pt_9`,`pt_10`,`pt_11`,`pt_12`,`oi_1`,`oi_2`,`oi_3`,`oi_4`,`oi_5`,`oi_6`,`oi_7`,`oi_8`,`oi_9`,`oi_10`,`oi_11`,`oi_12`,`eg_1`,`eg_2`,`eg_3`,`eg_4`,`eg_5`,`eg_6`,`eg_7`,`eg_8`,`eg_9`,`eg_10`,`eg_11`,`eg_12`,`eg_13`,`eg_14`,`eg_15`,`eg_16`,`eg_17`,`eg_18`,`eg_19`,`eg_20`,`eg_21`,`eg_22`,`eg_23`,`eg_24`) values 
(1,2,'Endorsed','2021-01-18','2021-01-18',2,NULL,'Sample Remarks',100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,100000.00,10.00,10.00,10.00,10.00,10.00,20.00,10.00,5.00,5.00,10.00,0.00,0.00,'-','Sample Output Indicator','-','-','-','Sample Output Indicator','Sample Output Indicator','-','-','Sample Output Indicator','-','-',0,11,0,0,0,11,0,5,4,0,0,0,0,11,0,0,0,11,0,5,3,0,0,0),
(2,3,'Draft','2021-01-18','2021-01-18',1,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(3,6,'Draft','2021-01-18','2021-01-18',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(4,7,'Draft','2021-01-18','2021-01-18',13,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(5,4,'Draft','2021-01-18','2021-01-18',14,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(6,8,'Draft','2021-01-18','2021-01-18',5,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(7,20,'Draft','2021-01-18','2021-01-18',14,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(8,21,'Endorsed','2021-01-18','2021-01-18',3,NULL,'For Review',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(9,22,'Draft','2021-01-18','2021-01-18',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(10,26,'Draft','2021-01-18','2021-01-18',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(11,5,'Draft','2021-01-18','2021-01-18',21,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(12,25,'Draft','2021-01-18','2021-01-18',21,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(13,35,'Draft','2021-01-18','2021-01-18',21,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(14,38,'Draft','2021-01-18','2021-01-18',21,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(15,44,'Draft','2021-01-18','2021-01-18',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(16,51,'Draft','2021-01-18','2021-01-18',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(17,30,'Draft','2021-01-19','2021-01-19',19,NULL,NULL,1.32,1.32,1.32,1.24,1.72,1.72,6.16,5.09,1.24,0.83,0.83,1.32,8.00,6.00,5.00,5.00,7.00,7.00,25.00,20.00,5.00,3.00,3.00,6.00,'-Source Developement','--Source Developement','--Source Developement','--Source Developement','-Construction of Pumping station/Installation of Electro-mechanical equipments & treatment facility','--Construction of Pumping station/Installation of Electro-mechanical equipments & treatment facility','--Construction of Pumping station/Installation of Electro-mechanical equipments & treatment facility','--Construction of Pumping station/Installation of Electro-mechanical equipments & treatment facility','--Construction of Pumping station/Installation of Electro-mechanical equipments & treatment facility','-Installation of Pipelines (Transmission & Distribution), Pipe fittings and appurtenances','--Installation of Pipelines (Transmission & Distribution), Pipe fittings and appurtenances','--Installation of Pipelines (Transmission & Distribution), Pipe fittings and appurtenances',4,0,0,0,10,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,1,0,0),
(18,70,'Draft','2021-01-19','2021-01-19',24,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(19,101,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(20,106,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(21,108,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(22,109,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(23,109,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(24,110,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(25,111,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(26,112,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(27,115,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(28,117,'Draft','2021-01-19','2021-01-19',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(29,114,'Draft','2021-01-19','2021-01-19',22,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

/*Table structure for table `agency_form2s` */

DROP TABLE IF EXISTS `agency_form2s`;

CREATE TABLE `agency_form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form2s` */

insert  into `agency_form2s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2021-01-18','2021-01-18','2020-12',276,NULL,NULL),
(2,'Draft','2021-01-18','2021-01-18','2020-12',49,NULL,NULL),
(3,'Draft','2021-01-18','2021-01-18','2020-12',46,NULL,NULL),
(4,'Draft','2021-01-18','2021-01-18','2020-12',27,NULL,NULL),
(5,'Draft','2021-01-18','2021-01-18','2020-12',47,NULL,NULL),
(6,'Draft','2021-01-18','2021-01-18','2020-12',53,NULL,NULL),
(7,'Draft','2021-01-18','2021-01-18','2020-12',44,NULL,NULL),
(8,'Draft','2021-01-18','2021-01-18','2020-12',29,NULL,NULL),
(9,'Draft','2021-01-18','2021-01-18','2020-12',41,NULL,NULL),
(10,'Draft','2021-01-18','2021-01-18','2020-12',30,NULL,NULL),
(11,'Draft','2021-01-18','2021-01-18','2020-12',12,NULL,NULL),
(12,'Draft','2021-01-18','2021-01-18','2020-12',13,NULL,NULL),
(13,'Draft','2021-01-18','2021-01-18','2020-12',15,NULL,NULL),
(14,'Draft','2021-01-18','2021-01-18','2020-12',50,NULL,NULL),
(15,'Draft','2021-01-18','2021-01-18','2020-12',14,NULL,NULL),
(16,'Draft','2021-01-18','2021-01-18','2020-12',45,NULL,NULL),
(17,'Draft','2021-01-19','2021-01-19','2020-12',43,NULL,NULL),
(18,'Draft','2021-01-19','2021-01-19','2020-12',48,NULL,NULL),
(19,'Draft','2021-01-19','2021-01-19','2020-12',51,NULL,NULL),
(20,'Submitted','2021-01-19','2021-01-19','2020-12',22,NULL,NULL);

/*Table structure for table `agency_form2s_projects` */

DROP TABLE IF EXISTS `agency_form2s_projects`;

CREATE TABLE `agency_form2s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form2s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `alloc_asof` decimal(20,2) DEFAULT 0.00,
  `alloc_month` decimal(20,2) DEFAULT 0.00,
  `releases_asof` decimal(20,2) DEFAULT 0.00,
  `releases_month` decimal(20,2) DEFAULT 0.00,
  `obligations_asof` decimal(20,2) DEFAULT 0.00,
  `obligations_month` decimal(20,2) DEFAULT 0.00,
  `expenditures_asof` decimal(20,2) DEFAULT 0.00,
  `expenditures_month` decimal(20,2) DEFAULT 0.00,
  `male` int(11) DEFAULT 0,
  `female` int(11) DEFAULT 0,
  `oi` text DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT 0.00,
  `tftm` decimal(20,2) DEFAULT 0.00,
  `atd` decimal(20,2) DEFAULT 0.00,
  `aftm` decimal(20,2) DEFAULT 0.00,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form2s_projects` */

insert  into `agency_form2s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form2s_id`,`nro_remarks`,`agency_remarks`,`alloc_asof`,`alloc_month`,`releases_asof`,`releases_month`,`obligations_asof`,`obligations_month`,`expenditures_asof`,`expenditures_month`,`male`,`female`,`oi`,`ttd`,`tftm`,`atd`,`aftm`) values 
(1,3,'Draft','2021-01-18','2021-01-18',4,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,'Canal',0.00,0.00,0.00,0.00),
(2,2,'Endorsed','2021-01-18','2021-01-18',1,NULL,'Sample Remarks',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2,0,'SAMPLE OUTPUT INDICATOR',0.00,0.00,0.00,0.00),
(3,6,'Draft','2021-01-18','2021-01-18',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(4,26,'Draft','2021-01-18','2021-01-18',9,NULL,NULL,6000000.00,0.00,6000000.00,0.00,5700000.00,0.00,5700000.00,0.00,24,0,'(1)Concrete Structure is finished. (2)Trusses were installed.  (3) Windows were partially installed.',0.00,5.00,95.00,0.00),
(5,21,'Endorsed','2021-01-18','2021-01-18',3,NULL,'For Review by my Head of Office',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(6,27,'Draft','2021-01-18','2021-01-18',12,NULL,NULL,4313484.00,4313484.00,0.00,0.00,4300000.00,4300000.00,0.00,0.00,8,5,'Number of Activated Sites',28.00,28.00,0.00,0.00),
(7,44,'Draft','2021-01-18','2021-01-18',9,NULL,NULL,26123000.00,0.00,26123000.00,0.00,26123000.00,1177586.00,26123000.00,1177586.00,34,1,'All the works are finished.',0.00,0.00,100.00,0.00),
(8,54,'Draft','2021-01-18','2021-01-18',15,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(9,70,'Draft','2021-01-18','2021-01-18',13,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(10,72,'Draft','2021-01-18','2021-01-18',12,NULL,NULL,7414624.00,7414624.00,0.00,0.00,7400000.00,7400000.00,0.00,0.00,8,5,'Number of Activated Sites',50.00,50.00,0.00,0.00),
(11,5,'Draft','2021-01-18','2021-01-18',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(12,25,'Draft','2021-01-18','2021-01-18',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(13,35,'Draft','2021-01-18','2021-01-18',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(14,38,'Draft','2021-01-18','2021-01-18',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(15,95,'Draft','2021-01-19','2021-01-19',13,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(16,19,'Draft','2021-01-19','2021-01-19',18,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(17,51,'Draft','2021-01-19','2021-01-19',19,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(18,7,'Draft','2021-01-19','2021-01-19',8,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(19,93,'Draft','2021-01-19','2021-01-19',12,NULL,NULL,19718784.00,19718784.00,0.00,0.00,19710000.00,19710000.00,0.00,0.00,8,5,'Number of Activated Sites',128.00,128.00,0.00,0.00),
(20,101,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(21,106,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(22,117,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(23,108,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(24,109,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(25,110,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(26,111,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(27,112,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(28,115,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(29,30,'Draft','2021-01-19','2021-01-19',20,NULL,NULL,5.00,5.00,1.00,1.00,1.00,1.00,1.00,1.00,4,0,'Source Development',8.00,8.00,1.00,1.00);

/*Table structure for table `agency_form3s` */

DROP TABLE IF EXISTS `agency_form3s`;

CREATE TABLE `agency_form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form3s` */

insert  into `agency_form3s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Submitted','2021-01-18','2021-01-18','2021-01',276,NULL,NULL),
(2,'Draft','2021-01-18','2021-01-18','2021-01',49,NULL,NULL),
(3,'Draft','2021-01-18','2021-01-18','2021-01',46,NULL,NULL),
(4,'Draft','2021-01-18','2021-01-18','2021-01',10,NULL,NULL),
(5,'Draft','2021-01-18','2021-01-18','2021-01',27,NULL,NULL),
(6,'Draft','2021-01-18','2021-01-18','2021-01',39,NULL,NULL),
(7,'Draft','2021-01-18','2021-01-18','2021-01',29,NULL,NULL),
(8,'Draft','2021-01-18','2021-01-18','2021-01',52,NULL,NULL),
(9,'Draft','2021-01-18','2021-01-18','2021-01',41,NULL,NULL),
(10,'Draft','2021-01-18','2021-01-18','2021-01',48,NULL,NULL),
(11,'Draft','2021-01-18','2021-01-18','2021-01',45,NULL,NULL),
(12,'Draft','2021-01-18','2021-01-18','2021-01',13,NULL,NULL),
(13,'Draft','2021-01-18','2021-01-18','2021-01',15,NULL,NULL),
(14,'Submitted','2021-01-19','2021-01-19','2021-01',22,NULL,NULL);

/*Table structure for table `agency_form3s_projects` */

DROP TABLE IF EXISTS `agency_form3s_projects`;

CREATE TABLE `agency_form3s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form3s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `findings` text DEFAULT NULL,
  `possible` text DEFAULT NULL,
  `recommendations` text DEFAULT NULL,
  `impstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form3s_projects` */

insert  into `agency_form3s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form3s_id`,`nro_remarks`,`agency_remarks`,`findings`,`possible`,`recommendations`,`impstatus`) values 
(1,2,'Endorsed','2021-01-18','2021-01-18',1,NULL,'Sample Remarks','Sample Findings\r\n\r\n1.','Sample Possible Reasons\r\n\r\n1.','Sample Recommendations\r\n\r\n1.',3),
(2,1,'Endorsed','2021-01-18','2021-01-18',1,NULL,'Sa','Findings','Causes','Recommendations',3),
(3,21,'Endorsed','2021-01-18','2021-01-18',3,NULL,'For Review of the Head of Office','1. At the start of the project, manpower and material quantity are insufficient that it cannot support the parallel activities of the project.\r\n2. The project engineer regularly communicates with our office project engineer for the weekly plan of completion.\r\n3. Delay in project completion','1. The contractor has another on-going project, hence outsourcing is made by the contractor where only few workers were found. Also, materials such as aggregate and steel reinforcement are out of stock in the local market due to other construction activities in the locality.\r\n2. To ensure that the two parties (implementor and the contractor) know that they have the common goal in completing the project. Problems that arise can be promptly resolved and mitigating measures can be established immediately.\r\n3. Due to or worldwide pandemic crisis, all construction projects were suspended since March 17, 2020 and resumed only last May 4, 2020. By then, the contractor resumed for only one week, after that the contractor requested for suspension of work due to delayed payment of 2 progress billing since the month of December 2019.','1. Problems on materials mentioned were already addressed since steel reinforcement and concrete works are almost completed, while manpower availability will be addressed right after the pandemic crisis since many are looking for a job.\r\n2. Continue the regular communication between the two (2) parties.\r\n3. Facilitate payment of contractor and require them to complete the project.',3),
(4,20,'Draft','2021-01-18','2021-01-18',8,NULL,NULL,NULL,NULL,NULL,2),
(5,5,'Draft','2021-01-18','2021-01-18',11,NULL,NULL,NULL,NULL,NULL,3),
(6,25,'Draft','2021-01-18','2021-01-18',11,NULL,NULL,NULL,NULL,NULL,3),
(7,27,'Draft','2021-01-18','2021-01-18',12,NULL,NULL,'No site has been activated','Contractor failed to deliver the intended installation and internet service due to inability to mobilize in the province','Process the termination of BC Net contract which supposedly should end last July 31, 2020 and look for possible new contractor',3),
(8,72,'Draft','2021-01-18','2021-01-19',12,NULL,NULL,'No site has been activated','Contractor failed to deliver intended installation and internet service due to inability to mobilize in the province','Process the termination of BC Net which supposedly should end last July 31, 2020 and look for a possible new contractor',3),
(9,70,'Draft','2021-01-18','2021-01-18',13,NULL,NULL,NULL,NULL,NULL,3),
(10,93,'Draft','2021-01-19','2021-01-19',12,NULL,NULL,'No site has been activated','Contractor failed to deliver intended installation and internet service due to inability to mobilize in the province','Process the termination of BC Net which supposedly should end last July 31, 2020 and look for a possible new contractor',3),
(11,30,'Draft','2021-01-19','2021-01-19',14,NULL,NULL,'1. The pandemic is one of the reasons for the well drillers to decline their interest in the project.\r\n2. According to well drillers the Approved Budget for the Contract (ABC) is not enough for the project, considering that the location of the project is in the island of Mindoro.','Bidding for two deep wells failed three times. No bidders.','1. Revision of Program of Works (POW)\r\n2. To increase the allocation for Source Development.',3),
(12,26,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,NULL,NULL,NULL,3),
(13,108,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,NULL,NULL,NULL,3),
(14,112,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,NULL,NULL,NULL,3),
(15,115,'Draft','2021-01-19','2021-01-19',9,NULL,NULL,NULL,NULL,NULL,3);

/*Table structure for table `agency_form4s` */

DROP TABLE IF EXISTS `agency_form4s`;

CREATE TABLE `agency_form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form4s` */

insert  into `agency_form4s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(1,'Draft','2021-01-18','2021-01-18','2021-01',276,NULL,NULL),
(2,'Draft','2021-01-18','2021-01-18','2021-01',49,NULL,NULL),
(3,'Draft','2021-01-18','2021-01-18','2021-01',41,NULL,NULL),
(4,'Draft','2021-01-18','2021-01-18','2021-01',52,NULL,NULL),
(5,'Draft','2021-01-18','2021-01-18','2021-01',46,NULL,NULL),
(6,'Draft','2021-01-18','2021-01-18','2021-01',45,NULL,NULL),
(7,'Draft','2021-01-18','2021-01-18','2021-01',13,NULL,NULL),
(8,'Draft','2021-01-19','2021-01-19','2021-01',22,NULL,NULL),
(9,'Draft','2021-01-19','2021-01-19','2021-01',12,NULL,NULL);

/*Table structure for table `agency_form4s_projects` */

DROP TABLE IF EXISTS `agency_form4s_projects`;

CREATE TABLE `agency_form4s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form4s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `objectives` text DEFAULT NULL,
  `indicator` text DEFAULT NULL,
  `results` text DEFAULT NULL,
  `impstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form4s_projects` */

insert  into `agency_form4s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form4s_id`,`nro_remarks`,`agency_remarks`,`objectives`,`indicator`,`results`,`impstatus`) values 
(1,2,'Endorsed','2021-01-18','2021-01-18',1,NULL,'-','1.','1','1',NULL),
(2,4,'Draft','2021-01-18','2021-01-18',4,NULL,NULL,NULL,NULL,NULL,NULL),
(3,21,'Draft','2021-01-18','2021-01-18',5,NULL,NULL,'Upgrade physical facility of the University','Completed academic building','The structure is already 100% completed.',NULL),
(4,5,'Draft','2021-01-18','2021-01-18',6,NULL,NULL,NULL,NULL,NULL,NULL),
(5,6,'Draft','2021-01-18','2021-01-18',3,NULL,NULL,NULL,NULL,NULL,NULL),
(6,27,'Draft','2021-01-18','2021-01-18',7,NULL,NULL,'To activate additional 28 Free Wi-Fi sites in the province of Marinduque','Additional 28 activated Free Wi-Fi sites in Marinduque','No mobilization from contractor yet',NULL),
(7,72,'Draft','2021-01-18','2021-01-18',7,NULL,NULL,'To activate additional 50 sites in Romblon','Additional 50 Free Wi-Fi sites','No mobilization from contractor yet',NULL),
(8,30,'Draft','2021-01-19','2021-01-19',8,NULL,NULL,NULL,NULL,NULL,NULL),
(9,101,'Draft','2021-01-19','2021-01-19',3,NULL,NULL,NULL,NULL,NULL,NULL),
(10,106,'Draft','2021-01-19','2021-01-19',3,NULL,NULL,NULL,NULL,NULL,NULL),
(11,44,'Draft','2021-01-19','2021-01-19',3,NULL,NULL,NULL,NULL,NULL,NULL),
(12,109,'Draft','2021-01-19','2021-01-19',3,NULL,NULL,NULL,NULL,NULL,NULL),
(13,110,'Draft','2021-01-19','2021-01-19',3,NULL,NULL,NULL,NULL,NULL,NULL),
(14,111,'Draft','2021-01-19','2021-01-19',3,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `agency_form5s` */

DROP TABLE IF EXISTS `agency_form5s`;

CREATE TABLE `agency_form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form5s` */

/*Table structure for table `agency_form5s_projects` */

DROP TABLE IF EXISTS `agency_form5s_projects`;

CREATE TABLE `agency_form5s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form5s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `allocation` decimal(20,2) DEFAULT NULL,
  `releases` decimal(20,2) DEFAULT NULL,
  `obligations` decimal(20,2) DEFAULT NULL,
  `expenditures` decimal(20,2) DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT NULL,
  `atd` decimal(20,2) DEFAULT NULL,
  `male` int(11) DEFAULT NULL,
  `female` int(11) DEFAULT NULL,
  `negativeslippage` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form5s_projects` */

/*Table structure for table `agency_form6s` */

DROP TABLE IF EXISTS `agency_form6s`;

CREATE TABLE `agency_form6s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form6s` */

/*Table structure for table `agency_form6s_projects` */

DROP TABLE IF EXISTS `agency_form6s_projects`;

CREATE TABLE `agency_form6s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form6s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `releases` decimal(20,2) DEFAULT NULL,
  `expenditures` decimal(20,2) DEFAULT NULL,
  `atd` decimal(20,2) DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT NULL,
  `issues` text DEFAULT NULL,
  `source` text DEFAULT NULL,
  `action` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form6s_projects` */

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `categories_name_unique` (`category`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category`,`created_at`,`updated_at`) values 
(1,'ELCAC','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Balik Probinsya Program (BP2)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Rehab and Recovery Projects (RRP)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(6,'Official Development Assistance (ODA)',NULL,NULL),
(7,'Regular',NULL,NULL),
(8,'Others',NULL,NULL);

/*Table structure for table `form1s` */

DROP TABLE IF EXISTS `form1s`;

CREATE TABLE `form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fy` int(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form1s` */

insert  into `form1s`(`id`,`fy`,`is_lock`,`created_at`,`updated_at`) values 
(1,2020,0,'2021-01-18','2021-01-18');

/*Table structure for table `form2s` */

DROP TABLE IF EXISTS `form2s`;

CREATE TABLE `form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form2s` */

insert  into `form2s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-12',0,'2021-01-18','2021-01-18'),
(2,'2021-02',1,'2021-01-18','2021-01-18');

/*Table structure for table `form3s` */

DROP TABLE IF EXISTS `form3s`;

CREATE TABLE `form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form3s` */

insert  into `form3s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2021-01',0,'2021-01-18','2021-01-18');

/*Table structure for table `form4s` */

DROP TABLE IF EXISTS `form4s`;

CREATE TABLE `form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form4s` */

insert  into `form4s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2021-01',0,'2021-01-18','2021-01-18');

/*Table structure for table `form5s` */

DROP TABLE IF EXISTS `form5s`;

CREATE TABLE `form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form5s` */

/*Table structure for table `form6s` */

DROP TABLE IF EXISTS `form6s`;

CREATE TABLE `form6s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form6s` */

/*Table structure for table `impstatuses` */

DROP TABLE IF EXISTS `impstatuses`;

CREATE TABLE `impstatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `impstatuses` */

insert  into `impstatuses`(`id`,`type`) values 
(1,'Ahead of Schedule'),
(2,'On Schedule'),
(3,'Behind Schedule');

/*Table structure for table `item_tag` */

DROP TABLE IF EXISTS `item_tag`;

CREATE TABLE `item_tag` (
  `item_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `item_tag_item_id_foreign` (`item_id`) USING BTREE,
  KEY `item_tag_tag_id_foreign` (`tag_id`) USING BTREE,
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `item_tag` */

insert  into `item_tag`(`item_id`,`tag_id`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(3,1);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `items_name_unique` (`name`) USING BTREE,
  KEY `items_category_id_foreign` (`category_id`) USING BTREE,
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`description`,`picture`,`category_id`,`status`,`date`,`show_on_homepage`,`options`,`created_at`,`updated_at`) values 
(1,'5 citybreak ideas for this year','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,1,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Top 10 restaurants in Italy','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Cocktail ideas for your birthday party','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `longitude` float(20,7) DEFAULT NULL,
  `latitude` float(20,7) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `locations` */

insert  into `locations`(`id`,`proj_id`,`location`,`created_at`,`updated_at`,`longitude`,`latitude`) values 
(1,1,'SAMPLE LOCATION 1','2021-01-18','2021-01-18',121.1755219,13.4040995),
(2,2,'Calapan City','2021-01-18','2021-01-18',121.1755371,13.4040833),
(3,4,'City Hall Complex, Guinobatan, Calapan City, Oriental Mindoro','2021-01-18','2021-01-18',13.3800144,121.1835175),
(4,5,'Brgy Liwanag, Odiongan, Romblon','2021-01-18','2021-01-18',121.9830399,12.3969278),
(5,6,'Brgy. Tanza, Boac, Marinduque','2021-01-18','2021-01-18',13.4548855,121.8437881),
(6,8,'Capitol Compound, Brgy. Payompon, Mamburao','2021-01-18','2021-01-18',120.6063232,13.2215271),
(7,9,'Mapaya I, San Jose, Occidental Mindoro','2021-01-18','2021-01-18',121.1903915,12.3850603),
(8,10,'Mapaya I, San Jose, Occidental Mindoro','2021-01-18','2021-01-18',121.1903915,12.3850603),
(9,11,'Mapaya I, San Jose, Occidental Mindoro','2021-01-18','2021-01-18',12.3850603,121.1900024),
(10,12,'Brgy. Rosacara, Bansud, Oriental Mindoro','2021-01-18','2021-01-18',12.8299351,121.4530487),
(11,13,'So. Tibunbon Caagutayan, San Teodoro Oriental Mindoro','2021-01-18','2021-01-18',13.4007854,120.9649963),
(12,14,'Brgy. San Roque, Bulalacao, Oriental Mindoro','2021-01-18','2021-01-18',12.3954105,121.4042740),
(13,15,'Various Barangay of Selected Municipality','2021-01-18','2021-01-18',120.6071701,13.2221880),
(14,16,'Various Barangay of Selected Municipality','2021-01-18','2021-01-18',120.6071701,13.2221880),
(15,17,'Various Barangay of Selected Municipality','2021-01-18','2021-01-18',120.6071701,13.2221880),
(16,18,'Various Barangay of Selected Municipality','2021-01-18','2021-01-18',120.6071701,13.2221880),
(17,19,'Mamburao','2021-01-18','2021-01-18',120.6072388,13.2221804),
(18,20,'Lalud- Pachoco, NIA Road Extn, Calapan City','2021-01-18','2021-01-18',13.4047480,121.1666489),
(19,21,'WPU-Main Campus, Barangay San Juan, Aborlan, Palawan, Region IV-B MIMAROPA','2021-01-18','2021-01-18',118.5585632,9.4430304),
(20,22,'Narra, Palawan','2021-01-18','2021-01-18',9.2692986,118394472.0000000),
(21,23,'Roxas','2021-01-18','2021-01-18',119.1927795,10.2588892),
(22,24,'Mamburao','2021-01-18','2021-01-18',120.6071777,13.2222328),
(23,25,'Poblacion, San Fernando, Romblon 5513','2021-01-18','2021-01-18',122.6011810,12.3049383),
(24,26,'Tanza, Boac, Marinduque','2021-01-18','2021-01-18',13.4644890,121.8490829),
(25,27,'Marinduque State College-Sta. Cruz Campus','2021-01-18','2021-01-18',121844960.0000000,13.4531822),
(26,27,'Marinduque State College-Sta. Cruz Campus','2021-01-18','2021-01-18',122.0323105,13.4788885),
(27,28,'Mamburao','2021-01-18','2021-01-18',120.6069946,13.2222509),
(28,29,'Mamburao, Occidental Mindoro','2021-01-18','2021-01-18',120.5394516,13.2531424),
(29,30,'Sablayan','2021-01-18','2021-01-18',120.7754974,12.8420696),
(30,31,'Brgy. Rosacara, Bansud,Oriental Mindoro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(31,27,'Dr. Damian Reyes Provincial Hospital','2021-01-18','2021-01-18',121.8290634,13.4426079),
(32,27,'Marinduque State College-Main Campus','2021-01-18','2021-01-18',121.8444901,13.4538422),
(33,27,'Provincial Capitol of Marinduque','2021-01-18','2021-01-18',121.8276825,13.4420080),
(34,32,'Brgy. Sabang, Pinamalayan, Oriental Mindoro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(35,33,'San Jose Occidental Mindoro','2021-01-18','2021-01-18',12.3675489,121.0799103),
(36,34,'Brgy. Payompon, Mamburao','2021-01-18','2021-01-18',120.6051254,13.2193995),
(37,35,'Agpudlos, San Andres, Romblon 5504','2021-01-18','2021-01-18',122.0080643,12.5996733),
(38,36,'Bo.Malakibay, Brgy. Punta Baja, Rizal, Palawan','2021-01-18','2021-01-18',0.0000000,0.0000000),
(39,37,'Brgy. Poblacion, Sta. Cruz, Occidental Mindoro','2021-01-18','2021-01-18',120.7185211,13.0834856),
(40,38,'Brgy Liwanag, Odiongan, Romblon','2021-01-18','2021-01-18',121.9827652,12.3967438),
(41,39,'Mamburao, Occidental Mindoro','2021-01-18','2021-01-18',120.5860367,13.2304792),
(42,40,'Brgy. Rosacara, Bansud,Oriental Mindoro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(43,41,'Sta. Cruz','2021-01-18','2021-01-18',120.7191010,13.0833864),
(44,41,'Paluan','2021-01-18','2021-01-18',120.4642410,13.4167747),
(45,41,'Sablayan','2021-01-18','2021-01-18',120780144.0000000,12.8438759),
(46,41,'Rizal','2021-01-18','2021-01-18',120963928.0000000,12.4595261),
(47,42,'Brgy. Sudlon, Rizal, Occidental Mindoro','2021-01-18','2021-01-18',120.9636459,12.4591093),
(48,43,'Brgy. Paetan, Sablayan, Occidental Mindoro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(49,44,'Tanza, Boac, Marinduque','2021-01-18','2021-01-18',13.4645224,121.8488998),
(50,45,'So. Binuhangin, Brgy. Tubili, Paluan','2021-01-18','2021-01-18',120.4938278,13.3284092),
(51,46,'So. Malapaga, Brgy. San Vicente, Sablyan, Occidental Mindoro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(52,47,'Brgy. III, Paluan','2021-01-18','2021-01-18',120.4642181,13.4168396),
(53,48,'Brgy. Masaquisi, Bongabong, Oriental Min doro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(54,49,'Occidental Mindoro State College','2021-01-18','2021-01-18',120.5912933,13.2273502),
(55,49,'San Jose National High School','2021-01-18','2021-01-18',121.0587616,12.3679419),
(56,49,'Sablayan National Comprehensive High School','2021-01-18','2021-01-18',120.7818832,12.8514004),
(57,49,'Occidental Mindoro National High School','2021-01-18','2021-01-18',123.6006622,13.2190905),
(58,49,'Magsaysay National High School','2021-01-18','2021-01-18',121.1379471,12.3043203),
(59,52,'MINOLO, PUERTO GALERA, ORIENTAL MINDORO TO MAMBURAO, OCCIDENTAL MINDORO','2021-01-18','2021-01-18',13.5001402,120.9546127),
(60,53,'BNHS, Brgy. Barahan, Sta. Cruz','2021-01-18','2021-01-18',120.7582397,13.0159941),
(61,54,'Province of Oriental Mindoro','2021-01-18','2021-01-18',121176936.0000000,13.4058256),
(62,54,'Province of Occidental Mindoro','2021-01-18','2021-01-18',120605168.0000000,13.2217188),
(63,54,'Province of Marinduque','2021-01-18','2021-01-18',121832704.0000000,13.4494696),
(64,54,'Province of Romblon','2021-01-18','2021-01-18',122296800.0000000,12.5530195),
(65,54,'Province of Palawan','2021-01-18','2021-01-18',118743944.0000000,9.7395840),
(66,27,'Marinduque State College-Gasan Campus','2021-01-18','2021-01-18',121.8789062,13.3175859),
(67,27,'Marinduque State College-Torrijos Campus','2021-01-18','2021-01-18',122.0997238,13.3325615),
(68,55,'Brgy. Barahan, Sta. Cruz','2021-01-18','2021-01-18',120.7580719,13.0163870),
(69,56,'Brgy. Labonan, Bongabong, Oriental Mindoro','2021-01-18','2021-01-18',0.0000000,0.0000000),
(70,57,'Brgy. Alimanguhan, San Vicente, Palawan','2021-01-18','2021-01-18',0.0000000,0.0000000),
(71,58,'Sitio Marabong, Batong Buhay, Sablayan, Occidental Mindoro','2021-01-18','2021-01-18',120.9291306,12.8613701),
(72,59,'Brgy. Magsaysay, Aborlan, Palawan','2021-01-18','2021-01-18',0.0000000,0.0000000),
(73,60,'Provincial Demonstration Farm, Merit, Victoria, Oriental Mindoro','2021-01-18','2021-01-18',121.8924332,13.1134005),
(74,63,'Sablayan','2021-01-18','2021-01-18',120.8681183,12.8938322),
(75,64,'Brgy. Mangangan I, Baco, Oriental Mindoro','2021-01-18','2021-01-18',121.0601730,13.3408699),
(76,65,'Roxas Substation-Alimanguan Junction','2021-01-18','2021-01-18',10.3229494,119.3374557),
(77,67,'Puerto Princesa City, Palawan','2021-01-18','2021-01-18',9.7795219,118735408.0000000),
(78,68,'Sablayan','2021-01-18','2021-01-18',120.8859863,12.6810732),
(79,69,'Abra de Ilog','2021-01-18','2021-01-18',120.7262115,13.4443798),
(80,69,'Mamburao','2021-01-18','2021-01-18',120.6071854,13.2222080),
(81,69,'Paluan','2021-01-18','2021-01-18',120.4642563,13.4167986),
(82,69,'Sta. Cruz','2021-01-18','2021-01-18',120.7190628,13.0834131),
(83,69,'San Jose','2021-01-18','2021-01-18',121.0677261,12.3526268),
(84,69,'Calintaan','2021-01-18','2021-01-18',120.9401855,12.5715446),
(85,70,'BOAC-MOGPOG BYPASS ROAD MARINDUQUE','2021-01-18','2021-01-18',121.8326645,13.4681292),
(86,71,'Brgy. Little Tanuan, Roxas, Oriental Mindoro','2021-01-18','2021-01-18',121.4625702,12.6239901),
(87,71,'Odiong, Roxas, Oriental Mindoro','2021-01-18','2021-01-18',121.4903717,12.6105900),
(88,72,'Romblon State University - Romblon College of Fisheries and Forestry - Sibuyan, Cajidiocan','2021-01-18','2021-01-18',122.6837997,12.3672800),
(89,72,'Romblon State University - Romblon College of Fisheries and Forestry - Calatrava','2021-01-18','2021-01-18',122.0608673,12.6172991),
(90,72,'Romblon Provincial Hospital','2021-01-18','2021-01-18',121.9841003,12.3944893),
(91,72,'Romblon State University - Main','2021-01-18','2021-01-18',121.9878311,12.3963575),
(92,72,'Provincial Capitol of Romblon','2021-01-18','2021-01-18',122.2751083,12.5788889),
(93,72,'Romblon State University-Sawang','2021-01-18','2021-01-18',122.2627335,12.5483379),
(94,72,'Romblon State University - Romblon College of Fisheries and Forestry - San Agustin','2021-01-18','2021-01-18',122.1323776,12.6046057),
(95,72,'Romblon State University - Romblon College of Fisheries and Forestry - San Andres','2021-01-18','2021-01-18',122.0711899,12.5638027),
(96,72,'Romblon State University - Sibuyan Polytechnic College - San Fernando','2021-01-18','2021-01-18',122.6015320,12.3055506),
(97,72,'San Jose District Hospital','2021-01-18','2021-01-18',121.9574738,12.0587244),
(98,72,'Romblon State University - Romblon College of Fisheries and Forestry - Sta. Fe','2021-01-18','2021-01-18',121.9943771,12.1567049),
(99,72,'Romblon State University - Romblon College of Fisheries and Forestry - Sta. Maria','2021-01-18','2021-01-18',122.1121826,12.4984245),
(100,74,'Puerto Princesa City, Palawan','2021-01-18','2021-01-18',9.7780638,118733392.0000000),
(101,75,'Sablayan','2021-01-18','2021-01-18',120.8713074,12.8875847),
(102,77,'Brgy. New Dagupan, Calintaan','2021-01-18','2021-01-18',120.9421158,12.5712337),
(103,78,'Alimanguan Junction-Taytay Substation, Palawan','2021-01-18','2021-01-18',10.6362896,119.4089737),
(104,79,'Calintaan','2021-01-18','2021-01-18',120.9431992,12.6179190),
(105,80,'Mamburao','2021-01-18','2021-01-18',120.6071854,13.2222080),
(109,81,'Brgy. Pitogo, Rizal','2021-01-18','2021-01-18',121.0894623,12.5062265),
(110,82,'Rizal','2021-01-18','2021-01-18',120.9996414,12.4692173),
(111,85,'San Jose','2021-01-18','2021-01-18',121.0530853,12.4004726),
(112,86,'Mamburao','2021-01-18','2021-01-18',120.6071854,13.2222080),
(113,88,'Boac, Marinduque and Mogpog, Marinduque','2021-01-18','2021-01-18',13.4742098,121.8614502),
(114,89,'Calintaan','2021-01-18','2021-01-18',120.9487610,12.5818596),
(115,84,'Mamburao','2021-01-18','2021-01-18',120.6071854,13.2222080),
(116,83,'Mamburao','2021-01-18','2021-01-18',120607184.0000000,13.2222080),
(117,90,'Magsaysay','2021-01-18','2021-01-18',121.1491852,12.3026657),
(118,91,'Mamburao','2021-01-18','2021-01-18',120607184.0000000,13.2222080),
(119,92,'Mamburao','2021-01-18','2021-01-18',120607184.0000000,13.2222080),
(120,94,'Brgy. Poblacion, Baco, Oriental Mindoro','2021-01-18','2021-01-18',121.0966034,13.3593702),
(121,95,'OCCIDENTAL MINDORO','2021-01-19','2021-01-19',120.8400269,13.4694538),
(122,97,'Mamburao','2021-01-19','2021-01-19',120.6071854,13.2222080),
(123,98,'Mamburao','2021-01-19','2021-01-19',120.6071854,13.2222080),
(124,99,'SABLAYAN, OCCIDENTAL MINDORO','2021-01-19','2021-01-19',120.9184418,12.9897156),
(125,101,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4547777,121.8446732),
(126,102,'ORIENTAL MINDORO','2021-01-19','2021-01-19',121.2989807,13.1163445),
(127,105,'Brgy. Mendez, Mogpog, Marinduque','2021-01-19','2021-01-19',121.9072189,13.5073137),
(128,93,'Western Philippines University','2021-01-19','2021-01-19',118.5594406,9.4436111),
(129,93,'Palawan State University-Araceli','2021-01-19','2021-01-19',119.9919434,10.5680552),
(130,93,'Palawan State University- Balabac','2021-01-19','2021-01-19',117.0633316,7.9855556),
(131,93,'Palawan State University-Bataraza','2021-01-19','2021-01-19',117.6438904,9.8688335),
(132,93,'Western Philippines University-Rio Tuba','2021-01-19','2021-01-19',119.6586075,8.5500002),
(133,93,'Palawan State University- Brooke\'s Point','2021-01-19','2021-01-19',117.8269424,8.8097219),
(134,93,'Southern Palawan Provincial Hospital','2021-01-19','2021-01-19',117.8358307,8.7933331),
(135,93,'Western Philippines University-Busuanga','2021-01-19','2021-01-19',119.9349976,12.1319447),
(136,93,'Palawan State University-Coron','2021-01-19','2021-01-19',120.1880569,12.0177774),
(137,93,'Culion Sanitarium and General Hospital','2021-01-19','2021-01-19',119.9933319,11.8352776),
(138,93,'Western Philippines University-Culion','2021-01-19','2021-01-19',120.0199966,11.8216667),
(139,93,'Cuyo District Hospital','2021-01-19','2021-01-19',121.0108337,10.8555555),
(140,93,'Palawan State University-Cuyo','2021-01-19','2021-01-19',121.0355530,10.8547220),
(141,93,'Palawan State University-Dumaran','2021-01-19','2021-01-19',119.6650009,10.5013885),
(142,93,'Palawan State University-New Ibajay, El Nido','2021-01-19','2021-01-19',119.4622192,11.2097225),
(143,93,'Western Philippines University-El Nido','2021-01-19','2021-01-19',119.4622192,11.2097225),
(144,93,'Palawan State University-Linapacan','2021-01-19','2021-01-19',119.8672256,11.4905558),
(145,106,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4548779,121.8444366),
(146,108,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4543867,121.8443604),
(147,93,'Palawan State University-Narra','2021-01-19','2021-01-19',118.4144440,9.2769442),
(148,93,'Camp General Artemio Ricarte Station Hospital','2021-01-19','2021-01-19',118.7708359,9.7469444),
(149,93,'Ospital ng Palawan','2021-01-19','2021-01-19',118.7441635,9.7480555),
(150,93,'Palawan State University','2021-01-19','2021-01-19',118.7338867,9.7780552),
(151,93,'Palawan State University-San Rafael, Puerto Princesa City','2021-01-19','2021-01-19',118.9672241,9.9899998),
(152,93,'Provincial Capitol of Palawan','2021-01-19','2021-01-19',118.7441635,9.7394447),
(153,93,'Western Philippines University-Puerto Princesa','2021-01-19','2021-01-19',118.7269440,9.7844448),
(154,93,'Palawan State University-Quezon','2021-01-19','2021-01-19',118.0008316,9.2299995),
(155,93,'Western Philippines University-Quezon','2021-01-19','2021-01-19',118.0008316,9.2299995),
(156,93,'Palawan State University-Rizal','2021-01-19','2021-01-19',117.3499985,8.6833334),
(157,93,'Palawan State University-Roxas','2021-01-19','2021-01-19',119.3494415,10.3338890),
(158,93,'Palawan State University - San Vicente','2021-01-19','2021-01-19',119.2669449,10.5255556),
(159,93,'Palawan State University-Sofronio Española','2021-01-19','2021-01-19',118.0333328,9.4194441),
(160,93,'Northern Palawan Provincial Hospital','2021-01-19','2021-01-19',119.5036087,10.7719440),
(161,93,'Palawan State University-Taytay','2021-01-19','2021-01-19',119.5183334,10.8216667),
(162,109,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4548855,121.8437881),
(163,110,'Brgy. Capayang, Mogpog, Marinduque','2021-01-19','2021-01-19',13.4971142,121.8583832),
(164,111,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4543867,121.8443604),
(165,112,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4531612,121.8453217),
(166,114,'Labangan, San Jose, Occidental Mindoro','2021-01-19','2021-01-19',121.0735168,12.3651552),
(167,115,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4544764,121.8449249),
(168,117,'Brgy. Tanza, Boac, Marinduque','2021-01-19','2021-01-19',13.4536619,121.8450470),
(169,119,'Mamburao','2021-01-19','2021-01-19',120607184.0000000,13.2222080);

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8mb4;

/*Data for the table `logs` */

insert  into `logs`(`id`,`log`,`created_at`,`updated_at`,`username`) values 
(1,'Added Project: NEDA SAMPLE PROJECT','2021-01-18 01:00:39','2021-01-18 01:00:39','NEDASAMPLE'),
(2,'Edited Project: NEDA SAMPLE PROJECT','2021-01-18 01:00:54','2021-01-18 01:00:54','NEDASAMPLE'),
(3,'Edited Project: NEDA SAMPLE PROJECT','2021-01-18 01:01:54','2021-01-18 01:01:54','NEDASAMPLE'),
(4,'Edited Project: NEDA SAMPLE PROJECT','2021-01-18 01:02:00','2021-01-18 01:02:00','NEDASAMPLE'),
(5,'Edited Project: NEDA SAMPLE PROJECT','2021-01-18 01:02:44','2021-01-18 01:02:44','NEDASAMPLE'),
(6,'Added Project: NEDA Mimaropa Building','2021-01-18 01:57:07','2021-01-18 01:57:07','NEDASAMPLE'),
(7,'Endorsed Project for Form 1: NEDA Mimaropa Building','2021-01-18 02:04:15','2021-01-18 02:04:15','NEDASAMPLE'),
(8,'Added Project: MONGPONG RIS','2021-01-18 02:05:30','2021-01-18 02:05:30','Lowell L. Lozano'),
(9,'Submitted Form 1','2021-01-18 02:07:14','2021-01-18 02:07:14','NEDASAMPLE'),
(10,'Endorsed Project for Form 2: NEDA Mimaropa Building','2021-01-18 02:47:15','2021-01-18 02:47:15','NEDASAMPLE'),
(11,'Added Project: Concreting of Access Road CGC Complex (COA Bldg to Motorpool)','2021-01-18 04:56:47','2021-01-18 04:56:47','Aristeo Joseph C. Genil'),
(12,'Added Project: Rehabilitation of Multi-Purpose Building - RSU Main Campus','2021-01-18 05:00:27','2021-01-18 05:00:27','Dr. Reynaldo P. Ramos'),
(13,'Added Project: Construction of MSC Gymnasium','2021-01-18 05:12:26','2021-01-18 05:12:26','Mr. Romell L. Morales'),
(14,'Endorsed Project for Form 3: NEDA Mimaropa Building','2021-01-18 05:14:13','2021-01-18 05:14:13','NEDASAMPLE'),
(15,'Endorsed Project for Form 3: NEDA SAMPLE PROJECT','2021-01-18 05:15:21','2021-01-18 05:15:21','NEDASAMPLE'),
(16,'Submitted Form 1','2021-01-18 05:16:11','2021-01-18 05:16:11','Eljun Francel A. Angeles'),
(17,'Submitted Form 1','2021-01-18 05:16:23','2021-01-18 05:16:23','Eljun Francel A. Angeles'),
(18,'Added Project: Inspection and Monitoring o PNP Offices/Units/Stations','2021-01-18 05:18:27','2021-01-18 05:18:27','Lorlaine F. Dolendo'),
(19,'Submitted Form 3','2021-01-18 05:37:52','2021-01-18 05:37:52','NEDASAMPLE'),
(20,'Endorsed Project for Form 4: NEDA Mimaropa Building','2021-01-18 05:43:26','2021-01-18 05:43:26','NEDASAMPLE'),
(21,'Submitted Form 4','2021-01-18 05:43:55','2021-01-18 05:43:55','NEDASAMPLE'),
(22,'Endorsed Project for Form 4: NEDA Mimaropa Building','2021-01-18 05:45:19','2021-01-18 05:45:19','NEDASAMPLE'),
(23,'Added Project: Construction of Multi-Purpose Hall (Phase II)','2021-01-18 05:56:32','2021-01-18 05:56:32','Emelita G. Agpalo'),
(24,'Added Project: Establishment of Small Water Impounding System Oriental Mindoro 54 cu. m.','2021-01-18 06:05:57','2021-01-18 06:05:57','John Philip M. Merced'),
(25,'Added Project: Establishment of Small Water Impounding System Oriental Mindoro 50 cu. m.','2021-01-18 06:11:28','2021-01-18 06:11:28','John Philip M. Merced'),
(26,'Added Project: Establishment of Small Water Impounding System Oriental Mindoro 50 cu. m. Bulalacao','2021-01-18 06:17:07','2021-01-18 06:17:07','John Philip M. Merced'),
(27,'Added Project: Balik Sigla sa Agrikultura , Cattle Dispersal Program','2021-01-18 06:25:09','2021-01-18 06:25:09','Bernadette T. Salgado'),
(28,'Added Project: Concreting of Road with slope protection (phase II)','2021-01-18 06:25:38','2021-01-18 06:25:38','Aristeo Joseph C. Genil'),
(29,'Edited Project: Rehabilitation of Multi-Purpose Building - RSU Main Campus','2021-01-18 06:27:05','2021-01-18 06:27:05','Dr. Reynaldo P. Ramos'),
(30,'Added Project: Construction of College of Business Management Academic Building','2021-01-18 06:29:40','2021-01-18 06:29:40','Engr. Ryan Jay V. Balcarcel'),
(31,'Added Project: Erpmes Sample Project','2021-01-18 06:31:18','2021-01-18 06:31:18','Mr. Mathew Micah Lamitar'),
(32,'Edited Project: Construction of Multi-Purpose Hall (Phase II)','2021-01-18 06:33:06','2021-01-18 06:33:06','Emelita G. Agpalo'),
(33,'Added Project: Malico CIS (Extension)','2021-01-18 06:33:07','2021-01-18 06:33:07','Lowell L. Lozano'),
(34,'Added Project: Balik Sigla sa Agrikultura - Broiler Dispersal Program','2021-01-18 06:35:39','2021-01-18 06:35:39','Bernadette T. Salgado'),
(35,'Edited Project: Balik Sigla sa Agrikultura , Cattle Dispersal Program','2021-01-18 06:36:16','2021-01-18 06:36:16','Bernadette T. Salgado'),
(36,'Added Project: Rehabilitation of Multi-Purpose Building - San Fernando Campus','2021-01-18 06:36:31','2021-01-18 06:36:31','Dr. Reynaldo P. Ramos'),
(37,'Added Project: Construction of President\'s Housing','2021-01-18 06:36:46','2021-01-18 06:36:46','Engr. Robert N. Lamonte'),
(38,'Edited Project: Rehabilitation of Multi-Purpose Building - RSU Main Campus','2021-01-18 06:37:35','2021-01-18 06:37:35','Dr. Reynaldo P. Ramos'),
(39,'Edited Project: Construction of President\'s Housing','2021-01-18 06:38:04','2021-01-18 06:38:04','Engr. Robert N. Lamonte'),
(40,'Added Project: Free Wi-Fi for All','2021-01-18 06:39:06','2021-01-18 06:39:06','Raiza Joyce A. Mendoza'),
(41,'Added Project: Balik Sigla sa Agrikultura - Layer Chicken Module','2021-01-18 06:39:31','2021-01-18 06:39:31','Bernadette T. Salgado'),
(42,'Edited Project: Rehabilitation of Multi-Purpose Building - RSU Main Campus','2021-01-18 06:45:24','2021-01-18 06:45:24','Dr. Reynaldo P. Ramos'),
(43,'Added Project: Special Maintenance Along Mamburao-Paluan Provincial Road (A)','2021-01-18 06:45:58','2021-01-18 06:45:58','Emelita G. Agpalo'),
(44,'Edited Project: Rehabilitation of Multi-Purpose Building - San Fernando Campus','2021-01-18 06:46:02','2021-01-18 06:46:02','Dr. Reynaldo P. Ramos'),
(45,'Edited Project: Rehabilitation of Multi-Purpose Building - San Fernando Campus','2021-01-18 06:46:31','2021-01-18 06:46:31','Dr. Reynaldo P. Ramos'),
(46,'Added Project: improvement and expansion of existing water supply system of Sablayan WD','2021-01-18 06:47:12','2021-01-18 06:47:12','Punzalan'),
(47,'Added Project: Construction of Centro Diversion Dam','2021-01-18 06:49:11','2021-01-18 06:49:11','Rolibeth G. Morillo '),
(48,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service','2021-01-18 06:49:13','2021-01-18 06:49:13','Raiza Joyce A. Mendoza'),
(49,'Added Project: Construction of Sabang Diversion Dam','2021-01-18 06:53:19','2021-01-18 06:53:19','Rolibeth G. Morillo '),
(50,'Added Project: Rehabilitation of CENRO San Jose Office Building','2021-01-18 06:53:19','2021-01-18 06:53:19','John Philip M. Merced'),
(51,'Added Project: Construction of Dapi Elementary School Multi-Purpose Hall','2021-01-18 06:54:28','2021-01-18 06:54:28','Emelita G. Agpalo'),
(52,'Endorsed Project for Form 1: Construction of College of Business Management Academic Building','2021-01-18 06:57:33','2021-01-18 06:57:33','Engr. Ryan Jay V. Balcarcel'),
(53,'Added Project: Construction of School Dormitory for the College of Agriculture, Fishery and Forestry','2021-01-18 06:57:57','2021-01-18 06:57:57','Dr. Reynaldo P. Ramos'),
(54,'Added Project: Rehabilitation of Malakibay Diversion Dam','2021-01-18 06:58:49','2021-01-18 06:58:49','Rolibeth G. Morillo '),
(55,'Added Project: Completion of Sta. Cruz RHU, Occidental Mindoro','2021-01-18 06:59:22','2021-01-18 06:59:22','Engr. Stephanie S. Sioco'),
(56,'Added Project: Rehabilitation of the College of Arts and Sciences Building','2021-01-18 07:01:21','2021-01-18 07:01:21','Dr. Reynaldo P. Ramos'),
(57,'Added Project: Construction of Crisis Center for Women and Children (Phase-I)','2021-01-18 07:01:26','2021-01-18 07:01:26','Emelita G. Agpalo'),
(58,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service','2021-01-18 07:01:35','2021-01-18 07:01:35','Raiza Joyce A. Mendoza'),
(59,'Added Project: Rehabilitation of Rosacara Diversion Dam','2021-01-18 07:03:22','2021-01-18 07:03:22','Rolibeth G. Morillo '),
(60,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service','2021-01-18 07:04:02','2021-01-18 07:04:02','Raiza Joyce A. Mendoza'),
(61,'Added Project: Distribution of mungbean seeds for crop diversification','2021-01-18 07:07:18','2021-01-18 07:07:18','Bernadette T. Salgado'),
(62,'Added Project: Construction/Rehabilitation and Expansion of Rizal Rural Health Unit','2021-01-18 07:07:45','2021-01-18 07:07:45','Engr. Stephanie S. Sioco'),
(63,'Added Project: Extension of Paetan Irrigation Canal','2021-01-18 07:07:51','2021-01-18 07:07:51','Rolibeth G. Morillo '),
(64,'Endorsed Project for Form 2: Construction of College of Business Management Academic Building','2021-01-18 07:08:05','2021-01-18 07:08:05','Engr. Ryan Jay V. Balcarcel'),
(65,'Added Project: Construction of Learning Resource Center and Museum','2021-01-18 07:09:23','2021-01-18 07:09:23','Engr. Robert N. Lamonte'),
(66,'Added Project: Construction of Concrete Bridge','2021-01-18 07:10:11','2021-01-18 07:10:11','Emelita G. Agpalo'),
(67,'Added Project: Concreting of San Vicente Irrigation Canal','2021-01-18 07:12:45','2021-01-18 07:12:45','Rolibeth G. Morillo '),
(68,'Endorsed Project for Form 3: Construction of College of Business Management Academic Building','2021-01-18 07:13:26','2021-01-18 07:13:26','Engr. Ryan Jay V. Balcarcel'),
(69,'Added Project: Construction of Multi-Purpose Hall','2021-01-18 07:14:55','2021-01-18 07:14:55','Emelita G. Agpalo'),
(70,'Added Project: Construction od Masaquisi Irrigation Canal','2021-01-18 07:16:02','2021-01-18 07:16:02','Rolibeth G. Morillo '),
(71,'Submitted Form 1','2021-01-18 07:16:10','2021-01-18 07:16:10','Engr. Ryan Jay V. Balcarcel'),
(72,'Added Project: neda cr','2021-01-18 07:19:17','2021-01-18 07:19:17','Jojo R. Datinguinoo'),
(73,'Added Project: UPGRADING/IMPROVEMENT/REHABILITATION OF ROMBLON-CAJIMOS-SABANG ROAD','2021-01-18 07:19:30','2021-01-18 07:19:30','Victorino N. Benedicto, Jr.'),
(74,'Added Project: REHAB OF MINOLO (PUERTO-GALERA)-MAMBURAO 69KV TL PROJECT','2021-01-18 07:19:56','2021-01-18 07:19:56','Marilou O. Oribiana'),
(75,'Added Project: Construction of Multi-Purpose Hall','2021-01-18 07:20:47','2021-01-18 07:20:47','Emelita G. Agpalo'),
(76,'Added Project: DOLE Integrated Livelihood Program','2021-01-18 07:21:02','2021-01-18 07:21:02','Kim Neko C. Bana'),
(77,'Edited Project: DOLE Integrated Livelihood Program','2021-01-18 07:21:26','2021-01-18 07:21:26','Kim Neko C. Bana'),
(78,'Edited Project: UPGRADING/IMPROVEMENT/REHABILITATION OF ROMBLON-CAJIMOS-SABANG ROAD','2021-01-18 07:21:56','2021-01-18 07:21:56','Victorino N. Benedicto, Jr.'),
(79,'Edited Project: Rehabilitation of Malakibay Diversion Dam','2021-01-18 07:22:24','2021-01-18 07:22:24','Jean E. Tirante'),
(80,'Edited Project: Construction of Learning Resource Center and Museum','2021-01-18 07:23:34','2021-01-18 07:23:34','Engr. Robert N. Lamonte'),
(81,'Edited Project: neda sample','2021-01-18 07:23:55','2021-01-18 07:23:55','Jojo R. Datinguinoo'),
(82,'Edited Project: Concreting of San Vicente Irrigation Canal','2021-01-18 07:24:08','2021-01-18 07:24:08','Jean E. Tirante'),
(83,'Edited Project: NEDA sample - Jojo','2021-01-18 07:24:12','2021-01-18 07:24:12','Jojo R. Datinguinoo'),
(84,'Added Project: Repair of Roofing of Multi-Purpose Hall and Construction of Stage','2021-01-18 07:24:38','2021-01-18 07:24:38','Emelita G. Agpalo'),
(85,'Edited Project: UPGRADING/IMPROVEMENT/REHABILITATION OF ROMBLON-CAJIMOS-SABANG ROAD','2021-01-18 07:25:05','2021-01-18 07:25:05','Victorino N. Benedicto, Jr.'),
(86,'Added Project: Constructioin of Labonan Diversion Dam','2021-01-18 07:25:10','2021-01-18 07:25:10','Rolibeth G. Morillo '),
(87,'Added Project: Construction of Alimanguhan Concrete Canal Lining','2021-01-18 07:29:44','2021-01-18 07:29:44','Rolibeth G. Morillo '),
(88,'Added Project: Extension of Valderama Irrigation Canal','2021-01-18 07:33:08','2021-01-18 07:33:08','Rolibeth G. Morillo '),
(89,'Added Project: Rice-Based Product Innovation and Enterprise Development Center','2021-01-18 07:35:05','2021-01-18 07:35:05','Jelyn E. Doctor'),
(90,'Added Project: Construction and Installation of Dumarao SPIS','2021-01-18 07:36:32','2021-01-18 07:36:32','Rolibeth G. Morillo '),
(91,'Added Project: Construction and Installation of Duongan SPIS','2021-01-18 07:39:30','2021-01-18 07:39:30','Rolibeth G. Morillo '),
(92,'Added Project: Upgrading/Improvement of JCT Ibud-Victoria Provincial Road (Phase II)','2021-01-18 07:39:30','2021-01-18 07:39:30','Emelita G. Agpalo'),
(93,'Added Project: Supply and Erection/Installation of Roxas-Taytay 69kV T/L Project, Schedule 1','2021-01-18 07:40:40','2021-01-18 07:40:40','Marilou O. Oribiana'),
(94,'Added Project: Construction and Installation of Ransang SPIS','2021-01-18 07:42:02','2021-01-18 07:42:02','Rolibeth G. Morillo '),
(95,'Added Project: Completion of Medical Building (Phase II)','2021-01-18 07:42:54','2021-01-18 07:42:54','Jose G. Buenconsejo'),
(96,'Added Project: Upgrading/Improvement of Ligaya Provincial Road (Completion)','2021-01-18 07:43:24','2021-01-18 07:43:24','Emelita G. Agpalo'),
(97,'Added Project: Distribution of onion seeds','2021-01-18 07:44:08','2021-01-18 07:44:08','Bernadette T. Salgado'),
(98,'Added Project: CONSTRUCTION OF BY-PASS AND DIVERSION ROADS, BOAC-MOGPOG BYPASS ROAD MARINDUQUE','2021-01-18 07:44:31','2021-01-18 07:44:31','Engr. Anacorita R. Mercader'),
(99,'Edited Project: Balik Sigla sa Agrikultura , Cattle Dispersal Program','2021-01-18 07:45:07','2021-01-18 07:45:07','Bernadette T. Salgado'),
(100,'Added Project: Productivity Improvement  of Cacao Processing Enterprise','2021-01-18 07:45:11','2021-01-18 07:45:11','Jelyn E. Doctor'),
(101,'Edited Project: Balik Sigla sa Agrikultura , Cattle Dispersal Program','2021-01-18 07:45:56','2021-01-18 07:45:56','Bernadette T. Salgado'),
(102,'Edited Project: Balik Sigla sa Agrikultura - Broiler Dispersal Program','2021-01-18 07:46:29','2021-01-18 07:46:29','Bernadette T. Salgado'),
(103,'Edited Project: Balik Sigla sa Agrikultura - Broiler Dispersal Program','2021-01-18 07:47:30','2021-01-18 07:47:30','Bernadette T. Salgado'),
(104,'Added Project: Supply and Delivery of 11 units 35-44hp Four Wheel Drive Tractor with Implements','2021-01-18 07:47:33','2021-01-18 07:47:33','Rolibeth G. Morillo '),
(105,'Added Project: Completion of Medical Building (Phase III)','2021-01-18 07:47:53','2021-01-18 07:47:53','Jose G. Buenconsejo'),
(106,'Edited Project: Balik Sigla sa Agrikultura - Layer Chicken Module','2021-01-18 07:47:53','2021-01-18 07:47:53','Bernadette T. Salgado'),
(107,'Added Project: Repair/Improvement of Roadside Structure Along JCT Ibud-Tagumpay Provincial Road','2021-01-18 07:47:58','2021-01-18 07:47:58','Emelita G. Agpalo'),
(108,'Edited Project: Balik Sigla sa Agrikultura - Layer Chicken Module','2021-01-18 07:48:10','2021-01-18 07:48:10','Bernadette T. Salgado'),
(109,'Edited Project: Completion of Medical Building (Phase II)','2021-01-18 07:50:21','2021-01-18 07:50:21','Jose G. Buenconsejo'),
(110,'Added Project: Supply and Delivery of 12 units Hand Tractor (Riding Type)','2021-01-18 07:52:15','2021-01-18 07:52:15','Rolibeth G. Morillo '),
(111,'Added Project: Upgrading/Concreting of Red Hills Barangay Road','2021-01-18 07:57:25','2021-01-18 07:57:25','Emelita G. Agpalo'),
(112,'Added Project: Supply and Erection/Installation of Roxas-Taytay 69kV T/L Project, Schedule 2','2021-01-18 07:57:30','2021-01-18 07:57:30','Marilou O. Oribiana'),
(113,'Added Project: Upgrading/Improvement of JNR Malpalon Provincial Road (Phase-II)','2021-01-18 07:59:49','2021-01-18 07:59:49','Emelita G. Agpalo'),
(114,'Added Project: Distribution of tilapia fingerlings','2021-01-18 08:05:01','2021-01-18 08:05:01','Bernadette T. Salgado'),
(115,'Added Project: Construction of Concrete Bridge','2021-01-18 08:05:19','2021-01-18 08:05:19','Emelita G. Agpalo'),
(116,'Edited Project: Distribution of tilapia fingerlings','2021-01-18 08:07:16','2021-01-18 08:07:16','Bernadette T. Salgado'),
(117,'Edited Project: Distribution of tilapia fingerlings','2021-01-18 08:08:01','2021-01-18 08:08:01','Bernadette T. Salgado'),
(118,'Added Project: Repair/Improvement of San Jose District Hospital Building','2021-01-18 08:15:10','2021-01-18 08:15:10','Emelita G. Agpalo'),
(119,'Added Project: Construction of Multi-Purpose Hall (Phase II)','2021-01-18 08:20:39','2021-01-18 08:20:39','Emelita G. Agpalo'),
(120,'Added Project: Distribution of assorted fruit tree seedlings for rehabilitation of typhoon Quinta','2021-01-18 08:31:05','2021-01-18 08:31:05','Bernadette T. Salgado'),
(121,'Edited Project: NEDA Mimaropa Building','2021-01-18 09:02:24','2021-01-18 09:02:24','NEDASAMPLE'),
(122,'Added Project: Establishment of Mushroom Spawn Production Facility in Baco (CEST)','2021-01-18 09:11:23','2021-01-18 09:11:23','Jelyn E. Doctor'),
(123,'Added Project: CONST. OF MISSING LINKS/NEW ROADS, MINDORO ISLAND CIRCUM. ROAD (PTO. GALERA-ABRA DE ILOG), KO454+893-KO455+601.72; KO455+753.52-KO456+419.11), OCCIDENTAL MINDORO','2021-01-19 00:30:12','2021-01-19 00:30:12','Engr. Anacorita R. Mercader'),
(124,'Edited Project: Distribution of onion seeds','2021-01-19 00:46:44','2021-01-19 00:46:44','Bernadette T. Salgado'),
(125,'Added Project: Supply and Delivery of 12units Rice Thresher','2021-01-19 01:00:12','2021-01-19 01:00:12','Rolibeth G. Morillo '),
(126,'Added Project: Distribution of tilapia fingerlings','2021-01-19 01:04:32','2021-01-19 01:04:32','Bernadette T. Salgado'),
(127,'Added Project: Distribution of tilapia fingerlings','2021-01-19 01:04:38','2021-01-19 01:04:38','Bernadette T. Salgado'),
(128,'Added Project: CONSTRUCTION OF MISSING LINK / NEW ROADS VICTORIA - SABLAYAN (MINDORO CROSS ISLAND ROAD) PHASE VI STA.5+441 - STA.6+220, SABLAYAN, OCCIDENTAL MINDORO','2021-01-19 01:22:10','2021-01-19 01:22:10','Engr. Anacorita R. Mercader'),
(129,'Edited Project: Completion of Sta. Cruz RHU, Occidental Mindoro','2021-01-19 01:36:37','2021-01-19 01:36:37','Engr. Stephanie S. Sioco'),
(130,'Edited Project: Construction/Rehabilitation and Expansion of Rizal Rural Health Unit','2021-01-19 01:37:01','2021-01-19 01:37:01','Engr. Stephanie S. Sioco'),
(131,'Edited Project: improvement and expansion of existing water supply system of Sablayan WD','2021-01-19 01:44:29','2021-01-19 01:44:29','Punzalan'),
(132,'Edited Project: MONGPONG RIS','2021-01-19 01:45:51','2021-01-19 01:45:51','Lowell L. Lozano'),
(133,'Added Project: Lumintao RIS (Repair of NIS)','2021-01-19 01:48:33','2021-01-19 01:48:33','Lowell L. Lozano'),
(134,'Added Project: Completion of a 2 storey School of Information and Computing Sciences Building','2021-01-19 01:49:58','2021-01-19 01:49:58','Mr. Romell L. Morales'),
(135,'Added Project: 20E00005 - ROAD WIDENING OF CALAPAN SOUTH, KO032+124-KO033+053; KO043+592-KO043+846; KO043+880-KO043+1000; KO044+456-KO044+1156; KO045+462-KO047+986; KO061+121-KO061+860, ORIENTAL MINDORO','2021-01-19 01:50:01','2021-01-19 01:50:01','Engr. Anacorita R. Mercader'),
(136,'Added Project: Monteclaro CIS (Repair of CIS)','2021-01-19 01:55:30','2021-01-19 01:55:30','Lowell L. Lozano'),
(137,'Edited Project: CONSTRUCTION OF BY-PASS AND DIVERSION ROADS, BOAC-MOGPOG BYPASS ROAD MARINDUQUE','2021-01-19 01:58:09','2021-01-19 01:58:09','Engr. Anacorita R. Mercader'),
(138,'Edited Project: NEDA Mimaropa Building Edited','2021-01-19 02:00:24','2021-01-19 02:00:24','NEDASAMPLE'),
(139,'Added Project: Anahawin CIS (Repair of CIS)','2021-01-19 02:05:24','2021-01-19 02:05:24','Lowell L. Lozano'),
(140,'Edited Project: Construction of College of Business Management Academic Building','2021-01-19 02:05:57','2021-01-19 02:05:57','Engr. Ryan Jay V. Balcarcel'),
(141,'Edited Project: improvement and expansion of existing water supply system of Sablayan WD','2021-01-19 02:10:09','2021-01-19 02:10:09','Punzalan'),
(142,'Edited Project: CONST. OF MISSING LINKS/NEW ROADS, MINDORO ISLAND CIRCUM. ROAD (PTO. GALERA-ABRA DE ILOG), KO454+893-KO455+601.72; KO455+753.52-KO456+419.11), OCCIDENTAL MINDORO','2021-01-19 02:10:27','2021-01-19 02:10:27','Engr. Anacorita R. Mercader'),
(143,'Added Project: Construction of New BHS with Birthing Facility','2021-01-19 02:10:29','2021-01-19 02:10:29','Engr. Stephanie S. Sioco'),
(144,'Edited Project: 20E00003 - CONST. OF MISSING LINKS/NEW ROADS, MINDORO ISLAND CIRCUM. ROAD (PTO. GALERA-ABRA DE ILOG), KO454+893-KO455+601.72; KO455+753.52-KO456+419.11), OCCIDENTAL MINDORO','2021-01-19 02:13:03','2021-01-19 02:13:03','Engr. Anacorita R. Mercader'),
(145,'Edited Project: 20E00002 - CONSTRUCTION OF BY-PASS AND DIVERSION ROADS, BOAC-MOGPOG BYPASS ROAD MARINDUQUE','2021-01-19 02:13:57','2021-01-19 02:13:57','Engr. Anacorita R. Mercader'),
(146,'Edited Project: 20E00004 - CONSTRUCTION OF MISSING LINK / NEW ROADS VICTORIA - SABLAYAN (MINDORO CROSS ISLAND ROAD) PHASE VI STA.5+441 - STA.6+220, SABLAYAN, OCCIDENTAL MINDORO','2021-01-19 02:15:00','2021-01-19 02:15:00','Engr. Anacorita R. Mercader'),
(147,'Edited Project: 20E00005 - ROAD WIDENING OF CALAPAN SOUTH, KO032+124-KO033+053; KO043+592-KO043+846; KO043+880-KO043+1000; KO044+456-KO044+1156; KO045+462-KO047+986; KO061+121-KO061+860, ORIENTAL MINDORO','2021-01-19 02:15:49','2021-01-19 02:15:49','Engr. Anacorita R. Mercader'),
(148,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Palawan','2021-01-19 02:18:22','2021-01-19 02:18:22','Raiza Joyce A. Mendoza'),
(149,'Edited Project: 20E00005 - ROAD WIDENING OF CALAPAN SOUTH, KO032+124-KO033+053; KO043+592-KO043+846; KO043+880-KO043+1000; KO044+456-KO044+1156; KO045+462-KO047+986; KO061+121-KO061+860, ORIENTAL MINDORO','2021-01-19 02:19:31','2021-01-19 02:19:31','Engr. Anacorita R. Mercader'),
(150,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Palawan','2021-01-19 02:23:07','2021-01-19 02:23:07','Raiza Joyce A. Mendoza'),
(151,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Palawan','2021-01-19 02:25:21','2021-01-19 02:25:21','Raiza Joyce A. Mendoza'),
(152,'Edited Project: Construction of College of Business Management Academic Building','2021-01-19 02:26:26','2021-01-19 02:26:26','Engr. Ryan Jay V. Balcarcel'),
(153,'Added Project: Construction of SBM Matalaba Building','2021-01-19 02:28:36','2021-01-19 02:28:36','Mr. Romell L. Morales'),
(154,'Edited Project: Inspection and Monitoring of PNP Offices/Units/Stations','2021-01-19 02:29:49','2021-01-19 02:29:49','Lorlaine F. Dolendo'),
(155,'Edited Project: Inspection and Monitoring of PNP Offices/Units/Stations','2021-01-19 02:30:37','2021-01-19 02:30:37','Lorlaine F. Dolendo'),
(156,'Added Project: Amnay RIS (Restoration of NIS)','2021-01-19 02:30:49','2021-01-19 02:30:49','Lowell L. Lozano'),
(157,'Added Project: Construction of ICTSC Building Phase II','2021-01-19 02:36:50','2021-01-19 02:36:50','Mr. Romell L. Morales'),
(158,'Edited Project: Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Palawan','2021-01-19 02:40:16','2021-01-19 02:40:16','Raiza Joyce A. Mendoza'),
(159,'Added Project: Completion of MSC Multi-Purpose Gymnasium','2021-01-19 02:44:43','2021-01-19 02:44:43','Mr. Romell L. Morales'),
(160,'Added Project: Construction of School of Liberal Arts Building','2021-01-19 02:48:30','2021-01-19 02:48:30','Mr. Romell L. Morales'),
(161,'Added Project: Construction of  2-Storey ICTSC Building','2021-01-19 02:53:10','2021-01-19 02:53:10','Mr. Romell L. Morales'),
(162,'Added Project: Construction of Two-Storey Technology and Livelihood Education Building, Main Campus','2021-01-19 02:56:31','2021-01-19 02:56:31','Mr. Romell L. Morales'),
(163,'Added Project: Emergency Case for the Supply nd Delivery of 4,362 bag NSIC, 1,149 Bags and 1,069 bags NSIC Hybrid Palay Seeds','2021-01-19 02:57:03','2021-01-19 02:57:03','Rolibeth G. Morillo'),
(164,'Added Project: Repair of Two-Storey 12-Classroom Building (Diego Silang Building)','2021-01-19 03:00:41','2021-01-19 03:00:41','Frances Darlyn S. Pangilinan, Ce'),
(165,'Added Project: Completion of SBM Building','2021-01-19 03:01:54','2021-01-19 03:01:54','Mr. Romell L. Morales'),
(166,'Edited Project: Repair of Two-Storey 12-Classroom Building (Diego Silang Building)','2021-01-19 03:03:03','2021-01-19 03:03:03','Frances Darlyn S. Pangilinan, Ce'),
(167,'Added Project: Emergency Case for the Supply nd Delivery of 7,037 bags, 800 Bags and 2,260 bags SL-12 Hybrid Palay Seeds','2021-01-19 03:04:53','2021-01-19 03:04:53','Rolibeth G. Morillo'),
(168,'Added Project: Construction of SHS Laboratory School Building','2021-01-19 03:07:31','2021-01-19 03:07:31','Mr. Romell L. Morales'),
(169,'Edited Project: Completion of a 2 Storey School of Information and Computing Sciences Building','2021-01-19 03:08:16','2021-01-19 03:08:16','Mr. Romell L. Morales'),
(170,'Added Project: Emergency Case for the Supply nd Delivery of 2,210 bag, 4,353 Bags and 1,432 bags NSIC Hybrid Palay Seeds','2021-01-19 03:10:23','2021-01-19 03:10:23','Rolibeth G. Morillo'),
(171,'Edited Project: Repair of Two-Storey 12-Classroom Building (Diego Silang Building)','2021-01-19 03:10:46','2021-01-19 03:10:46','Frances Darlyn S. Pangilinan, Ce'),
(172,'Added Project: Distribution of OPV VAR 6 Corn Seeds','2021-01-19 03:50:09','2021-01-19 03:50:09','Bernadette T. Salgado'),
(173,'Submitted Form 1','2021-01-19 04:03:47','2021-01-19 04:03:47','Punzalan'),
(174,'Submitted Form 2','2021-01-19 04:04:06','2021-01-19 04:04:06','Punzalan'),
(175,'Submitted Form 3','2021-01-19 04:04:25','2021-01-19 04:04:25','Punzalan');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_100000_create_password_resets_table',1),
(2,'2019_01_15_100000_create_roles_table',1),
(3,'2019_01_15_110000_create_users_table',1),
(4,'2019_01_17_121504_create_categories_table',1),
(5,'2019_01_21_130422_create_tags_table',1),
(6,'2019_01_21_163402_create_items_table',1),
(7,'2019_01_21_163414_create_item_tag_table',1),
(8,'2019_03_06_132557_add_photo_column_to_users_table',1),
(9,'2019_03_06_143255_add_fields_to_items_table',1),
(10,'2019_03_20_090438_add_color_tags_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `password_resets` */

insert  into `password_resets`(`email`,`token`,`created_at`) values 
('bnlayon@neda.gov.ph','$2y$10$WUp4C7xUrQOe9M/IldHV3OhDkvjrJHAoiHbvIfgFu74qN52jaO3Cy','2020-11-09 06:00:27');

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `implementingagency` int(11) NOT NULL,
  `sector` int(11) NOT NULL,
  `fundingsource` int(11) NOT NULL,
  `mode` varchar(500) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `category` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `cost` decimal(50,2) NOT NULL,
  `odatype` varchar(10) DEFAULT NULL,
  `others` varchar(200) DEFAULT NULL,
  `gaayear` date DEFAULT NULL,
  `lfptype` varchar(50) DEFAULT NULL,
  `otherslfp` varchar(100) DEFAULT NULL,
  `rrp` varchar(100) DEFAULT NULL,
  `catothers` varchar(100) DEFAULT NULL,
  `contractor` varchar(250) DEFAULT NULL,
  `modeothers` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `projects` */

insert  into `projects`(`id`,`title`,`implementingagency`,`sector`,`fundingsource`,`mode`,`start`,`end`,`created_at`,`updated_at`,`category`,`status`,`cost`,`odatype`,`others`,`gaayear`,`lfptype`,`otherslfp`,`rrp`,`catothers`,`contractor`,`modeothers`) values 
(1,'NEDA SAMPLE PROJECT',276,2,2,'By admin','2021-01-01','2021-01-31','2021-01-18','2021-01-18',7,'Ongoing',1200003.90,'Grant',NULL,'2021-01-18','GAA',NULL,NULL,NULL,NULL,NULL),
(2,'NEDA Mimaropa Building Edited',276,2,2,'By admin','2021-01-01','2021-12-31','2021-01-18','2021-01-19',1,'Ongoing',12000000.00,'Grant',NULL,NULL,'Corporate',NULL,NULL,NULL,NULL,NULL),
(3,'MONGPONG RIS',27,2,2,'By contract','2020-01-01','2020-03-12','2021-01-18','2021-01-19',7,'Completed',7500000.00,'Grant',NULL,'2020-01-01','GAA',NULL,NULL,NULL,'KEJAMARENIK CONSTRUCTION & CONST. SUPPLIES',NULL),
(4,'Concreting of Access Road CGC Complex (COA Bldg to Motorpool)',52,2,2,'By contract','2020-11-03','2020-12-27','2021-01-18','2021-01-18',7,'Completed',1689910.00,NULL,NULL,NULL,'Others','General Fund',NULL,NULL,'876 Construction & Supply',NULL),
(5,'Rehabilitation of Multi-Purpose Building - RSU Main Campus',45,3,2,'By contract','2019-09-20','2020-06-20','2021-01-18','2021-01-18',7,'Completed',24995000.00,'Grant',NULL,'2019-01-01','GAA',NULL,NULL,NULL,'Orientech Construction and Development Corporation',NULL),
(6,'Construction of MSC Gymnasium',41,2,2,'By contract','2017-03-16','2020-08-24','2021-01-18','2021-01-18',7,'Completed',10000000.00,NULL,NULL,'2016-01-01','GAA',NULL,NULL,NULL,'EXAATTO BUILDERS INC.',NULL),
(7,'Inspection and Monitoring of PNP Offices/Units/Stations',29,5,2,'By admin','2020-02-03','2020-09-30','2021-01-18','2021-01-19',7,'Ongoing',540000.00,'Grant',NULL,NULL,'GAA',NULL,NULL,NULL,NULL,NULL),
(8,'Construction of Multi-Purpose Hall (Phase II)',48,2,2,'By contract','2020-07-31','2021-09-18','2021-01-18','2021-01-18',7,'Completed',3500000.00,'Grant',NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'Capitol Builders & Construction Supplies',NULL),
(9,'Enhancing the Post Harvest Facility through Infrared Grain Dryer Technology of Kooperatiba ng Pamayanang Kristiyano ng Mapaya (KPKM)',16,1,1,'By contract','2020-08-25','2023-08-25','2021-01-18','2021-01-18',6,'Ongoing',1658730.40,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Kooperatiba ng Pamayanang Kristiyano ng Mapaya (KPKM)',NULL),
(12,'Establishment of Small Water Impounding System Oriental Mindoro 54 cu. m.',11,2,4,'By contract','2020-01-01','2020-12-31','2021-01-18','2021-01-18',7,'Ongoing',3206000.00,NULL,'GAA',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(13,'Establishment of Small Water Impounding System Oriental Mindoro 50 cu. m.',11,1,4,'By contract','2020-01-01','2020-12-31','2021-01-18','2021-01-18',7,'Ongoing',3000000.00,NULL,'GAA',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(14,'Establishment of Small Water Impounding System Oriental Mindoro 50 cu. m. Bulalacao',11,1,4,'By contract','2020-01-01','2020-12-31','2021-01-18','2021-01-18',7,'Completed',3662500.00,NULL,'GAA',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(15,'Construction/Installation of Solar Street Lights for the Provincial Government of Occidental Mindoro',48,2,4,'By contract','2020-07-31','2021-03-17','2021-01-18','2021-01-18',7,'Completed',24975000.00,'Grant','LGSF',NULL,'Others','LGSF',NULL,NULL,'Equator Energy Corporation',NULL),
(19,'Balik Sigla sa Agrikultura , Cattle Dispersal Program',48,1,2,'By admin','2020-12-01','2021-02-28','2021-01-18','2021-01-18',3,'Ongoing',6968680.00,'Grant',NULL,NULL,'20% Development Fund',NULL,NULL,NULL,NULL,NULL),
(20,'Concreting of Road with slope protection (phase II)',52,2,2,'By contract','2020-11-03','2021-01-01','2021-01-18','2021-01-18',7,'Ongoing',3899774.88,NULL,NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'B5 BUilders and supplies',NULL),
(21,'Construction of College of Business Management Academic Building',46,2,2,'By contract','2019-10-17','2020-04-14','2021-01-18','2021-01-19',7,'Completed',10.00,'Grant',NULL,'2019-04-29','GAA',NULL,NULL,NULL,'Kayano Trading and Construction',NULL),
(22,'Erpmes Sample Project',50,2,1,'By admin','2021-01-18','2021-12-18','2021-01-18','2021-01-18',6,'Ongoing',5000000.00,'Loan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(23,'Malico CIS (Extension)',27,2,2,'By contract','2020-05-06','2020-10-03','2021-01-18','2021-01-18',7,'Completed',5028000.00,NULL,NULL,'2020-01-01','GAA',NULL,NULL,NULL,'Yurich Builders & Construction Supply',NULL),
(24,'Balik Sigla sa Agrikultura - Broiler Dispersal Program',48,1,2,'By admin','2020-12-01','2021-02-28','2021-01-18','2021-01-18',3,'Ongoing',3940700.00,'Grant',NULL,NULL,'20% Development Fund',NULL,NULL,NULL,NULL,NULL),
(25,'Rehabilitation of Multi-Purpose Building - San Fernando Campus',45,3,2,'By contract','2019-12-27','2019-06-07','2021-01-18','2021-01-18',7,'Ongoing',13702000.00,'Grant',NULL,'2019-01-01','GAA',NULL,NULL,NULL,'JoJean Construction and Development Corporation',NULL),
(26,'Construction of President\'s Housing',41,2,2,'By admin','2018-06-06','2019-02-08','2021-01-18','2021-01-18',7,'Ongoing',6000000.00,'Grant',NULL,'2018-01-01','GAA',NULL,NULL,NULL,NULL,NULL),
(27,'Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Marinduque',13,2,2,'By contract','2020-02-26','2020-07-31','2021-01-18','2021-01-18',8,'Ongoing',4300000.00,'Grant',NULL,NULL,'Fund 164',NULL,NULL,'Public Places','BC Net, Inc.',NULL),
(28,'Balik Sigla sa Agrikultura - Layer Chicken Module',48,1,2,'By admin','2020-12-01','2021-02-28','2021-01-18','2021-01-18',3,'Ongoing',6718600.00,'Grant',NULL,NULL,'20% Development Fund',NULL,NULL,NULL,NULL,NULL),
(29,'Special Maintenance Along Mamburao-Paluan Provincial Road (A)',48,2,2,'By contract','2020-09-28','2020-10-27','2021-01-18','2021-01-18',7,'Completed',2000000.00,NULL,NULL,NULL,'Others','PEO-MOOE',NULL,NULL,'Asianstar Construction and Supply Corporation',NULL),
(30,'improvement and expansion of existing water supply system of Sablayan WD',22,2,4,'By admin','2020-10-12','2022-10-02','2021-01-18','2021-01-19',7,'Ongoing',25.00,'Grant','GAA-2017',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(31,'Construction of Centro Diversion Dam',8,2,1,'By contract','2020-04-22','2020-10-19','2021-01-18','2021-01-18',7,'Completed',4472168.32,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(32,'Construction of Sabang Diversion Dam',8,2,1,'By contract','2020-04-22','2020-10-19','2021-01-18','2021-01-18',7,'Completed',5368869.26,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(33,'Rehabilitation of CENRO San Jose Office Building',11,2,4,'By contract','2020-01-01','2020-12-31','2021-01-18','2021-01-18',7,'Ongoing',500000.00,NULL,'GAA',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(34,'Construction of Dapi Elementary School Multi-Purpose Hall',48,2,2,'By contract','2020-12-11','2021-05-04','2021-01-18','2021-01-18',7,'Ongoing',3500000.00,NULL,NULL,NULL,'Others','SEF',NULL,NULL,'Johnsev Enterprises',NULL),
(35,'Construction of School Dormitory for the College of Agriculture, Fishery and Forestry',45,2,2,'By contract','2020-02-12','2021-07-26','2021-01-18','2021-01-18',7,'Ongoing',17582500.00,'Grant',NULL,'2020-01-01','GAA',NULL,NULL,NULL,NULL,NULL),
(36,'Rehabilitation of Malakibay Diversion Dam',8,2,1,'By contract','2020-05-01','2020-10-28','2021-01-18','2021-01-18',3,'Completed',4897262.41,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'JB and  Son Construction',NULL),
(37,'Completion of Sta. Cruz RHU, Occidental Mindoro',12,2,2,'By contract','2020-06-17','2020-10-14','2021-01-18','2021-01-19',8,'Completed',2821441.80,'Grant',NULL,'2019-01-01','GAA',NULL,NULL,'Completion of Facility','AMM Construction',NULL),
(38,'Rehabilitation of the College of Arts and Sciences Building',45,3,2,'By contract','2020-12-29','2021-07-26','2021-01-18','2021-01-18',7,'Ongoing',12812500.00,NULL,NULL,'2020-01-01','GAA',NULL,NULL,NULL,NULL,NULL),
(39,'Construction of Crisis Center for Women and Children (Phase-I)',48,2,2,'By contract','2020-12-11','2021-05-14','2021-01-18','2021-01-18',7,'Ongoing',3500000.00,NULL,NULL,NULL,'Others','GAD-1011',NULL,NULL,'P-Square Contracting & Services',NULL),
(40,'Rehabilitation of Rosacara Diversion Dam',8,2,1,'By contract','2020-04-22','2020-07-21','2021-01-18','2021-01-18',7,'Completed',1342134.67,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(41,'Distribution of mungbean seeds for crop diversification',48,1,2,'By admin','2020-10-01','2020-12-31','2021-01-18','2021-01-18',3,'Completed',50000.00,NULL,NULL,NULL,'Others','OPA General Fund','Quinta',NULL,NULL,NULL),
(42,'Construction/Rehabilitation and Expansion of Rizal Rural Health Unit',12,2,2,'By contract','2020-06-17','2021-02-11','2021-01-18','2021-01-19',8,'Ongoing',10628928.00,'Grant',NULL,'2018-01-01','GAA',NULL,NULL,'Construction/Rehab/Expansion','AMM Construction',NULL),
(43,'Extension of Paetan Irrigation Canal',8,2,1,'By contract','2020-04-22','2020-11-13','2021-01-18','2021-01-18',7,'Completed',5479892.75,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(44,'Construction of Learning Resource Center and Museum',41,2,2,'By contract','2018-11-20','2020-06-05','2021-01-18','2021-01-18',7,'Completed',26123000.00,'Grant',NULL,'2018-01-01','GAA',NULL,NULL,NULL,'Performance Builders and Development Corporation',NULL),
(45,'Construction of Concrete Bridge',48,2,2,'By contract','2019-12-19','2020-07-08','2021-01-18','2021-01-18',7,'Completed',25000000.00,NULL,NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'Asianstar Construction and Supply Corporation',NULL),
(46,'Concreting of San Vicente Irrigation Canal',8,2,1,'By contract','2020-04-22','2020-11-13','2021-01-18','2021-01-18',7,'Completed',7119895.52,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(47,'Construction of Multi-Purpose Hall',48,2,2,'By contract','2020-02-21','2020-09-08','2021-01-18','2021-01-18',7,'Ongoing',3000000.00,NULL,NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'Capitol Builders & Construction Supplies',NULL),
(48,'Construction od Masaquisi Irrigation Canal',8,2,1,'By contract','2020-04-22','2020-07-21','2021-01-18','2021-01-18',7,'Completed',2884213.43,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(49,'Enhancing the S&T Ecosystem in Public Senior High School Through Versatile Instrumentation System for Science Education and Research (VISSER) Technology',16,4,1,'By contract','2020-02-18','2023-02-18','2021-01-18','2021-01-18',6,'Ongoing',1195730.40,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Department of Education Occidental Mindoro Division Office',NULL),
(50,'NEDA sample - Jojo',276,3,2,'By contract','2020-07-06','2021-02-04','2021-01-18','2021-01-18',7,'Ongoing',6519654.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Jojo Enterprises',NULL),
(51,'UPGRADING/IMPROVEMENT/REHABILITATION OF ROMBLON-CAJIMOS-SABANG ROAD',51,2,1,'By contract','2018-03-05','2020-09-23','2021-01-18','2021-01-18',6,'Completed',70556120.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'CS RAYOS CONSTRUCTION',NULL),
(52,'REHAB OF MINOLO (PUERTO-GALERA)-MAMBURAO 69KV TL PROJECT',30,2,2,'By contract','2019-08-23','2020-12-31','2021-01-18','2021-01-18',7,'Ongoing',318254504.95,NULL,'SA',NULL,'GAA',NULL,NULL,NULL,'HANSEI CORPORATION',NULL),
(53,'Construction of Multi-Purpose Hall',48,2,2,'By contract','2020-02-21','2020-06-07','2021-01-18','2021-01-18',7,'Completed',3200000.00,NULL,NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'Juan Antonio Construction',NULL),
(54,'DOLE Integrated Livelihood Program',14,1,4,'Others','2020-01-01','2020-12-31','2021-01-18','2021-01-18',7,'Completed',17898000.00,'Grant','GAA',NULL,NULL,NULL,NULL,'Special Fund',NULL,'Accredited Co-partner and Direct Administration'),
(55,'Repair of Roofing of Multi-Purpose Hall and Construction of Stage',48,2,2,'By contract','2020-02-21','2020-05-21','2021-01-18','2021-01-18',7,'Ongoing',2500000.00,NULL,NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'Juan Antonio Construction',NULL),
(56,'Constructioin of Labonan Diversion Dam',8,2,1,'By contract','2020-04-22','2021-01-07','2021-01-18','2021-01-18',7,'Ongoing',3579197.70,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'MP Cabrera Construction',NULL),
(57,'Construction of Alimanguhan Concrete Canal Lining',8,2,1,'By contract','2020-05-04','2020-11-13','2021-01-18','2021-01-18',7,'Completed',4985058.48,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'West Blue Builders',NULL),
(58,'Improving the Spider Lily-Based Handicraft Production of Marabong Community Resources Management Association',16,1,1,'By contract','2020-02-18','2023-02-18','2021-01-18','2021-01-18',6,'Ongoing',1102281.60,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'LGU-Sablayan',NULL),
(59,'Extension of Valderama Irrigation Canal',8,2,1,'By contract','2020-05-28','2020-11-21','2021-01-18','2021-01-18',7,'Completed',4992669.50,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Jas Trading an Construction',NULL),
(60,'Rice-Based Product Innovation and Enterprise Development Center',16,1,1,'By contract','2020-08-27','2023-08-27','2021-01-18','2021-01-18',6,'Ongoing',2040000.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Provincial Government of Oriental Mindoro',NULL),
(61,'Construction and Installation of Dumarao SPIS',8,2,1,'By contract','2020-04-08','2021-03-02','2021-01-18','2021-01-18',7,'Ongoing',6992869.88,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'JB and  Son Construction',NULL),
(62,'Construction and Installation of Duongan SPIS',8,2,1,'By contract','2020-10-04','2021-02-21','2021-01-18','2021-01-18',7,'Ongoing',6997750.81,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'HG Agri Industrial Macheneries',NULL),
(63,'Upgrading/Improvement of JCT Ibud-Victoria Provincial Road (Phase II)',48,2,4,'By contract','2020-09-21','2021-01-10','2021-01-18','2021-01-18',7,'Ongoing',24753700.00,NULL,'CMGP',NULL,NULL,NULL,NULL,NULL,'Capitol Builders & Construction Supplies',NULL),
(64,'Establishment of Tilapia Intensive Aquaculture Facility in Mangangan I, Baco (CEST Project)',16,1,1,'By contract','2020-01-07','2021-07-01','2021-01-18','2021-01-18',6,'Ongoing',1200000.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Baco Association of Backyard Hograiser',NULL),
(65,'Supply and Erection/Installation of Roxas-Taytay 69kV T/L Project, Schedule 1',30,2,2,'By contract','2019-10-09','2020-10-02','2021-01-18','2021-01-18',7,'Ongoing',330071689.70,NULL,NULL,'2019-06-15','GAA',NULL,NULL,NULL,'S. L. Development Construction Corporation',NULL),
(66,'Construction and Installation of Ransang SPIS',8,2,1,'By contract','2020-06-04','2021-01-30','2021-01-18','2021-01-18',7,'Ongoing',6989978.81,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'JB and  Son Construction',NULL),
(67,'Completion of Medical Building (Phase II)',44,3,2,'By contract','2019-06-27','2020-08-21','2021-01-18','2021-01-18',7,'Ongoing',50000000.00,'Grant','GAA 2019','2019-01-01','GAA',NULL,NULL,NULL,'JB & Sons Trading & Construction',NULL),
(68,'Upgrading/Improvement of Ligaya Provincial Road (Completion)',48,2,4,'By contract','2020-11-04','2021-02-13','2021-01-18','2021-01-18',7,'Ongoing',18620500.00,NULL,'CMGP',NULL,NULL,NULL,NULL,NULL,'STX Enterprises',NULL),
(69,'Distribution of onion seeds',48,1,2,'By admin','2020-12-01','2020-12-31','2021-01-18','2021-01-19',3,'Completed',500000.00,'Grant',NULL,NULL,'Others','OPA General Fund','Quinta',NULL,NULL,NULL),
(70,'20E00002 - CONSTRUCTION OF BY-PASS AND DIVERSION ROADS, BOAC-MOGPOG BYPASS ROAD MARINDUQUE',15,2,2,'By contract','2020-02-24','2020-12-19','2021-01-18','2021-01-19',7,'Ongoing',102735393.71,'Grant','GAA','2020-10-01','GAA',NULL,NULL,NULL,'PERSAN CONSTRUCTION, INC.',NULL),
(71,'Productivity Improvement  of Cacao Processing Enterprise',16,1,1,'By contract','2020-02-19','2021-04-01','2021-01-18','2021-01-18',6,'Ongoing',1062495.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'Samahan ng Magtatanim ng Kakaw/Kape ng Isla ng Mindoro (SAMAKAKIM)',NULL),
(72,'Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Romblon',13,2,2,'By contract','2020-02-26','2020-07-31','2021-01-18','2021-01-18',8,'Ongoing',7400000.00,NULL,NULL,NULL,'Fund 164',NULL,NULL,'Public Places','BC Net, Inc.',NULL),
(73,'Supply and Delivery of 11 units 35-44hp Four Wheel Drive Tractor with Implements',8,5,1,'Others','2020-07-07','2020-08-08','2021-01-18','2021-01-18',7,'Completed',3285000.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'NCB Fort Tractor Philippines'),
(74,'Completion of Medical Building (Phase III)',44,3,2,'By contract','2019-12-26','2020-12-21','2021-01-18','2021-01-18',7,'Ongoing',50000000.00,NULL,NULL,'2019-01-01','GAA',NULL,NULL,NULL,'JB & Sons Trading & Construction',NULL),
(75,'Repair/Improvement of Roadside Structure Along JCT Ibud-Tagumpay Provincial Road',48,2,4,'By contract','2020-11-04','2021-02-13','2021-01-18','2021-01-18',7,'Ongoing',2500000.00,NULL,'CMGP-2020',NULL,NULL,NULL,NULL,NULL,'Bernardo Builders',NULL),
(76,'Supply and Delivery of 12 units Hand Tractor (Riding Type)',8,5,1,'Others','2020-05-27','2020-06-30','2021-01-18','2021-01-18',7,'Completed',178800.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'NCB Fort Tractor Philippines and 3RMetal Craft'),
(77,'Upgrading/Concreting of Red Hills Barangay Road',48,2,4,'By contract','2020-09-15','2020-12-26','2021-01-18','2021-01-18',7,'Ongoing',14000000.00,NULL,'CMGP-2020',NULL,NULL,NULL,NULL,NULL,'M. C. Martinez Construction',NULL),
(78,'Supply and Erection/Installation of Roxas-Taytay 69kV T/L Project, Schedule 2',30,2,2,'By contract','2019-08-15','2020-08-08','2021-01-18','2021-01-18',7,'Ongoing',321789033.50,NULL,NULL,'2019-06-15','GAA',NULL,NULL,NULL,'D. M. Consunji, Inc.',NULL),
(79,'Upgrading/Improvement of JNR Malpalon Provincial Road (Phase-II)',48,2,4,'By contract','2020-09-21','2021-01-12','2021-01-18','2021-01-18',7,'Ongoing',24961300.00,NULL,'CMGP-2020',NULL,NULL,NULL,NULL,NULL,'F. A. Torres Construction',NULL),
(80,'Distribution of tilapia fingerlings',48,1,4,'By admin','2020-11-01','2020-12-31','2021-01-18','2021-01-18',3,'Completed',349999.00,'Grant','OPA General Fund',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(81,'Construction of Concrete Bridge',48,2,2,'By contract','2019-12-19','2020-05-28','2021-01-18','2021-01-18',7,'Ongoing',20000000.00,NULL,NULL,NULL,'20% Development Fund',NULL,NULL,NULL,'F. A. Torres Construction',NULL),
(82,'Upgrading/Improvement of JNR Sto. Niño-Pitogo-Aguas-Limlim Provincial Road (Phase-III)',48,2,4,'By contract','2020-09-21','2021-01-12','2021-01-18','2021-01-18',7,'Ongoing',16710200.00,NULL,'CMGP-2020',NULL,NULL,NULL,NULL,NULL,'Bernardo Builders',NULL),
(83,'Distribution of 12,000 certified palay seeds from DA-RFO I for rehab for typhoon Quinta',48,1,4,'By admin','2020-12-01','2021-05-31','2021-01-18','2021-01-18',3,'Ongoing',18240000.00,'Grant','DA-RFO',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(84,'Distribution of 12,000 certified palay seeds from DA-RFO I for rehab for typhoon Quinta',48,1,4,'By admin','2020-12-01','2021-05-31','2021-01-18','2021-01-18',3,'Ongoing',18240000.00,'Grant','DA-RFO',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(85,'Repair/Improvement of San Jose District Hospital Building',48,2,4,'By contract','2019-04-02','2020-12-11','2021-01-18','2021-01-18',7,'Ongoing',20000000.00,NULL,'Tobacco Excise Tax',NULL,NULL,NULL,NULL,NULL,'EJD Swertres Tinsmith & Builders',NULL),
(86,'Distribution of 12,000 certified palay seeds from DA-RFO I for rehab for typhoon Quinta',48,1,4,'By admin','2020-12-01','2021-05-31','2021-01-18','2021-01-18',3,'Ongoing',1.00,NULL,'DA-RFO',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(87,'Distribution of 12,000 certified palay seeds from DA-RFO I for rehab for typhoon Quinta',48,1,4,'By admin','2020-12-01','2021-05-31','2021-01-18','2021-01-18',3,'Ongoing',18240000.00,NULL,'DA-RFO',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(88,'Rehab and Transfer of Existing 10MVA Power Transformer at BOAC Substation Including Supply, Delivery, Construction/Installation, Test and Commissioning of NEw 10MVA Mogpog Substation',30,2,2,'By contract','2019-10-30','2020-08-24','2021-01-18','2021-01-18',7,'Ongoing',188468282.53,NULL,NULL,'2019-06-15','GAA',NULL,NULL,NULL,'Power Dimension, Inc.',NULL),
(89,'Construction of Multi-Purpose Hall (Phase II)',48,2,4,'By contract','2020-09-21','2021-01-12','2021-01-18','2021-01-18',7,'Completed',1120000.00,NULL,'CMGP-2020',NULL,NULL,NULL,NULL,NULL,'AMM Const. & Const. Supplies',NULL),
(90,'Upgrading/Concreting of Barangay Poblacion Farm to Market Road (So. Olima - So. Genaro)',48,2,4,'By contract','2020-05-22','2021-06-05','2021-01-18','2021-01-18',7,'Ongoing',97988000.00,NULL,'PRDP',NULL,NULL,NULL,NULL,NULL,'F. A. Torres Construction',NULL),
(91,'Distribution of assorted fruit tree seedlings for rehabilitation of typhoon Quinta',48,1,4,'By admin','2020-11-01','2021-06-30','2021-01-18','2021-01-18',3,'Ongoing',752600.00,NULL,'DA RFO MIMAROPA',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(92,'Distribution of 15,000 bags hybrid seeds from DA RFO 1 for rehabilitation of TY Quinta',48,1,4,'By admin','2020-12-01','2021-05-31','2021-01-18','2021-01-18',3,'Ongoing',67500000.00,NULL,'DA RFO MIMAROPA',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(93,'Free Wi-Fi for All | Free Public Internet Access Program - Managed Internet Service in Palawan',13,2,2,'By contract','2020-02-26','2020-07-31','2021-01-18','2021-01-18',8,'Ongoing',19710000.00,'Grant',NULL,NULL,'Fund 164',NULL,NULL,'Public Places','BC Net, Inc.',NULL),
(94,'Establishment of Mushroom Spawn Production Facility in Baco (CEST)',16,1,1,'By contract','2020-04-29','2021-07-01','2021-01-18','2021-01-18',6,'Ongoing',890000.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,'LGU-Baco',NULL),
(95,'20E00003 - CONST. OF MISSING LINKS/NEW ROADS, MINDORO ISLAND CIRCUM. ROAD (PTO. GALERA-ABRA DE ILOG), KO454+893-KO455+601.72; KO455+753.52-KO456+419.11), OCCIDENTAL MINDORO',15,2,2,'By contract','2020-02-24','2021-02-12','2021-01-19','2021-01-19',7,'Ongoing',113482042.76,'Grant','GAA','2020-10-01','GAA',NULL,NULL,NULL,'MARCBILT CONSTRUCTION, INC.',NULL),
(96,'Supply and Delivery of 12units Rice Thresher',8,5,1,'Others','2020-06-06','2020-07-03','2021-01-19','2021-01-19',7,'Completed',1794000.00,'Grant',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'NCB Fort Tractor Philippines and All Certified Equipment Trading Corporation'),
(97,'Distribution of tilapia fingerlings',48,1,4,'By admin','2020-11-01','2020-11-30','2021-01-19','2021-01-19',3,'Completed',258000.00,NULL,'BFAR',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(98,'Distribution of tilapia fingerlings',48,1,4,'By admin','2020-11-01','2020-11-30','2021-01-19','2021-01-19',3,'Completed',258000.00,NULL,'BFAR',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL),
(99,'20E00004 - CONSTRUCTION OF MISSING LINK / NEW ROADS VICTORIA - SABLAYAN (MINDORO CROSS ISLAND ROAD) PHASE VI STA.5+441 - STA.6+220, SABLAYAN, OCCIDENTAL MINDORO',15,2,2,'By contract','2020-02-24','2021-02-14','2021-01-19','2021-01-19',7,'Ongoing',118150831.76,'Grant','GAA','2020-10-01','GAA',NULL,NULL,NULL,'STX ENTERPRISES',NULL),
(100,'Lumintao RIS (Repair of NIS)',27,2,2,'By contract','2020-01-01','2020-12-31','2021-01-19','2021-01-19',7,'Completed',2000000.00,NULL,NULL,'2020-01-01','GAA',NULL,NULL,NULL,'F.A. TORRES CONSTRUCTION',NULL),
(101,'Completion of a 2 Storey School of Information and Computing Sciences Building',41,2,2,'By contract','2018-01-03','2020-06-04','2021-01-19','2021-01-19',7,'Completed',10000000.00,'Grant',NULL,'2017-01-01','GAA',NULL,NULL,NULL,'TRI-LINK BUILDERS & MARKETING, INC.',NULL),
(102,'20E00005 - ROAD WIDENING OF CALAPAN SOUTH, KO032+124-KO033+053; KO043+592-KO043+846; KO043+880-KO043+1000; KO044+456-KO044+1156; KO045+462-KO047+986; KO061+121-KO061+860, ORIENTAL MINDORO',15,2,2,'By contract','2020-06-11','2021-03-25','2021-01-19','2021-01-19',7,'Ongoing',104005130.15,'Grant','GAA','2020-10-01','GAA',NULL,NULL,NULL,'STX ENTERPRISES',NULL),
(103,'Monteclaro CIS (Repair of CIS)',27,2,2,'By contract','2020-01-01','2020-12-31','2021-01-19','2021-01-19',7,'Completed',2406000.00,NULL,NULL,'2020-01-01','GAA',NULL,NULL,NULL,'N. SALES CONSTRUCTION',NULL),
(104,'Anahawin CIS (Repair of CIS)',27,2,2,'By contract','2020-01-01','2020-12-31','2021-01-19','2021-01-19',7,'Ongoing',10000000.00,NULL,NULL,'2020-12-31','GAA',NULL,NULL,NULL,'F.A. TORRES CONSTRUCTION',NULL),
(105,'Construction of New BHS with Birthing Facility',12,2,2,'By contract','2020-03-05','2020-10-30','2021-01-19','2021-01-19',8,'Ongoing',2470416.40,NULL,NULL,'2018-01-01','GAA',NULL,NULL,'New Construction','FEAJ Construction and General Services',NULL),
(106,'Construction of SBM Matalaba Building',41,2,2,'By contract','2017-08-20','2020-09-30','2021-01-19','2021-01-19',7,'Completed',10000000.00,NULL,NULL,'2016-01-01','GAA',NULL,NULL,NULL,'EXAATTO BUILDERS INC.',NULL),
(107,'Amnay RIS (Restoration of NIS)',27,2,2,'By contract','2020-12-05','2020-08-28','2021-01-19','2021-01-19',7,'Completed',1375000.00,NULL,NULL,'2020-12-31','GAA',NULL,NULL,NULL,'KEJAMARENIK CONSTRUCTION & CONST. SUPPLIES',NULL),
(108,'Construction of ICTSC Building Phase II',41,2,2,'By admin','2017-02-16','2017-12-31','2021-01-19','2021-01-19',7,'Ongoing',1500000.00,NULL,NULL,'2016-01-01','GAA',NULL,NULL,NULL,NULL,NULL),
(109,'Completion of MSC Multi-Purpose Gymnasium',41,2,2,'By contract','2018-11-26','2020-05-20','2021-01-19','2021-01-19',7,'Completed',40000000.00,NULL,NULL,'2017-01-01','GAA',NULL,NULL,NULL,'PERFORMANCE BUILDERS AND DEVELOPMENT CORPORATION',NULL),
(110,'Construction of School of Liberal Arts Building',41,2,2,'By admin','2019-04-10','2020-02-20','2021-01-19','2021-01-19',7,'Completed',10000000.00,NULL,NULL,NULL,'Fund 164',NULL,NULL,NULL,NULL,NULL),
(111,'Construction of  2-Storey ICTSC Building',41,2,2,'By contract','2020-02-10','2020-09-05','2021-01-19','2021-01-19',7,'Completed',7669764.21,NULL,NULL,NULL,'Fund 164',NULL,NULL,NULL,'P.R.N CONSTRUCTION',NULL),
(112,'Construction of Two-Storey Technology and Livelihood Education Building, Main Campus',41,2,2,'By contract','2020-02-20','2020-12-31','2021-01-19','2021-01-19',7,'Ongoing',13444545.73,NULL,NULL,'2020-01-01','GAA',NULL,NULL,NULL,'PERFORMANCE BUILDERS AND DEVELOPMENT CORPORATION',NULL),
(113,'Emergency Case for the Supply nd Delivery of 4,362 bag NSIC, 1,149 Bags and 1,069 bags NSIC Hybrid Palay Seeds',8,1,1,'Others','2020-05-26','2020-10-01','2021-01-19','2021-01-19',3,'Completed',32850000.00,'Grant',NULL,NULL,NULL,NULL,'COVID 19',NULL,NULL,'Seedworks Philippines Inc.'),
(114,'Repair of Two-Storey 12-Classroom Building (Diego Silang Building)',43,2,2,'By contract','2020-07-09','2021-01-29','2021-01-19','2021-01-19',3,'Completed',3538193.40,'Grant','GAA',NULL,'Fund 164',NULL,'Ursula',NULL,'AMM Construction and Construction Supplies',NULL),
(115,'Completion of SBM Building',41,2,2,'By contract','2020-06-01','2021-04-25','2021-01-19','2021-01-19',7,'Ongoing',15998281.02,NULL,NULL,'2019-01-01','GAA',NULL,NULL,NULL,'R.R. ENCABO CONTRUCTORS, INC.',NULL),
(116,'Emergency Case for the Supply nd Delivery of 7,037 bags, 800 Bags and 2,260 bags SL-12 Hybrid Palay Seeds',8,1,1,'Others','2020-05-26','2020-06-26','2021-01-19','2021-01-19',3,'Completed',50485000.00,'Grant',NULL,NULL,NULL,NULL,'COVID 19',NULL,NULL,'SL Agritech Corporation'),
(117,'Construction of SHS Laboratory School Building',41,2,2,'By contract','2020-12-01','2021-12-31','2021-01-19','2021-01-19',7,'Ongoing',12000000.00,NULL,NULL,NULL,'Fund 164',NULL,NULL,NULL,'STO. CRISTO CONSTRUCTION AND TRAIDING INC.',NULL),
(118,'Emergency Case for the Supply nd Delivery of 2,210 bag, 4,353 Bags and 1,432 bags NSIC Hybrid Palay Seeds',8,1,1,'Others','2020-05-26','2020-06-26','2021-01-19','2021-01-19',3,'Completed',39975000.00,'Grant',NULL,NULL,NULL,NULL,'COVID 19',NULL,NULL,'St. Joseph AgriMarketing Corporation'),
(119,'Distribution of OPV VAR 6 Corn Seeds',48,1,4,'By admin','2020-12-01','2020-12-31','2021-01-19','2021-01-19',3,'Ongoing',360000.00,NULL,'BPI',NULL,NULL,NULL,'Quinta',NULL,NULL,NULL);

/*Table structure for table `provinces` */

DROP TABLE IF EXISTS `provinces`;

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `provinces` */

insert  into `provinces`(`id`,`province`) values 
(1,'Occidental Mindoro'),
(2,'Oriental Mindoro'),
(3,'Marinduque'),
(4,'Romblon'),
(5,'Palawan');

/*Table structure for table `provincesprojects` */

DROP TABLE IF EXISTS `provincesprojects`;

CREATE TABLE `provincesprojects` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `province_id` int(1) DEFAULT NULL,
  `proj_id` int(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=utf8mb4;

/*Data for the table `provincesprojects` */

insert  into `provincesprojects`(`id`,`province_id`,`proj_id`,`created_at`,`updated_at`) values 
(1,1,100,'2021-01-12 01:47:37','2021-01-12 01:47:37'),
(2,3,100,'2021-01-12 01:47:37','2021-01-12 01:47:37'),
(5,2,1,'2021-01-18 01:00:39','2021-01-18 01:00:39'),
(6,2,2,'2021-01-18 01:57:07','2021-01-18 01:57:07'),
(7,1,3,'2021-01-18 02:05:30','2021-01-18 02:05:30'),
(8,2,4,'2021-01-18 04:56:47','2021-01-18 04:56:47'),
(9,4,5,'2021-01-18 05:00:27','2021-01-18 05:00:27'),
(10,3,6,'2021-01-18 05:12:26','2021-01-18 05:12:26'),
(11,1,8,'2021-01-18 05:56:32','2021-01-18 05:56:32'),
(12,1,9,'2021-01-18 05:57:48','2021-01-18 05:57:48'),
(13,1,10,'2021-01-18 05:58:22','2021-01-18 05:58:22'),
(14,1,11,'2021-01-18 06:03:28','2021-01-18 06:03:28'),
(15,2,12,'2021-01-18 06:05:57','2021-01-18 06:05:57'),
(16,2,13,'2021-01-18 06:11:28','2021-01-18 06:11:28'),
(17,2,14,'2021-01-18 06:17:07','2021-01-18 06:17:07'),
(18,1,15,'2021-01-18 06:18:13','2021-01-18 06:18:13'),
(19,1,16,'2021-01-18 06:18:33','2021-01-18 06:18:33'),
(20,1,17,'2021-01-18 06:18:56','2021-01-18 06:18:56'),
(21,1,18,'2021-01-18 06:20:20','2021-01-18 06:20:20'),
(22,1,19,'2021-01-18 06:25:09','2021-01-18 06:25:09'),
(23,2,20,'2021-01-18 06:25:38','2021-01-18 06:25:38'),
(24,5,21,'2021-01-18 06:29:40','2021-01-18 06:29:40'),
(25,5,22,'2021-01-18 06:31:18','2021-01-18 06:31:18'),
(26,5,23,'2021-01-18 06:33:07','2021-01-18 06:33:07'),
(27,1,24,'2021-01-18 06:35:39','2021-01-18 06:35:39'),
(28,4,25,'2021-01-18 06:36:31','2021-01-18 06:36:31'),
(29,3,26,'2021-01-18 06:36:46','2021-01-18 06:36:46'),
(30,3,27,'2021-01-18 06:39:06','2021-01-18 06:39:06'),
(33,1,28,'2021-01-18 06:39:31','2021-01-18 06:39:31'),
(34,1,29,'2021-01-18 06:45:58','2021-01-18 06:45:58'),
(35,1,30,'2021-01-18 06:47:12','2021-01-18 06:47:12'),
(36,2,31,'2021-01-18 06:49:11','2021-01-18 06:49:11'),
(37,2,32,'2021-01-18 06:53:19','2021-01-18 06:53:19'),
(38,1,33,'2021-01-18 06:53:19','2021-01-18 06:53:19'),
(39,1,34,'2021-01-18 06:54:28','2021-01-18 06:54:28'),
(40,4,35,'2021-01-18 06:57:57','2021-01-18 06:57:57'),
(41,5,36,'2021-01-18 06:58:49','2021-01-18 06:58:49'),
(42,1,37,'2021-01-18 06:59:22','2021-01-18 06:59:22'),
(43,4,38,'2021-01-18 07:01:21','2021-01-18 07:01:21'),
(44,1,39,'2021-01-18 07:01:26','2021-01-18 07:01:26'),
(45,2,40,'2021-01-18 07:03:22','2021-01-18 07:03:22'),
(46,1,41,'2021-01-18 07:07:18','2021-01-18 07:07:18'),
(47,1,42,'2021-01-18 07:07:45','2021-01-18 07:07:45'),
(48,1,43,'2021-01-18 07:07:51','2021-01-18 07:07:51'),
(49,3,44,'2021-01-18 07:09:23','2021-01-18 07:09:23'),
(50,1,45,'2021-01-18 07:10:11','2021-01-18 07:10:11'),
(51,1,46,'2021-01-18 07:12:45','2021-01-18 07:12:45'),
(52,1,47,'2021-01-18 07:14:55','2021-01-18 07:14:55'),
(53,2,48,'2021-01-18 07:16:02','2021-01-18 07:16:02'),
(54,1,49,'2021-01-18 07:18:57','2021-01-18 07:18:57'),
(55,2,50,'2021-01-18 07:19:17','2021-01-18 07:19:17'),
(56,4,51,'2021-01-18 07:19:30','2021-01-18 07:19:30'),
(57,1,52,'2021-01-18 07:19:56','2021-01-18 07:19:56'),
(58,2,52,'2021-01-18 07:19:56','2021-01-18 07:19:56'),
(59,1,53,'2021-01-18 07:20:47','2021-01-18 07:20:47'),
(60,1,54,'2021-01-18 07:21:02','2021-01-18 07:21:02'),
(61,2,54,'2021-01-18 07:21:02','2021-01-18 07:21:02'),
(62,3,54,'2021-01-18 07:21:02','2021-01-18 07:21:02'),
(63,4,54,'2021-01-18 07:21:02','2021-01-18 07:21:02'),
(64,5,54,'2021-01-18 07:21:02','2021-01-18 07:21:02'),
(65,1,55,'2021-01-18 07:24:38','2021-01-18 07:24:38'),
(66,2,56,'2021-01-18 07:25:10','2021-01-18 07:25:10'),
(67,5,57,'2021-01-18 07:29:44','2021-01-18 07:29:44'),
(68,1,58,'2021-01-18 07:30:53','2021-01-18 07:30:53'),
(69,5,59,'2021-01-18 07:33:08','2021-01-18 07:33:08'),
(70,2,60,'2021-01-18 07:35:05','2021-01-18 07:35:05'),
(71,5,61,'2021-01-18 07:36:32','2021-01-18 07:36:32'),
(72,2,62,'2021-01-18 07:39:30','2021-01-18 07:39:30'),
(73,1,63,'2021-01-18 07:39:30','2021-01-18 07:39:30'),
(74,2,64,'2021-01-18 07:39:35','2021-01-18 07:39:35'),
(75,5,65,'2021-01-18 07:40:40','2021-01-18 07:40:40'),
(76,5,66,'2021-01-18 07:42:02','2021-01-18 07:42:02'),
(77,5,67,'2021-01-18 07:42:54','2021-01-18 07:42:54'),
(78,1,68,'2021-01-18 07:43:24','2021-01-18 07:43:24'),
(79,1,69,'2021-01-18 07:44:08','2021-01-18 07:44:08'),
(80,3,70,'2021-01-18 07:44:31','2021-01-18 07:44:31'),
(81,2,71,'2021-01-18 07:45:11','2021-01-18 07:45:11'),
(82,4,72,'2021-01-18 07:46:39','2021-01-18 07:46:39'),
(83,1,73,'2021-01-18 07:47:33','2021-01-18 07:47:33'),
(84,2,73,'2021-01-18 07:47:33','2021-01-18 07:47:33'),
(85,5,73,'2021-01-18 07:47:33','2021-01-18 07:47:33'),
(86,5,74,'2021-01-18 07:47:53','2021-01-18 07:47:53'),
(87,1,75,'2021-01-18 07:47:58','2021-01-18 07:47:58'),
(88,3,76,'2021-01-18 07:52:15','2021-01-18 07:52:15'),
(89,4,76,'2021-01-18 07:52:15','2021-01-18 07:52:15'),
(90,1,77,'2021-01-18 07:57:25','2021-01-18 07:57:25'),
(91,5,78,'2021-01-18 07:57:30','2021-01-18 07:57:30'),
(92,1,79,'2021-01-18 07:59:49','2021-01-18 07:59:49'),
(93,1,80,'2021-01-18 08:05:01','2021-01-18 08:05:01'),
(94,1,81,'2021-01-18 08:05:19','2021-01-18 08:05:19'),
(95,1,82,'2021-01-18 08:09:59','2021-01-18 08:09:59'),
(96,1,83,'2021-01-18 08:12:20','2021-01-18 08:12:20'),
(97,1,84,'2021-01-18 08:13:01','2021-01-18 08:13:01'),
(98,1,85,'2021-01-18 08:15:10','2021-01-18 08:15:10'),
(99,1,86,'2021-01-18 08:15:54','2021-01-18 08:15:54'),
(100,1,87,'2021-01-18 08:18:37','2021-01-18 08:18:37'),
(101,3,88,'2021-01-18 08:20:12','2021-01-18 08:20:12'),
(102,1,89,'2021-01-18 08:20:39','2021-01-18 08:20:39'),
(103,1,90,'2021-01-18 08:29:01','2021-01-18 08:29:01'),
(104,1,91,'2021-01-18 08:31:05','2021-01-18 08:31:05'),
(105,1,92,'2021-01-18 08:34:24','2021-01-18 08:34:24'),
(106,5,93,'2021-01-18 08:36:31','2021-01-18 08:36:31'),
(107,2,94,'2021-01-18 09:11:23','2021-01-18 09:11:23'),
(108,1,95,'2021-01-19 00:30:12','2021-01-19 00:30:12'),
(109,3,96,'2021-01-19 01:00:12','2021-01-19 01:00:12'),
(110,4,96,'2021-01-19 01:00:12','2021-01-19 01:00:12'),
(111,1,97,'2021-01-19 01:04:32','2021-01-19 01:04:32'),
(112,1,98,'2021-01-19 01:04:38','2021-01-19 01:04:38'),
(113,1,99,'2021-01-19 01:22:10','2021-01-19 01:22:10'),
(114,1,100,'2021-01-19 01:48:33','2021-01-19 01:48:33'),
(115,3,101,'2021-01-19 01:49:58','2021-01-19 01:49:58'),
(116,2,102,'2021-01-19 01:50:01','2021-01-19 01:50:01'),
(117,1,103,'2021-01-19 01:55:30','2021-01-19 01:55:30'),
(118,3,2,'2021-01-19 02:00:24','2021-01-19 02:00:24'),
(119,4,2,'2021-01-19 02:00:24','2021-01-19 02:00:24'),
(120,1,104,'2021-01-19 02:05:24','2021-01-19 02:05:24'),
(121,3,105,'2021-01-19 02:10:29','2021-01-19 02:10:29'),
(122,3,106,'2021-01-19 02:28:36','2021-01-19 02:28:36'),
(123,1,107,'2021-01-19 02:30:49','2021-01-19 02:30:49'),
(124,3,108,'2021-01-19 02:36:50','2021-01-19 02:36:50'),
(125,3,109,'2021-01-19 02:44:43','2021-01-19 02:44:43'),
(126,3,110,'2021-01-19 02:48:30','2021-01-19 02:48:30'),
(127,3,111,'2021-01-19 02:53:10','2021-01-19 02:53:10'),
(128,3,112,'2021-01-19 02:56:31','2021-01-19 02:56:31'),
(129,1,113,'2021-01-19 02:57:03','2021-01-19 02:57:03'),
(130,2,113,'2021-01-19 02:57:03','2021-01-19 02:57:03'),
(131,3,113,'2021-01-19 02:57:03','2021-01-19 02:57:03'),
(132,4,113,'2021-01-19 02:57:03','2021-01-19 02:57:03'),
(133,5,113,'2021-01-19 02:57:03','2021-01-19 02:57:03'),
(134,1,114,'2021-01-19 03:00:41','2021-01-19 03:00:41'),
(135,4,115,'2021-01-19 03:01:54','2021-01-19 03:01:54'),
(136,1,116,'2021-01-19 03:04:53','2021-01-19 03:04:53'),
(137,2,116,'2021-01-19 03:04:53','2021-01-19 03:04:53'),
(138,3,116,'2021-01-19 03:04:53','2021-01-19 03:04:53'),
(139,4,116,'2021-01-19 03:04:53','2021-01-19 03:04:53'),
(140,5,116,'2021-01-19 03:04:53','2021-01-19 03:04:53'),
(141,3,117,'2021-01-19 03:07:31','2021-01-19 03:07:31'),
(142,1,118,'2021-01-19 03:10:23','2021-01-19 03:10:23'),
(143,2,118,'2021-01-19 03:10:23','2021-01-19 03:10:23'),
(144,5,118,'2021-01-19 03:10:23','2021-01-19 03:10:23'),
(145,1,119,'2021-01-19 03:50:09','2021-01-19 03:50:09');

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` varchar(100) DEFAULT NULL,
  `reporttype` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `reports` */

insert  into `reports`(`id`,`report`,`reporttype`) values 
(1,'Masterlist of Projects',1),
(2,'Masterlist of Agency Projects',2),
(3,'Sample Chart',1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `roles_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'Admin','This is the administration role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Agency','This is the implementing agency role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Member','This is the member role','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `sectors` */

DROP TABLE IF EXISTS `sectors`;

CREATE TABLE `sectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sectors` */

insert  into `sectors`(`id`,`sector`) values 
(1,'Economic'),
(2,'Infrastructure'),
(3,'Social'),
(4,'Government Institutional Development'),
(5,'Others');

/*Table structure for table `sources` */

DROP TABLE IF EXISTS `sources`;

CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sources` */

insert  into `sources`(`id`,`type`) values 
(1,'Official Development Assistance'),
(2,'Local Financing'),
(3,'Public-Private Partnership'),
(4,'Others');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `tags_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`color`,`created_at`,`updated_at`) values 
(1,'Hot','#f44336','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Trending','#9c27b0','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'New','#00bcd4','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `agency_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  KEY `users_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=384 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`picture`,`role_id`,`agency_id`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Administrator','bnlayon@neda.gov.ph','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i','profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg',1,0,'zXoulvIZd1rL3h62EAKFcfPT8WqvAWWTiABmspouITg8IfDpwWD9TLvaDCJj','2020-07-24 06:35:23','2020-11-03 05:24:35'),
(276,'Roy A. Dimayuga','radimayuga@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(277,'Amalia S. Tuppil','astuppil@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(278,'Francis Bayani M. de Guzman','fmdeguzman@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/TEWaxWKNw3digbXGGpTbzaD8XFNiV2jBZh2FkzSF.jpeg',1,0,NULL,'0000-00-00 00:00:00','2021-01-15 08:09:02'),
(279,'Loreto H. Castillo','lhcastillo@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(280,'Jojo R. Datinguinoo','jrdatinguinoo@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00',NULL),
(281,'Keizher Marasigan','ktmarasigan@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',1,0,NULL,'0000-00-00 00:00:00','2021-01-14 06:19:58'),
(282,'Joel Paule','jmpaule@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:11:04'),
(283,'Kenneth Joy Arteza','ktarteza@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:14:21'),
(284,'Ruther John Col-long','rbcol-long@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:12:12'),
(285,'Jhunjun Fajutagana','jffajutagana@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:12:58'),
(286,'Nepo Jerome Benter','ngbenter@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:15:06'),
(288,'Chara Lois T. Eje','cteje@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:10:40'),
(289,'Beltisezara Garcia','bagarcia@neda.gov.ph',NULL,'$2y$10$rj6c1ahOI2hIeHzucj60LuyPq64JBumQqnCZs9Pmcphz0//YyyUzW','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-18 03:38:56'),
(290,'RD Susan A. Sumbeling','sasumbeling@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:15:55'),
(291,'ARD Bernardino A. Atienza, Jr.','baatienza@neda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',3,0,NULL,'0000-00-00 00:00:00','2021-01-14 07:16:27'),
(292,'Jelyn B. Gabuco','CAAPAREA4Gabuco',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,4,NULL,'0000-00-00 00:00:00',NULL),
(293,'Elsa V. Morales','CPDOCALAPANCITYMorales',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,52,NULL,'0000-00-00 00:00:00',NULL),
(294,'Merlie Jaye R. Dignadice','CPDO PUERTOPRINCESACITYDignadice',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,53,'WCPQyjxt7IMlGAYGPvrnEfkZGBCkamuAiuuXk8b4iLlJ36u6Qru1y1BpiAqD','0000-00-00 00:00:00',NULL),
(295,'Melvin T. Tirao','DARTirao',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,7,NULL,'0000-00-00 00:00:00',NULL),
(296,'Jonas Paolo M. Saludo','DENRSaludo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,11,NULL,'0000-00-00 00:00:00',NULL),
(297,'Laurente A. Samala','larry.samala@deped.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,9,'tthxNuUaxq8EwjGw4a8zfa76agtYNEavhBm7ekq01rcCSRizc0B2VD3q9Vdi','0000-00-00 00:00:00','2021-01-19 01:13:02'),
(298,'Rusella G. Muñoz','DICTMuñoz',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,13,NULL,'0000-00-00 00:00:00','2021-01-15 03:56:22'),
(299,'Lorenzo F. Suarez','DILGSuarez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,18,NULL,'0000-00-00 00:00:00',NULL),
(301,'Jelyn E. Doctor','DOSTDoctor',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(302,'Gladys A. Quesea','DOTQuesea',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,19,NULL,'0000-00-00 00:00:00',NULL),
(303,'Dennis M. Albano','DOTrAlbano',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,21,NULL,'0000-00-00 00:00:00',NULL),
(304,'Engr. Marilou M. Lawas','DPWHLawas',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,15,NULL,'0000-00-00 00:00:00',NULL),
(305,'Antonio C. Cirilo','DSWDCirilo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(306,'Mary Ann M. Hernandez','MinSCATHernandez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,42,NULL,'0000-00-00 00:00:00',NULL),
(307,'Engr. Robert N. Lamonte','MSCLamonte',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,41,NULL,'0000-00-00 00:00:00',NULL),
(308,'Mary Joy A. Sta. Ana','NAPOLCOMAna',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,29,NULL,'0000-00-00 00:00:00',NULL),
(309,'James B. Panado','panadojames.nea@gmail.com',NULL,'$2y$10$8..adnWAw8rEgyMuHGdPFeGnsAR.YvzfBisz1iU6AJ2uT5.d8kYtq','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,25,'3OYCcxVtOm9MPjpcIwaNQ4OIlJTiqP66GtkW9hbJHb71izjV0cn34OEbddj1','0000-00-00 00:00:00','2021-01-19 01:13:35'),
(310,'Lowell L. Lozano','NIALozano',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,27,NULL,'0000-00-00 00:00:00',NULL),
(311,'Maria Camille Louise C. Chen','NNCChen',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,28,NULL,'0000-00-00 00:00:00',NULL),
(312,'Roger M. Lived','NPCLived',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,30,'cW03nMlGf86vIYj0A11HgkVtCoVRA3mrCcuaZ1UXfNTEHXPAtSyreMH4WR27','0000-00-00 00:00:00',NULL),
(313,'Emelita G. Agpalo','PPDOOccidentalMindoroAgpalo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,48,NULL,'0000-00-00 00:00:00',NULL),
(314,'Edmin L. Distajo','PPDOOrientalMindoroDistajo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,49,'ntCgAatOiDYK4tV2uny5kbkm9v6uS85vg8ElY1se4AYPx0EinXoTjoywOwd0','0000-00-00 00:00:00',NULL),
(315,'Sherry Rose M. Peralta','PPDOPalawanPeralta',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,50,NULL,'0000-00-00 00:00:00',NULL),
(316,'Amabel S Liao','PSULiao',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,44,NULL,'0000-00-00 00:00:00',NULL),
(317,'Dr. Reynaldo P. Ramos','rsu.rpramos2020@gmail.com',NULL,'$2y$10$mpw9o4hWXrpalRS5q1zsaOTLAu2WBWhflRdZYd0NrHJDcVnYb5PEO','profile/21n548UW4oMc1BYEFD5WYgJaz2BZ1PwQjMYyr8AA.png',2,45,'IUYPfAfwaGzJzM4zvho2HYfsyUnyuABfCt6jRZRnmCLfbyhStkAejbpsG06O','0000-00-00 00:00:00','2021-01-19 01:12:25'),
(318,'TESDA MIMAROPA','region4b.rod@tesda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/GqimDogxPvzQKwkX7sDBcEdIOSpk62FgdwTDPaCt.png',2,39,'wRsFbgB0wRto7CfUdaHTYPDHUwALLRqhKcqd1tCP2y4yusAY0KmrdM7JVjnj','0000-00-00 00:00:00','2021-01-18 05:36:07'),
(319,'Ms. Gene Michelle S. Paduga','wpubudget@gmail.com',NULL,'$2y$10$EVGiun.CWEKS0O6J.wSBRO5vcyjoSBq5tTpT8PYwaxVW8p/uW/1Sm','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,46,'Xxyzd1GGT2LxCsge4FNz25MlJxYMHQmoA4klqCUTp8QTNvJTL6BDRSlTJoQj','0000-00-00 00:00:00','2021-01-19 01:19:29'),
(320,'Jean E. Tirante','DATirante',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,8,NULL,'0000-00-00 00:00:00',NULL),
(321,'Gloren R. Hinlo','POPCOMHinlo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,278,NULL,'0000-00-00 00:00:00',NULL),
(322,'Ssg Roy A Malaque (CAV) PA','2IDPA',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,2,NULL,'0000-00-00 00:00:00',NULL),
(323,'Dwane Darcy D. Cayonte','DOECayonte',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,10,NULL,'0000-00-00 00:00:00',NULL),
(324,'Engr. Benidicto B. Allago','DOHAllago',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,12,NULL,'0000-00-00 00:00:00',NULL),
(325,'Carnilo Orevillo','LWUAOrevillo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,22,NULL,'0000-00-00 00:00:00',NULL),
(326,'Joemer M. Samong','NCIPSamong',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,24,NULL,'0000-00-00 00:00:00',NULL),
(327,'Allinah Janzen O Magnaye','OCDMagnaye',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,31,NULL,'0000-00-00 00:00:00',NULL),
(328,'Wenceslao M. Paguia, Jr., Phd','OMSCPhd',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,43,NULL,'0000-00-00 00:00:00',NULL),
(329,'Jovito B. Bumatay','PFIDABumatay',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,34,NULL,'0000-00-00 00:00:00',NULL),
(330,'Melanie Ronquillo','PIAMIMAROPARonquillo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,35,NULL,'0000-00-00 00:00:00',NULL),
(331,'Margarito P. Dimaiilig','PPAPMOMindoroDimaiilig',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,279,NULL,'0000-00-00 00:00:00',NULL),
(332,'Engr. Relly W. Madarcos','PPAPMOPalawanMadarcos',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,280,NULL,'0000-00-00 00:00:00',NULL),
(333,'Emerson V. Caisip','PSACaisip',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,38,NULL,'0000-00-00 00:00:00',NULL),
(334,'Engr. Christine Mendoza','PPDOMarinduqueMendoza',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,47,'1sEg0TzeNyQ8hG2Nd38sxdZk5CFUErKPxl7eB8oCfr1uRBR4LUgPxS1mQNwN','0000-00-00 00:00:00',NULL),
(335,'Kathlaine May B. Esteban','CAAPAREA4Esteban',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,4,NULL,'0000-00-00 00:00:00',NULL),
(336,'Aristeo Joseph C. Genil','anino09@ymail.com',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/GgguEms5mhAAlapl86TndDBwZ0FTiKDK0t8JjH5E.png',2,52,NULL,'0000-00-00 00:00:00','2021-01-19 02:06:07'),
(337,'Michael Rio L. Daquer','michaelrio.daquer@yahoo.com',NULL,'$2y$10$qbxpxVG8nEaqphan5sHaQu12OuYbzYSZw8qlKFvhvAVnWONPbNt1e','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,53,NULL,'0000-00-00 00:00:00','2021-01-19 01:13:09'),
(338,'Melissa D. Punsalan','DARPunsalan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,7,NULL,'0000-00-00 00:00:00',NULL),
(339,'John Philip M. Merced','DENRMerced',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,11,NULL,'0000-00-00 00:00:00',NULL),
(340,'Merleen B. Abante','DepEdAbante',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,9,NULL,'0000-00-00 00:00:00',NULL),
(341,'Raiza Joyce A. Mendoza','raizajoyce.mendoza@dict.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,13,NULL,'0000-00-00 00:00:00','2021-01-19 01:09:45'),
(342,'Michael Casto A. Ras','DILGRas',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,18,'oIWKQXvTdd4FiWBIyMEFa271DOR3vwbfLxOGS7uBqlqZwBfIjHpnD63iFmbk','0000-00-00 00:00:00',NULL),
(343,'Kimberly D. Olicia','DOLEOlicia',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,14,NULL,'0000-00-00 00:00:00',NULL),
(344,'Zenchin Geri G. Pormento','DOSTPormento',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(345,'Faye Angeli A. Reyes','DOTReyes',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,19,'v5iw3b0gHBDe9C9Ql6jsrmbb6kOoxiL2upcp8M71Fclb5HIgKqCNEFDh82Qg','0000-00-00 00:00:00',NULL),
(346,'Luxmi F. De Leon','DOTrLeon',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,21,NULL,'0000-00-00 00:00:00',NULL),
(347,'Engr. Anacorita R. Mercader','DPWHMercader',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,15,'q2FA84lyKXcEKK6FV2T8ND4cSbxsMWPpx5U995z0Bh9YK5tQjaCCNEtWvRHd','0000-00-00 00:00:00',NULL),
(348,'Jhun Mark G. Villaroza','DSWDVillaroza',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,'0000-00-00 00:00:00',NULL),
(349,'Engr. Jennie T. Fernando','MinSCATFernando',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,42,NULL,'0000-00-00 00:00:00',NULL),
(350,'Mr. Romell L. Morales','MSCMorales',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,41,'dl5xOvaFoffH1VWfwFZNj8quI0KDWnnrqbS6jB7wwKnJGRUumCgD7ho9dAUH','0000-00-00 00:00:00',NULL),
(351,'Lorlaine F. Dolendo','len.dolendo@yahoo.com',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/mnyANhDS8uiHGpQm51bmj0EzZxoHLVpMmTe8s97o.jpeg',2,29,NULL,'0000-00-00 00:00:00','2021-01-18 02:56:06'),
(352,'Rodolfo D. Evangelista','NEAEvangelista',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,25,NULL,'0000-00-00 00:00:00',NULL),
(353,'Maria Victoria O. Malenab','NIAMalenab',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,27,NULL,'0000-00-00 00:00:00',NULL),
(354,'Bianca Louise Veronica M. Estrella','NNCEstrella',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,28,NULL,'0000-00-00 00:00:00',NULL),
(355,'Marilou O. Oribiana','NPCOribiana',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,30,'AdDamwn5bawKwiTfTy50X2nWtoxFOISUumaKxtdOXWjGgyebr0ba89dRiQIh','0000-00-00 00:00:00',NULL),
(356,'Bernadette T. Salgado','ppdo_occmin@yahoo.com',NULL,'$2y$10$kpFFvvYhV4Gpdqo8ZI2G8ehSjzM0yAlcrVAkt4JHQghtEXfhk8KL2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,48,'QQis8yiTxKys3c0gmZulKc8fNqraFutSJFDUFMh1DDWMUrMLrfACNYgFs6IP','0000-00-00 00:00:00','2021-01-19 01:11:58'),
(357,'Maria Margarita V. Lopez','PPDOOrientalMindoroLopez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,49,NULL,'0000-00-00 00:00:00',NULL),
(358,'Mr. Mathew Micah Lamitar','ppdopalawan@gmail.com',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/9nD3tDau2LXHnBQsTM5DHvLroLd0uzH58mDfN4VP.jpeg',2,50,'Den4HnZFltU3SLBpx7oWLWnofjn2Dp7xurS62FOlgYm8V0f84rxWHGhpFBUC','0000-00-00 00:00:00','2021-01-18 06:26:11'),
(359,'Jose G. Buenconsejo','jbuenconsejo@psu.palawan.edu.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/UewRFWKEwlLzzBnNM7WDNkx91Gi2irfXpDFjbvln.png',2,44,NULL,'0000-00-00 00:00:00','2021-01-18 08:00:02'),
(360,'Engr. Jason F. Rufon','RSURufon',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,45,NULL,'0000-00-00 00:00:00',NULL),
(361,'Eljun Francel A. Angeles','efaangeles@tesda.gov.ph',NULL,'$2y$10$6FZxPgtLkBOmOdKmmWw.6OLZDBapFA7nBqY7I25mTHIev2HYtwU1S','profile/tVjbEe0iV3oMd1VJH0ArU8btWwhFb9vyzXtdqp8L.jpeg',2,39,'SfWpIPgvxuhwB1aXhLVFHyFt4pcC0EI3XCRzQmSUyrqI7VtshhmS9cnbiKyO','0000-00-00 00:00:00','2021-01-18 06:01:53'),
(362,'Engr. Ryan Jay V. Balcarcel','mailtoryanjay@gmail.com',NULL,'$2y$10$ywgCrsEbFYZPKEuL111JquTmD8aW4s7LMTwK6uzOqyXkH71qbpvSy','profile/E3fwjqzQSJoZNnPEOLMviLmTHA5VUqcX0nzSuPZB.jpeg',2,46,'U2D72UwqhMQJqOuYiCOFcrtgUz7OZG0HXsIKWbHxgEC8LQi3sAfBCzvERa0u','0000-00-00 00:00:00','2021-01-19 01:11:36'),
(363,'Rolibeth G. Morillo','rolibethmorillo@yahoo.com.ph',NULL,'$2y$10$CGHqz7bRsQoyZuetbW0Fhesx0Tsq2yGd49KHPES5YmnwKcuECSa0S','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,8,'PCc6uNhNRLLPRzc60ke0eDyVOQmnOmOcdxtsnxTE3GDvU2wWbQrKLpWcdkIg','0000-00-00 00:00:00','2021-01-19 01:12:02'),
(364,'Sharmaine C. Ricardo','POPCOMRicardo',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,278,NULL,'0000-00-00 00:00:00',NULL),
(366,'Roland Victor A. Aguilar','DOEAguilar',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,10,'pBZnhENHH4tOMFe38KzQuR0djlN5LcdUrgvo8xumEdP8Za1gMXEKzG9BsxAc','0000-00-00 00:00:00',NULL),
(367,'Engr. Stephanie S. Sioco','DOHSioco',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,12,'Dm5WypXuXKH5DKheIN3i6BrzRQD3Rpfp1yOQFjnu7f6IT2Iw1zA8FatH2hNg','0000-00-00 00:00:00',NULL),
(368,'Punzalan','LWUAPunzalan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,22,NULL,'0000-00-00 00:00:00',NULL),
(369,'Mary Ann L. Delos Santos','NCIPSantos',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,24,NULL,'0000-00-00 00:00:00',NULL),
(370,'Jefrie G Rodriguez','OCDRodriguez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,31,NULL,'0000-00-00 00:00:00',NULL),
(371,'Frances Darlyn S. Pangilinan, Ce','OMSCCe',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,43,NULL,'0000-00-00 00:00:00',NULL),
(372,'Zellica Mae C. Bautista','PFIDABautista',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,34,NULL,'0000-00-00 00:00:00',NULL),
(373,'Madonna Punzalan','PIAMIMAROPAPunzalan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,35,NULL,'0000-00-00 00:00:00',NULL),
(374,'Ronald O. Matibag','PPAPMOMindoroMatibag',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,279,NULL,'0000-00-00 00:00:00',NULL),
(375,'Juna M. Sanchez','PPAPMOPalawanSanchez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,280,'ky06OE8PQC31p9BJeXgSWDYUBihn9n5YvQZjoAhyLxRchhALDhGCUxOkGClK','0000-00-00 00:00:00',NULL),
(376,'Rachel L. Vasquez','PSAVasquez',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,38,NULL,'0000-00-00 00:00:00',NULL),
(377,'Anna Christina Ibahan','PPDOMarinduqueIbahan',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,47,'6QiwB8uhAxslRkeO5BO4YJhYj5zSViLutsgqmQnWp7pZDBVq4t4pszH1iRij','0000-00-00 00:00:00',NULL),
(379,'Sara J. Marasigan','sjmarasigan@neda.gov.ph',NULL,'$2y$10$8QeuvZMQB3oZyF6rbatzautC5bamaZZy3PaANp0WHkeZx7Cg6cJH.',NULL,3,276,NULL,'2021-01-14 08:58:41','2021-01-14 08:58:41'),
(380,'NEDASAMPLE','neda@neda.gov.ph',NULL,'$2y$10$PE8pHr00G7fxdOOjA7HOo.ZpypPXdRRi/UJP68oBH/bp2KaskQ7I2','profile/q5oxHYnK6PAnUVMkgIv7l88s4mqpgKuaUNcNJeFa.jpeg',2,276,NULL,'2021-01-14 09:11:23','2021-01-18 00:37:29'),
(381,'Kim Neko C. Bana','DOLEBana',NULL,'$2y$10$FcVwoUY15Kh0HPIdmb49EOgjtyEfFPCDBiUTsjqEwFVv/plZyooXO',NULL,2,14,'C5Mb3Bp0gnCrYrTgk6Wmvogk4WIdzk40gko3iQbqHVd0usyMZh0LAffM3ZGL','2021-01-14 09:18:50','2021-01-15 03:58:55'),
(382,'Gemma Etis','PPDORomblonEtis',NULL,'$2y$10$.2AD5yvsQjibz7bNZv.zH.xrSy6e/i0fS4j3tJPog1PKfuhTSDvFy',NULL,2,49,NULL,'2021-01-15 03:49:47','2021-01-15 03:49:47'),
(383,'Victorino N. Benedicto, Jr.','PPDORomblonBenedicto',NULL,'$2y$10$5RbcXfnQYOmwQeu796gR6eIBx9kqmysRkRUaRQaP1xWLPZDtbshhm',NULL,2,51,NULL,'2021-01-15 03:54:53','2021-01-15 03:54:53');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
