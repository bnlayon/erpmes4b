/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.4.17-MariaDB : Database - erpmes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erpmes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `erpmes`;

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` varchar(100) DEFAULT NULL,
  `reporttype` int(1) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

/*Data for the table `reports` */

insert  into `reports`(`id`,`report`,`reporttype`,`active`) values (1,'Masterlist of Projects',1,0),(2,'Masterlist of Agency Projects',2,1),(3,'Masterlist of Agency Projects By Category',2,1),(4,'Masterlist of Agency Projects By Spatial',2,1),(5,'Masterlist of Agency Projects By Sector',2,1),(6,'Masterlist of Projects By Category',1,0),(7,'Masterlist of Projects By Spatial',1,0),(8,'Masterlist of Projects By Sector',1,0),(9,'Masterlist of Projects By Agency',1,0),(10,'Masterlist of Projects in Form 5 By Category',1,1),(11,'Masterlist of Projects in Form 5 By Spatial',1,0),(12,'Masterlist of Projects in Form 5 By Sector',1,1),(13,'Masterlist of Projects in Form 5 By Agency',1,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
