/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 10.4.11-MariaDB : Database - erpmes
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`erpmes` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `erpmes`;

/*Table structure for table `agencies` */

DROP TABLE IF EXISTS `agencies`;

CREATE TABLE `agencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UACS_DPT_ID` varchar(2) DEFAULT NULL,
  `motheragency_id` int(11) DEFAULT NULL,
  `UACS_AGY_ID` varchar(3) DEFAULT NULL,
  `Category` varchar(10) DEFAULT NULL,
  `UACS_DPT_DSC` varchar(100) DEFAULT NULL,
  `Abbreviation` varchar(10) DEFAULT NULL,
  `UACS_AGY_DSC` varchar(100) DEFAULT NULL,
  `head` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

/*Data for the table `agencies` */

insert  into `agencies`(`id`,`UACS_DPT_ID`,`motheragency_id`,`UACS_AGY_ID`,`Category`,`UACS_DPT_DSC`,`Abbreviation`,`UACS_AGY_DSC`,`head`) values 
(2,NULL,NULL,NULL,NULL,NULL,'PA2','2ND INFANTRY DIVISION, PHILIPPINE ARMY ',NULL),
(3,NULL,NULL,NULL,NULL,NULL,'BFAR','BUREAU OF FISHERIES AND AQUATIC RESOURCES MIMAROPA',NULL),
(4,NULL,NULL,NULL,NULL,NULL,'CAAP','CIVIL AVIATION AUTHORITY OF THE PHILIPPINES',NULL),
(5,NULL,NULL,NULL,NULL,NULL,'CHED','COMMISSION ON HIGHER EDUCATION MIMAROPA',NULL),
(6,NULL,NULL,NULL,NULL,NULL,'CPD','COMMISSION ON POPULATION AND DEVELOPMENT MIMAROPA',NULL),
(7,NULL,NULL,NULL,NULL,NULL,'DAR','DEPARTMENT OF AGRARIAN REFORM MIMAROPA',NULL),
(8,NULL,NULL,NULL,NULL,NULL,'DA','DEPARTMENT OF AGRICULTRE MIMAROPA',NULL),
(9,NULL,NULL,NULL,NULL,NULL,'DEPED','DEPARTMENT OF EDUCATION MIMAROPA',NULL),
(10,NULL,NULL,NULL,NULL,NULL,'DOE','DEPARTMENT OF ENERGY',NULL),
(11,NULL,NULL,NULL,NULL,NULL,'DENR','DEPARTMENT OF ENVIRONMENT AND NATURAL RESOURCES MIMAROPA',NULL),
(12,NULL,NULL,NULL,NULL,NULL,'DOH','DEPARTMENT OF HEALTH MIMAROPA',NULL),
(13,NULL,NULL,NULL,NULL,NULL,'DICT','DEPARTMENT OF INFORMATION AND COMMUNICATIONS TECHNOLOGY - LUZON CLUSTER 3',NULL),
(14,NULL,NULL,NULL,NULL,NULL,'DOLE','DEPARTMENT OF LABOR AND EMPLOYMENT MIMAROPA',NULL),
(15,NULL,NULL,NULL,NULL,NULL,'DPWH','DEPARTMENT OF PUBLIC WORKS AND HIGHWAYS MIMAROPA',NULL),
(16,NULL,NULL,NULL,NULL,NULL,'DOST','DEPARTMENT OF SCIENCE AND TECHNOLOGY MIMAROPA',NULL),
(17,NULL,NULL,NULL,NULL,NULL,'DSWD','DEPARTMENT OF SOCIAL WELFARE AND DEVELOPMENT MIMAROPA',NULL),
(18,NULL,NULL,NULL,NULL,NULL,'DILG','DEPARTMENT OF THE INTERIOR AND LOCAL GOVERNMENT MIMAROPA',NULL),
(19,NULL,NULL,NULL,NULL,NULL,'DOT','DEPARTMENT OF TOURISM MIMAROPA',NULL),
(20,NULL,NULL,NULL,NULL,NULL,'DTI','DEPARTMENT OF TRADE AND INDUSTRY MIMAROPA',NULL),
(21,NULL,NULL,NULL,NULL,NULL,'DOTR','DEPARTMENT OF TRANSPORTATION',NULL),
(22,NULL,NULL,NULL,NULL,NULL,'LWUA','LOCAL WATER UTILITIES ADMINISTRATION',NULL),
(23,NULL,NULL,NULL,NULL,NULL,'NCCA','NATIONAL COMMISSION FOR CULTURE AND THE ARTS',NULL),
(24,NULL,NULL,NULL,NULL,NULL,'NCIP','NATIONAL COMMISSION ON INDIGENOUS PEOPLES MIMAROPA',NULL),
(25,NULL,NULL,NULL,NULL,NULL,'NEA','NATIONAL ELECTRIFICATION ADMINISTRATION',NULL),
(26,NULL,NULL,NULL,NULL,NULL,'NHA','NATIONAL HOUSING AUTHORITY REGION 4',NULL),
(27,NULL,NULL,NULL,NULL,NULL,'NIA','NATIONAL IRRIGATION ADMINISTRATION MIMAROPA',NULL),
(28,NULL,NULL,NULL,NULL,NULL,'NNC','NATIONAL NUTRITION COUNCIL MIMAROPA',NULL),
(29,NULL,NULL,NULL,NULL,NULL,'NAPOLCOM','NATIONAL POLICE COMMISSION MIMAROPA',NULL),
(30,NULL,NULL,NULL,NULL,NULL,'NPC','NATIONAL POWER CORPORATION',NULL),
(31,NULL,NULL,NULL,NULL,NULL,'OCD','OFFICE OF CIVIL DEFENSE MIMAROPA',NULL),
(32,NULL,NULL,NULL,NULL,NULL,'PCA','PHILIPPINE COCONUT AUTHORITY MIMAROPA ',NULL),
(33,NULL,NULL,NULL,NULL,NULL,'PDEA','PHILIPPINE DRUG ENFORCEMENT AGENCY MIMAROPA',NULL),
(34,NULL,NULL,NULL,NULL,NULL,'PFIDA','PHILIPPINE FIBER INDUSTRY DEVELOPMENT AUTHORITY 4',NULL),
(35,NULL,NULL,NULL,NULL,NULL,'PIA','PHILIPPINE INFORMATION AGENCY MIMAROPA',NULL),
(36,NULL,NULL,NULL,NULL,NULL,'PNP','PHILIPPINE NATIONAL POLICE MIMAROPA',NULL),
(37,NULL,NULL,NULL,NULL,NULL,'PPA','PHILIPPINE PORTS AUTHORITY',NULL),
(38,NULL,NULL,NULL,NULL,NULL,'PSA','PHILIPPINE STATISTICS AUTHORITY MIMAROPA',NULL),
(39,NULL,NULL,NULL,NULL,NULL,'TESDA','TECHNICAL EDUCATION AND SKILLS DEVELOPMENT AUTHORITY MIMAROPA',NULL),
(40,NULL,NULL,NULL,NULL,NULL,'PAWC','WESTERN COMMAND, PHILIPPINE ARMY',NULL),
(41,NULL,NULL,NULL,NULL,NULL,'MSC','MARINDUQUE STATE COLLEGE',NULL),
(42,NULL,NULL,NULL,NULL,NULL,'MSCAT','MINDORO STATE COLLEGE OF AGRICULTURE AND TECHNOLOGY',NULL),
(43,NULL,NULL,NULL,NULL,NULL,'OMSC','OCCIDENTAL MINDORO STATE COLLEGE',NULL),
(44,NULL,NULL,NULL,NULL,NULL,'PSU','PALAWAN STATE UNIVERSITY',NULL),
(45,NULL,NULL,NULL,NULL,NULL,'RSU','ROMBLON STATE UNIVERSITY',NULL),
(46,NULL,NULL,NULL,NULL,NULL,'WPU','WESTERN PHILIPPINES UNIVERSITY',NULL),
(47,NULL,NULL,NULL,NULL,NULL,'MAR','PROVINCIAL GOVERNMENT OF MARINDUQUE',NULL),
(48,NULL,NULL,NULL,NULL,NULL,'OCM','PROVINCIAL GOVERNMENT OF OCCIDENTAL MINDORO',NULL),
(49,NULL,NULL,NULL,NULL,NULL,'ORM','PROVINCIAL GOVERNMENT OF ORIENTAL MINDORO',NULL),
(50,NULL,NULL,NULL,NULL,NULL,'PAL','PROVINCIAL GOVERNMENT OF PALAWAN',NULL),
(51,NULL,NULL,NULL,NULL,NULL,'ROM','PROVINCIAL GOVERNMENT OF ROMBLON',NULL),
(52,NULL,NULL,NULL,NULL,NULL,'CAL','CITY GOVERNMENT OF CALAPAN ',NULL),
(53,NULL,NULL,NULL,NULL,NULL,'PP','CITY GOVERNMENT OF PUERTO PRINCESA ',NULL),
(276,'24',0,'001','HEAD','National Economic and Development Authority (NEDA)','NEDA','National Economic and Development Authority (NEDA)','Acting Secretary Karl Kendrick T. Chua');

/*Table structure for table `agency_form1s` */

DROP TABLE IF EXISTS `agency_form1s`;

CREATE TABLE `agency_form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `fy` int(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form1s` */

insert  into `agency_form1s`(`id`,`status`,`created_at`,`updated_at`,`fy`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(11,'Submitted','2020-11-10','2020-11-23',2020,276,NULL,NULL),
(12,'Draft','2020-11-10','2020-11-10',2020,44,NULL,NULL),
(13,'Draft','2020-11-10','2020-11-10',2020,35,NULL,NULL),
(14,'Draft','2020-11-10','2020-11-10',2020,27,NULL,NULL),
(15,'Draft','2020-11-10','2020-11-10',2020,43,NULL,NULL),
(16,'Draft','2020-11-10','2020-11-10',2020,49,NULL,NULL),
(17,'Draft','2020-11-10','2020-11-10',2020,17,NULL,NULL),
(18,'Draft','2020-11-10','2020-11-10',2020,50,NULL,NULL),
(19,'Draft','2020-11-10','2020-11-10',2020,13,NULL,NULL),
(20,'Draft','2020-11-10','2020-11-10',2020,37,NULL,NULL),
(21,'Draft','2020-11-10','2020-11-10',2020,14,NULL,NULL),
(22,'Draft','2020-11-10','2020-11-10',2020,28,NULL,NULL),
(23,'Draft','2020-11-10','2020-11-10',2020,29,NULL,NULL),
(24,'Draft','2020-11-10','2020-11-10',2020,21,NULL,NULL),
(25,'Draft','2020-11-10','2020-11-10',2020,45,NULL,NULL),
(26,'Draft','2020-11-10','2020-11-10',2020,41,NULL,NULL),
(27,'Draft','2020-11-10','2020-11-10',2020,7,NULL,NULL),
(28,'Draft','2020-11-10','2020-11-10',2020,10,NULL,NULL),
(29,'Draft','2020-11-10','2020-11-10',2020,8,NULL,NULL),
(30,'Draft','2020-11-10','2020-11-10',2020,52,NULL,NULL),
(31,'Draft','2020-11-10','2020-11-10',2020,30,NULL,NULL),
(32,'Draft','2020-11-10','2020-11-10',2020,46,NULL,NULL),
(33,'Draft','2020-11-10','2020-11-10',2020,16,NULL,NULL),
(34,'Draft','2020-11-10','2020-11-10',2020,11,NULL,NULL),
(35,'Draft','2020-11-10','2020-11-10',2020,51,NULL,NULL),
(36,'Draft','2020-11-10','2020-11-10',2020,53,NULL,NULL),
(37,'Draft','2020-11-10','2020-11-10',2020,6,NULL,NULL),
(38,'Draft','2020-11-10','2020-11-10',2020,15,NULL,NULL),
(39,'Draft','2020-11-11','2020-11-11',2020,4,NULL,NULL),
(40,'Draft','2020-11-11','2020-11-11',2020,42,NULL,NULL),
(41,'Draft','2020-11-11','2020-11-11',2020,25,NULL,NULL),
(42,'Draft','2020-11-11','2020-11-11',2020,31,NULL,NULL),
(43,'Draft','2020-11-11','2020-11-11',2020,48,NULL,NULL),
(44,'Draft','2020-11-11','2020-11-11',2020,39,NULL,NULL),
(45,'Submitted','2020-11-11','2020-11-11',2020,22,NULL,NULL),
(46,'Draft','2020-11-11','2020-11-11',2020,34,NULL,NULL);

/*Table structure for table `agency_form1s_projects` */

DROP TABLE IF EXISTS `agency_form1s_projects`;

CREATE TABLE `agency_form1s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form1s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `fs_1` decimal(10,2) DEFAULT 0.00,
  `fs_2` decimal(10,2) DEFAULT 0.00,
  `fs_3` decimal(10,2) DEFAULT 0.00,
  `fs_4` decimal(10,2) DEFAULT 0.00,
  `fs_5` decimal(10,2) DEFAULT 0.00,
  `fs_6` decimal(10,2) DEFAULT 0.00,
  `fs_7` decimal(10,2) DEFAULT 0.00,
  `fs_8` decimal(10,2) DEFAULT 0.00,
  `fs_9` decimal(10,2) DEFAULT 0.00,
  `fs_10` decimal(10,2) DEFAULT 0.00,
  `fs_11` decimal(10,2) DEFAULT 0.00,
  `fs_12` decimal(10,2) DEFAULT 0.00,
  `pt_1` decimal(10,2) DEFAULT 0.00,
  `pt_2` decimal(10,2) DEFAULT 0.00,
  `pt_3` decimal(10,2) DEFAULT 0.00,
  `pt_4` decimal(10,2) DEFAULT 0.00,
  `pt_5` decimal(10,2) DEFAULT 0.00,
  `pt_6` decimal(10,2) DEFAULT 0.00,
  `pt_7` decimal(10,2) DEFAULT 0.00,
  `pt_8` decimal(10,2) DEFAULT 0.00,
  `pt_9` decimal(10,2) DEFAULT 0.00,
  `pt_10` decimal(10,2) DEFAULT 0.00,
  `pt_11` decimal(10,2) DEFAULT 0.00,
  `pt_12` decimal(10,2) DEFAULT 0.00,
  `oi_1` varchar(250) DEFAULT '-',
  `oi_2` varchar(250) DEFAULT '-',
  `oi_3` varchar(250) DEFAULT '-',
  `oi_4` varchar(250) DEFAULT '-',
  `oi_5` varchar(250) DEFAULT '-',
  `oi_6` varchar(250) DEFAULT '-',
  `oi_7` varchar(250) DEFAULT '-',
  `oi_8` varchar(250) DEFAULT '-',
  `oi_9` varchar(250) DEFAULT '-',
  `oi_10` varchar(250) DEFAULT '-',
  `oi_11` varchar(250) DEFAULT '-',
  `oi_12` varchar(250) DEFAULT '-',
  `eg_1` int(11) DEFAULT 0,
  `eg_2` int(11) DEFAULT 0,
  `eg_3` int(11) DEFAULT 0,
  `eg_4` int(11) DEFAULT 0,
  `eg_5` int(11) DEFAULT 0,
  `eg_6` int(11) DEFAULT 0,
  `eg_7` int(11) DEFAULT 0,
  `eg_8` int(11) DEFAULT 0,
  `eg_9` int(11) DEFAULT 0,
  `eg_10` int(11) DEFAULT 0,
  `eg_11` int(11) DEFAULT 0,
  `eg_12` int(11) DEFAULT 0,
  `eg_13` int(11) DEFAULT 0,
  `eg_14` int(11) DEFAULT 0,
  `eg_15` int(11) DEFAULT 0,
  `eg_16` int(11) DEFAULT 0,
  `eg_17` int(11) DEFAULT 0,
  `eg_18` int(11) DEFAULT 0,
  `eg_19` int(11) DEFAULT 0,
  `eg_20` int(11) DEFAULT 0,
  `eg_21` int(11) DEFAULT 0,
  `eg_22` int(11) DEFAULT 0,
  `eg_23` int(11) DEFAULT 0,
  `eg_24` int(11) DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form1s_projects` */

insert  into `agency_form1s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form1s_id`,`nro_remarks`,`agency_remarks`,`fs_1`,`fs_2`,`fs_3`,`fs_4`,`fs_5`,`fs_6`,`fs_7`,`fs_8`,`fs_9`,`fs_10`,`fs_11`,`fs_12`,`pt_1`,`pt_2`,`pt_3`,`pt_4`,`pt_5`,`pt_6`,`pt_7`,`pt_8`,`pt_9`,`pt_10`,`pt_11`,`pt_12`,`oi_1`,`oi_2`,`oi_3`,`oi_4`,`oi_5`,`oi_6`,`oi_7`,`oi_8`,`oi_9`,`oi_10`,`oi_11`,`oi_12`,`eg_1`,`eg_2`,`eg_3`,`eg_4`,`eg_5`,`eg_6`,`eg_7`,`eg_8`,`eg_9`,`eg_10`,`eg_11`,`eg_12`,`eg_13`,`eg_14`,`eg_15`,`eg_16`,`eg_17`,`eg_18`,`eg_19`,`eg_20`,`eg_21`,`eg_22`,`eg_23`,`eg_24`) values 
(2,19,'Endorsed','2020-11-10','2020-11-10',11,NULL,'SAMPLE REMARKS',0.00,0.00,56.78,0.00,0.00,0.00,0.00,0.00,1000000.00,1000000.00,1000000.00,23.90,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,70.00,10.00,5.00,0.00,'-','-','-','-','-','Sample Output Indicator','Sample Output Indicator','Sample Output Indicator','Sample Output Indicator','Sample Output Indicator','Sample Output Indicator','Sample Output Indicator',0,0,0,0,0,0,0,0,0,10,5,0,0,0,0,0,0,0,0,0,0,10,5,0),
(3,21,'Endorsed','2020-11-10','2020-11-10',16,NULL,'Sample Endorsement',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,500000.00,500000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,'-','-','-','-','-','-','-','-','-','-','3 km or road','2 km of road',0,0,0,0,0,0,0,0,0,0,10,10,0,0,0,0,0,0,0,0,0,0,3,5),
(4,24,'Endorsed','2020-11-10','2020-11-10',18,NULL,'Sample PGO Palawan Project',0.00,0.00,0.00,0.00,0.00,84057636.89,0.00,0.00,87206802.42,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,35.00,0.00,0.00,86.00,0.00,0.00,0.00,'-','-','-','-','Number of Roads Rehabilitated','Number of Roads Rehabilitated','-','-','Number of Roads Rehabilitated','-','-','-',0,0,0,0,0,10,0,0,10,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0),
(5,20,'Endorsed','2020-11-10','2020-11-10',19,NULL,'ERPMES Form1',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,'-','-','-','-','-','-','-','-','-','-','-number of sites activated','-number of sites activated',0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,4,4),
(6,29,'Draft','2020-11-10','2020-11-10',20,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(7,30,'Endorsed','2020-11-10','2020-11-10',21,NULL,'Sample Remarks',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,160000.00,160000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,'-','-','-','-','-','-','-','-','-','-','-50 workers benefitted','-50 workers benefitted',0,0,0,0,0,0,0,0,0,0,25,25,0,0,0,0,0,0,0,0,0,0,25,25),
(8,34,'Endorsed','2020-11-10','2020-11-10',26,NULL,'No Employment Generated (By Contract)',0.00,0.00,2000000.00,0.00,0.00,3000000.00,0.00,0.00,3000000.00,0.00,0.00,2000000.00,0.00,0.00,20.00,0.00,0.00,30.00,0.00,0.00,30.00,0.00,0.00,20.00,'-','-','-Clearing and Site Preparation','-','-','Form Works, Steel Works and Pouring of Concrete','-','-','Roofing, Electrical and Sanitary Works','-','-','Finishing Works',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(9,40,'Draft','2020-11-10','2020-11-10',24,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,7.19,3.00,0.00,0.00,0.00,0.00,52.00,3.00,6.00,0.00,0.00,0.00,0.00,0.00,24.00,37.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',20,20,25,25,25,25,25,25,30,30,25,25,2,2,2,2,2,2,2,2,2,2,2,2),
(10,36,'Endorsed','2020-11-10','2020-11-10',27,NULL,'Sample DAR Project 1',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,150000.00,350000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,20.00,30.00,'-','-','-','-','-','-','-','-','-','-','-Electrical and Plumbing Works','Finishing and Painting Works-',0,0,0,0,0,0,0,0,0,0,15,20,0,0,0,0,0,0,0,0,0,0,5,8),
(11,48,'Draft','2020-11-10','2020-11-11',30,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,'-Inception Report','Monthly report 1','monthly report 2','-Monthly report 3','-Monthly report 4','-Monthly report 5','-Monthly report 6','-Monthly report 7','-Monthly report 8','-Monthly report  9','-Draft Final Report','Final Report',12,12,12,12,12,12,12,13,12,13,12,0,4,3,3,3,3,3,3,3,3,3,3,0),
(12,47,'Draft','2020-11-10','2020-11-10',24,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,9.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','Masonry Works and Concrete Works (300m)','Masonry Works, Concrete Works, Metal Works (300m)','-',0,0,0,0,0,0,0,0,0,20,22,0,0,0,0,0,0,0,0,0,0,2,2,0),
(13,25,'Endorsed','2020-11-10','2020-11-10',19,NULL,'ERPMES Form 1',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2.00,2.00,'-','-','-','-','-','-','-','-','-','-','-technical assistance provided','-technical assistance provided',0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0),
(14,26,'Draft','2020-11-10','2020-11-10',22,NULL,NULL,0.00,0.00,0.00,76150.00,57400.00,57400.00,0.00,3750.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,30.00,30.00,30.00,0.00,10.00,0.00,0.00,0.00,0.00,'-','-','-','- no. of LGUs and local nutrition workers monitored and evaluated','- no. of LGUs and local nutrition workers monitored and evaluated','- no. of LGUs and local nutrition workers monitored and evaluated','-','- Deliberation of monitoring and evaluation results conducted','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(15,53,'Draft','2020-11-10','2020-11-10',22,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,45000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,100.00,0.00,'-','-','-','-','-','-','-','-','-','-','- 1 issue of MIMAROPA Nutritimes','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(16,50,'Draft','2020-11-10','2020-11-10',23,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,100.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','Number of crime prevention activities conducted and monitored','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(17,54,'Draft','2020-11-10','2020-11-10',34,NULL,NULL,0.00,0.00,67500.00,0.00,0.00,67500.00,0.00,0.00,225000.00,0.00,0.00,90000.00,0.00,0.00,15.00,0.00,0.00,15.00,0.00,0.00,30.00,0.00,0.00,20.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(18,56,'Endorsed','2020-11-10','2020-11-10',11,NULL,'ok',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,60.00,40.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,60.00,40.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,1,1),
(19,42,'Draft','2020-11-10','2020-11-11',35,NULL,NULL,6.21,6.21,6.21,6.21,6.21,6.21,6.21,6.21,6.21,6.21,6.21,6.31,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,12.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(20,52,'Draft','2020-11-10','2020-11-10',20,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(21,37,'Endorsed','2020-11-10','2020-11-10',17,NULL,'Sample',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,300000.00,340000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,'-','-','-','-','-','-','-','-','-','-','50% completion','100% completed',0,0,0,0,0,0,0,0,0,0,5,5,0,0,0,0,0,0,0,0,0,0,3,3),
(22,33,'Draft','2020-11-10','2020-11-10',20,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,3000.00,4000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(23,35,'Draft','2020-11-10','2020-11-11',37,NULL,NULL,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,8.00,17.00,25.00,33.00,42.00,50.00,58.00,67.00,75.00,83.00,92.00,100.00,'-Monthly Accomplishment Report','-Monthly Accomplishment Report','-Quarterly Accomplishment Report','-Monthly Accomplishment Report','-Monthly Accomplishment Report','-Quarterly Accomplishment Report','-Monthly Accomplishment Report','-Monthly Accomplishment Report','-Quarterly Accomplishment Report','-Monthly Accomplishment Report','-Monthly Accomplishment Report','-Quarterly Accomplishment Report',10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10),
(24,58,'Endorsed','2020-11-10','2020-11-10',32,NULL,'The contractor has already requested its final billing last November 6, 2020.',4.92,1.03,0.00,0.00,0.40,0.45,0.44,0.50,0.71,0.48,1.07,0.00,52.00,11.00,0.00,0.00,4.00,5.00,5.00,5.00,7.00,5.00,6.00,0.00,'- Concrete work completion','- External masonry wall is almost 50% completed','- Suspension of project due to COVID pandemic','- Suspension of project due to COVID pandemic (45 CD)','- The contractor requested for suspension due to 2 unbilled progress billing (60 CD)','- Start of masonry wall installation and  metal works (trusses) in parallel work','- External masonry wall is almost 100% completed while roof works must be 100% installed','- Concrete works for slab-on-grade must be 50% completed, while masonry works including plastering works must be already completed','- The implementing agency approved the contractor\'s request for extension due to rainy season. Installation of ceiling boards as well as painting works is on-going.','- Tile works, ceiling works, painting works (walls and ceiling) as well as electrical fixtures  and stainless railings installation','- The building is already completed with all its architectural finishes','-',27,28,28,0,8,8,12,14,14,14,14,0,1,0,0,0,0,0,0,0,0,1,1,0),
(26,64,'Endorsed','2020-11-10','2020-11-10',29,NULL,'82% in progress',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(28,66,'Draft','2020-11-10','2020-11-11',29,NULL,NULL,0.00,0.00,0.00,782841.71,785841.71,782841.71,782841.71,782841.70,782841.71,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,2,2,2,2,2,2,0,0,0,0,0,0,2,2,2,2,2,2,0,0,0),
(30,63,'Draft','2020-11-10','2020-11-10',14,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2.00,10.00,25.00,35.00,50.00,75.00,90.00,100.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(32,57,'Draft','2020-11-10','2020-11-10',12,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(33,73,'Endorsed','2020-11-10','2020-11-23',11,NULL,'ssdsdds',7.50,3.50,3.50,3.50,3.50,3.50,3.50,3.50,3.50,3.50,3.50,7.50,10.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,8.00,10.00,'Inception Report','Monthly Report 2','Monthly Report 3','Monthly Report 4','Monthly Report 5','Midterm Report','Monthly Report 7','Monthly Report 8','Monthly Report 9','Monthly Report 10','Draft Final Report','Final Report',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(34,74,'Endorsed','2020-11-10','2020-11-10',29,NULL,'completed',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(35,75,'Draft','2020-11-11','2020-11-11',39,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(36,79,'Endorsed','2020-11-11','2020-11-23',11,NULL,'Feasibility dfdfdfdfasasas',15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,'-Inveption Report','Monthly Report 2','Monthly Report 3','Monthly Report 4','Monthly Report 5','Midterm Report','Monthly Report 7','Monthly Report 8','Monthly Report 9','Monthly Report 10','Draft Final Report','Final Report',5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5),
(37,80,'Draft','2020-11-11','2020-11-11',20,NULL,NULL,15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,'-Inception Report','-Monthly Report 2','-Monthly Report 3','-Monthly Report 4','-Monthly Report 5','-Midterm Report','-Monthly Report 7','-Monthly Report 8','-Monthly Report 9','-Monthly Report  10','-Draft Final Report','-Final Report',5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5),
(38,78,'Draft','2020-11-11','2020-11-11',29,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-initial report','-monthly report1','-monthly report 2','-monthly report 3','-monthly report 4','-monthly report 5','-final report','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(39,84,'Draft','2020-11-11','2020-11-11',30,NULL,NULL,15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(40,83,'Endorsed','2020-11-11','2020-11-11',24,NULL,'on schedule',30.00,20.00,20.00,30.00,30.00,40.00,30.00,20.00,20.00,5.00,5.00,0.00,5.00,10.00,10.00,10.00,10.00,15.00,10.00,10.00,10.00,5.00,5.00,0.00,'-mobilization, delivery of materials','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-construction of bulkhead','-demobilization','-',40,50,50,50,50,50,50,50,50,40,20,0,2,2,2,2,2,2,2,2,2,2,2,0),
(41,85,'Draft','2020-11-11','2020-11-11',42,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(42,65,'Draft','2020-11-11','2020-11-11',18,NULL,NULL,12.00,11.00,24.00,1.00,1.20,12.00,23.00,12.00,2.00,12.30,13.30,12.20,1.00,12.00,1.00,2.00,2.00,1.00,1.00,1.00,1.00,1.00,1.00,1.00,'-','-','-','-','-','-','-','-','-','-','-','-',5,0,0,0,0,0,0,0,0,0,0,0,-5,0,0,0,0,0,0,0,0,0,0,0),
(43,82,'Draft','2020-11-11','2020-11-11',41,NULL,NULL,15.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(44,86,'Draft','2020-11-11','2020-11-11',40,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(45,87,'Draft','2020-11-11','2020-11-11',44,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(46,51,'Draft','2020-11-11','2020-11-11',13,NULL,NULL,10.00,10.00,10.00,10.00,10.00,10.00,0.00,0.00,0.00,0.00,0.00,0.00,50.00,50.00,50.00,50.00,50.00,50.00,0.00,0.00,0.00,0.00,0.00,0.00,'-Number of radio program produced','-Number of radio program produced','-Number of radio program produced','-Number of radio program produced','-Number of radio program produced','-Number of radio program produced','-','-','-','-','-','-',1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0),
(47,90,'Endorsed','2020-11-11','2020-11-11',45,NULL,'eRPMES Training',2.00,0.00,0.00,0.00,5.00,0.00,0.00,0.00,5.00,0.00,13.00,0.00,0.00,0.00,0.00,15.00,0.00,0.00,0.00,20.00,0.00,30.00,0.00,100.00,'-','-','-','Source -deepwewell  completred, capacity 25.0 l/s','-Pump house/Electro-Mech/Disinfection unit constructed/installed','-','Transmission lines laid/tested','-','-','-Distribuitioond lines laid/tested','-Service connections installed','-Admin building completed',5,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0),
(48,90,'Draft','2020-11-11','2020-11-11',45,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(49,39,'Draft','2020-11-11','2020-11-11',33,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(50,32,'Draft','2020-11-11','2020-11-11',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'-','-','-','-','-','-','-','-','-','-','-','-',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
(51,91,'Endorsed','2020-11-11','2020-11-11',20,NULL,'sample only ppa pmo palawan',15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,15.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,7.00,15.00,'-Inception Report','-Monthly Report 2','-Monthly Report 3','-Monthly Report 4','-Monthly Report 5','-Interim Report','-Monthly Report 7','-Monthly Report 8','-Monthly Report 9','-Monthly Report 10','-Draft Final Report','- Final Report',5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5);

/*Table structure for table `agency_form2s` */

DROP TABLE IF EXISTS `agency_form2s`;

CREATE TABLE `agency_form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form2s` */

insert  into `agency_form2s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(3,'Draft','2020-11-10','2020-11-10','2020-07',49,NULL,NULL),
(4,'Draft','2020-11-10','2020-11-10','2020-07',45,NULL,NULL),
(5,'Draft','2020-11-10','2020-11-10','2020-07',16,NULL,NULL),
(7,'Draft','2020-11-10','2020-11-10','2020-07',29,NULL,NULL),
(10,'Draft','2020-11-10','2020-11-10','2020-07',276,NULL,NULL),
(11,'Draft','2020-11-10','2020-11-10','2020-07',50,NULL,NULL),
(12,'Draft','2020-11-10','2020-11-10','2020-07',14,NULL,NULL),
(13,'Draft','2020-11-10','2020-11-10','2020-07',13,NULL,NULL),
(14,'Draft','2020-11-10','2020-11-10','2020-07',44,NULL,NULL),
(16,'Draft','2020-11-10','2020-11-10','2020-07',27,NULL,NULL),
(17,'Draft','2020-11-10','2020-11-10','2020-07',51,NULL,NULL),
(18,'Draft','2020-11-10','2020-11-10','2020-07',12,NULL,NULL),
(19,'Draft','2020-11-10','2020-11-10','2020-07',28,NULL,NULL),
(20,'Draft','2020-11-10','2020-11-10','2020-07',37,NULL,NULL),
(21,'Draft','2020-11-10','2020-11-10','2020-07',19,NULL,NULL),
(22,'Draft','2020-11-10','2020-11-10','2020-07',17,NULL,NULL),
(23,'Draft','2020-11-10','2020-11-10','2020-07',7,NULL,NULL),
(24,'Draft','2020-11-10','2020-11-10','2020-07',21,NULL,NULL),
(25,'Submitted','2020-11-10','2020-11-10','2020-07',41,NULL,NULL),
(26,'Draft','2020-11-10','2020-11-10','2020-07',8,NULL,NULL),
(27,'Draft','2020-11-10','2020-11-10','2020-07',30,NULL,NULL),
(28,'Draft','2020-11-10','2020-11-10','2020-07',46,NULL,NULL),
(29,'Draft','2020-11-10','2020-11-10','2020-07',42,NULL,NULL),
(30,'Draft','2020-11-10','2020-11-10','2020-07',43,NULL,NULL),
(31,'Draft','2020-11-10','2020-11-10','2020-07',6,NULL,NULL),
(32,'Draft','2020-11-10','2020-11-10','2020-07',34,NULL,NULL),
(33,'Draft','2020-11-10','2020-11-10','2020-07',11,NULL,NULL),
(34,'Draft','2020-11-11','2020-11-11','2020-07',4,NULL,NULL),
(35,'Draft','2020-11-11','2020-11-11','2020-07',48,NULL,NULL),
(36,'Draft','2020-11-11','2020-11-11','2020-07',25,NULL,NULL),
(37,'Draft','2020-11-11','2020-11-11','2020-07',15,NULL,NULL),
(38,'Draft','2020-11-11','2020-11-11','2020-07',39,NULL,NULL),
(39,'Submitted','2020-11-11','2020-11-11','2020-07',22,NULL,NULL);

/*Table structure for table `agency_form2s_projects` */

DROP TABLE IF EXISTS `agency_form2s_projects`;

CREATE TABLE `agency_form2s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form2s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `alloc_asof` decimal(20,2) DEFAULT 0.00,
  `alloc_month` decimal(20,2) DEFAULT 0.00,
  `releases_asof` decimal(20,2) DEFAULT 0.00,
  `releases_month` decimal(20,2) DEFAULT 0.00,
  `obligations_asof` decimal(20,2) DEFAULT 0.00,
  `obligations_month` decimal(20,2) DEFAULT 0.00,
  `expenditures_asof` decimal(20,2) DEFAULT 0.00,
  `expenditures_month` decimal(20,2) DEFAULT 0.00,
  `male` int(11) DEFAULT 0,
  `female` int(11) DEFAULT 0,
  `oi` text DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT 0.00,
  `tftm` decimal(20,2) DEFAULT 0.00,
  `atd` decimal(20,2) DEFAULT 0.00,
  `aftm` decimal(20,2) DEFAULT 0.00,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form2s_projects` */

insert  into `agency_form2s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form2s_id`,`nro_remarks`,`agency_remarks`,`alloc_asof`,`alloc_month`,`releases_asof`,`releases_month`,`obligations_asof`,`obligations_month`,`expenditures_asof`,`expenditures_month`,`male`,`female`,`oi`,`ttd`,`tftm`,`atd`,`aftm`) values 
(3,22,'Draft','2020-11-10','2020-11-10',3,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,'0.00',0.00,0.00,0.00,0.00),
(4,39,'Draft','2020-11-10','2020-11-10',5,NULL,NULL,174231.00,49038.00,174230.00,49038.00,172113.00,27423.00,150399.00,6708.00,0,0,'0.00',0.00,0.00,0.00,0.00),
(5,48,'Draft','2020-11-10','2020-11-10',8,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,30,10,NULL,0.00,0.00,0.00,0.00),
(6,24,'Endorsed','2020-11-10','2020-11-10',11,NULL,'Sample Endorsement RPMES Form2',208011950.00,84057637.00,208011950.00,123954313.00,171264439.00,87206802.00,0.00,0.00,2,1,'Number of road rehab',75.00,80.00,80.00,80.00),
(7,19,'Endorsed','2020-11-10','2020-11-10',10,NULL,'Sample Remarks here',81.00,61.00,60.00,60.00,60.00,60.00,60.00,60.00,1000,1,'SAMPLE OUTPUT INDICATOR',50.00,80.00,80.00,50.00),
(8,30,'Draft','2020-11-10','2020-11-10',12,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(9,50,'Draft','2020-11-10','2020-11-10',7,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(10,42,'Draft','2020-11-10','2020-11-10',17,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(11,56,'Draft','2020-11-10','2020-11-10',40,NULL,NULL,0.00,20.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(12,32,'Endorsed','2020-11-10','2020-11-10',3,NULL,'sample',3.00,3.00,3.00,3.00,3.00,3.00,3.00,3.00,10,0,'160 LM road',100.00,100.00,100.00,100.00),
(13,57,'Draft','2020-11-10','2020-11-10',14,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(14,26,'Draft','2020-11-10','2020-11-10',19,NULL,NULL,194700.00,0.00,15000.00,15000.00,179700.00,179700.00,15000.00,15000.00,0,0,'No. of LGUs and local nutrition workers evaluated',100.00,100.00,0.00,0.00),
(15,29,'Draft','2020-11-10','2020-11-10',20,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(16,43,'Draft','2020-11-10','2020-11-10',21,NULL,NULL,110.00,25.00,25.00,25.00,10.00,10.00,10.00,10.00,40,20,'1.5 km drainage output with 2 outlets',100.00,100.00,80.00,80.00),
(17,37,'Endorsed','2020-11-10','2020-11-10',22,NULL,'sample',640000.00,640000.00,640000.00,640000.00,300000.00,300000.00,0.00,0.00,5,3,'Phase 1 completion',50.00,20.00,30.00,10.00),
(18,47,'Draft','2020-11-10','2020-11-10',24,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(19,34,'Endorsed','2020-11-10','2020-11-10',25,NULL,'No Employment Generated(By Contract)',8000000.00,0.00,8000000.00,0.00,8000000.00,2000000.00,8000000.00,2000000.00,0,0,'Completed',20.00,20.00,20.00,20.00),
(20,31,'Endorsed','2020-11-10','2020-11-11',15,NULL,'on-going',27.00,4.00,10.00,10.00,5.00,5.00,0.00,0.00,20,2,'Mobilization and demobilization',100.00,100.00,95.00,95.00),
(21,36,'Draft','2020-11-10','2020-11-10',23,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(22,20,'Endorsed','2020-11-10','2020-11-10',13,NULL,'ERPMES Form 2',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,2,4,'number of activated sites',4.00,4.00,4.00,4.00),
(23,27,'Draft','2020-11-10','2020-11-10',5,NULL,NULL,186000.00,100000.00,186000.00,100000.00,100000.00,50000.00,100000.00,50000.00,0,0,'Conduct of nine (9) forums',50.00,50.00,30.00,30.00),
(24,35,'Draft','2020-11-10','2020-11-10',31,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(25,33,'Draft','2020-11-10','2020-11-10',20,NULL,NULL,24533542.00,0.00,13417016.00,0.00,13417016.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(26,45,'Endorsed','2020-11-10','2020-11-10',24,NULL,'Dredging',50.00,45.00,45.00,45.00,45.00,45.00,45.00,45.00,40,2,'Dredging',25.00,10.00,26.00,11.00),
(27,61,'Draft','2020-11-10','2020-11-10',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(28,58,'Endorsed','2020-11-10','2020-11-10',28,NULL,'This was based on the report that we have submitted for the 3rd quarter report last October 2020',10.00,0.00,10.00,0.00,10.00,0.00,7.00,2.00,38,1,'Roofing is installed, ceiling is already installed and painted as well as walling, Tile works is almost complete as well as electrical works and plumbing works',100.00,7.00,89.00,8.00),
(29,53,'Draft','2020-11-10','2020-11-10',19,NULL,NULL,45000.00,45000.00,45000.00,45000.00,45000.00,45000.00,0.00,0.00,0,0,'MIMAROPA Nutritimes published',100.00,100.00,0.00,0.00),
(30,63,'Draft','2020-11-10','2020-11-10',16,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,11,1,'Canal Lining',100.00,10.00,100.00,10.00),
(31,65,'Draft','2020-11-10','2020-11-10',11,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(32,64,'Endorsed','2020-11-10','2020-11-10',26,NULL,'actual physical progress 82%',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(33,66,'Endorsed','2020-11-10','2020-11-10',26,NULL,'on going construction',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(34,54,'Draft','2020-11-10','2020-11-10',33,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(35,73,'Draft','2020-11-10','2020-11-10',40,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(36,74,'Endorsed','2020-11-10','2020-11-10',26,NULL,'completed',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(37,75,'Draft','2020-11-11','2020-11-11',34,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(38,80,'Endorsed','2020-11-11','2020-11-11',20,NULL,'Project delayed',100.00,0.00,100.00,0.00,85.00,0.00,15.00,0.00,4,6,'Monthly',22.00,7.00,15.00,0.00),
(39,79,'Endorsed','2020-11-11','2020-11-27',10,NULL,'Project delayed',800000.00,0.00,100.00,0.00,85.00,0.00,15.00,0.00,4,6,'Monthly Report 2',22.00,7.00,15.00,0.00),
(40,83,'Endorsed','2020-11-11','2020-11-11',24,NULL,'on schedule',220.00,20.00,218.00,30.00,0.00,0.00,30.00,0.00,40,2,'Construction of Bulkhead',15.00,15.00,15.00,15.00),
(41,78,'Draft','2020-11-11','2020-11-11',26,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(42,78,'Endorsed','2020-11-11','2020-11-11',26,NULL,'with approved suspension due to covid for suspension due to planting season',0.00,0.00,547982.00,0.00,547982.00,0.00,82199.00,0.00,2,2,'Extension of Paetan Canal',0.00,2191956.00,821983.00,921983.00),
(43,82,'Draft','2020-11-11','2020-11-11',36,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,4,6,'Monthly Report',22.00,7.00,15.00,0.00),
(44,28,'Draft','2020-11-11','2020-11-11',35,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,4,6,NULL,0.00,0.00,0.00,0.00),
(45,44,'Draft','2020-11-11','2020-11-11',35,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(46,46,'Draft','2020-11-11','2020-11-11',35,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(47,87,'Draft','2020-11-11','2020-11-11',38,NULL,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0,0,NULL,0.00,0.00,0.00,0.00),
(48,90,'Endorsed','2020-11-11','2020-11-11',39,NULL,'eRPMES   Training',25.00,3.00,3.00,3.00,0.00,0.00,0.00,0.00,5,2,'Source Dev - well completed',18.00,18.00,20.00,20.00),
(49,91,'Endorsed','2020-11-11','2020-11-11',20,NULL,'sample only ppa pmo palawan',100.00,0.00,100.00,0.00,85.00,0.00,0.00,0.00,8,2,'Monthly',25.00,7.00,15.00,0.00);

/*Table structure for table `agency_form3s` */

DROP TABLE IF EXISTS `agency_form3s`;

CREATE TABLE `agency_form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form3s` */

insert  into `agency_form3s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(24,'Draft','2020-11-10','2020-11-10','2020-09',16,NULL,NULL),
(25,'Draft','2020-11-10','2020-11-10','2020-09',46,NULL,NULL),
(26,'Draft','2020-11-10','2020-11-10','2020-09',15,NULL,NULL),
(28,'Draft','2020-11-10','2020-11-10','2020-09',49,NULL,NULL),
(29,'Draft','2020-11-10','2020-11-10','2020-09',51,NULL,NULL),
(30,'Draft','2020-11-10','2020-11-10','2020-09',53,NULL,NULL),
(31,'Draft','2020-11-10','2020-11-10','2020-09',50,NULL,NULL),
(32,'Draft','2020-11-10','2020-11-10','2020-09',17,NULL,NULL),
(33,'Draft','2020-11-10','2020-11-10','2020-09',14,NULL,NULL),
(34,'Draft','2020-11-10','2020-11-10','2020-09',27,NULL,NULL),
(35,'Draft','2020-11-10','2020-11-10','2020-09',37,NULL,NULL),
(36,'Draft','2020-11-10','2020-11-10','2020-07',276,NULL,NULL),
(37,'Draft','2020-11-10','2020-11-10','2020-09',7,NULL,NULL),
(39,'Draft','2020-11-10','2020-11-10','2020-09',44,NULL,NULL),
(41,'Draft','2020-11-10','2020-11-10','2020-09',48,NULL,NULL),
(42,'Draft','2020-11-10','2020-11-10','2020-09',12,NULL,NULL),
(43,'Draft','2020-11-10','2020-11-10','2020-09',30,NULL,NULL),
(44,'Draft','2020-11-10','2020-11-10','2020-09',19,NULL,NULL),
(45,'Draft','2020-11-10','2020-11-10','2020-09',28,NULL,NULL),
(46,'Draft','2020-11-10','2020-11-10','2020-09',21,NULL,NULL),
(47,'Draft','2020-11-10','2020-11-10','2020-09',13,NULL,NULL),
(49,'Draft','2020-11-10','2020-11-10','2020-09',8,NULL,NULL),
(50,'Draft','2020-11-10','2020-11-10','2020-09',52,NULL,NULL),
(51,'Submitted','2020-11-10','2020-11-10','2020-09',41,NULL,NULL),
(52,'Draft','2020-11-10','2020-11-10','2020-09',42,NULL,NULL),
(53,'Draft','2020-11-10','2020-11-10','2020-09',11,NULL,NULL),
(54,'Draft','2020-11-11','2020-11-11','2020-09',25,NULL,NULL),
(55,'Draft','2020-11-11','2020-11-11','2020-09',39,NULL,NULL),
(56,'Draft','2020-11-11','2020-11-11','2020-09',29,NULL,NULL),
(57,'Draft','2020-11-11','2020-11-11','2020-09',22,NULL,NULL);

/*Table structure for table `agency_form3s_projects` */

DROP TABLE IF EXISTS `agency_form3s_projects`;

CREATE TABLE `agency_form3s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form3s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `findings` text DEFAULT NULL,
  `possible` text DEFAULT NULL,
  `recommendations` text DEFAULT NULL,
  `impstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form3s_projects` */

insert  into `agency_form3s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form3s_id`,`nro_remarks`,`agency_remarks`,`findings`,`possible`,`recommendations`,`impstatus`) values 
(2,58,'Endorsed','2020-11-10','2020-11-10',25,NULL,'The original contract was already 100% completed, only a portion of the variation order (additional work order) is remaining','1. At the start of the project, manpower and material quantity are insufficient that it cannot support the parallel activities of the project.\r\n\r\n2. The project engineer regularly communicates with our office project engineer for the weekly plan of completion.\r\n\r\n3. Delay in project completion','1.  The contractor has another on-going project, hence outsourcing is made by the contractor where only few workers were found. Also, materials such as aggregate and steel reinforcement are out of stock in the local market due to other construction activities in the locality.\r\n\r\n2. To ensure that the two parties (implementor and the contractor) know that they have the common goal in completing the project. Problems that arise can be promptly resolved and mitigating measures can be established immediately.\r\n\r\n3. Due to or worldwide pandemic crisis, all construction projects were suspended since March 17, 2020 and resumed only last May 4, 2020. By then, the contractor resumed for only one week, after that the contractor requested for suspension of work due to delayed payment of 2 progress billing since the month of December 2019.','1. Problems on materials mentioned were already addressed since steel reinforcement and concrete works are almost completed, while manpower availability will be addressed right after the pandemic crisis since many are looking for a job.\r\n\r\n2. Continue the regular communication between the two (2) parties.\r\n\r\n3. Facilitate payment of contractor and require them to complete the project.',3),
(3,22,'Draft','2020-11-10','2020-11-10',27,NULL,NULL,'There were transverse cracks.','Substandard construction materials were used','Remove and replace defective road portions',2),
(4,56,'Draft','2020-11-10','2020-11-10',36,NULL,NULL,NULL,NULL,NULL,2),
(5,19,'Endorsed','2020-11-10','2020-11-10',36,NULL,'Sample Remarks','Sample Findings','Sample','Sample',3),
(6,30,'Draft','2020-11-10','2020-11-10',33,NULL,NULL,NULL,NULL,NULL,3),
(7,31,'Endorsed','2020-11-10','2020-11-10',30,NULL,'sample only','delays in some ground works','affected by typhoon quinta','additional allotment of manpower or overtime on some groundworks',2),
(8,33,'Draft','2020-11-10','2020-11-10',35,NULL,NULL,NULL,NULL,NULL,3),
(9,37,'Endorsed','2020-11-10','2020-11-10',32,NULL,'N/A','Project is ahead on the schedule with 10% advance completion on the target','Complete and Advance Heavy Equipment\r\nWorkers render an overtime\r\nMaterials delivery is on time','Highly recommended',1),
(10,42,'Endorsed','2020-11-10','2020-11-10',29,NULL,'For coordination with the contractor/s','There were no workers on the project site','manpower shortage due to covid 19 pandemic','Work overtime',3),
(11,63,'Draft','2020-11-10','2020-11-10',34,NULL,NULL,'SAMPLE','SAMPLE','SAMPLE',3),
(12,57,'Draft','2020-11-10','2020-11-10',39,NULL,NULL,NULL,NULL,NULL,2),
(13,24,'Endorsed','2020-11-10','2020-11-10',31,NULL,'SAmple Form 3-2','Road Rehab Findings','Delay delivery of Sample Project Exception','Continuous follow up of Sample Project Exception',3),
(14,28,'Draft','2020-11-10','2020-11-10',40,NULL,NULL,NULL,'difficulty to transport drugs, medicines, medication medical equipment due to present situation',NULL,2),
(15,36,'Endorsed','2020-11-10','2020-11-10',37,NULL,'Sample Project DAR 1','Insufficient on the applied base course on Sta. 150 to Sta. 200.','The Contractor didnt follow the  minimum thickness based on plans and specifications.','The Contractor should strictly follows the plans and specifications.',3),
(16,61,'Endorsed','2020-11-10','2020-11-10',31,NULL,'Sample Form3-1','On time delivery of materials for Sample Project Exception','Not Available','Not Applicable',2),
(17,26,'Draft','2020-11-10','2020-11-10',45,NULL,NULL,'MELLPI was not conducted for all the LGUs and local nutrition workers','Suspension of monitoring activities caused by COVID-19','To conduct monitoring activities for next year following standard health protocols and guidelines from Central Office',3),
(18,47,'Draft','2020-11-10','2020-11-10',46,NULL,NULL,'The Project is ahead of schedule.',NULL,NULL,1),
(19,20,'Endorsed','2020-11-10','2020-11-10',47,NULL,'ERPMES Form 3','No movement yet from contractor','Delayed due to COVID and Typhoons','Continuous meeting with contractor for the activation of sites',3),
(20,65,'Draft','2020-11-10','2020-11-10',31,NULL,NULL,'asdfsafasjflakjfjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj\r\nasddasfs\r\nasfasdf\r\nfafafd\r\nafafasdf\r\nasfdasfa\r\nasfsfa\r\nasfaf\r\nasfdas\r\na\r\nfa\r\nsdf\r\nas\r\nfa\r\nf\r\na\r\nfd\r\nf\r\na\r\nf\r\nasf\r\nasd\r\nf\r\nsf\r\nafsa\r\nf\r\ns\r\nfa\r\nsf\r\na\r\nfa\r\nsf\r\naf\r\n\r\ndasf\r\naf\r\na\r\nfa\r\nsf\r\naf\r\nad\r\nfa\r\nf\r\na\r\nfda\r\ndfa\r\n\r\naf\r\nda\r\nf\r\nasf\r\nsa\r\nf\r\nsa\r\ndf\r\ndf\r\nsd\r\nf\r\naf\r\nad\r\nfa\r\n\r\naf\r\ndf',NULL,NULL,3),
(21,43,'Draft','2020-11-10','2020-11-10',44,NULL,NULL,'Implementation of the project is consistent with the identified timeline','cooperation of various stakeholders in the project implementation','continuous consultations with stakeholders',2),
(22,45,'Endorsed','2020-11-10','2020-11-10',46,NULL,'Continuous monitoring of the project and make sure compliance with specs','Added additional manpower and equiptment','delivered materials in advance','take advantage of good weather condition',1),
(23,49,'Draft','2020-11-10','2020-11-10',44,NULL,NULL,'delays in negotiations on the acquisition of lots','proposed area is a CADT area','creation of a team to coordinate with NCIP',3),
(24,34,'Endorsed','2020-11-10','2020-11-10',51,NULL,'Sample Form3-MSC','Projects were not completed on Schedule.','Uncontrollable weather condition caused the delay of the project.','Catch-up plan of the Contractor should be made to avoid the delay of the project.',3),
(25,53,'Draft','2020-11-10','2020-11-10',45,NULL,NULL,NULL,NULL,NULL,2),
(26,66,'Endorsed','2020-11-10','2020-11-11',49,NULL,'subject for validation','Provincial Engineers in charge in the project should submit proper/correct report','not tally with the physical report','need to conduct retooling',2),
(28,59,'Draft','2020-11-10','2020-11-10',53,NULL,NULL,NULL,NULL,NULL,3),
(29,27,'Draft','2020-11-10','2020-11-10',24,NULL,NULL,'Preparations were delayed, hence the activities were also rescheduled, and the platform was changed (forums will be conducted via zoom app)','Implementation of community quarantine due to the covid19 pandemic','Maximize different online video conferencing platforms to proceed with activities that can be done virtually',3),
(30,60,'Draft','2020-11-10','2020-11-10',53,NULL,NULL,NULL,NULL,NULL,2),
(32,62,'Draft','2020-11-10','2020-11-10',53,NULL,NULL,NULL,NULL,NULL,2),
(33,64,'Draft','2020-11-10','2020-11-11',49,NULL,NULL,'only 82% in actual Physical progress','due to  amid covid 19','request for extension of work',2),
(34,74,'Draft','2020-11-10','2020-11-10',49,NULL,NULL,NULL,NULL,NULL,2),
(35,80,'Draft','2020-11-11','2020-11-11',35,NULL,NULL,'Project is delayed due to inaccessible site','Flooding, strict Protocols','Online gathering of data',3),
(36,82,'Draft','2020-11-11','2020-11-11',54,NULL,NULL,'Delay of the Projects','Flooding, strict Protocols for COVID','Exceptions to have a rapid pass',3),
(37,79,'Draft','2020-11-11','2020-11-11',36,NULL,NULL,'Project is delayed due to inaccessible site','Flooding, strict Protocols','Online gathering of data',3),
(38,28,'Draft','2020-11-11','2020-11-11',41,NULL,NULL,NULL,'project delayed due to inaccessible site',NULL,3),
(39,83,'Endorsed','2020-11-11','2020-11-11',46,NULL,'ensure compliance with specs and as per approved timelines','added additional manpower and equiptment','delivered construction materials in advance','take advantage of good weather, make sure compliance with specs',2),
(40,46,'Draft','2020-11-11','2020-11-11',41,NULL,NULL,'delayed implementation','project delayed due to pandemic',NULL,3),
(41,87,'Draft','2020-11-11','2020-11-11',55,NULL,NULL,NULL,NULL,NULL,2),
(42,32,'Endorsed','2020-11-11','2020-11-11',28,NULL,'sample','no shoulder provided','no ROW','Inform the contractor to complete the provision of road shoulders as specified in the POW',2),
(43,90,'Endorsed','2020-11-11','2020-11-11',57,NULL,'erPMES Training','Source dev complete, tested @ 20.0 liters per second at 4.25 m drawdown','Efficient well construction cvontractor,  good aquifer, water potable','Continue close monitoring',1),
(44,91,'Endorsed','2020-11-11','2020-11-11',35,NULL,'sample only ppa pmo palawan','Delayed physical accomplishment','Inadequate manpower and equipment, lockdown due to COVID-19 Pandemic','The Contractor must provide additional resources in order to cope up with the schedule',3);

/*Table structure for table `agency_form4s` */

DROP TABLE IF EXISTS `agency_form4s`;

CREATE TABLE `agency_form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form4s` */

insert  into `agency_form4s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(2,'Draft','2020-11-10','2020-11-10','2020-09',46,NULL,NULL),
(3,'Submitted','2020-11-10','2020-11-11','2020-09',276,NULL,NULL),
(4,'Draft','2020-11-10','2020-11-10','2020-09',53,NULL,NULL),
(5,'Draft','2020-11-11','2020-11-11','2020-09',50,NULL,NULL),
(6,'Draft','2020-11-11','2020-11-11','2020-09',25,NULL,NULL),
(7,'Draft','2020-11-11','2020-11-11','2020-09',22,NULL,NULL),
(8,'Draft','2020-11-11','2020-11-11','2020-09',27,NULL,NULL),
(9,'Draft','2020-11-11','2020-11-11','2020-09',48,NULL,NULL),
(10,'Draft','2020-11-11','2020-11-11','2020-09',30,NULL,NULL),
(11,'Submitted','2020-11-11','2020-11-11','2020-09',52,NULL,NULL),
(12,'Draft','2020-11-11','2020-11-11','2020-09',8,NULL,NULL),
(13,'Draft','2020-11-11','2020-11-11','2020-09',37,NULL,NULL),
(14,'Draft','2020-11-11','2020-11-11','2020-09',49,NULL,NULL),
(15,'Draft','2020-11-11','2020-11-11','2020-09',39,NULL,NULL),
(16,'Draft','2020-11-11','2020-11-11','2020-09',14,NULL,NULL),
(17,'Draft','2020-11-11','2020-11-11','2020-09',17,NULL,NULL),
(18,'Draft','2020-11-11','2020-11-11','2020-09',13,NULL,NULL),
(19,'Draft','2020-11-11','2020-11-11','2020-09',21,NULL,NULL),
(20,'Draft','2020-11-11','2020-11-11','2020-09',28,NULL,NULL),
(21,'Draft','2020-11-11','2020-11-11','2020-09',19,NULL,NULL),
(22,'Draft','2020-11-11','2020-11-11','2020-09',16,NULL,NULL);

/*Table structure for table `agency_form4s_projects` */

DROP TABLE IF EXISTS `agency_form4s_projects`;

CREATE TABLE `agency_form4s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form4s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `objectives` text DEFAULT NULL,
  `indicator` text DEFAULT NULL,
  `results` text DEFAULT NULL,
  `impstatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form4s_projects` */

insert  into `agency_form4s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form4s_id`,`nro_remarks`,`agency_remarks`,`objectives`,`indicator`,`results`,`impstatus`) values 
(2,58,'Draft','2020-11-10','2020-11-10',2,NULL,NULL,'Upgrade physical facility of the University','Completed academic building','The structure is almost 90% completion stage since it only resumed its construction phase last June 30, 2020 due to pandemic issue and project suspension requested by the contractor because of delayed payment of billing. The project is expected to be completed on November 10, 2020.',NULL),
(3,82,'Draft','2020-11-11','2020-11-11',6,NULL,NULL,NULL,NULL,NULL,NULL),
(4,65,'Draft','2020-11-11','2020-11-11',5,NULL,NULL,'sample','sample','sample',NULL),
(5,56,'Endorsed','2020-11-11','2020-11-11',3,NULL,'Sample','To build two classrooms  by November 30, 2021','Student to Classroom ratio','Actual Student Classroom ratio',NULL),
(6,31,'Draft','2020-11-11','2020-11-11',4,NULL,NULL,NULL,NULL,NULL,NULL),
(7,46,'Endorsed','2020-11-11','2020-11-11',9,NULL,'none','service','reformed','better way of life',NULL),
(8,63,'Draft','2020-11-11','2020-11-11',8,NULL,NULL,NULL,NULL,NULL,NULL),
(9,63,'Draft','2020-11-11','2020-11-11',8,NULL,NULL,NULL,NULL,NULL,NULL),
(10,63,'Draft','2020-11-11','2020-11-11',8,NULL,NULL,NULL,NULL,NULL,NULL),
(11,84,'Endorsed','2020-11-11','2020-11-11',11,NULL,'sample','sample','sample','sample',NULL),
(12,29,'Draft','2020-11-11','2020-11-11',13,NULL,NULL,NULL,NULL,NULL,NULL),
(13,79,'Endorsed','2020-11-11','2020-11-11',3,NULL,'Sample','Sample','Sample','Sample',NULL),
(14,80,'Draft','2020-11-11','2020-11-11',13,NULL,NULL,NULL,NULL,NULL,NULL),
(15,87,'Draft','2020-11-11','2020-11-11',15,NULL,NULL,NULL,NULL,NULL,NULL),
(16,24,'Draft','2020-11-11','2020-11-11',5,NULL,NULL,NULL,NULL,NULL,NULL),
(17,76,'Draft','2020-11-11','2020-11-11',16,NULL,NULL,NULL,NULL,NULL,NULL),
(18,37,'Endorsed','2020-11-11','2020-11-11',17,NULL,'gg',NULL,NULL,NULL,NULL),
(19,20,'Endorsed','2020-11-11','2020-11-11',18,NULL,'ERPMES Form 4','Increase the number of activated sites','Number of activated Free Wi-Fi sites',NULL,NULL),
(20,32,'Draft','2020-11-11','2020-11-11',14,NULL,NULL,NULL,NULL,NULL,NULL),
(21,47,'Draft','2020-11-11','2020-11-11',19,NULL,NULL,NULL,NULL,NULL,NULL),
(22,26,'Draft','2020-11-11','2020-11-11',20,NULL,NULL,'To monitor and evaluate implementation of nutrition programs at the LGU level.\r\nTo monitor and evaluate the local nutrition workers in coordinating and implementing nutrition programs in their LGUs.','Number of LGUs monitored and evaluated.\r\nNumber of local nutrition workers monitored and evaluated.','No LGUs and local nutrition workers monitored.',NULL),
(23,53,'Draft','2020-11-11','2020-11-11',20,NULL,NULL,'To release regional issue of Nutritimes.','One issue of regional Nutritimes released.','No results yet, ongoing procurement for the publishing of Nutritimes.',NULL),
(24,43,'Draft','2020-11-11','2020-11-11',21,NULL,NULL,'Output1:Urban infrastructure and services improved','Expand the water supply capacity in Coron to 7,680 m3/day                          \r\nExpand coverage to 78% of resident population by 2027.\r\nFree water supply connections provided to urban poor households 2019 baseline: 720 out of 1,000 Poor households headed by females Project Progress Reports\r\nSewerage treatment capacity increased to 6,400 m3/day (Coron 3,373m3/d, El Nido 627 m3/d) constructed and operational 31.6 additional km of drainage network rehabilitated or newly constructed and operational (Coron: 18.6 km; El Nido: 13 km)\r\n31.35 tons/day of solid waste properly collected, disposed of, or recycled in El Nido by 2027','n/a',NULL),
(25,27,'Draft','2020-11-11','2020-11-11',22,NULL,NULL,'Sample objectives here','Results indicator here','Observed Results here',NULL),
(26,90,'Draft','2020-11-11','2020-11-11',7,NULL,NULL,'Increase service area coverage of Sablayan Water District to afford more people in rural areas access to safe potable water.',NULL,NULL,NULL),
(27,91,'Endorsed','2020-11-11','2020-11-11',13,NULL,'sample only ppa pmo palawan','To provide a safe and stable structure with the increasing port activities that will help boost the economic development of the municipality.','9m x 35m newly constructed rock causeway, 9m x 7m newly constructed box culvert','Increased port activities,',NULL);

/*Table structure for table `agency_form5s` */

DROP TABLE IF EXISTS `agency_form5s`;

CREATE TABLE `agency_form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form5s` */

insert  into `agency_form5s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(35,'Draft','2020-12-02','2020-12-02','2020-07',276,NULL,NULL),
(36,'Draft','2020-12-02','2020-12-02','2020-11',276,NULL,NULL);

/*Table structure for table `agency_form5s_projects` */

DROP TABLE IF EXISTS `agency_form5s_projects`;

CREATE TABLE `agency_form5s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form5s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `allocation` decimal(20,2) DEFAULT NULL,
  `releases` decimal(20,2) DEFAULT NULL,
  `obligations` decimal(20,2) DEFAULT NULL,
  `expenditures` decimal(20,2) DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT NULL,
  `atd` decimal(20,2) DEFAULT NULL,
  `male` int(11) DEFAULT NULL,
  `female` int(11) DEFAULT NULL,
  `negativeslippage` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form5s_projects` */

insert  into `agency_form5s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form5s_id`,`nro_remarks`,`agency_remarks`,`allocation`,`releases`,`obligations`,`expenditures`,`ttd`,`atd`,`male`,`female`,`negativeslippage`) values 
(39,19,NULL,'2020-12-02','2020-12-02',35,NULL,NULL,81.00,60.00,60.00,60.00,50.00,80.00,1000,1,0),
(40,79,NULL,'2020-12-02','2020-12-02',35,NULL,NULL,800000.00,100.00,85.00,15.00,22.00,15.00,4,6,1);

/*Table structure for table `agency_form6s` */

DROP TABLE IF EXISTS `agency_form6s`;

CREATE TABLE `agency_form6s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `period` varchar(11) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `nro_status_review` varchar(20) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form6s` */

insert  into `agency_form6s`(`id`,`status`,`created_at`,`updated_at`,`period`,`agency_id`,`nro_status_review`,`nro_remarks`) values 
(70,'Draft','2020-12-02','2020-12-02','2020-07',276,NULL,NULL),
(74,'Draft','2020-12-02','2020-12-02','2020-11',276,NULL,NULL);

/*Table structure for table `agency_form6s_projects` */

DROP TABLE IF EXISTS `agency_form6s_projects`;

CREATE TABLE `agency_form6s_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `agency_form6s_id` int(11) DEFAULT NULL,
  `nro_remarks` text DEFAULT NULL,
  `agency_remarks` text DEFAULT NULL,
  `releases` decimal(20,2) DEFAULT NULL,
  `expenditures` decimal(20,2) DEFAULT NULL,
  `atd` decimal(20,2) DEFAULT NULL,
  `ttd` decimal(20,2) DEFAULT NULL,
  `issues` text DEFAULT NULL,
  `source` text DEFAULT NULL,
  `action` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `agency_form6s_projects` */

insert  into `agency_form6s_projects`(`id`,`project_id`,`status`,`created_at`,`updated_at`,`agency_form6s_id`,`nro_remarks`,`agency_remarks`,`releases`,`expenditures`,`atd`,`ttd`,`issues`,`source`,`action`) values 
(62,79,NULL,'2020-12-02','2020-12-02',70,NULL,NULL,100.00,15.00,15.00,22.00,'Project is delayed due to inaccessible site','Submitted RPMES input forms of NEDA as of 2020-07','Project delayed');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `categories_name_unique` (`category`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category`,`created_at`,`updated_at`) values 
(1,'ELCAC','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Balik Probinsya Program (BP2)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Rehab and Recovery Projects (RRP)','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(6,'Official Development Assistance (ODA)',NULL,NULL);

/*Table structure for table `form1s` */

DROP TABLE IF EXISTS `form1s`;

CREATE TABLE `form1s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fy` int(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form1s` */

insert  into `form1s`(`id`,`fy`,`is_lock`,`created_at`,`updated_at`) values 
(1,2020,0,'2020-07-31','2020-10-12');

/*Table structure for table `form2s` */

DROP TABLE IF EXISTS `form2s`;

CREATE TABLE `form2s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form2s` */

insert  into `form2s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(2,'2020-07',0,'2020-08-26','2020-09-28');

/*Table structure for table `form3s` */

DROP TABLE IF EXISTS `form3s`;

CREATE TABLE `form3s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form3s` */

insert  into `form3s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-09',0,'2020-09-14','2020-09-28');

/*Table structure for table `form4s` */

DROP TABLE IF EXISTS `form4s`;

CREATE TABLE `form4s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form4s` */

insert  into `form4s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(1,'2020-09',0,'2020-09-28','2020-10-02');

/*Table structure for table `form5s` */

DROP TABLE IF EXISTS `form5s`;

CREATE TABLE `form5s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form5s` */

insert  into `form5s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(4,'2020-11',1,'2020-11-23','2020-11-23'),
(5,'2020-07',1,'2020-11-25','2020-11-25');

/*Table structure for table `form6s` */

DROP TABLE IF EXISTS `form6s`;

CREATE TABLE `form6s` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `period` varchar(11) NOT NULL,
  `is_lock` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `form6s` */

insert  into `form6s`(`id`,`period`,`is_lock`,`created_at`,`updated_at`) values 
(4,'2020-11',1,'2020-11-16','2020-11-16'),
(5,'2020-07',1,'2020-11-25','2020-11-25');

/*Table structure for table `impstatuses` */

DROP TABLE IF EXISTS `impstatuses`;

CREATE TABLE `impstatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `impstatuses` */

insert  into `impstatuses`(`id`,`type`) values 
(1,'Ahead of Schedule'),
(2,'On Schedule'),
(3,'Behind Schedule');

/*Table structure for table `item_tag` */

DROP TABLE IF EXISTS `item_tag`;

CREATE TABLE `item_tag` (
  `item_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `item_tag_item_id_foreign` (`item_id`) USING BTREE,
  KEY `item_tag_tag_id_foreign` (`tag_id`) USING BTREE,
  CONSTRAINT `item_tag_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `item_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `item_tag` */

insert  into `item_tag`(`item_id`,`tag_id`) values 
(1,1),
(1,2),
(1,3),
(2,1),
(3,1);

/*Table structure for table `items` */

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `show_on_homepage` tinyint(1) NOT NULL DEFAULT 0,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `items_name_unique` (`name`) USING BTREE,
  KEY `items_category_id_foreign` (`category_id`) USING BTREE,
  CONSTRAINT `items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `items` */

insert  into `items`(`id`,`name`,`description`,`picture`,`category_id`,`status`,`date`,`show_on_homepage`,`options`,`created_at`,`updated_at`) values 
(1,'5 citybreak ideas for this year','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,1,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Top 10 restaurants in Italy','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Cocktail ideas for your birthday party','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet nulla nulla. Donec luctus lorem justo, ut ullamcorper eros pellentesque ut. Etiam scelerisque dapibus lorem, vitae maximus ante condimentum quis. Maecenas ac arcu a lacus aliquet elementum posuere id nunc. Curabitur sem lorem, faucibus ac enim ut, vestibulum feugiat ante. Fusce hendrerit leo nibh, nec consectetur nulla venenatis et. Nulla tincidunt neque quam, sit amet tincidunt quam blandit in. Nunc fringilla rutrum tortor, sit amet bibendum augue convallis a. Etiam mauris orci, sollicitudin eu condimentum sed, dictum ut odio. Sed vel ligula in lectus scelerisque ornare.Mauris dolor nisl, finibus eget sem in, ultrices semper libero. Nullam accumsan suscipit tortor, a vestibulum sapien imperdiet quis. Donec pretium mauris quis lectus sodales accumsan. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt semper orci eu molestie. Vivamus fermentum enim vitae magna elementum, quis iaculis augue tincidunt. Donec fermentum quam facilisis sem dictum rutrum. Nunc nec urna lectus. Nulla nec ultrices lorem. Integer ac ante massa.',NULL,2,'published','2020-07-24',1,'[\"0\",\"1\"]','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `locations` */

DROP TABLE IF EXISTS `locations`;

CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proj_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `longitude` float(20,7) DEFAULT NULL,
  `latitude` float(20,7) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `locations` */

insert  into `locations`(`id`,`proj_id`,`location`,`created_at`,`updated_at`,`longitude`,`latitude`) values 
(3,10,'SAMPLE LOCATION PROJECT 1','2020-08-06','2020-10-27',118.5593262,9.7312298),
(5,10,'SAMPLE LOCATION PROJECT 2','2020-08-07','2020-10-27',122.0398026,12.4104605),
(6,14,'PASIG CITY','2020-08-07','2020-10-06',118.5593262,9.7312298),
(7,14,'MANDALUYONG CITY','2020-10-06','2020-10-06',118.5593262,9.7312298),
(8,16,'SAMPLE LOCATION 1','2020-11-03','2020-11-03',12.8902998,12.8902998),
(9,18,'SAMPLE LOCATION','2020-11-10','2020-11-10',118.0000000,9.0000000),
(10,19,'SAMPLE LOCATION 1','2020-11-10','2020-11-10',119.0000000,9.0000000),
(11,19,'SAMPLE LOCATION 2','2020-11-10','2020-11-10',119.0000000,9.0000000),
(12,20,'Oriental Mindoro','2020-11-10','2020-11-10',121.1886978,13.3956032),
(13,21,'Naujan','2020-11-10','2020-11-10',10.0000000,15.0000000),
(14,22,'Naujan','2020-11-10','2020-11-10',10.0000000,15.0000000),
(15,23,'MIMAROPA','2020-11-10','2020-11-10',1.0000000,-1.0000000),
(16,24,'Provincewide','2020-11-10','2020-11-10',1.0000000,8.0000000),
(17,25,'Occidental Mindoro','2020-11-10','2020-11-10',121.0737915,12.3635988),
(18,26,'Regionwide','2020-11-10','2020-11-10',0.0000000,0.0000000),
(19,27,'Boac, Marinduque','2020-11-10','2020-11-10',0.0000000,0.0000000),
(20,28,'Occidental Mindoro','2020-11-10','2020-11-10',4.0000000,5.0000000),
(21,29,'Port of Bansud, Oriental Mindoro','2020-11-10','2020-11-10',12.8619576,121.4899826),
(22,30,'Calapan City, Oriental Mindoro','2020-11-10','2020-11-10',121.1645966,13.3771000),
(23,31,'BARANGAY LUZVIMINDA','2020-11-10','2020-11-10',9.6705847,118.7332611),
(24,32,'Balatero, Puerto Galera','2020-11-10','2020-11-10',0.0000000,0.0000000),
(25,33,'Port of Puerto Princesa, Barangay Bagong Pag-Asa, Puerto Princesa City, Palawan','2020-11-10','2020-11-10',119.0000000,8.0000000),
(26,34,'Tanza, Boac, Marinduque','2020-11-10','2020-11-10',112.0000000,8.0000000),
(27,36,'Pinamalayan Oriental Mindoro','2020-11-10','2020-11-10',13.0459003,121.4645004),
(28,37,'Malibago, Pola, Oriental Mindoro','2020-11-10','2020-11-10',13.0999002,121.4485016),
(30,39,'Occidental Mindoro State College, San Jose National High School , Sablayan National Comprehensive High,Occidental Mindoro National High School, Magsaysay National High School','2020-11-10','2020-11-10',0.0000000,0.0000000),
(31,40,'Brgy. Tayamaan, Mamburao Occidental Mindoro','2020-11-10','2020-11-10',120342.6328125,131337.9843750),
(32,41,'Oriental Mindoro','2020-11-10','2020-11-10',90.0000000,90.0000000),
(33,42,'Odiongan, Romblon','2020-11-10','2020-11-10',121.9919968,12.4020004),
(34,30,'Odiongan, Romblon','2020-11-10','2020-11-10',122.0155029,12.4047003),
(35,30,'San Jose, Occidental Mindoro','2020-11-10','2020-11-10',121.0605087,12.3553457),
(36,43,'El Nido','2020-11-10','2020-11-10',11.1955605,119.4075012),
(37,44,'Occidental Mindoro','2020-11-10','2020-11-10',2.0000000,2.0000000),
(38,45,'Kalayaan, Palawan','2020-11-10','2020-11-10',20.0000000,20.0000000),
(39,46,'Brgy. Tayamaan, Mamburao, Occidental Mindoro','2020-11-10','2020-11-10',3.0000000,4.0000000),
(40,47,'Masiga, Gasan, Marinduque','2020-11-10','2020-11-11',13.3583002,121.8275986),
(41,48,'Camilmil, Oriental Mindoro','2020-11-10','2020-11-10',121.1823807,13.4011202),
(42,49,'Cadyang Falls','2020-11-10','2020-11-10',1200.0000000,12012.0000000),
(43,51,'Oriental Mindoro','2020-11-10','2020-11-10',0.0000000,0.0000000),
(44,51,'Occidental Mindoro','2020-11-10','2020-11-10',0.0000000,0.0000000),
(45,51,'Marinduque','2020-11-10','2020-11-10',0.0000000,0.0000000),
(46,51,'Romblon','2020-11-10','2020-11-10',0.0000000,0.0000000),
(47,51,'Palawan','2020-11-10','2020-11-10',0.0000000,0.0000000),
(48,52,'Port of Puerto Princesa, Bgy. Bagong Pag-Asa, Puerto Princesa City, Palawan','2020-11-10','2020-11-10',119.0000000,8.0000000),
(49,53,'MIMAROPA','2020-11-10','2020-11-10',0.0000000,0.0000000),
(53,54,'Occidental Mindoro','2020-11-10','2020-11-10',13.1023998,120.7650986),
(54,55,'SAMPLE NPC PROJECT - BERNARD','2020-11-10','2020-11-10',12.8902998,12.8902998),
(55,56,'Calapan City, Oriental Mindoro','2020-11-10','2020-11-10',1.0000000,-1.0000000),
(56,57,'Puerto Princesa City, Palawan','2020-11-10','2020-11-10',9.7311859,12.4112206),
(57,58,'WPU-Main Campus, Barangay San Juan, Aborlan, Palawan, Region IV-B MIMAROPA','2020-11-10','2020-11-10',1044222.0000000,671108.0000000),
(58,59,'Brgy. San Roque, Bulalacao, Oriental Mindoro','2020-11-10','2020-11-10',1.0000000,1.0000000),
(59,60,'So. Tibunbon Caagutayan, San Teodoro Oriental Mindoro','2020-11-10','2020-11-10',1.0000000,1.0000000),
(60,61,'Provincewide','2020-11-10','2020-11-10',2.0000000,3.0000000),
(61,62,'Brgy. Rosacara, Bansud','2020-11-10','2020-11-10',1.0000000,1.0000000),
(62,63,'Rizal, Occidental Mindoro','2020-11-10','2020-11-10',121.1933136,12.3816309),
(63,64,'Santa Cruz, Marinduque','2020-11-10','2020-11-10',0.0000000,0.0000000),
(64,65,'PALAWAN','2020-11-10','2020-11-10',9.1234560,12.1234560),
(65,66,'Dumara, Paawan','2020-11-10','2020-11-10',0.0000000,0.0000000),
(66,67,'Oriental Mindoro','2020-11-10','2020-11-10',13.0565004,121.4068985),
(67,68,'Palawan','2020-11-10','2020-11-10',9.8348999,118.7384033),
(68,69,'Occidental Mindoro','2020-11-10','2020-11-10',13.1023998,120.7650986),
(69,70,'Oriental Mindoro','2020-11-10','2020-11-10',13.0565004,121.4068985),
(70,71,'Romblon','2020-11-10','2020-11-10',12.5778999,122.2690964),
(71,72,'Palawan','2020-11-10','2020-11-10',9.8348999,118.7384033),
(72,73,'Laylay, Boac, Marinduque','2020-11-10','2020-11-10',121.8170013,13.4350004),
(73,74,'So. Malakbay Brgy,Punta Baja, Rizal, Palawan','2020-11-10','2020-11-10',0.0000000,0.0000000),
(74,75,'Puerto Princesa International Airport, San Miguel, Puerto Princesa City','2020-11-11','2020-11-11',944.0000000,118.0000000),
(75,76,'San Manuel, Puerto Princesa City, Palawan','2020-11-11','2020-11-11',118.7634964,9.7706003),
(76,77,'Boac','2020-11-11','2020-11-11',13.4399996,121.8399963),
(77,78,'Brgy. Paetan, Sablayan,Occidental Mindoro','2020-11-11','2020-11-11',0.0000000,0.0000000),
(78,79,'Villa Cervesa, Victoria, Oriental Mindoro','2020-11-11','2020-11-11',121.1656036,13.1150570),
(79,80,'Villa Cervesa, Victoria, Oriental Mindoro','2020-11-11','2020-11-11',121.1656036,13.1150570),
(80,81,'Bgy. San Miguel, Puerto Princesa','2020-11-11','2020-11-11',121165608.0000000,13.1150570),
(81,82,'Villa Cervesa, Victoria, Oriental Mindoro','2020-11-11','2020-11-11',121.1656036,13.1150570),
(82,83,'Kalayaan, Palawan','2020-11-11','2020-11-11',20.0000000,20.0000000),
(83,84,'Villa Cervesa, Victoria, Oriental Mindoro','2020-11-11','2020-11-11',121.1656036,13.1150570),
(84,85,'Victoria','2020-11-11','2020-11-11',141.7700043,151.5500031),
(85,86,'Alcate, Victoria, Or. Mindoro','2020-11-11','2020-11-11',12.0000000,9.0000000),
(86,87,'Calapan City','2020-11-11','2020-11-11',1.0000000,1.0000000),
(87,88,'Puerto Princesa, Palawan','2020-11-11','2020-11-11',4.0000000,4.0000000),
(88,89,'MINOLO, PUERTO GALERA, ORIENTAL MINDORO TO MAMBURAO, OCCIDENTAL MINDORO','2020-11-11','2020-11-11',120.9546967,13.4496002),
(89,90,'Bgy Poblacion, Sablayan','2020-11-11','2020-11-11',12.8560305,120.9095688),
(90,91,'Barangay Salvacion, Busuanga, Palawan','2020-11-11','2020-11-11',119.9241638,12.1466665),
(91,92,'SAMPLE LOCATION 1','2020-11-23','2020-11-23',1.0000000,1.0000000),
(92,93,'SAMPLE OTHERS','2020-12-01','2020-12-01',12.8902998,12.8902998);

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

/*Data for the table `logs` */

insert  into `logs`(`id`,`log`,`created_at`,`updated_at`,`username`) values 
(1,'Edited Project: SAMPLE PROJECT STATUS','2020-11-23 02:18:27','2020-11-23 02:18:27','Administrator'),
(2,'Endorse Project: ','2020-11-23 02:25:23','2020-11-23 02:25:23','NEDA'),
(3,'Endorse Project: 79','2020-11-23 02:26:08','2020-11-23 02:26:08','NEDA'),
(4,'Endorse Project: Flood Control Feasibility Study','2020-11-23 02:27:45','2020-11-23 02:27:45','NEDA'),
(5,'Submitted Form 1','2020-11-23 02:42:33','2020-11-23 02:42:33','NEDA'),
(6,'Added Project: SAMPLE OTHERS','2020-12-01 06:21:44','2020-12-01 06:21:44','Administrator'),
(7,'Edited Project: SAMPLE OTHERS','2020-12-01 06:44:41','2020-12-01 06:44:41','Administrator'),
(8,'Edited Project: SAMPLE OTHERS','2020-12-01 06:46:42','2020-12-01 06:46:42','Administrator'),
(9,'Edited Project: SAMPLE OTHERS','2020-12-01 06:47:08','2020-12-01 06:47:08','Administrator'),
(10,'Edited Project: 2021 National Science and Technology Week','2020-12-01 06:56:11','2020-12-01 06:56:11','Administrator');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_100000_create_password_resets_table',1),
(2,'2019_01_15_100000_create_roles_table',1),
(3,'2019_01_15_110000_create_users_table',1),
(4,'2019_01_17_121504_create_categories_table',1),
(5,'2019_01_21_130422_create_tags_table',1),
(6,'2019_01_21_163402_create_items_table',1),
(7,'2019_01_21_163414_create_item_tag_table',1),
(8,'2019_03_06_132557_add_photo_column_to_users_table',1),
(9,'2019_03_06_143255_add_fields_to_items_table',1),
(10,'2019_03_20_090438_add_color_tags_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `password_resets` */

insert  into `password_resets`(`email`,`token`,`created_at`) values 
('bnlayon@neda.gov.ph','$2y$10$WUp4C7xUrQOe9M/IldHV3OhDkvjrJHAoiHbvIfgFu74qN52jaO3Cy','2020-11-09 06:00:27');

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `implementingagency` int(11) NOT NULL,
  `sector` int(11) NOT NULL,
  `fundingsource` int(11) NOT NULL,
  `mode` varchar(500) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `category` int(11) NOT NULL,
  `status` varchar(15) NOT NULL,
  `cost` int(50) NOT NULL,
  `odatype` varchar(10) DEFAULT NULL,
  `others` varchar(200) DEFAULT NULL,
  `gaayear` date DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `projects` */

insert  into `projects`(`id`,`title`,`implementingagency`,`sector`,`fundingsource`,`mode`,`start`,`end`,`created_at`,`updated_at`,`category`,`status`,`cost`,`odatype`,`others`,`gaayear`) values 
(19,'SAMPLE NEDA PROJECT 1',276,2,2,'SAMPLE MODE OF IMPLEMENTATION','2020-01-01','2020-12-31','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(20,'Free Wi-Fi for All',13,4,2,'BC Net, Inc.','2020-11-11','2021-05-20','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(21,'Concreting of Sampaguita Road',49,2,4,'By Contract - c/o  ABC123 Builders','2020-11-11','2020-12-11','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(22,'Concreting of Sampaguita Road',49,2,4,'By Contract - c/o  ABC123 Builders','2020-11-11','2020-12-11','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(23,'Establishment of the Registry of Barangay Inhabitants and Migrants (RBIM)',6,3,2,'City/Municipal Led Implementation','2021-01-01','2021-12-31','2020-11-10','2020-11-10',2,'',0,'','','0000-00-00'),
(24,'Roads and Bridges Development Program',50,2,2,'Agency to Agency','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(25,'eBPLS',13,4,2,'Admin','2020-11-10','2020-11-11','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(26,'Monitoring and Evaluation of Local Level Plan Implementation (MELLPI)',28,3,4,'Small value procurement','2020-04-01','2020-08-31','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(27,'2021 National Science and Technology Week',16,4,4,'N/A','2021-05-10','2021-06-10','2020-11-10','2020-12-01',6,'Ongoing',0,'Grant','sdfghjkl',NULL),
(28,'Combat COVID-19-Procurement and Distribution of Medical and Non-Medical Equipment',48,3,2,'by administration','2020-04-16','2020-12-10','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(29,'Rehabilitation of Bansud Port',37,2,4,'By Contract - Orient Star Construction, Inc.','2020-11-05','2021-05-03','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(30,'DOLE Tulong Pangkabuhayan Para sa Ating Disadvantaged/Displaced Workers',14,5,4,'DOLE MIMAROPA','2020-11-16','2020-12-15','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(31,'CONSTRUCTION OF SATELLITE CITY HALL AT LUZVIMINDA RURAL SERVICE CENTER',53,4,2,'BY CONTRACT - BONIFACIO M. CARLOS CONSTRUCTION','2020-08-12','2021-08-11','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(32,'Concreting of Road Gap',49,2,2,'by Contract','2020-11-10','2020-12-10','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(33,'Repair of Damaged Concrete Pavement, Drainage Cover, R.C. Platform Deck/Slab and Jacketing of Pile Caps',37,2,4,'by Contract , JC PINON CONSTRUCTION INC.','2019-10-15','2020-07-28','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(34,'Construction of MSC Gymnasium',41,2,4,'Performance Builders Development Corporation (By Contract)','2017-03-16','2017-10-10','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(35,'Responsible Parenthood and Family Planning',6,3,1,'City/Municipal Implementation','2021-01-01','2021-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(36,'Sample Project DAR 1',7,5,2,'By Contract','2020-11-10','2020-12-10','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(37,'Improvement of Water System level 2',17,2,2,'Community','2019-11-09','2019-12-10','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(38,'Project 1',29,5,4,'Contract','2020-11-10','2020-12-10','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(39,'Enhancing the S&T Ecosystem in Public Senior High School Through Versatile Instrumentation System for Science Education and Research (VISSER) Technology',16,4,1,'By contract','2020-01-01','2021-01-01','2020-11-10','2020-11-11',6,'',0,'','','0000-00-00'),
(40,'Tayamaan port Development Project',21,2,4,'By Contract (C.T Leoncio Construction and Trading)','2019-01-28','2020-11-16','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(41,'OPERATIONS DEVELOPMENT COMMUNICATIONS PROGRAM-RADIO PROGRAM',35,4,1,'Admin','2020-10-01','2020-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(42,'Completion of Hospital/Construction of Bldg. 3 - Phase 2 at Romblon Provincial Hospital',51,3,4,'By Contract','2018-06-09','2019-06-02','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(43,'Sustainable Tourism Development Project - Drainage',19,2,1,'by contract','2022-01-01','2025-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(44,'Supply and Delivery of Personal Protective Equipment for COVID-19 response',48,3,2,'by administration','2020-04-16','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(45,'Brgy. Pag-asa Port',21,2,4,'Mamsar Construction','2018-06-10','2020-05-30','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(46,'Construction of Halfway House of Former Rebels',48,3,2,'by administration','2020-02-10','2020-12-25','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(47,'Marinduque Airport Development Project (Construction of CHB Fence)',21,2,4,'by Contract - Advance Tech Corp.','2020-07-29','2020-12-25','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(48,'Construction of Multi-Purpose Building',52,2,2,'Contract','2020-06-23','2021-02-23','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(49,'Sustainable Tourism Development Project - Coron Water Supply',19,2,1,'by contract','2022-01-01','2025-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(50,'Implementation and Monitoring of Crime Prevention Programs',29,5,4,'2020 GAA (Fund source)','2020-09-01','2020-09-30','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(51,'PRODUCTION AND DISSEMINATION OF TRADITIONAL MASS MEDIA',35,4,1,'Admin','2020-10-01','2020-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(52,'Various Maintenance of Port Physical Facilities Phase I',37,2,4,'By Contract, Orient Star Const. Inc.','2020-10-20','2021-04-12','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(53,'Publication of Regional Nutritimes',28,3,4,'Small value procurement','2020-11-01','2020-11-30','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(54,'NGP 2nd Year  M&P- Occidental Mindoro 134 has.',11,1,2,'Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(55,'SAMPLE NPC PROJECT - BERNARD',30,1,1,'SAMPLE MODE','2020-11-10','2020-11-10','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(56,'NEDA MIMAROPA Regional Office Building',276,2,2,'By Contracts','2020-11-10','2020-11-30','2020-11-10','2020-11-10',1,'',0,'','','0000-00-00'),
(57,'Completion of Medical Building (Phase II)',44,3,2,'Contract JB & Son Trading & Construction','2020-01-01','2020-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(58,'Construction of College of Business Management Academic Building',46,2,4,'By Contract (Kayano Trading and Construction)','2019-10-17','2020-04-14','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(59,'Establishment of Small Water Impounding System Oriental Mindoro 50 cu. m.',11,2,2,'by Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(60,'Establishment of Small Water Impounding System Oriental Mindoro 50 cu. m.',11,2,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(61,'Counterpart Funds for PRDP Sample',50,2,2,'Administration','2020-01-01','2020-12-31','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(62,'Establishment of Small Water Impounding System Oriental Mindoro 54 cu. m.',11,2,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(63,'LUMINTAO RIS',27,2,2,'Contracts','2020-05-12','2020-09-08','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(64,'Improvement/Upgrading of Pantayan-Jolo-Napo Farm to Market Road',8,2,1,'Contract: DQT Builders Corporation','2020-01-22','2020-10-14','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(65,'SAMPLE PROJECT 2',50,2,2,'ABCDEF','2020-11-15','2020-11-30','2020-11-10','2020-11-11',1,'',0,'','','0000-00-00'),
(66,'Construction and uprading of Junction Salan-Culasian-Magsaysay Farm to Market Road',8,2,1,'Contract: Ivy Michelle Trading nd Constructon','2020-01-18','2020-09-11','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(67,'NGP 3rd Year  M&P- Oriental Mindoro 12,233 has.',11,1,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(68,'NGP 2nd Year  M&P- Palawan 331 has.',11,1,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(69,'NGP 3rd Year  M&P- Occidental Mindoro 3,186 has.',11,1,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(70,'NGP 3rd Year  M&P- Oriental Mindoro 2,461 has.',11,1,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(71,'NGP 3rd Year  M&P- Romblon 133 has.',11,1,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(72,'NGP 3rd Year  M&P- Palawan 6,209 has.',11,1,2,'GAA Contract','2020-01-01','2020-12-31','2020-11-10','2020-11-10',3,'',0,'','','0000-00-00'),
(73,'Laylay Light Stattion Rehabilitation and Development Project',276,2,2,'by contract','2021-01-01','2021-12-30','2020-11-10','2020-11-10',2,'',0,'','','0000-00-00'),
(74,'Rehabilitation of Malakibay Diversion Dam',8,2,1,'By Contract : JB and Son Construction','2020-01-05','2020-10-28','2020-11-10','2020-11-10',6,'',0,'','','0000-00-00'),
(75,'Improvement of PTB',4,2,4,'By contract','2020-02-15','2020-12-10','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(76,'DOLE Sample Project 2',14,5,4,'DOLE MIMAROPA','2020-06-01','2020-12-31','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(77,'Farm to Market Road',276,1,1,'by Contract','2020-01-01','2020-12-31','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(78,'Extension of Paetan Irrigation Canal',8,2,1,'By Contract: MP Cabrera Construction','2020-04-22','2020-11-13','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(79,'Flood Control Feasibility Study',276,2,1,'by contract','2021-01-15','2022-01-15','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(80,'Flood Control Feasibility Study',37,2,1,'by contract','2021-01-15','2022-01-15','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(81,'Flood Control Feasibility Study',53,2,1,'By Contract','2021-01-15','2020-01-15','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(82,'Flood Control Feasibility Study',25,2,1,'by contract','2021-01-15','2022-01-15','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(83,'Brgy. Pag-asa Port Phase 2',21,2,4,'MOA','2020-01-01','2020-11-30','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(84,'Flood Control Feasibility Study',52,2,1,'Contract','2021-01-15','2021-09-11','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(85,'Flood Control',31,5,4,'Other Agency','2020-11-26','2021-06-24','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(86,'Rehabilitation of Admin Building',42,2,1,'By Contract','2020-11-11','2021-06-21','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(87,'Tesda ORMIN PTC',39,4,3,'Eljunangeles','2020-11-11','2022-11-11','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(88,'REVITALIZATION OF GRASSROOTS COMMUNICATION',35,4,1,'Admin','2021-01-15','2021-06-15','2020-11-11','2020-11-11',6,'',0,'','','0000-00-00'),
(89,'REHAB OF MINOLO (PUERTO-GALERA)-MAMBURAO 69KV TL PROJECT',30,2,4,'BY CONTRACT','2019-08-23','2020-08-16','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(90,'Sablayan Water District Wayter Supply System Improvement',22,2,2,'By Administration','2020-12-25','2022-02-14','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(91,'Busuanga Port Development Project',37,2,4,'by Contract','2021-01-20','2022-01-20','2020-11-11','2020-11-11',3,'',0,'','','0000-00-00'),
(92,'SAMPLE PROJECT STATUS',276,2,2,'SAMPLE MODE','2020-11-23','2020-11-23','2020-11-23','2020-11-23',2,'Ongoing',121212,'','','0000-00-00'),
(93,'SAMPLE OTHERS',8,2,1,'SAMPLE OTHERS','2020-12-01','2020-12-01','2020-12-01','2020-12-01',3,'Completed',232323,'Loan',NULL,NULL);

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report` varchar(100) DEFAULT NULL,
  `reporttype` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `reports` */

insert  into `reports`(`id`,`report`,`reporttype`) values 
(1,'Masterlist of Projects',1),
(2,'Masterlist of Agency Projects',2);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `roles_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`description`,`created_at`,`updated_at`) values 
(1,'Admin','This is the administration role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Agency','This is the implementing agency role','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'Member','This is the member role','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `sectors` */

DROP TABLE IF EXISTS `sectors`;

CREATE TABLE `sectors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sectors` */

insert  into `sectors`(`id`,`sector`) values 
(1,'Economic'),
(2,'Infrastructure'),
(3,'Social'),
(4,'Government Institutional Development'),
(5,'Others');

/*Table structure for table `sources` */

DROP TABLE IF EXISTS `sources`;

CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `sources` */

insert  into `sources`(`id`,`type`) values 
(1,'Official Development Assistance'),
(2,'Local Financing'),
(3,'Public-Private Partnership'),
(4,'Others'),
(5,'GAA');

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `tags_name_unique` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `tags` */

insert  into `tags`(`id`,`name`,`color`,`created_at`,`updated_at`) values 
(1,'Hot','#f44336','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(2,'Trending','#9c27b0','2020-07-24 06:35:23','2020-07-24 06:35:23'),
(3,'New','#00bcd4','2020-07-24 06:35:23','2020-07-24 06:35:23');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `agency_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`) USING BTREE,
  KEY `users_role_id_foreign` (`role_id`) USING BTREE,
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`picture`,`role_id`,`agency_id`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Administrator','bnlayon@neda.gov.ph','2020-07-24 06:35:23','$2y$10$W9hB2vXLRvokUE7YrQiwOeV2Pv2v7/92zDIjRz8uzyuE4vv01715i','profile/W3tDucYtneeYOJEAjma11eBlF4p5oM3rZEWNVaYa.jpeg',1,0,'SxhJxrTuOdfKOw6hKDBgDZMZm9pOLzPozuskHmD4YdbaPfIMIrJSWju3DIPm','2020-07-24 06:35:23','2020-11-03 05:24:35'),
(2,'NEDA','neda@neda.gov.ph','2020-07-24 06:35:23','$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,276,'2wZlCMQrZEhjDuDZsiGHvgBqn6A0IgDSyhStRcin6mK2ci3m8EOZLku7BkNo','2020-07-24 06:35:23','2020-11-03 05:32:45'),
(217,'PA2','pa2@pa2.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,2,NULL,NULL,NULL),
(218,'BFAR','bfar@bfar.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,3,NULL,NULL,NULL),
(219,'CAAP','caap@caap.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,4,NULL,NULL,NULL),
(220,'CHED','ched@ched.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,5,NULL,NULL,NULL),
(221,'CPD','cpd@cpd.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,6,NULL,NULL,NULL),
(222,'DAR','dar@dar.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,7,'zm8NNBArNy4pqHBDJTp8Kzbe8nidYLrmxgi4Bh6KuutYDiF70gXQyDQ17MOl',NULL,NULL),
(223,'DA','da@da.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,8,NULL,NULL,NULL),
(224,'DEPED','deped@deped.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,9,NULL,NULL,NULL),
(225,'DOE','doe@doe.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,10,NULL,NULL,NULL),
(226,'DENR','denr@denr.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,11,NULL,NULL,NULL),
(227,'DOH','doh@doh.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,12,'2BFR1nls0gg1HdENpjxaEUtUxQ7Cm85T3AnLW9D5Im603qesByRGVGPllInx',NULL,NULL),
(228,'DICT','dict@dict.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,13,NULL,NULL,NULL),
(229,'DOLE MIMAROPA','dole@dole.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/wMnee9t69k6SkeGlorM7S3AsLryTV2TGvcJtghHT.png',2,14,NULL,NULL,'2020-11-10 04:52:20'),
(230,'DPWH','dpwh@dpwh.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,15,NULL,NULL,NULL),
(231,'DOST','dost@dost.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,16,NULL,NULL,NULL),
(232,'DSWD','dswd@dswd.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,17,NULL,NULL,NULL),
(233,'DILG','dilg@dilg.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,18,NULL,NULL,NULL),
(234,'DOT','dot@dot.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,19,NULL,NULL,NULL),
(235,'DTI','dti@dti.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,20,NULL,NULL,NULL),
(236,'DOTR','dotr@dotr.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,21,NULL,NULL,'2020-11-10 03:16:02'),
(237,'LWUA','lwua@lwua.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,22,NULL,NULL,NULL),
(238,'NCCA','ncca@ncca.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,23,NULL,NULL,NULL),
(239,'NCIP','ncip@ncip.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,24,NULL,NULL,NULL),
(240,'NEA','nea@nea.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,25,'eArNhqufN34WHOXZHCCXHJXff2YEVMPJ7VpKu90VhFwlqiYOMqBJG6C3te6y',NULL,NULL),
(241,'NHA','nha@nha.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,26,NULL,NULL,NULL),
(242,'NIA','nia@nia.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,27,NULL,NULL,NULL),
(243,'NNC','nnc@nnc.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,28,NULL,NULL,NULL),
(244,'NAPOLCOM','napolcom@napolcom.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/sVkx3rXhBNsgJOg0Y8EqTOJGxIJDPLWEynfiF3jb.jpeg',2,29,NULL,NULL,'2020-11-10 04:50:24'),
(245,'NPC','npc@npc.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,30,NULL,NULL,NULL),
(246,'OCD','ocd@ocd.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,31,NULL,NULL,NULL),
(247,'PCA','pca@pca.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,32,NULL,NULL,NULL),
(248,'PDEA','pdea@pdea.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,33,NULL,NULL,NULL),
(249,'PFIDA','pfida@pfida.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,34,NULL,NULL,NULL),
(250,'PIA','pia@pia.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,35,'x3GsTIyaL0Vdx14izkJLloKyokX7rn8FQn3dc9pTf8mtIS0RI3efnh3tEOYi',NULL,NULL),
(251,'PNP','pnp@pnp.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,36,NULL,NULL,NULL),
(252,'PPA','ppa@ppa.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,37,'zp868Cut1Zvw1um4HCgT2NOHyXh2C1xLjCXbbV1uujY2SpAunC8jEelP3A5z',NULL,NULL),
(253,'PSA','psa@psa.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,38,NULL,NULL,NULL),
(254,'TESDA','tesda@tesda.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,39,'YVddIyV9eYrOLVtDxqQr2iq84m9ayd6pxoqJ5NIMKgT7qpjttKy99WfPi5R0',NULL,NULL),
(255,'PAWC','pawc@pawc.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,40,NULL,NULL,NULL),
(256,'MSC','msc@msc.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,41,'NW3oXEvkBhHEoFphvXdyY5KpptUPGOsOrkvZmBnjWt4Xxqlf2R2kgV9DUqLl',NULL,NULL),
(257,'MSCAT','mscat@mscat.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,42,NULL,NULL,NULL),
(258,'OMSC','omsc@omsc.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,43,'XXhfbiidyd1qIWIvL85j7PUmoJAUsEj34wRJPp4mt8lzENzlYIuGd3FFUKDw',NULL,NULL),
(259,'PSU','psu@psu.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,44,NULL,NULL,NULL),
(260,'RSU','rsu@rsu.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,45,'p0uakvv4EI9lVHDZeIDhRYw9HZJP5BrDRva7Xifhaehc6XSgWapTYHvcd0sc',NULL,NULL),
(261,'WPU','wpu@wpu.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,46,'dwkk3RmfL7juSY4Ren5Hzies6u0FYR88FrUGTp2T9BLwlE7XOxVWmseGektf',NULL,NULL),
(262,'MAR','mar@mar.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,47,'thNzxP1aThnuNCrSUgZcjf79ybzjgOfP00687uE1tPeUTBDS8NdKz2fAts1s',NULL,NULL),
(263,'OCM','ocm@ocm.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,48,'kONyfGhzFF5Wd2RPmTmJiTFMITNSjG8fjz6wYjYc9cOx2rxZsWB3TDzLvAHr',NULL,NULL),
(264,'ORM','orm@orm.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,49,NULL,NULL,NULL),
(265,'PAL','pal@pal.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,50,'smwEPzj77Zs4LjbA8seFYGPg6z7M4RRG5siKZqLFN2fwIlY8PQ7dQpzhZH05',NULL,NULL),
(266,'ROM','rom@rom.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,51,'D8VvMRU0NOGVeTXVwPgvroitUoBewjBE7SEdvIh8pj6ahsDSZkroS0XC4YF3',NULL,NULL),
(267,'CAL','cal@cal.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,52,'8IIzy8T4M5dTqCGqgt0GaIebAFbpGyOMTyw5zkuAmSp5T3d4vajVcyiPGUad',NULL,NULL),
(268,'PP','pp@pp.gov.ph',NULL,'$2y$12$BSIh23UPsaIM/bd9.PJh2.Yli5vEL2G2s2UJHnmS869LZQSJWTmR2','profile/vI0XsAwT0N8jmz86kFK9PbG1ZrDDwCUHbIfmU0Cp.jpg',2,53,NULL,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
